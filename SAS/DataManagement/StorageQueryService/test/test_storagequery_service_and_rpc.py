#!/usr/bin/env python3

import unittest, unittest.mock
import logging
import os
import shutil
import sys
from threading import Thread
from lofar.messaging.messagebus import  TemporaryExchange
from lofar.sas.datamanagement.storagequery.service import createService
from lofar.sas.datamanagement.storagequery.rpc import StorageQueryRPC
from lofar.sas.datamanagement.storagequery.cache import CacheManager
from lofar.common.test_utils import integration_test

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s %(threadName)s', level=logging.INFO, stream=sys.stdout)


@integration_test
class TestStorageQueryServiceAndRPC(unittest.TestCase):
    @classmethod
    def setUpClass(cls):

        cls.stored_tmss_ids = set()

        DIR_BYTES = 4096 # each dir adds 4096 bytes to a du
        cls.total_bytes_stored = DIR_BYTES
        cls.projects_bytes_stored = {}
        cls.observation_bytes_stored = {}

        cls.DATA_DIR_PATH = os.path.join(os.getcwd(), "test_data")
        cls.PROJECTS_DIR_PATH = os.path.join(cls.DATA_DIR_PATH, "test-projects")
        cls.PROJECTS = ['LC100_001', 'LC100_002']
        cls.tmss_id2project_map = {}

        for i, project in enumerate(cls.PROJECTS):
            cls.projects_bytes_stored[project] = DIR_BYTES
            cls.total_bytes_stored += DIR_BYTES

            for j in range(10):
                tmss_id = 999000 + (10*i+j)

                cls.stored_tmss_ids.add(tmss_id)
                cls.tmss_id2project_map[tmss_id] = project

                obs_dir = 'L%d' % (tmss_id,)
                obs_dir_path = os.path.join(cls.PROJECTS_DIR_PATH, project, obs_dir)
                os.makedirs(obs_dir_path, exist_ok=True)

                cls.observation_bytes_stored[tmss_id] = DIR_BYTES
                cls.projects_bytes_stored[project] += DIR_BYTES
                cls.total_bytes_stored += DIR_BYTES

                obs_data_file_path = os.path.join(obs_dir_path, 'data.txt')

                with open(obs_data_file_path, 'wt') as file:
                    data = 1000*(i+1)*(j+1)*r'a'
                    file.write(data)
                    num_bytes = len(data)
                    cls.total_bytes_stored += num_bytes
                    cls.projects_bytes_stored[project] += num_bytes
                    cls.observation_bytes_stored[tmss_id] += num_bytes

        cls.ssh_cmd_list_patcher = unittest.mock.patch('lofar.common.cep4_utils.ssh_cmd_list', lambda host,user: [])
        cls.ssh_cmd_list_patcher.start()

        cls.tmssclient_get_subtask_output_dataproducts_patcher = unittest.mock.patch('lofar_tmss_client.tmss_http_rest_client.TMSSsession.get_subtask_output_dataproducts')
        tmssclient_get_subtask_output_dataproducts_mock = cls.tmssclient_get_subtask_output_dataproducts_patcher.start()
        tmssclient_get_subtask_output_dataproducts_mock.side_effect = lambda tmss_id: [{'directory': os.path.join(cls.PROJECTS_DIR_PATH, cls.tmss_id2project_map[tmss_id], 'L%d' % (tmss_id,))}]

        cls.tmssclient_get_subtask_patcher = unittest.mock.patch('lofar_tmss_client.tmss_http_rest_client.TMSSsession.get_subtask')
        tmssclient_get_subtask_mock = cls.tmssclient_get_subtask_patcher.start()
        tmssclient_get_subtask_mock.side_effect = lambda tmss_id: {'id': tmss_id, 'subtask_type': 'observation'}

        cls.tmp_exchange = TemporaryExchange(cls.__class__.__name__)
        cls.tmp_exchange.open()

        cls.cache = CacheManager(mountpoint=cls.DATA_DIR_PATH, exchange=cls.tmp_exchange.address)
        cls.cache.open()

        cls.service = createService(cls.tmp_exchange.address, cache_manager=cls.cache)
        cls.service.start_listening()

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls.DATA_DIR_PATH)
        cls.cache.close()
        cls.service.stop_listening()
        cls.tmp_exchange.close()
        cls.ssh_cmd_list_patcher.stop()
        cls.tmssclient_get_subtask_output_dataproducts_patcher.stop()
        cls.tmssclient_get_subtask_patcher.stop()


    def test_getTMSSIdsFoundOnDisk(self):
        with StorageQueryRPC.create(self.tmp_exchange.address) as rpc:
            found_tmss_ids = set(rpc.getTMSSIdsFoundOnDisk())
            self.assertEqual(self.stored_tmss_ids, found_tmss_ids)

    def test_getDiskUsagesForAllTMSSIds(self):
        with StorageQueryRPC.create(self.tmp_exchange.address) as rpc:
            results = rpc.getDiskUsagesForAllTMSSIds(force_update=True)
            self.assertEqual(self.stored_tmss_ids, set(results.keys()))

            for tmss_id in self.stored_tmss_ids:
                self.assertEqual(self.observation_bytes_stored[tmss_id], results[tmss_id]['disk_usage'])

    def test_getDiskUsageForProjectsDirAndSubDirectories(self):
        with StorageQueryRPC.create(self.tmp_exchange.address) as rpc:
            result = rpc.getDiskUsageForProjectsDirAndSubDirectories(force_update=True)
            self.assertTrue(result['found'])

            self.assertEqual(self.PROJECTS_DIR_PATH, result['projectdir']['path'])
            self.assertEqual(self.total_bytes_stored, result['projectdir']['disk_usage'])

            for project in self.PROJECTS:
                project_path = os.path.join(self.PROJECTS_DIR_PATH, project)
                self.assertTrue(project_path in result['sub_directories'])
                self.assertEqual(self.projects_bytes_stored[project], result['sub_directories'][project_path]['disk_usage'])

    def test_getDiskUsageForProjectDirAndSubDirectories(self):
        with StorageQueryRPC.create(self.tmp_exchange.address) as rpc:
            for project in self.PROJECTS:
                result = rpc.getDiskUsageForProjectDirAndSubDirectories(project_name=project, force_update=True)
                self.assertEqual(self.projects_bytes_stored[project], result['projectdir']['disk_usage'])

    def test_getDiskUsageForTask(self):
        with StorageQueryRPC.create(self.tmp_exchange.address) as rpc:
            for tmss_id in self.stored_tmss_ids:
                results = rpc.getDiskUsageForTask(tmss_id=tmss_id, force_update=True)
                self.assertEqual(self.observation_bytes_stored[tmss_id], results['disk_usage'])

    def test_getDiskUsageForTasks(self):
        with StorageQueryRPC.create(self.tmp_exchange.address) as rpc:
            results = rpc.getDiskUsageForTasks(tmss_ids=list(self.stored_tmss_ids), force_update=True)
            for tmss_id in self.stored_tmss_ids:
                self.assertTrue(tmss_id in results['tmss_ids'])
                self.assertEqual(self.observation_bytes_stored[tmss_id], results['tmss_ids'][tmss_id]['disk_usage'])

    def test_survive_ddos(self):
        '''spam the service. It should be able to handle that.
        It's interesting to analyze the logging, which reports on how all requests are handled in parallel.'''
        with StorageQueryRPC.create(self.tmp_exchange.address) as rpc:

            # spamming, spawn many large getDiskUsageForTasks calls in parallel
            threads = []
            for i in range(10):
                threads.append(Thread(target=rpc.getDiskUsageForTasks,
                                      kwargs={'tmss_ids': list(self.stored_tmss_ids), 'force_update': True}))
            for thread in threads:
                thread.start()
            for thread in threads:
                thread.join()

            # and to a final check. Service should still be reachable and deliver proper results
            self.test_getDiskUsageForTasks()

if __name__ == '__main__':
    unittest.main()

