#!/bin/bash
if diff /globaldata/tbb/nowdumping.py /globaldata/tbb/notdumping.py
then
if  ! slurm-job-list.sh  | grep run-tbbwriter | grep -q PENDING
then
echo "Cannot cancel tbbwriter, please first add another job to the queue"
else
tbbid=$(sacct --name=run-tbbwriter --user=lofarsys -o jobid -n -s R -X)
echo "Cancelling datawriter under id "$tbbid
scancel $tbbid
fi
if  ! slurm-job-list.sh  | grep run-tbbmetadata | grep -q PENDING
then
echo "Cannot cancel tbbmetadata, please first add another job to the queue"
else
mdid=$(sacct --name=run-tbbmetadata --user=lofarsys -o jobid -n -s R -X)
echo "Cancelling metadata under id "$mdid
scancel $mdid
fi
sleep 90
echo "New tbbwriter id: "$(sacct --name=run-tbbwriter --user=lofarsys -o jobid -n -s R -X)
echo "New metadataa id: "$(sacct --name=run-tbbmetadata --user=lofarsys -o jobid -n -s R -X)
else
echo "TBBs are currently dumping, cannot restart, please try again later"
fi

