# coding=utf-8
"""
This program is meant to load the station tests and RTSM present in a certain directory to the database
"""
import argparse
import logging
import sys
from glob import glob
from os import path
from lofar.maintenance.mdb.client import read_stationtest_rtsm_output_to_dict,\
    send_stationtest_rtsm_content_to_address

logger = logging.getLogger('mdb_loader')


def setup_argument_parser():
    """
    Set up the argument parser and returns it
    :return: ArgumentParser instance filled with the command arguments
    """
    parser = argparse.ArgumentParser(prog='mdb_loader')
    parser.add_argument('path', help='format or path format of the files to load. ex. /where/is/stored/*.dat')
    parser.add_argument('--address', help='address of the server. default [localhost]:8000',
                        default='http://localhost:8000')

    return parser


def obtain_file_list(file_path):
    """
    Searches in the directory the list of files which path satisfy the pattern specified in file_path
    :param file_path: strings that represents the path pattern to be satisfied
    :return: a list of file paths
    """
    logger.info('listing file in %s', file_path)
    file_list = list(filter(path.isfile, glob(file_path)))
    logger.debug('found files: %s', file_list)
    return file_list


def obtain_stationtest_file_list_and_process(file_path_pattern, address):
    """
    Finds all the stationtest files which path satisfy the file_path_pattern and send them to address that implement the
    MaintenanceDB API
    :param file_path_pattern: file path pattern
    :param address: address of the REST API
    """
    for file_path in obtain_file_list(file_path_pattern):
        logging.info('processing file %s', file_path)
        content = read_stationtest_rtsm_output_to_dict(file_path)
        if content is None:
            logger.error('error processing file %s', file_path)
            continue
        if send_stationtest_rtsm_content_to_address(address, content):
            logger.info('file %s processed', file_path)
        else:
            logger.error('error on file %s', file_path)
            sys.exit(1)


def main():
    """
    Main entry point of the program
    """
    parser = setup_argument_parser()
    args = parser.parse_args()
    obtain_stationtest_file_list_and_process(args.path, args.address)