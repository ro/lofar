#include "DatapointAttribute.hpp"
#include "DatapointRule.hpp"

#include <limits>

#include <QSqlField>
#include <QVariant>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QStringList>
#include <QDomElement>

#include <QDebug>

DatapointAttribute::DatapointAttribute():
    m_max{std::numeric_limits<double>::max()},
    m_min{std::numeric_limits<double>::min()},
    checkBoundaries{false}
{

}

DatapointAttribute::DatapointAttribute(QString name, QString alias, QString type, QString unit, QString value, double minValue, double maxValue, bool check):
    m_name{name}, m_alias{alias}, m_type{type}, m_unit{unit}, m_value{value}, m_max{maxValue}, m_min{minValue}, checkBoundaries{check}
{

}

QString const DatapointAttribute::formatValue() const{
    if(m_type == "STRING"){

    	QSqlField field{m_alias, QVariant::String};
    	field.setValue(m_value);

    	QSqlDatabase db = QSqlDatabase::database();

    	if(db.isValid() == true) {
    		return db.driver()->formatValue(field);
    	}else{
    		qWarning() << "Default database it is not configured";
    		return "\'" + m_value + "\'";
    	}
    }else{
        return m_value;
    }
}


QString DatapointAttribute::composeDatapointURI(DatapointRule &rule){
    return rule.name() + m_name;
}

void DatapointAttribute::setParentRule(DatapointRule& rule)
{
    m_uri = composeDatapointURI(rule);
}

const QString& DatapointAttribute::URI() const
{
    return m_uri;
}

void DatapointAttribute::setURI(const QString& uri)
{
    m_uri = uri;
}

const QString& DatapointAttribute::name() const
{
    return m_name;
}

void DatapointAttribute::setName(const QString& name)
{
    m_name = name;
}

const QString& DatapointAttribute::alias() const
{
    return m_alias;
}

void DatapointAttribute::setAlias(const QString& alias)
{
    m_alias = alias;
}

const QString& DatapointAttribute::type() const
{
    return m_type;
}

void DatapointAttribute::setType(const QString& type)
{
    m_type = type;
}

const QString& DatapointAttribute::unit() const
{
    return m_unit;
}

void DatapointAttribute::setUnit(const QString& unit)
{
    m_unit = unit;
}

const QString& DatapointAttribute::value() const
{
    return m_value;
}

void DatapointAttribute::setValue(const QString& value)
{
    m_value = value;
}

const double& DatapointAttribute::upperLimit() const
{
    return m_max;
}

void DatapointAttribute::setUpperLimit(const double upperLimit)
{
    m_max = upperLimit;
}

const double& DatapointAttribute::lowerLimit() const
{
    return m_min;
}

void DatapointAttribute::setLowerLimit(const double lowerLimit)
{
    m_min = lowerLimit;
}


const QString DatapointAttribute::toString() const {
    return "DatapointAttribute(" + QStringList({m_name, m_alias, m_type, m_unit, m_value}).join(", ") + ")";
}

bool DatapointAttribute::isBoundariesCheckEnabled(){
    return checkBoundaries;
}

void DatapointAttribute::enableBoundariesCheck(){
    checkBoundaries = true;
}

bool DatapointAttribute::isInRange(QString value){
    if(checkBoundaries){
        const double d_value = value.toDouble();
        return d_value > m_min && d_value < m_max;
    }else{
        return false;
    }
}

DatapointAttribute readAttribute(const QDomElement &attributeEntry){
    DatapointAttribute attribute;

    attribute.m_alias = attributeEntry.attribute("alias", "");
    attribute.m_name = attributeEntry.attribute("name", "");
    attribute.m_type = attributeEntry.attribute("type", "");
    attribute.m_unit = attributeEntry.attribute("unit", "");
    attribute.m_value = attributeEntry.attribute("value", "");


    if(attributeEntry.hasAttribute("max")){
        attribute.enableBoundariesCheck();
        attribute.m_max = attributeEntry.attribute("max").toDouble();
    }

    if(attributeEntry.hasAttribute("min")){
        attribute.enableBoundariesCheck();
        attribute.m_min = attributeEntry.attribute("min").toDouble();
    }
    return attribute;
}
