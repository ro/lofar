""" This scripts runs tbbmd to add metadata to TBB files using a parset.
  tbbmd can be found at /home/veen//dal/DAL3/build/src/apps/tbbmd
  It has the following options:
  --antennaPositionDir arg        Directory containing antenna position files
  --dipoleCalibrationDelayDir arg Directory containing calibration delay files
  --metadataDir "/opt/cep/LofIm/daily/Tue/lofar_build/LOFAR/MAC/Deployment/data/StaticMetaData/"             Directory containing static metadata
  --antennaSet arg (=UNDEFINED) antennaSet # Already set
  --telescope "LOFAR"               telescope
  --observer arg                observer #already set
# set these for piggybacking? Or are they trigger dependend...
  --projectID  arg               projectID
  --projectTitle 'Observation.Campaign.title'            projectTitle #already set?
  --projectPI 'Observation.Campaign.PI'
  --projectCoI 'Observation.Campaign.CO_I'
  --projectContact 'Observation.Campaign.contact'
  --observationID arg          #already set?
  --startMJD arg
  --startTAI arg
  --startUTC 'Observation.startTime'
  --endMJD arg
  --endTAI arg
  --endUTC 'Observation.stopTime'
  --frequencyMin arg
  --frequencyMax arg
  --frequencyCenter arg
  --frequencyUnit arg
  --clockFrequency 'Observation.sampleClock'
  --clockFrequencyUnit 'MHz'
  --antennaSet arg #already set?
  --filterSelection 'Observation.bandFilter'
  --target 'Observation.AnaBeam.target'
  --systemVersion arg
  --pipelineName arg
  --pipelineVersion arg
  --icdNumber arg
  --icdVersion arg
  --notes arg
  --input-file arg              Input file


"""
import sys
import os
import subprocess
import time
doTAI=True
try:
    import pytmf
except ImportError:
    print("Cannot import pytmf. TAI support disabled")
    doTAI=False

doMove=True
try:
    from pycrtools import lora
except ImportError:
    print("Cannot import lora, not moving files")
    doMove=False

def timeToMJD(timestamp):
    try:
        timetuple=time.strptime(timestamp,"'%Y-%m-%d %H:%M:%S'")
    except:
        timetuple=time.strptime(timestamp,'%Y-%m-%d %H:%M:%S')
    timefloat=time.mktime(timetuple)
    timeMJD=timefloat/86400+40587
    timestr=str(timeMJD)
    if len(timestr)>13:
        timestr=timestr[0:13]
    return timestr

def timeToTAI(timestamp):
    try:
        timetuple=time.strptime(timestamp,"'%Y-%m-%d %H:%M:%S'")
    except:
        timetuple=time.strptime(timestamp,'%Y-%m-%d %H:%M:%S')
    timefloat=time.mktime(timetuple)
    timefloat+=pytmf.delta_tai_utc(timefloat)
    return time.strftime("%Y-%m-%dT%H:%M:%S",time.gmtime(timefloat))

def olddate(timestamp):
    try:
        timetuple=time.strptime(timestamp,"'%Y-%m-%d %H:%M:%S'")
    except:
        timetuple=time.strptime(timestamp,'%Y-%m-%d %H:%M:%S')
    timefloat=time.mktime(timetuple)
    reftime=time.strptime("2012-06-01","%Y-%m-%d")
    reftime2=time.mktime(reftime)
    return timefloat<reftime2

def getTimestamp(filename):
    fileinfo=subprocess.Popen(["h5ls","-rv",filename],stdout=subprocess.PIPE)
    lines=fileinfo.stdout.readlines()
    for num,l in enumerate(lines):
        if "TIME" in l:
            timestamp = int(lines[num+2].split()[1])
    return timestamp


def newFilename(filename,subdir="LORAtriggered"):
    fspl=filename.split(filename)
    try:
        newFilename=os.path.join(fspl[0]+"/"+subdir+"/"+fspl[1])
    except:
        newFilename=False
    return newFilename



if len(sys.argv)<3:
    print("ERROR, usage: python addmetadata.py <datadir> <obsid>")
    exit()
print(sys.argv)
#programname="/opt/lofarsoft/release/bin/tbbmd"
programname="/data/home/lofarsys/tbb-writer/run/docker-launch-metadata.sh"
datadir=sys.argv[1]+"/"    #"/data/TBB/VHECRtest/"
obsid=sys.argv[2]#"L33273"
filenames=[datadir+f for f in os.listdir(datadir) if obsid in f]#["L33273_metadatatest.h5"]
from currentobs import *
p=get_all_parameters_new(obsid)
kwm={}
#kwm["--metadataDir"]="/home/veen/lus/src/code/data/lofar/antennapositions/"
kwm["--antennaPositionDir"]="/opt/lofarsoft/data/lofar/antennapositions/"
kwm["--dipoleCalibrationDelayDir"]="/opt/lofarsoft/data/lofar/dipole_calibration_delay/"
#  --antennaSet arg (=UNDEFINED) antennaSet # Already set
kwm["--antennaSet"]=p['Observation.antennaSet']
kwm["--telescope"]="LOFAR" #               telescope

kwm["--observer"]=p['Observation.Campaign.PI'] #arg                observer #already set
# set these for piggybacking? Or are they trigger dependend...
#  --projectID  arg               projectID
kwm["--projectTitle"]=p['Observation.Campaign.name']
#  --projectPI 'Observation.Campaign.PI'
#  --projectCoI 'Observation.Campaign.CO_I'
kwm["--projectContact"]="a.corstanje@astro.ru.nl" # 'Observation.Campaign.contact'
kwm["--observationID"]="L"+p['Observation.tmssID']

kwm["--startMJD"]=timeToMJD(p['Observation.startTime'])
if doTAI:
    kwm["--startTAI"]=timeToTAI(p['Observation.startTime'])
kwm["--startUTC"]=p['Observation.startTime'].replace(" ","T")
kwm["--endMJD"]=timeToMJD(p['Observation.stopTime'])
kwm["--endUTC"]=p['Observation.stopTime'].replace(" ","T")

if doTAI:
    kwm["--endTAI"]=timeToTAI(p['Observation.stopTime'])
#  --frequencyMin arg
#  --frequencyMax arg
#  --frequencyCenter arg
#  --frequencyUnit arg
kwm["--clockFrequency"]=p['Observation.sampleClock']
kwm["--clockFrequencyUnit"]='MHz'
kwm["--antennaSet"]=p['Observation.antennaSet']
kwm["--filterSelection"]=p['Observation.bandFilter']
if 'Observation.Beam[0].target' in list(p.keys()):
    kwm["--target"]=p['Observation.Beam[0].target']
#  --systemVersion arg
#  --pipelineName arg
#  --pipelineVersion arg
#  --icdNumber arg
#  --icdVersion arg
#  --notes arg
for f in filenames:
    print("updating file",f,"...", end=' ')
    kwm["--input-file"]=f
    command=[]
    command.append(programname)
    for k,v in kwm.items():
        command.append(k)
        command.append(v)
    proc2=subprocess.Popen(command)
    proc2.wait()
    print("done")


