from rest_framework.serializers import ModelSerializer
from ..models.station import Station


class StationSerializer(ModelSerializer):
    class Meta:
        model = Station
        fields = '__all__'
        depth = 1