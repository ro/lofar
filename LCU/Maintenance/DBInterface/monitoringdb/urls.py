from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.documentation import include_docs_urls

from .views.wincc_view import *
from .views.controllers import *
from .views.logs_view import *
from .views.rtsm_views import *
from .views.station_test_views import *

log_router = routers.DefaultRouter()
log_router.register(r'action_log', ActionLogViewSet)

# WinCC
wincc_router = routers.DefaultRouter()
wincc_router.register(r'antenna_status', WinCCAntennaStatusViewSet)

# Station test
station_test_router = routers.DefaultRouter()

station_test_router.register(r'element_error', ElementErrorViewSet)
station_test_router.register(r'element', ElementViewSet)
station_test_router.register(r'component_error', ComponentErrorViewSet)
station_test_router.register(r'component', ComponentViewSet)

station_test_router.register(r'station', StationViewSet)
station_test_router.register(r'', StationTestViewSet)


# RTSM
rtsm_router = routers.DefaultRouter()

rtsm_router.register(r'errors', RTSMErrorsViewSet)
rtsm_router.register(r'error_sample', RTSMErrorSampleViewSet)

rtsm_router.register(r'error_summary_plot', RTSMSummaryPlot, base_name='rtsm-summary-plot')

rtsm_router.register(r'', RTSMObservationViewSet)

urlpatterns = [

    url(r'^api/stationtests/', include(station_test_router.urls)),
    url(r'^api/rtsm/', include(rtsm_router.urls)),
    url(r'^api/wincc/', include(wincc_router.urls)),

    url(r'^api/log/', include(log_router.urls)),

    url(r'^api/api-auth', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/stationtests/raw/insert', insert_raw_station_test),
    url(r'^api/rtsm/raw/insert', insert_raw_rtsm_test),
    url(r'^api/view/ctrl_stationoverview', ControllerStationOverview.as_view()),
    url(r'^api/view/ctrl_stationtestsummary', ControllerStationTestsSummary.as_view()),
    url(r'^api/view/ctrl_latest_observation', ControllerLatestObservations.as_view()),
    url(r'^api/view/ctrl_stationtest_statistics', ControllerStationTestStatistics.as_view()),
    url(r'^api/view/ctrl_list_component_error_types', ControllerAllComponentErrorTypes.as_view()),
    url(r'^api/view/ctrl_station_component_errors', ControllerStationComponentErrors.as_view()),
    url(r'^api/view/ctrl_station_component_element_errors',
        ControllerStationComponentElementErrors.as_view()),
    url(r'^api/docs', include_docs_urls(title='Monitoring DB API'))
]
