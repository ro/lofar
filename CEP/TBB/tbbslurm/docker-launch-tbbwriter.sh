#!/bin/bash
#exec docker run \
#exec ~mol/docker-run-killable.sh \
exec docker-run-slurm.sh \
    --rm --net=host \
    --stop-signal=SIGKILL \
    -u `id -u` -e USER=$USER -e HOME=$HOME \
    -e SLURM_JOB_ID=$SLURM_JOB_ID \
    -v $HOME/.ssh:$HOME/.ssh:ro \
    -v /data/projects:/data/projects \
    -v /data/log:/data/log \
    -v /globaldata/tbb:/globaldata/tbb \
    --cap-add=sys_nice --cap-add=sys_admin \
    lofar-tbbwriter-control:latest /bin/bash -c 'source /opt/lofar/lofarinit.sh;python3 /opt/lofar/lib/python3.6/site-packages/lofar/cep/tbb/TBBTMSSBusListener.py "$@"'

