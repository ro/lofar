from ast import literal_eval
import os
import functools

DEFAULT_STATION_POSITIONS_PARSET_FILEPATH = os.path.expandvars("$LOFARROOT/etc/StaticMetaData/StationPositions.parset")

@functools.lru_cache()
def parse_station_coordinates() -> dict:
    """
    :return: a dict mapping station field name, e.g. "CS002_LBA", to a dict containing geocentric coordinates
    """
    station_coordinates = {}
    expected_comment_found = False
    with open(os.path.expandvars(DEFAULT_STATION_POSITIONS_PARSET_FILEPATH), 'r') as f:
        for line in f.readlines():
            if "Note: DE605 coordinates are EPOCH 2017.5" in line:
                expected_comment_found = True
            line = line.strip()
            if line and line.startswith('PIC.Core.'):
                key, value = line.split('=')
                key = key.replace('PIC.Core.', '').replace('.phaseCenter', '').strip()
                eval_value = literal_eval(value.strip())
                field_coords = {'coordinate_system': 'ITRF2005', 'epoch': '2017.5' if 'DE605' in key else '2015.5', 'x': eval_value[0], 'y': eval_value[1], 'z': eval_value[2]}  # 'coordinate_system': 'ITRF2005' is correct or is this 'WGS84'?!
                station_coordinates[key] = field_coords
    if not expected_comment_found:
        raise ValueError('File contents are missing an expected comment, indicating that special cases are no longer present in the coordinates file. Revise the code to reflect that.')
    return station_coordinates

