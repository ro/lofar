import React, { PureComponent } from 'react';
import { getInspectPageURLFromSASid } from 'utils/LOFARDefinitions.js'

import styles from './styles.module.scss'

/**
 * ObservationInspectTag: create a clickable observation ID element.
 * On click, open 'inspect' page (external web app) in new window.
 */
class ObservationInspectTag extends PureComponent {

    tooltip = "Click to open inspection page in new window";

    clicked = () => {
        const url = getInspectPageURLFromSASid(this.props.observationId)
        window.open(url)
    }

    render () {
        const observationId = this.props.observationId;
        return (
            <span onClick={this.clicked} className={styles.link} title={this.tooltip}>
                {observationId}
            </span>
        )
    }
}
export default ObservationInspectTag;
