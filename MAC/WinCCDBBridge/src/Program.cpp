#include "Program.hpp"

#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#include <QDomDocument>
#include <QDomElement>
#include <QDomNode>

#include <QXmlInputSource>

#include <QtNetwork>
#include <QUrl>
#include <QSqlQuery>
#include <QSqlError>


#include <exception>

using LOFAR::WINCCWRAPPER::WinCCWrapper;

class ConfigurationFileReadError : public std::exception {
private:
    std::string message;
public:
    ConfigurationFileReadError(std::string path): message{"Error reading file " + path} {}

    const char * what() const throw () {
        return message.c_str();
    }
};

Program::Program(QObject * parent, const QString& configurationFile, const QString & projectName): QObject{parent},
    database{new QSqlDatabase}
{
    wrapper.reset(new LOFAR::WINCCWRAPPER::WinCCWrapper{"Program", projectName.toStdString(), 0});

    try{
        readConfigurationFile(configurationFile);

        // Event connect
        // -- WINCC side
        wrapper->set_connect_datapoints_callback([=](std::map<std::string, std::string> values){
                                              emit datapointChanged(values); });
        // -- QT side
        connect(this, &Program::datapointChanged, this, &Program::consolePrint);
        connect(this, &Program::datapointChanged, this, &Program::messageDispatch);
    }
    catch (std::exception & e){
        qCritical() << "An error occurred while reading the configuration file: "<< e.what();
        qFatal("Cannot read configuration file");
    }

}

Program::~Program()
{
}

void Program::run()
{
    database->open();
    connectToDatapoints();
    emit startRulesCheckpointTriggers();

    while(true){
        qApp->processEvents();
        wrapper->wait_for_event(0, 500);
    }
    wrapper->exit();
    database->close();
    qInfo()<< "Closing worker threads";

}

void Program::consolePrint(std::map<std::string, std::string> values)
{

    qDebug() << "Received: ";
    for(const auto & value: values){
        qDebug()<< value.first.c_str() << " " << value.second.c_str();
    }
}

void Program::messageDispatch(std::map<std::string, std::string> values)
{
    if(values.size() == 0) return;

    const std::string & uri = values.begin()->first;


    DatapointRule * rule = uriToRule[uri];
    if(rule == nullptr) qWarning() << "CANNOT FIND RULE FOR"<< uri.c_str();

    qInfo() << "update has been triggered for" <<uri.c_str() << "with name " << rule->name();
    if(rule->hasToTriggerUpdate(values)){

        const QString queryString{rule->sqlStatement()};
        QSqlDatabase db{QSqlDatabase::database()};

        QSqlQuery query{db};
        query.prepare(queryString);

        QMapIterator<QString, QVariant> boundValuesIt(query.boundValues());

        while(boundValuesIt.hasNext()){
        	boundValuesIt.next();
        	qDebug()<< "key" << boundValuesIt.key()<< "value" << boundValuesIt.value();
        }

        rule->bindValues(query, values);


        if(query.exec() != true) qWarning() << query.lastError();

        qInfo() << "Executed query " << query.executedQuery();
        qInfo() << rule->name() << "  SQL:" << queryString;
    }else{
        qInfo() << "Skipping update of "<< uri.c_str() << " with rule name " << rule->name() << "values all in range";
    }

}

const QJsonDocument readXMLFile(QFile & file){

    QString buffer;

    file.open(QIODevice::ReadOnly | QIODevice::Text);
    try{
        buffer = file.readAll();
        file.close();
    }catch(std::exception &e){
        file.close();
        qCritical()<<"An error occurred while reading configuration file"<< e.what();
        throw e;
    }
    return QJsonDocument::fromJson(buffer.toUtf8());
}

void Program::readDBConfigurationSection(QSettings & configuration){

    configuration.beginGroup("Database");
    const QString name = configuration.value("name").toString();
    const QString type = configuration.value("type").toString();
    const QString address = configuration.value("address").toString();
    const int port = configuration.value("port").toInt();
    const QString user = configuration.value("user").toString();
    const QString pass = configuration.value("pass").toString();
    configuration.endGroup();

    QSqlDatabase db = QSqlDatabase::addDatabase(type);

    db.setHostName(address);
    db.setDatabaseName(name);
    db.setPort(port);
    db.setUserName(user);
    db.setPassword(pass);

    qInfo()<<db;

    *database = db;
}

void Program::readConfigurationFile(QString fileName){
    QFile pathToConfigFile{fileName};

    if(pathToConfigFile.exists()){
        QSettings configuration{fileName, QSettings::IniFormat};
        readDBConfigurationSection(configuration);

    }else{
        throw ConfigurationFileReadError{fileName.toStdString()};
    }
}

void Program::connectToDatapoints(){

    std::cout<< "Datapoint rules are " << dataPointRules.size();

    for(std::shared_ptr<DatapointRule> & rule: dataPointRules){
        std::vector<std::string> dataPoints{rule->getDatapointsURI()};

        for(std::string name : dataPoints) qDebug()<< "Registering " << dataPoints.size() << "rule "<< name.c_str();
        wrapper->connect_datapoints_multi(dataPoints);
    }
}

void Program::checkpointRequested(DatapointRule * rule){

    std::map<std::string, std::string> values;
    qInfo() << "Processing rule " << rule->name();
    try {

        for(const std::string & uri: rule->getDatapointsURI()){
                values[uri] = wrapper->get_formatted_datapoint(uri);
                qDebug() << "uri:"<<uri.c_str() << "\t" << wrapper->get_formatted_datapoint(uri).c_str();
        }

        QSqlDatabase db = QSqlDatabase::database();
        QSqlQuery query{db};
        query.prepare(rule->sqlStatement());

        rule->bindValues(query, values);
        if(!query.exec()) qWarning() << query.lastError();
        qInfo() << "Processed rule " << rule->name();

        qDebug() << "Executed query " << query.executedQuery();
    }catch (...) {
        qWarning() << "cannot update rule " << rule->name();
    }
}



QDomDocument readFile(QString fileName){
    QFile datapointFile{fileName};

    if(datapointFile.exists()){

        QDomDocument parsedFile;
        QXmlInputSource sourceXML{&datapointFile};
        parsedFile.setContent(&sourceXML, true);
        datapointFile.close();
        return parsedFile;

    }else{
        throw ConfigurationFileReadError{fileName.toStdString()};
    }
}

void Program::loadDatapointsConfiguration(QString configurationFile)
{
    QDomDocument parsedContent{readFile(configurationFile)};

    dataPointRules = readDatapointRules(parsedContent);
    for(std::shared_ptr<DatapointRule> & rule: dataPointRules){
        connect(rule.get(), &DatapointRule::requestCheckpoint, this, &Program::checkpointRequested);
        connect(this, &Program::startRulesCheckpointTriggers, rule.get(), &DatapointRule::startCheckpointTimer);

        for(std::string uri: rule->getDatapointsURI()){
            uriToRule[uri] = rule.get();
            qDebug()<< "URI="<< uri.c_str() << "SQL: " << rule->sqlStatement();
        }
    }
}

#include "Program.moc"
