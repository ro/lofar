from django.test import TestCase

import lofar.maintenance.django_postgresql.settings as settings

import lofar.maintenance.monitoringdb.serializers.component_error as component_error
from lofar.maintenance.monitoringdb.models.station import Station
from lofar.maintenance.monitoringdb.models.station_test import StationTest
from datetime import datetime

from lofar.maintenance.monitoringdb.models.component_error import ComponentError


class TestStationTestInsert(TestCase):
    def test_insert_station_test_no_component_errors(self):
        pass


class TestComponentErrorInsert(TestCase):
    def test_insert_component_error_with_details(self):

        station_entry = Station(location='Here', name='CS001C', type='C')
        station_entry.save()
        station_test_entry = StationTest(start_datetime=datetime(2017, 4, 12, 0,0,0),
                                         end_datetime=datetime(2017, 4, 12, 1,0,0),
                                         checks='Some checks',
                                         station=station_entry
                                         )
        station_test_entry.save()
        test_data = {'component': {'component_id': 50, 'type': 'LBL'}, 'details': {'xoff': '11', 'yoff': '25', 'xval': 77.0, 'yval': 75.9}, 'type': 'DOWN'}

        component_error_entry = component_error.ComponentErrorSerializer.insert_component_error(station_test_entry,
                                                                        station_entry,
                                                                        test_data)
        component_error_entry.save()

        id = component_error_entry.pk

        print(ComponentError.objects.get(pk=id).details)