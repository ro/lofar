from django.db import models
from .component import Component
from .station_test import StationTest
from django.contrib.postgres.fields import JSONField


class ComponentError(models.Model):
    type = models.CharField(max_length=50)
    component = models.ForeignKey(Component, on_delete=models.DO_NOTHING)
    station_test = models.ForeignKey(StationTest, on_delete=models.DO_NOTHING,
                                     related_name='component_errors')
    details = JSONField(default=dict)

    class Meta:
        unique_together = ('station_test', 'component', 'type')
