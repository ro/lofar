#!/bin/bash
#exec docker run \
#exec ~mol/docker-run-killable.sh \
exec docker-run-slurm.sh \
    --rm --net=host \
    --stop-signal=SIGKILL \
    -u `id -u` -e USER=$USER -e HOME=$HOME \
    -e SLURM_JOB_ID=$SLURM_JOB_ID \
    -v $HOME/.ssh:$HOME/.ssh:ro \
    -v /data/projects:/data/projects \
    -v /data/log:/data/log \
    -v /data/config/tbb:/data/config/tbb \
    -v /globaldata/tbb:/globaldata/tbb \
    -e LOFARENV=PRODUCTION \
    lofar-tbbwriter-control:latest /bin/bash -c 'source /opt/lofar/lofarinit.sh;exec TBBTMSSBusListener-metadata.py'

