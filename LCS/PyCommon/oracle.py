#!/usr/bin/env python3

# Copyright (C) 2012-2015  ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

# $Id$

'''
Module with nice postgres helper methods and classes.
'''

import logging
from datetime import datetime, timedelta
import cx_Oracle
import re
from lofar.common.dbcredentials import DBCredentials
from lofar.common.database import AbstractDatabaseConnection, DatabaseError, DatabaseConnectionError, DatabaseExecutionError, FETCH_NONE, FETCH_ONE, FETCH_ALL
from lofar.common.util import single_line_with_single_spaces

logger = logging.getLogger(__name__)


class OracleDBError(DatabaseError):
    pass

class OracleDBConnectionError(OracleDBError, DatabaseConnectionError):
    pass

class OracleDBQueryExecutionError(OracleDBError, DatabaseExecutionError):
    pass

class OracleDatabaseConnection(AbstractDatabaseConnection):
    def _do_create_connection_and_cursor(self):
        connection = cx_Oracle.connect(self._dbcreds.user,
                                       self._dbcreds.password,
                                       "%s:%s/%s" % (self._dbcreds.host, self._dbcreds.port, self._dbcreds.database))

        cursor = connection.cursor()
        return connection, cursor

    @property
    def is_connected(self) -> bool:
        return super().is_connected and self._connection.handle!=0

    def _is_recoverable_connection_error(self, error: cx_Oracle.DatabaseError) -> bool:
        '''test if cx_Oracle.DatabaseError is a recoverable connection error'''
        if isinstance(error, cx_Oracle.OperationalError) and re.search('connection', str(error), re.IGNORECASE):
            return True

        if error.code is not None:
            # # see https://docs.oracle.com/cd/B19306_01/server.102/b14219.pdf
            # TODO: check which oracle error code indicates a recoverable connection error.
            pass

        return False

    def _do_execute_query(self, query, qargs=None, fetch=FETCH_NONE):
        '''execute the query and reconnect upon OperationalError'''
        query_log_line = self._queryAsSingleLine(query, qargs)

        try:
            self.connect_if_needed()

            # log
            logger.debug('executing query: %s', query_log_line)

            # execute (and time it)
            start = datetime.utcnow()
            if qargs:
                arg_cntr = 0
                while '%s' in query:
                    query = query.replace('%s', ':arg_%03d'%arg_cntr, 1)
                    arg_cntr += 1
                assert arg_cntr == len(qargs)

            self._cursor.execute(query, qargs or tuple())

            # use rowfactory to turn the results into dicts of (col_name, value) pairs
            if self._cursor.description:
                columns = [col[0] for col in self._cursor.description]
                self._cursor.rowfactory = lambda *col_args: dict(zip(columns, col_args))

            elapsed = datetime.utcnow() - start
            elapsed_ms = 1000.0 * elapsed.total_seconds()

            # log execution result
            logger.info('executed query in %.1fms%s yielding %s rows: %s', elapsed_ms,
                                                                           ' (SLOW!)' if elapsed_ms > 250 else '', # for easy log grep'ing
                                                                           self._cursor.rowcount,
                                                                           query_log_line)

            self._commit_selects_if_needed(query)

            # fetch and return results
            if fetch == FETCH_ONE:
                row = self._cursor.fetchone()
                return row if row is not None else None
            if fetch == FETCH_ALL:
                return [row for row in self._cursor.fetchall() if row is not None]
            return []

        except cx_Oracle.OperationalError as oe:
            if self._is_recoverable_connection_error(oe):
                raise OracleDBConnectionError("Could not execute query due to connection errors. '%s' error=%s" %
                                                (query_log_line,
                                                 single_line_with_single_spaces(oe)))
            else:
                self._log_error_rollback_and_raise(oe, query_log_line)

        except Exception as e:
            self._log_error_rollback_and_raise(e, query_log_line)

    def _log_error_rollback_and_raise(self, e: Exception, query_log_line: str):
        error_string = single_line_with_single_spaces(e)
        logger.error("Rolling back query=\'%s\' due to error: \'%s\'" % (query_log_line, error_string))
        self.rollback()
        if isinstance(e, OracleDBError):
            # just re-raise our OracleDBError
            raise
        else:
            # wrap original error in OracleDBQueryExecutionError
            raise OracleDBQueryExecutionError("Could not execute query '%s' error=%s" % (query_log_line, error_string))


if __name__ == '__main__':
    logging.basicConfig(format = '%(asctime)s %(levelname)s %(message)s', level = logging.INFO)

    dbcreds = DBCredentials().get('LTA')
    print(dbcreds.stringWithHiddenPassword())

    with OracleDatabaseConnection(dbcreds=dbcreds) as db:
        from pprint import pprint
        pprint(db.executeQuery("SELECT table_name, owner, tablespace_name FROM all_tables", fetch=FETCH_ALL))
        # pprint(db.executeQuery("SELECT * FROM awoper.aweprojects", fetch=FETCH_ALL))
        #pprint(db.executeQuery("SELECT * FROM awoper.aweprojectusers", fetch=FETCH_ALL))
