# $Id$
include(LofarCTestPython)

enable_coverage(3)

set(py_files test_rtsm_models.py
             test_wincc_models.py)
python_install(${py_files} DESTINATION lofar/maintenance/test)

lofar_add_test(t_rtsm_models)
lofar_add_test(t_wincc_models)
