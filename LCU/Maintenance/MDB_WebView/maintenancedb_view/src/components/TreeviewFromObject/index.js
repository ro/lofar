import React from 'react';
import TreeView from 'react-treeview';
import 'react-treeview/react-treeview.css';


/**
 * TreeviewFromObject
 * Function component that renders Render an (nested) object as a treeview.
 * Each key of the object will be a leaf in the tree, if the value is an object
 * it will be rendered as child leaf.
 *
 * @props:
 * label: [string] The label of the root node in the tree view
 * objdata [object] The object to render.
 */
const TreeviewFromObject = ({label, objdata}) => {

    if (!objdata) {
        return null;
    }

    if (typeof objdata !== "object"){
        return objdata;
    }

    let tree_nodes = [];

    for (let key of Object.keys(objdata)){
        let value = objdata[key];
        let child;
        if (typeof value === "object") {
            child = <TreeviewFromObject key={key} label={key} objdata={value} />
        } else {
            child = <div key={key}>
                        {key}: <em>{value}</em>
                    </div>
        }
        tree_nodes.push(child)
    }

    return (
        <TreeView key={label} nodeLabel={label}>
            {tree_nodes}
        </TreeView>
    );
}


export default TreeviewFromObject;
