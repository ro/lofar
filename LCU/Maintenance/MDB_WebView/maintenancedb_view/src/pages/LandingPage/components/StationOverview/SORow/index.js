import React, {Component} from 'react';
import {withRouter} from "react-router";

import StationTestBadge from '../StationTestBadge'
import RTSMBadge from '../RTSMBadge'

// CSS
//import {styles} from './styles.module.scss'


/**
 * SORow; Class to render the row for a station in the StationOverview.
 */
class SORowC extends Component {

    renderStationName() {
        return this.props.data.station_name;
    }

    renderStationTests() {
        let data = this.props.data;
        return data.station_tests.map((testData) => <StationTestBadge key={testData.start_datetime} station={data.station_name} data={testData}/>);
    }

    renderRTSM() {
        if (!this.props.data.rtsm || this.props.data.rtsm.length === 0) {
            return "No RTSM data found"
        }

        return this.props.data.rtsm.map((testData) => <RTSMBadge key={testData.observation_id} data={testData}/>);
    }
    onClick() {
        let station = this.props.data.station_name;
        this.props.history.push(`/station_overview?station=${station}`);
    }

    render() {
        return (<tr>
            <th scope="row" onClick={()=>this.onClick()}>{this.renderStationName()}</th>
            <td>{this.renderStationTests()}</td>
            <td>{this.renderRTSM()}</td>
        </tr>);
    }
}


const SORow = withRouter(SORowC);

export default SORow;
