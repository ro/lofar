import logging
from typing import Dict

from celery import shared_task
from django.db import transaction

# Station Models
from lofar.maintenance.monitoringdb.models.component import Component
from lofar.maintenance.monitoringdb.models.component_error import ComponentError
from lofar.maintenance.monitoringdb.models.element import Element
from lofar.maintenance.monitoringdb.models.element_error import ElementError
from lofar.maintenance.monitoringdb.models.log import ActionLog
# RTSM Models
from lofar.maintenance.monitoringdb.models.rtsm import RTSMObservation, RTSMError, RTSMErrorSample
from lofar.maintenance.monitoringdb.models.signals.generic_test_insert import on_inserting_test
from lofar.maintenance.monitoringdb.models.station import Station
# Station Test Models
from lofar.maintenance.monitoringdb.models.station_test import StationTest
from lofar.maintenance.monitoringdb.rtsm_test_raw_parser import parse_rtsm_test
from lofar.maintenance.monitoringdb.station_test_raw_parser import parse_raw_station_test, \
    station_type_from_station_name

logger = logging.getLogger(__name__)
from django.db.models.signals import post_save

post_save.connect(on_inserting_test, sender=RTSMObservation, dispatch_uid='test_inserted')
post_save.connect(on_inserting_test, sender=StationTest, dispatch_uid='test_inserted')


def create_component_errors(station: Station, station_test: StationTest, component_errors_data):
    component_list = []
    component_error_list = []
    for component_error in component_errors_data:
        component = component_error.pop('component')
        component_entity, _ = Component.objects.get_or_create(station=station, **component)

        component_list.append(component_entity)
        component_error_entity, _ = ComponentError.objects.get_or_create(component=component_entity,
                                                                         station_test=station_test,
                                                                         type=component_error[
                                                                             'type'])
        component_error_entity.details.update(component_error['details'])
        component_error_entity.save()

        component_error_list.append(component_error_entity)

    return component_list, component_error_list


def create_element_errors(station, station_test, elements_error_data):
    element_errors = []
    for element_error_data in elements_error_data:
        component_error = element_error_data.pop('component_error')
        component = component_error.pop('component')
        element = element_error_data.pop('element')

        component, _ = Component.objects.get_or_create(station=station, **component)
        component_error, _ = ComponentError.objects.get_or_create(station_test=station_test,
                                                                  component=component,
                                                                  **component_error)
        element, _ = Element.objects.get_or_create(**element, component=component)

        element_error, _ = ElementError.objects.get_or_create(element=element,
                                                              component_error=component_error,
                                                              **element_error_data)

        element_errors.append(element_error)


def create_station(station_data):

    station, created = Station.objects.update_or_create(defaults=station_data,
                                                        name=station_data['name'])


    return station


def create_station_test(station: Station, station_test_data):
    station_test, created = StationTest.objects.get_or_create(station=station, **station_test_data)
    return station_test


@transaction.atomic
def create_station_tests(station_tests_data):
    parsed_tests = parse_raw_station_test(station_tests_data)

    for station_test_data in parsed_tests:
        station_data = station_test_data.pop('station')
        component_errors = station_test_data.pop('component_errors')
        element_errors = station_test_data.pop('element_errors')

        station = create_station(station_data)

        station_test = create_station_test(station, station_test_data)

        create_component_errors(station,
                                station_test,
                                component_errors)
        create_element_errors(station, station_test, element_errors)


def create_rtsm_error(observation:RTSMObservation, rtsm_error):
    start_frequency = rtsm_error.pop('start_frequency')
    stop_frequency = rtsm_error.pop('stop_frequency')
    n_samples = observation.samples
    samples = rtsm_error.pop('samples')

    count = rtsm_error.pop('count')
    percentage = count / float(n_samples)
    rtsm_error, created = RTSMError.get_or_create_error_from_rtsm_result(observation=observation,
                                                                count=count,
                                                                percentage=percentage,
                                                                **rtsm_error)

    if created:
        rtsm_error_samples = [RTSMErrorSample(error=rtsm_error,
                                              start_frequency=start_frequency,
                                              stop_frequency=stop_frequency,
                                              **error_sample) for error_sample in samples]
        RTSMErrorSample.objects.bulk_create(rtsm_error_samples)
    else:
        for error_sample in samples:
            RTSMErrorSample.objects.get_or_create(error=rtsm_error,
                                                  start_frequency=start_frequency,
                                                  stop_frequency=stop_frequency,
                                                  **error_sample)

    return rtsm_error


@transaction.atomic
def create_rtsm_test(rtsm_observation) -> RTSMObservation:
    errors = rtsm_observation.pop('errors')

    station_name = rtsm_observation.pop('station_name')
    sas_id = rtsm_observation.pop('observation_id')

    station = create_station(dict(name=station_name,
                                     type=station_type_from_station_name(station_name)))

    rtsm_observation, _ = RTSMObservation.objects.get_or_create(station=station,
                                                                sas_id=sas_id,
                                                                **rtsm_observation)
    for error in errors:
        create_rtsm_error(rtsm_observation, error)
    return rtsm_observation


@shared_task(ignore_result=True)
def insert_station_test(action_log_id: ActionLog, raw_tests):
    action_log = ActionLog.objects.get(pk=action_log_id)
    try:
        logger.debug('handling raw request_data for data %s', raw_tests)
        parsed_content = parse_raw_station_test(raw_tests)
        if parsed_content is None:
            raise Exception('cannot parse test {}'.format(raw_tests))
        create_station_tests(raw_tests)
        logger.info('data parsed successfully for test')
        action_log.what = 'INSERTED'
        action_log.save()

    except Exception as e:
        action_log.what = 'INSERT_FAILED'
        action_log.save()
        raise e


@shared_task(ignore_result=True)
def insert_rtsm_test(action_log_id: ActionLog, request_data: Dict):
    action_log = ActionLog.objects.get(pk=action_log_id)
    try:
        logger.debug('handling raw request_data for %s', request_data)
        content = request_data['content']
        station_name = request_data['station_name']
        entry = parse_rtsm_test(content)

        entry.update(station_name=station_name)

        rtsm_test = create_rtsm_test(entry)

        action_log.what = 'INSERTED'
        action_log.save()
        logger.info('RTSM parsed successfully for obsid %d and station %s',
                    rtsm_test.sas_id,
                    rtsm_test.station.name)

    except Exception as e:
        action_log.what = 'INSERT_FAILED'
        action_log.save()
        raise e
