import django.db.models as models


class ActionLog(models.Model):
    entry_id = models.IntegerField(help_text='database id of the concerned object')
    model_name = models.CharField(max_length=100)
    who = models.CharField(max_length=100)
    when = models.DateTimeField(auto_now_add=True)
    what = models.CharField(max_length=1000)
