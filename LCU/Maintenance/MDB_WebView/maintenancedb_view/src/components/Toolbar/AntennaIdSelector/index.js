import React, {Component} from 'react';
import {connect} from "react-redux";
import {Input} from 'reactstrap';

// History handling
import { push } from 'connected-react-router';
import { store } from "redux/store.js";

import {setAntennaID} from "redux/actions/mainFiltersActions";
import {stationTypeFromName, AntennaIdsPerTypeStationType} from "utils/LOFARDefinitions";

// CSS
import toolbarStyles from '../styles.module.scss'
import styles from './styles.module.scss'



/**
 * AntennaIdSelector:
 * Select box for selecting an antenna Id. This component is part of {@link module:Toolbar}.
 */
class AntennaIdSelectorC extends Component {

    antennaIdList(type, stationType){
        const antennaIdRange = AntennaIdsPerTypeStationType[stationType][type];
        let options = [];
        for(let i=antennaIdRange.start; i<antennaIdRange.end; i++){
            options.push(<option key={i} value={i}>{i}</option>);
        }
        return options
    }
    onSelectedAntenna = (e) => {
        //this.props.setAntennaID(Number(e.target.value));
        store.dispatch(push(`?antenna_id=${e.target.value}`))
    };

    render() {
        const stationType = stationTypeFromName(this.props.selectedStation);

        const options = this.antennaIdList(this.props.antennaType, stationType);

        return (
        <div className={toolbarStyles.toolbar_ctrl}>
            <label>Antenna id</label>
            <Input type="select"
                   value={this.props.antennaId}
                   onChange={this.onSelectedAntenna}
                   bsSize="sm"
                   className={styles.antenna_id_select}>
                {options}
            </Input>
        </div>);
    }

}

const mapStateAntennaIdSelector = state => {
    return {
        selectedStation: state.mainFilters.selectedStation,
        antennaId: state.mainFilters.antennaId,
        antennaType: state.mainFilters.antennaType
    };
};

const mapDispatchAntennaIdSelector = {
    setAntennaID
};

export const AntennaIdSelector = connect(mapStateAntennaIdSelector, mapDispatchAntennaIdSelector)(AntennaIdSelectorC);
