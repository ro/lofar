#!/bin/bash
#exec docker run \
#exec ~mol/docker-run-killable.sh \
exec docker-run-slurm.sh \
    --rm --net=host \
    --stop-signal=SIGKILL \
    -u `id -u` -e USER=$USER -e HOME=$HOME \
    -e SLURM_JOB_ID=$SLURM_JOB_ID \
    -v $HOME/.ssh:$HOME/.ssh:ro \
    -v /data/projects:/data/projects \
    -v /data/log:/data/log \
    -v /data/home/lofarsys/tbb-writer/:/data/home/lofarsys/tbb-writer/ \
    -v /globaldata/tbb:/globaldata/tbb \
    -e LOFARENV=PRODUCTION \
    --cap-add=sys_nice --cap-add=sys_admin \
    lofar-tbbwriter-control:TBB-Task8589 /data/home/lofarsys/tbb-writer/run/run-TBBTMSSBusListenerdatawriter.py "$@"

