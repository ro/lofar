from collections import OrderedDict

import django.db.models as models

from .component import Component
from .component import antenna_type_antenna_id_to_component_type_component_id
from .element import Element
from .station import Station

COMPONENT_TYPE_TO_ANTENNA_TYPE = dict(HBA='HBA', LBH='LBA', LBL='LBA')
STATUS_CODE_TO_STATUS = {0: 'OFF',
                         10: 'OPERATIONAL',
                         20: 'MAINTENANCE',
                         30: 'TEST',
                         40: 'SUSPICIOUS',
                         50: 'BROKEN',
                         60: 'BEYOND REPAIR',
                         70: 'MISSING DATAPOINT'}


def to_antenna_type(component_type):
    return COMPONENT_TYPE_TO_ANTENNA_TYPE.get(component_type, None)


def to_status(status_code):
    return STATUS_CODE_TO_STATUS.get(status_code, 'UNKNOWN')


def _format_result(raw_query_result, antenna_type):
    results = OrderedDict()
    for row in raw_query_result:
        status_description = OrderedDict()
        antenna_id = row.antenna_id
        results[str(antenna_id)] = status_description
        status_description['inserted_at'] = row.last_entry_timestamp
        status_description['status'] = to_status(row.status_code)
        status_description['status_code'] = row.status_code
        status_description['reason'] = row.status_code_reason
        status_description['last_modified'] = row.status_code_modified_at

        if antenna_type is 'HBA':
            elements = OrderedDict()
            status_description['elements'] = elements

            for i in range(16):
                element_status = OrderedDict()
                elements[str(i)] = element_status

                element_status['status_code'] = getattr(row, "element_%.02d" % i, )
                element_status['status'] = to_status(element_status['status_code'])

                element_status['reason'] = getattr(row, "element_%.02d_reason" % i, )
                element_status['last_modified'] = getattr(row,
                                                          "element_%.02d_modified_at" % i, )
    return results


def latest_status_per_station_raw(queryset, station, to_date):
    """
    Retrieve the latest antenna statuses for a given station and antenna type
    :param queryset: source queryset
    :param station: station name
    :type station: str
    :param to_date: select the latest test before the to date
    :type to_date: datetime.dattime
    :return: the status of an antenna per antenna id
    """
    raw_results = queryset.raw('''
    SELECT *
    FROM antenna_statuses master
    INNER JOIN (
    SELECT antenna_type, antenna_id, MAX("timestamp") last_entry_timestamp 
    FROM antenna_statuses
    WHERE status_code_modified_at <= %(to_date)s AND station=%(station_name)s
    GROUP BY antenna_type, antenna_id 
    ) slave ON master.antenna_type=slave.antenna_type AND master.antenna_id = slave.antenna_id AND last_entry_timestamp=master.timestamp
    WHERE master.station=%(station_name)s
    ''', dict(to_date=to_date, station_name=station.rstrip('C')))

    return raw_results


def latest_status_per_station_and_antenna_type(queryset, station, antenna_type, to_date):
    """
    Retrieve the latest antenna statuses for a given station and antenna type
    :param queryset: source queryset
    :param station: station name
    :type station: str
    :param component_type: component type name
    :type component_type: str
    :param to_date: select the latest test before the to date
    :type to_date: datetime.datetime
    :return: the status of an antenna per antenna id
    """
    raw_results = queryset.raw('''
    SELECT *
    FROM antenna_statuses master
    INNER JOIN (
    SELECT antenna_type, antenna_id, MAX("timestamp") last_entry_timestamp 
    FROM antenna_statuses
    WHERE status_code_modified_at <= %s AND station=%s
    GROUP BY antenna_type, antenna_id 
    ) slave ON master.antenna_type=slave.antenna_type AND master.antenna_id = slave.antenna_id AND last_entry_timestamp=master.timestamp
    WHERE master.station=%s AND master.antenna_type=%s
    ''', [to_date, station.rstrip('C'), station.rstrip('C'), antenna_type])


    return _format_result(raw_results, antenna_type)


def latest_status_per_station_and_component_type(queryset, station, component_type, to_date):
    """
    Retrieve the latest antenna statuses for a given station and component type
    :param queryset: source queryset
    :param station: station name
    :type station: str
    :param component_type: component type name
    :type component_type: str
    :param to_date: select the latest test before the to date
    :type to_date: datetime.datetime
    :return: the status of an antenna per antenna id
    """
    antenna_type = to_antenna_type(component_type)
    if antenna_type:

        results = latest_status_per_station_and_antenna_type(queryset, station, antenna_type,
                                                             to_date)
        return {antenna_id: results[antenna_id] for antenna_id in results
                if antenna_type_antenna_id_to_component_type_component_id(station, component_type,
                                                                          int(antenna_id))[
                    0] == component_type}
    else:
        return OrderedDict()


def latest_status_per_station_and_component_type_antenna_id(queryset, station,
                                                            component_type,
                                                            component_id,
                                                            to_date):
    """
    Retrieve the latest antenna statuses for a given station and component type and id
    :param queryset: source queryset
    :param station: station name
    :type station: str
    :param component_type: component type name
    :type component_type: str
    :param to_date: select the latest test before the to date
    :type to_date: datetime.datetime
    :return: the status of an antenna per antenna id
    """
    antenna_type = to_antenna_type(component_type)
    if antenna_type:
        return latest_status_per_station_and_antenna_type_antenna_id(queryset, station,
                                                                     antenna_type,
                                                                     component_id,
                                                                     to_date)
    else:
        return OrderedDict()


def latest_status_per_station_and_antenna_type_antenna_id(queryset, station,
                                                          antenna_type,
                                                          antenna_id,
                                                          to_date):
    """
    Retrieve the latest antenna statuses for a given station and antenna type and id
    :param queryset: source queryset
    :param station: station name
    :type station: str
    :param antenna_type: antenna type name
    :type antenna_type: str
    :param to_date: select the latest test before the to date
    :type to_date: datetime.datetime
    :return: the status of an antenna per antenna id
    """
    raw_results = queryset.raw('''
    SELECT *
    FROM antenna_statuses master
    INNER JOIN (
    SELECT station, antenna_type, antenna_id, MAX("timestamp") last_entry_timestamp 
    FROM antenna_statuses
    WHERE timestamp <= %s
    GROUP BY station, antenna_type, antenna_id 
    ) slave ON master.station=slave.station AND master.antenna_type=slave.antenna_type AND master.antenna_id = slave.antenna_id AND last_entry_timestamp=master.timestamp
    WHERE master.station=%s AND master.antenna_type=%s AND master.antenna_id=%s
    ''', [to_date, station.rstrip('C'), antenna_type, antenna_id])
    return _format_result(raw_results, antenna_type)


class WinCCAntennaStatus(models.Model):
    class Meta:
        db_table = "antenna_statuses"
        ordering = ['-timestamp', 'station', 'antenna_type', 'antenna_id']
        indexes = [
            models.Index(fields=['station', 'antenna_type', 'antenna_id', 'timestamp'])
        ]

    timestamp = models.DateTimeField(auto_now_add=True)
    station = models.CharField(max_length=10, default=None)
    antenna_type = models.CharField(max_length=4, default=None)
    antenna_id = models.IntegerField()

    status_code = models.IntegerField()
    status_code_reason = models.CharField(max_length=1000)
    status_code_modified_at = models.DateTimeField(max_length=100)

    element_00 = models.IntegerField(null=True)
    element_00_reason = models.CharField(max_length=1000, null=True)
    element_00_modified_at = models.DateTimeField(max_length=100, null=True)

    element_01 = models.IntegerField(null=True)
    element_01_reason = models.CharField(max_length=1000, null=True)
    element_01_modified_at = models.DateTimeField(max_length=100, null=True)

    element_02 = models.IntegerField(null=True)
    element_02_reason = models.CharField(max_length=1000, null=True)
    element_02_modified_at = models.DateTimeField(max_length=100, null=True)

    element_03 = models.IntegerField(null=True)
    element_03_reason = models.CharField(max_length=1000, null=True)
    element_03_modified_at = models.DateTimeField(max_length=100, null=True)

    element_04 = models.IntegerField(null=True)
    element_04_reason = models.CharField(max_length=1000, null=True)
    element_04_modified_at = models.DateTimeField(max_length=100, null=True)

    element_05 = models.IntegerField(null=True)
    element_05_reason = models.CharField(max_length=1000, null=True)
    element_05_modified_at = models.DateTimeField(max_length=100, null=True)

    element_06 = models.IntegerField(null=True)
    element_06_reason = models.CharField(max_length=1000, null=True)
    element_06_modified_at = models.DateTimeField(max_length=100, null=True)

    element_07 = models.IntegerField(null=True)
    element_07_reason = models.CharField(max_length=1000, null=True)
    element_07_modified_at = models.DateTimeField(max_length=100, null=True)

    element_08 = models.IntegerField(null=True)
    element_08_reason = models.CharField(max_length=1000, null=True)
    element_08_modified_at = models.DateTimeField(max_length=100, null=True)

    element_09 = models.IntegerField(null=True)
    element_09_reason = models.CharField(max_length=1000, null=True)
    element_09_modified_at = models.DateTimeField(max_length=100, null=True)

    element_10 = models.IntegerField(null=True)
    element_10_reason = models.CharField(max_length=1000, null=True)
    element_10_modified_at = models.DateTimeField(max_length=100, null=True)

    element_11 = models.IntegerField(null=True)
    element_11_reason = models.CharField(max_length=1000, null=True)
    element_11_modified_at = models.DateTimeField(max_length=100, null=True)

    element_12 = models.IntegerField(null=True)
    element_12_reason = models.CharField(max_length=1000, null=True)
    element_12_modified_at = models.DateTimeField(max_length=100, null=True)

    element_13 = models.IntegerField(null=True)
    element_13_reason = models.CharField(max_length=1000, null=True)
    element_13_modified_at = models.DateTimeField(max_length=100, null=True)

    element_14 = models.IntegerField(null=True)
    element_14_reason = models.CharField(max_length=1000, null=True)
    element_14_modified_at = models.DateTimeField(max_length=100, null=True)

    element_15 = models.IntegerField(null=True)
    element_15_reason = models.CharField(max_length=1000, null=True)
    element_15_modified_at = models.DateTimeField(max_length=100, null=True)


class EntityStatus(models.Model):
    status_code = models.IntegerField(default=0)
    status_description = models.CharField(max_length=20)
    status_message = models.CharField(max_length=100, default='')
    modified_at = models.DateTimeField(null=True, default=None)

    class Meta:
        abstract = True


class StationStatus(models.Model):
    station = models.ForeignKey(Station, on_delete=models.DO_NOTHING, null=True)


class ComponentStatus(EntityStatus):
    station_status = models.ForeignKey(StationStatus, on_delete=models.CASCADE,
                                       related_name='component_status')
    component = models.ForeignKey(Component, on_delete=models.DO_NOTHING)


class ElementStatus(EntityStatus):
    component_status = models.ForeignKey(ComponentStatus, on_delete=models.CASCADE,
                                         related_name='element_status')
    element = models.ForeignKey(Element, on_delete=models.DO_NOTHING)
