#!/bin/bash
DATE=$(date +%Y%m%dT%H%M%S)
echo logfile /data/log/tbbwriteparset-$DATE-cpu??.log
sleep 60
./start-tbbwriteparset.sh
exec srun -N 1 \
    --partition=cpu \
    --cpus-per-task=1 \
    --job-name="tbbwriteparset" \
    --output=/data/log/tbbwriter/tbbwriteparset-$DATE-%N.log \
    /data/home/lofarsys/tbb-writer/run/docker-launch-tbbwriteparset.sh


