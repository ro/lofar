export const SELECT_ANTENNA_ERROR = "SELECT_ANTENNA_ERROR";
export const UNPIN_ANTENNA_ERROR = "PIN_ANTENNA_ERROR";

export const selectAntennaError = (data, doPin = false) => ({
  type: SELECT_ANTENNA_ERROR,
  payload: { data, doPin }
});

export const unpinAntennaError = () => ({
  type: UNPIN_ANTENNA_ERROR,
  payload: {}
});
