#!/usr/bin/env python3

import sys
from math import sqrt, cos, sin, pi, asin, atan2
import subprocess
import itertools


class RingCoordinates:
    """
    This has been taken from RTCP/Cobalt test tRinGCoordinates.py

    Original RingCoordinates implementation (+ Vlad's fix).
    Taken from parset.py in RTCP\\Run\\src\\LOFAR\\parset
    """

    def __init__(self, numrings, width, center, position_angle):
        """
        :param numrings: number of hexagonal rings to form
        :param width: 'radius' of innermost ring / distance between rings (angle as float)
        :param center: the center pointing around which to generate rings (angles as tuple)
        :param position_angle: rotate the hexagonal rings with respect to the sky
        :return: list of pointings (angles as tuple) / length: 3*numrings**2 - 3*numrings + 1
        """
        self.numrings = numrings
        self.width = width
        self.center = center
        self.position_angle = position_angle

    def len_edge(self):
        """
          _
         / \
         \_/
         |.|
        """
        return self.width / sqrt(3)

    def len_width(self):
        """
          _
         / \
         \_/
        |...|
        """
        return 2 * self.len_edge()

    def len_height(self):
        """
         _  _
        / \ :
        \_/ _

        """
        return self.width

    def delta_width(self):
        """
         _
        / \_
        \_/ \
          \_/
         |.|
        """
        return 1.5 * self.len_edge()

    def delta_height(self):
        """
         _
        / \_  -
        \_/ \ -
          \_/
        """
        return 0.5 * self.len_height()

    def offsets(self):
        if self.numrings == 0:
            return []

        offsets = [(0, 0)]  # start with central beam

        # stride for each side, starting from the top, clock-wise
        dl = [0] * 6
        dm = [0] * 6

        #  _
        # / \_
        # \_/ \
        #   \_/
        dl[0] = self.delta_width()
        dm[0] = -self.delta_height()

        #  _
        # / \
        # \_/
        # / \
        # \_/
        dl[1] = 0
        dm[1] = -self.len_height()

        #    _
        #  _/ \
        # / \_/
        # \_/
        dl[2] = -self.delta_width()
        dm[2] = -self.delta_height()

        #  _
        # / \_
        # \_/ \
        #   \_/
        dl[3] = -self.delta_width()
        dm[3] = self.delta_height()

        #  _
        # / \
        # \_/
        # / \
        # \_/
        dl[4] = 0
        dm[4] = self.len_height()

        #    _
        #  _/ \
        # / \_/
        # \_/
        dl[5] = self.delta_width()
        dm[5] = self.delta_height()

        # ring 1-n: create the pencil beams from the inner ring outwards
        for r in range(1, self.numrings + 1):
            # start from the top
            l = 0.0
            m = self.len_height() * r

            for side in range(6):
                # every side has length r
                for b in range(r):
                    offsets.append((l, m))
                    l += dl[side]
                    m += dm[side]

        return offsets

    def coordinates(self):
        return [project(offset, self.center, self.position_angle) for offset in self.offsets()]


def project(offset, center, position_angle):
    """
    This function applies Euler angle matrix rotations to add an angular offset to a reference/center pointing.
    Specifically, it converts the offset with respect to a cartesian vector on a sphere, referenced to a
    cartesian unit vector (1, 0, 0). The sphere is then rotated around the two angles of the center pointing,
    and the resulting vector is converted back to angles to obtain the projected pointing.
    (see https://en.wikipedia.org/wiki/Rotation_matrix)
    """
    offset1, offset2 = offset
    angle1, angle2 = center

    # Angular offset (rho) and position angle (theta)
    rho = sqrt(offset1 ** 2 + offset2 ** 2)
    theta = atan2(offset1, offset2) - position_angle

    # Generate cartesian vector (rho, theta = 0, 0 -> x, y, z = 1, 0, 0)
    x, y, z = (cos(rho),
               sin(theta) * sin(rho),
               cos(theta) * sin(rho))

    # Rotate around y-axis by -angle2
    x1, y1, z1 = (cos(angle2) * x - sin(angle2) * z,
                  y,
                  sin(angle2) * x + cos(angle2) * z)

    # Rotate around z-axis by angle1
    x2, y2, z2 = (cos(angle1) * x1 - sin(angle1) * y1,
                  sin(angle1) * x1 + cos(angle1) * y1,
                  z1)

    # Recompute spherical angles
    return (atan2(y2, x2) % (2 * pi), asin(z2) / sqrt(x2 * x2 + y2 * y2 + z2 * z2))
