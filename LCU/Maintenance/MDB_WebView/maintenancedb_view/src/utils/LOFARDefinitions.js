export const HBATilesPerStationType = {
  C: 96,
  R: 96,
  I: 96
};

export const LBHAntennasPerStationType = {
  C: 48,
  R: 48,
  I: 96
};

export const LBLAntennasPerStationType = {
  C: 48,
  R: 48,
  I: 48
};

export const AntennaIdsPerTypeStationType = {
  C: {
    HBA: {
      start: 0,
      end: 96,
      number: 96
    },
    LBL: {
      start: 48,
      end: 96,
      number: 48
    },
    LBH: {
      start: 0,
      end: 48,
      number: 48
    }
  },
  R: {
    HBA: {
      start: 0,
      end: 96,
      number: 96
    },
    LBL: {
      start: 48,
      end: 96,
      number: 48
    },
    LBH: {
      start: 0,
      end: 48,
      number: 48
    }
  },
  I: {
    HBA: {
      start: 0,
      end: 96,
      number: 96
    },
    LBH: {
      start: 0,
      end: 96,
      number: 96
    },
    LBL: {
      start: 0,
      end: 0,
      number: 0
    }
  },
};

export function stationTypeFromName(stationName){
  if(stationName === undefined) return undefined;
  if(stationName.includes('CS')) return 'C';
  if(stationName.includes('RS')) return 'R';
  return 'I'
}

export function getInspectPageURLFromSASid(sasId){
  const url =`https://proxy.lofar.eu/inspect/HTML/${sasId}/index.html`
  return url
}

export const LOFARTESTS = {
    ST: "Status",    // WinCC status
    S: "StationTest",
    R: "RTSM"
}

export function componentStatusOK(status) {
    return (status === 'OPERATIONAL' || status === 'OFF' || status === null);
}
export function componentStatusBeyondRepair(status) {
    return (status === 'BEYOND REPAIR');
}
