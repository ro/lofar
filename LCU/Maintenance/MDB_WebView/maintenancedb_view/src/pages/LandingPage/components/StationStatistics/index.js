import React, {Component} from 'react';
import AutoLoadWrapper from 'components/AutoLoadWrapper'
import ReactVegaLite from 'react-vega-lite'
import {
    Navbar,
    NavbarBrand
} from 'reactstrap';
import Toolbar from './Toolbar'

// CSS
import styles from './styles.module.scss'


class StationStatisticsC extends Component {

    state = {
        histogramType: 'per_error_type'
    }

    ref = React.createRef()

    static getBaseSpec() {
        return {
            "$schema": "https://vega.github.io/schema/vega-lite/v2.json",
            "mark": {
                "type": "bar"
            },
            "autosize": {
                "type": "fit",
                "contains": "padding"
            },
            "selection":{
                "highlight": {"type":"single", "empty":"none","on": "mouseover"},
            },
            "config": {
                "legend": {
                    "columns": 2,
                }
            },
            "encoding": {
                "x": {
                    "field": "time",
                    "type": "ordinal",
                    "axis": {
                        "title": "day"
                    }
                },
                "y": {
                    "field": "n_errors",
                    "type": "quantitative",
                    "axis": {
                        "title": "number of errors"
                    },

                },
                opacity: {
                    "condition": {"selection": "highlight", "value": .6},
                    value: 1
                }
            },
        }
    }

    static getErrorsPerTypeSpec() {
        let schema = StationStatisticsC.getBaseSpec();
        schema.encoding["color"] = {
            "field": "error_type",
            "type": "nominal"
        }
        schema.encoding["tooltip"] = [
            {
                field: "n_errors",
                type: "quantitative"
            }, {
                field: "error_type",
                type: "nominal"
            }
        ]
        return schema;
    }

    static getErrorsPerStationSpec() {
        let schema = StationStatisticsC.getBaseSpec();
        schema.encoding["color"] = {
            "field": "station_name",
            "type": "nominal"
        };
        schema.encoding["tooltip"] = [
            {
                field: "n_errors",
                type: "quantitative"
            }, {
                field: "station_name",
                type: "nominal"
            }
        ];
        schema.config.legend.columns = 3;
        return schema;
    }

    getErrorsPerStation() {
        if (this.props.data.errors_per_station !== undefined) {
            return {values: this.props.data.errors_per_station};
        }
    }

    getErrorsPerType() {
        if (this.props.data.errors_per_type !== undefined) {
            return {values: this.props.data.errors_per_type};
        }
    }

    getSpecData(histogram_type) {
        switch (histogram_type) {
            case "per_error_type":
                return {spec: StationStatisticsC.getErrorsPerTypeSpec(), data: this.getErrorsPerType()};
            case "per_station":
                return {spec: StationStatisticsC.getErrorsPerStationSpec(), data: this.getErrorsPerStation()};
            default:
                return {spec: StationStatisticsC.getErrorsPerTypeSpec(), data: this.getErrorsPerType()};
        }
    }

    onSwitchHistogramType = (e) => {
        this.setState({histogramType: e.target.value})
    }

    // Do not (re)render when data is loading (performance improvement)
    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.isLoading) {
            return false;
        }
        return true;
    }

    render() {
        const {spec, data} = this.getSpecData(this.state.histogramType);
        if (this.ref.current !== null) {
            const width = this.ref.current.clientWidth;
            const height = this.ref.current.clientHeight;
            spec.width = width
            spec.height = height

        }
        return (<React.Fragment>
            <Navbar className={styles.navbar+" react-grid-item-header justify-content-between"} >
                <NavbarBrand className={styles.brand}>
                    <h5 className="react-grid-item-header">
                        Station statistics
                    </h5>
                </NavbarBrand>
                <Toolbar histogramType={this.state.histogramType}
                         switchHistogramEvent={this.onSwitchHistogramType}/>
            </Navbar>
            <div className="react-grid-item-body" id="plot" ref={this.ref}>
                <ReactVegaLite spec={spec} data={data} enableHover={true}/>
            </div>
        </React.Fragment>);
    }
}
/* Add some magic; use the AutoLoadWrapper to create a HOC that handles the
   auto-loading of the data for StationOverviewC.
 */
const StationStatistics = AutoLoadWrapper(StationStatisticsC);

export default StationStatistics;
