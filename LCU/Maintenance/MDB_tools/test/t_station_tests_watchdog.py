import unittest

from lofar.maintenance.utils.cli.station_tests_watchdog import *
from mock import patch, MagicMock, call

logger = logging.getLogger(__name__)


class TestStationTestsWatchdog(unittest.TestCase):
    def setUp(self):
        self.mock_path = 'mydirectory'
        self.mock_address = 'http://fakeaddress.fk'
        self.mock_filename = 'likename'
        self.mock_content = 'fake_content'

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.mdb_client')
    def test_on_created_event_read_test_file(self, mock_mdb_client):
        event = ('UNUSED', 'IN_CLOSE_WRITE', self.mock_path, self.mock_filename)

        handle_inotify_event_create_file(event, self.mock_filename, self.mock_address)

        mock_mdb_client.read_stationtest_rtsm_output_to_dict.assert_called_with(self.mock_path +
                                                                                '/' +
                                                                                self.mock_filename)

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.mdb_client')
    def test_on_created_event_send_test_file_content(self, mock_mdb_client):
        event = ('UNUSED', 'IN_CLOSE_WRITE', self.mock_path, self.mock_filename)

        mock_mdb_client.read_stationtest_rtsm_output_to_dict.return_value = self.mock_content

        handle_inotify_event_create_file(event, self.mock_filename, self.mock_address)

        mock_mdb_client.send_stationtest_rtsm_content_to_address.assert_called_with(
            self.mock_address,
            self.mock_content)

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.mdb_client')
    def test_on_created_event_wrong_expected_name_not_send_test_file_content(self, mock_mdb_client):
        event = ('UNUSED', 'IN_MODIFIED', self.mock_path, self.mock_filename)
        mock_mdb_client.read_stationtest_rtsm_output_to_dict.return_value = self.mock_content

        handle_inotify_event_create_file(event, 'wrong_name', self.mock_address)

        mock_mdb_client.send_stationtest_rtsm_content_to_address.assert_not_called()

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.mdb_client')
    def test_on_modified_event_not_send_test_file_content(self, mock_mdb_client):
        event = ('UNUSED', 'IN_MODIFIED', self.mock_path, self.mock_filename)
        mock_mdb_client.read_stationtest_rtsm_output_to_dict.return_value = self.mock_content

        handle_inotify_event_create_file(event, self.mock_filename, self.mock_address)

        mock_mdb_client.send_stationtest_rtsm_content_to_address.assert_not_called()

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.mdb_client')
    def test_on_modified_event_not_read_test_file_content(self, mock_mdb_client):
        mock_mdb_client.read_stationtest_rtsm_output_to_dict.return_value = self.mock_content
        event = ('UNUSED', 'IN_MODIFIED', self.mock_path, self.mock_filename)

        handle_inotify_event_create_file(event, self.mock_filename, self.mock_address)

        mock_mdb_client.read_stationtest_rtsm_output_to_dict.assert_not_called()

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.mdb_client')
    def test_on_created_event_not_send_if_file_not_test(self, mock_mdb_client):
        event = ('UNUSED', 'IN_CREATED', self.mock_path, self.mock_filename)

        mock_mdb_client.read_stationtest_rtsm_output_to_dict.return_value = self.mock_content
        mock_mdb_client.read_stationtest_rtsm_output_to_dict.side_effect = ValueError()

        handle_inotify_event_create_file(event, self.mock_filename, self.mock_address)

        mock_mdb_client.send_stationtest_rtsm_content_to_address.assert_not_called()

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.mdb_client')
    def test_on_created_event_not_send_if_file_error_reading_file(self, mock_mdb_client):
        event = ('UNUSED', 'IN_CREATED', self.mock_path, self.mock_filename)

        mock_mdb_client.read_stationtest_rtsm_output_to_dict.return_value = self.mock_content
        mock_mdb_client.read_stationtest_rtsm_output_to_dict.side_effect = IOError()

        handle_inotify_event_create_file(event, self.mock_filename, self.mock_address)

        mock_mdb_client.send_stationtest_rtsm_content_to_address.assert_not_called()

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.'
           'handle_inotify_event_create_file')
    def test_main_wait_event_loop_unexpected_exception_keyboard_interrupt(self,
                                                            handle_inotify_event_create_file_mock):
        active_watch_time = 3
        test_event = ('_', 'fake_type', 'fake_path', 'fake_file')
        observer_mock = MagicMock()
        observer_mock.event_gen.return_value = [test_event]
        handle_inotify_event_create_file_mock.side_effect = [[], Exception(), KeyboardInterrupt()]

        self.assertTrue(
            main_wait_event_loop(observer_mock, 'fake_name', self.mock_address, active_watch_time))
        handle_inotify_event_create_file_mock.assert_has_calls(
            [call(test_event, 'fake_name', self.mock_address)] * 3)

    @patch('inotify.adapters.Inotify')
    def test_instantiate_file_creation_event_observer(self, inotify_mock):
        mock_path = 'path_to_file'
        inotify_object_partial_mock = MagicMock(inotify.adapters.Inotify())
        inotify_mock.return_value = inotify_object_partial_mock
        inotify_object_partial_mock.add_watch = MagicMock()

        instantiate_file_creation_event_observer(mock_path)
        inotify_object_partial_mock.add_watch.assert_called_with(mock_path)

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.'
           'setup_command_argument_parser')
    def test_get_settings_from_command_arguments(self, mock_setup_argument_parser):
        argument_parser = MagicMock()
        mock_setup_argument_parser.return_value = argument_parser
        argument_parser.parse_args = MagicMock()

        get_settings_from_command_arguments([])

        mock_setup_argument_parser.assert_called()
        argument_parser.parse_args.assert_called()

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.'
           'setup_logging_framework')
    def test_apply_global_settings_set_verbose_logging(self, mock_setup_logging_framework):
        partial_mock_settings = MagicMock()
        partial_mock_settings.v = True

        apply_global_settings(partial_mock_settings)

        mock_setup_logging_framework.assert_called_with(logging.DEBUG)

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.'
           'setup_logging_framework')
    def test_apply_global_settings_do_not_set_verbose_logging(self, mock_setup_logging_framework):
        partial_mock_settings = MagicMock()
        partial_mock_settings.v = False

        apply_global_settings(partial_mock_settings)

        mock_setup_logging_framework.assert_not_called()

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.'
           'main_wait_event_loop')
    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.'
           'instantiate_file_creation_event_observer')
    def test_station_tests_watchdog(self,
                                    mock_instantiate_file_creation_event_observer,
                                    mock_main_wait_event_loop):
        mock_arguments = [self.mock_path, self.mock_filename, '--addr', self.mock_address]

        mock_observer = MagicMock()
        mock_instantiate_file_creation_event_observer.return_value = mock_observer

        station_tests_watchdog(mock_arguments)

        mock_instantiate_file_creation_event_observer.assert_called_with(self.mock_path)
        mock_main_wait_event_loop.assert_called_with(mock_observer, self.mock_filename,
                                                     self.mock_address, MAIN_LOOP_WAIT_TIME)

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.'
           'main_wait_event_loop')
    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.'
           'instantiate_file_creation_event_observer')
    def test_parsing_command_arguments_correctly(self,
                                                 mock_instantiate_file_creation_event_observer,
                                                 mock_main_wait_event_loop):
        mock_arguments = [self.mock_path, self.mock_filename, '--addr', self.mock_address]

        mock_observer = MagicMock()
        mock_instantiate_file_creation_event_observer.return_value = mock_observer

        station_tests_watchdog(mock_arguments)

        mock_instantiate_file_creation_event_observer.assert_called_with(self.mock_path)
        mock_main_wait_event_loop.assert_called_with(mock_observer, self.mock_filename,
                                                     self.mock_address, MAIN_LOOP_WAIT_TIME)

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.'
           'sys')
    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.'
           'logging')
    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.'
           'main_wait_event_loop')
    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.'
           'instantiate_file_creation_event_observer')
    def test_default_logging_level_info(self,
                                        mock_instantiate_file_creation_event_observer,
                                        mock_main_wait_event_loop,
                                        mock_logging,
                                        partial_mock_sys):
        mock_arguments = [self.mock_path, self.mock_filename, '--addr', self.mock_address]
        partial_mock_sys.argv = ['program_filename'] + mock_arguments
        mock_observer = MagicMock()
        mock_instantiate_file_creation_event_observer.return_value = mock_observer

        main()

        mock_instantiate_file_creation_event_observer.assert_called_with(self.mock_path)
        mock_main_wait_event_loop.assert_called_with(mock_observer, self.mock_filename,
                                                     self.mock_address, MAIN_LOOP_WAIT_TIME)

        mock_logging.basicConfig.assert_called_once_with(
            format="%(asctime)s %(levelname)s: %(message)s",
            level=logging.INFO)


if __name__ == '__main__':
    setup_logging_framework(logging.DEBUG)
    unittest.main()
