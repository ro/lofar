import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, ButtonGroup} from 'reactstrap';
import {setDateRange} from "redux/actions/mainFiltersActions";

import DatePicker from 'react-datepicker';
import moment from 'moment';

// CSS
import 'react-datepicker/dist/react-datepicker.css';
import toolbarStyles from '../styles.module.scss'
import './styles.module.scss'


/**
 * Class to display a secondary header for selecting data filters.
 * The state is managed by the LandingPage class.
 */
class DateRangeSelectorC extends Component {

    handleChange(obj) {
        var startDate = obj.startDate,
            endDate = obj.endDate;

        startDate = startDate || this.props.startDate;
        endDate = endDate || this.props.endDate;

        if (startDate.isAfter(endDate)) {
          endDate = startDate;
        }

        this.props.setDateRange({
            startDate: startDate,
            endDate: endDate
        });
    };

    handleChangeStart = (startDate) => {
        return this.handleChange({
            startDate: startDate
        });
    };

    handleChangeEnd = (endDate) => {
        return this.handleChange({
            endDate: endDate
        });
    };

    onPeriodClick = (i) => {
        this.handleChange({
            startDate: moment().subtract(i, 'days'),
            endDate: moment()
        });

    }

    periodIsActive(ndays) {
        let now = moment().format("YYYY-MMM-DD"),
            past = moment().subtract(ndays, 'days').format("YYYY-MMM-DD");
        if (this.props.endDate.format("YYYY-MMM-DD") === now && this.props.startDate.format("YYYY-MMM-DD") === past) {
            return true;
        }
        return false;
    }

    render() {
        return (<div className={toolbarStyles.toolbar_ctrl}>
            <label>Period</label>
            <ButtonGroup size="sm">
                <Button color="info"
                        onClick={() => this.onPeriodClick(7)}
                        active={this.periodIsActive(7)}>
                    1 wk
                </Button>
                <Button color="info"
                        onClick={() => this.onPeriodClick(14)}
                        active={this.periodIsActive(14)}>
                    2 wk
                </Button>
                <Button color="info"
                        onClick={() => this.onPeriodClick(28)}
                        active={this.periodIsActive(28)}>
                    4 wk
                </Button>
            </ButtonGroup>
            <DatePicker
                selected={this.props.startDate}
                selectsStart
                dateFormat="YYYY-MMM-DD"
                className='form-control form-control-sm'
                startDate={this.props.startDate}
                endDate={this.props.endDate}
                onChange={this.handleChangeStart}
            />
            <DatePicker
                selected={this.props.endDate}
                selectsEnd
                dateFormat="YYYY-MMM-DD"
                className='form-control form-control-sm'
                startDate={this.props.startDate}
                endDate={this.props.endDate}
                onChange={this.handleChangeEnd}
            />
        </div>);
    }
}

const mapStateDateRangeSelector = state => {
    return {
        startDate: state.mainFilters.startDate,
        endDate: state.mainFilters.endDate
    };
};

const mapDispatchDateRangeSelector = {
    setDateRange
};

export const DateRangeSelector = connect(
    mapStateDateRangeSelector,
    mapDispatchDateRangeSelector
)(DateRangeSelectorC);
