import time
import subprocess
import os
import signal
import datetime

from TBBsetup import *

if "HOSTNAME" not in list(os.environ.keys()):
    from socket import gethostname
    os.environ["HOSTNAME"]=gethostname()

def stopdatawriter(proclist):
    for p in proclist:
        p[2].terminate()
        print("terminating ",p)
    for p in proclist:
        p[2].wait()


def stopTBBraw2h5(lsenode,username=os.environ["USER"]):
    proc=subprocess.Popen(['ps','-u'+username,'-f'],stdout=subprocess.PIPE)
    a=proc.stdout.readlines()
    print("Processes running")
    for line in a:
        print(line)
        if "TBBraw2h5" in line:
            if "ssh "+lsenode in line or lsenode=="this":
                #"/opt/usg/daily/dal/" in line:
                os.kill(int(line.split()[1]),signal.SIGKILL)
                print("Last process killed")


def startTBBraw2h5(lsenode,filename,timeouttime=3,datamaindir='/data3/TBB/',datawriter=datawriter,logdir=logdir,project="UNDEFINED",observer="UNDEFINED",observationID="UNDEFINED",filterSelection="UNDEFINED",antennaSet="UNDEFINED"):
    print("Starting datawriter ")
    if lsenode != "this":
        print("lsenode not local")
        proc=subprocess.Popen(['ssh',lsenode,'mkdir','-p',datamaindir+filename],stdout=subprocess.PIPE)
        proc.wait()

        proc=subprocess.Popen(['ssh',lsenode,'mkdir','-p',logdir],stdout=subprocess.PIPE)
        proc.wait()

        proc=subprocess.Popen(['ssh',lsenode,'ls',datamaindir+filename],stdout=subprocess.PIPE)
        proc.wait()
        dirlist=proc.stdout.readlines()
        print(dirlist)
        nextrun=False
        runnr=0
        #for f0 in dirlist:
        #     if filename in f0:
        #         nextrun=True
        #runlist=[int(d[3:]) for d in dirlist if 'run' in d]
        #if len(runlist) > 0:
        #     runnr=max(runlist)+1

        print(nextrun)
        if nextrun:
            TBBlogfile=open(logdir+filename+'_'+str(runnr)+'.log','w')
            proc=subprocess.Popen(['ssh',lsenode,'mkdir','-p',datamaindir+filename+'/run'+str(runnr)],stdout=subprocess.PIPE)
            proc.wait()
            print("Multiple station support disabled in this library")
            proc=subprocess.Popen(['ssh',lsenode,datawriter,'-P31664','-P31665','-P31666','-P31667','-P31668','-P31669','-P31670','-P31671','-P31672','-P31673','-P31674','-P31675','-K','-V','-M','-R'+str(timeouttime),'-O'+datamaindir+filename+'/run'+str(runnr)+'/'+filename,'--observer',observer,'--project',project,'--observationID',observationID,'--filterSelection',filterSelection,'--antennaSet',antennaSet],stdout=TBBlogfile)
        else:
            TBBlogfile=open(logdir+"TBBraw2h5_"+os.environ["HOSTNAME"]+"_"+filename+'.log','w')
            proc=subprocess.Popen(['ssh',lsenode,datawriter,'-P31664','-P31665','-P31666','-P31667','-P31668','-P31669','-P31670','-P31671','-P31672','-P31673','-P31674','-P31675','-K','-V','-M','-R'+str(timeouttime),'-O'+datamaindir+filename+'/'+filename,'--observer',observer,'--project',project,'--observationID',observationID,'--filterSelection',filterSelection,'--antennaSet',antennaSet],stdout=TBBlogfile)
        return TBBlogfile,proc.pid,proc
    else:
        print("starting making file dir")
        proc=subprocess.Popen(['mkdir','-p',datamaindir+filename],stdout=subprocess.PIPE)
        proc.wait()
        print("starting making log  dir")

        proc=subprocess.Popen(['mkdir','-p',logdir],stdout=subprocess.PIPE)
        proc.wait()


        #proc=subprocess.Popen(['ls',datamaindir+filename],stdout=subprocess.PIPE)
        #proc.wait()
        #dirlist=proc.stdout.readlines()
        #print dirlist
        nextrun=False
        runnr=0
        #for f0 in dirlist:
        #     if filename in f0:
        #         nextrun=True
        #runlist=[int(d[3:]) for d in dirlist if 'run' in d]
        #if len(runlist) > 0:
        #     runnr=max(runlist)+1

        print(nextrun)
        if nextrun:
            print("Using next run")
            TBBlogfile=open(logdir+'TBBraw2h5_'+filename+'_'+str(runnr)+'.log','w')
            proc=subprocess.Popen(['mkdir','-p',datamaindir+filename+'/run'+str(runnr)],stdout=subprocess.PIPE)
            proc.wait()
            proc=subprocess.Popen([datawriter,'-P31664','-P31665','-P31666','-P31667','-P31668','-P31669','-P31670','-P31671','-P31672','-P31673','-P31674','-P31675','-K','-V','-M','-R'+str(timeouttime),'-O'+datamaindir+filename+'/run'+str(runnr)+'/'+filename,'--observer',observer,'--project',project,'--observationID',observationID,'--filterSelection',filterSelection,'--antennaSet',antennaSet],stdout=TBBlogfile)
        else:
            hostname=os.environ["HOSTNAME"].replace('.cep4.control.lofar','')
            print("Hostname =",hostname)
            timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%dT%H%M%S')
            TBBlogfile=open(logdir+'TBBraw2h5_'+hostname+"_"+timestamp+'.log','w')
            proc=subprocess.Popen([datawriter,'-P31664','-P31665','-P31666','-P31667','-P31668','-P31669','-P31670','-P31671','-P31672','-P31673','-P31674','-P31675','-K','-V','-M','-R'+str(timeouttime),'-O'+datamaindir+filename+'/'+filename,'--observer',observer,'--project',project,'--observationID',observationID,'--filterSelection',filterSelection,'--antennaSet',antennaSet],stdout=TBBlogfile)
    return TBBlogfile,proc.pid,proc

def startdatawriter(filename,timeouttime=timeouttime,project="UNDEFINED",observer="UNDEFINED",observationID="UNDEFINED",filterSelection="UNDEFINED",antennaSet="UNDEFINED",datamaindir=datadir):
    if filename == "date" :
        filename=time.strftime('TBBdata-%Y-%m-%d')
    print(datawriter)
    proc = startTBBraw2h5(lsenode=storageip,filename=filename,timeouttime=timeouttime,datamaindir=datamaindir,datawriter=datawriter,logdir=logdir,project=project,observer=observer,observationID=observationID,filterSelection=filterSelection,antennaSet=antennaSet)
    return [proc]
