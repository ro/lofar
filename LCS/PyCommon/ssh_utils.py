# Copyright (C) 2012-2015  ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

import logging
logger = logging.getLogger(__name__)

def ssh_cmd_list(host, user='lofarsys', disable_strict_host_key_checking: bool=False, disable_remote_pseudo_terminal: bool=False):
    '''
    returns a subprocess compliant command list to do an ssh call to the given node
    :param host: the node name or ip address
    :param user: optional username, defaults to 'lofarsys'
    :param strict_host_key_checking: uses ssh option -o StrictHostKeyChecking=no to prevent prompts about host keys
    :param disable_remote_pseudo_terminal: uses ssh option -T to disable remote pseudo terminal
    :return: a subprocess compliant command list
    '''
    cmd_list = ['ssh']
    if disable_remote_pseudo_terminal:
        cmd_list.append('-T')
    if disable_strict_host_key_checking:
        cmd_list.append('-o StrictHostKeyChecking=no')
    cmd_list.append(user+'@'+host)
    return cmd_list


