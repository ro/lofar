import logging
import sys
import os
import re
from argparse import ArgumentParser

from lofar.maintenance.mdb import client as mdb_client
import inotify.adapters
logger = logging.getLogger('station_tests_watchdog')

MAIN_LOOP_WAIT_TIME = 1


def setup_logging_framework(logging_level=logging.INFO):
    """
    Setup the logging style for a desired logging_level
    :param logging_level: logging level lower of which the messages will be filtered out
    """
    logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s",
                        level=logging_level)


def station_tests_watchdog(args):
    """
    Main function
    :param args: command line arguments
    """
    settings = get_settings_from_command_arguments(args)
    apply_global_settings(settings)
    observer = instantiate_file_creation_event_observer(settings.path)
    main_wait_event_loop(observer, settings.expected_filename, settings.addr, MAIN_LOOP_WAIT_TIME)


def get_settings_from_command_arguments(args):
    """
    Get the settings from the the command arguments
    :param args: command line arguments
    :return: the station_test_watchdog settings
    """
    argument_parser = setup_command_argument_parser()
    settings = argument_parser.parse_args(args)
    return settings


def setup_command_argument_parser():
    """
    Setup the command arguments parser with the station_test_watchdog different options
    and returns it
    :return: ArgumentParser with the station_test_watchdog options
    """
    parser = ArgumentParser(description="Station tests watchdog listens for new file events on a"
                                        "specified directory and if the file is a stationtest"
                                        "or a RTSM the file is loaded to the MaintenanceDB")
    parser.add_argument('path', help="file system path to monitor for new file events")
    parser.add_argument('expected_filename', help="Considers events only on file with a name"
                                                  "that matches the specified regular expression")

    parser.add_argument('--addr', help='MaintenanceDB http address',
                        default='lofarmonitortest.control.lofar')

    parser.add_argument('-v', help='verbose logging', action='store_false', default=False)

    return parser


def apply_global_settings(settings):
    """
    Apply the settings to the global variables (e.g. the logging level)
    :param settings: command line argument settings
    :return:
    """
    if settings.v:
        setup_logging_framework(logging.DEBUG)


def instantiate_file_creation_event_observer(path):
    """
    Create an observer object that listens to creation events of station test
    and RTSM in a specific dir and sends the content of these files to the REST api of
    the MaintenanceDB at the given address
    :param path: the dir path to watch for events
    :return: the observer object
    """
    inotify_adapter = inotify.adapters.Inotify()
    inotify_adapter.add_watch(path)
    return inotify_adapter


def main_wait_event_loop(observer,
                         expected_filename,
                         mdb_address,
                         active_watch_time,
                         run_once=False):
    """
    Wait for any exception raised
    if it receives a keyboard interrupt it stops the listener
    :param observer: inotify interface observer
    :param expected_filename: regex that describes the expected filename
    :param mdb_address: url of the MaintenanceDB REST API
    :param active_watch_time: time of active watching of inotify events
    :param run_once: watch for inotify events only once and timeout after
     active_watch_time
    """
    while True:
        try:
            events = observer.event_gen(yield_nones=False, timeout_s=active_watch_time)
            for event in events:
                handle_inotify_event_create_file(event, expected_filename, mdb_address)
            if run_once:
                break
        except KeyboardInterrupt:
            logger.info("Keyboard interrupt. Exiting main listener")
            return True
        except Exception as e:
            logger.info('Unexpected exception occurred: %s. \n Exiting main listener', e)


def handle_inotify_event_create_file(event, expected_filename, mdb_address):
        """
        On file creation event handler
        :param mdb_address: url of the MaintenanceDB REST API
        :param event: the list that describes the creation event
        """
        if _is_file_created(event) and _does_filename_matches_specified_regex(expected_filename, event):
            logger.debug('File created handling event %s', event)

            handle_event_file_created(mdb_address, event)
        else:
            logger.debug("Neglecting non file creation event : %s", event)


def _does_filename_matches_specified_regex(regex, event):
    """
    Checks whether the file name of the event matched the specified regex
    :param regex: desired regex
    :param event: inotify event
    :return: true if the file name matches the regex
    """
    return bool(re.match(regex, event[3]))


def _is_file_created(event):
    """
    Checks if the creation event refers to a file
    :param event: the list that describes the creation event
    :return: True if it refers to a file being created False otherwise
    """
    return 'IN_CLOSE_WRITE' in event[1] or 'IN_MOVED_TO' in event[1] or 'IN_CREATE' in event[1]


def handle_event_file_created(mdb_address, event):
    """
    Handle the file created event sending the file to the MaintenanceDB REST API.
    If the file content doesnt match a RTSM or a station tests, it logs the event and skips
    the insertion.

    :param mdb_address: url of the MaintenanceDB REST API
    :param event: inotify event list description
    """
    (_, type_name, path, filename) = event
    file_path = os.path.join(path, filename)

    try:
        file_content = mdb_client.read_stationtest_rtsm_output_to_dict(file_path)
        mdb_client.send_stationtest_rtsm_content_to_address(mdb_address, file_content)
    except ValueError:
        logger.info('file %s is neither a station test nor a RTSM output. skipping insertion',
                    file_path)
    except Exception as e:
        logger.error('error processing file %s: %s', file_path, e)


def main():
    setup_logging_framework()
    station_tests_watchdog(sys.argv[1:])


if __name__ == '__main__':
    main()