#!/usr/bin/env python3

import logging
import time
import unittest
import uuid
import os.path
import shutil
from unittest.mock import patch
from lofar.common.test_utils import integration_test

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

from lofar.common.test_utils import exit_with_skipped_code_if_skip_integration_tests
exit_with_skipped_code_if_skip_integration_tests()

from lofar.messaging import TemporaryExchange
from lofar.messaging.messagelogger import MessageLogger

testname = 't_ingestpipeline_%s' % uuid.uuid1()


# patch (mock) the LTAClient class during these tests.
# when the ingestpipeline instantiates an LTAClient it will get the mocked class.
with patch('lofar.lta.ingest.server.ltaclient.LTAClient', autospec=True) as MockLTAClient:
    ltamock = MockLTAClient.return_value

    # patch (mock) the convert_surl_to_turl method during these tests.
    with patch('lofar.lta.ingest.common.srm.convert_surl_to_turl') as mock_convert_surl_to_turl:
        mock_convert_surl_to_turl.side_effect = lambda surl: surl.replace('srm', 'gsiftp')

        from lofar.lta.ingest.common.job import createJobXml, parseJobXml
        from lofar.lta.ingest.server.ltaclient import LTAClient # <-- thanks to magick mock, we get the mocked ltaclient
        from lofar.lta.ingest.server.ingestpipeline import *
        from lofar.lta.ingest.test import ltastubs

        class TestIngestPipeline(unittest.TestCase):
            def setUp(self):
                self.test_dir_path = None

                self.tmp_exchange = TemporaryExchange(testname)
                self.tmp_exchange.open()

                # hook a MessageLogger to the bus, so we can read in the logs what's send around
                self.message_logger = MessageLogger(exchange=self.tmp_exchange.address)
                self.message_logger.start_listening()

                ltastubs.stub()
                self.ltaclient = LTAClient()

            def tearDown(self):
                ltastubs.un_stub()
                self.message_logger.stop_listening()
                self.tmp_exchange.close()

                if self.test_dir_path and os.path.exists(self.test_dir_path):
                    logger.info("removing test dir: %s", self.test_dir_path)
                    shutil.rmtree(self.test_dir_path, True)


            @integration_test
            def test_directory_with_TMSS(self):
                try:
                    from lofar.sas.tmss.test.test_environment import TMSSTestEnvironment
                except (ImportError, ModuleNotFoundError):
                    raise unittest.SkipTest("Cannot run test because the TMSSTestEnvironment cannot be imported. Did you run cmake with BUILD_PACKAGES for both LTAIngest and TMSS?")

                try:
                    from lofar.messaging.messagebus import TemporaryExchange

                    # create TMSSTestEnvironment with a running ingest_tmss_adapter
                    # assume the ingest_tmss_adapter works correctly. It is tested in t_ingesttmssadapter.
                    with TMSSTestEnvironment(exchange=self.tmp_exchange.address, populate_schemas=True) as tmss_test_env:
                        try:
                            from lofar.lta.ingest.server.ingest_tmss_adapter import IngestTMSSAdapter
                        except ModuleNotFoundError:
                            raise unittest.SkipTest("Cannot run test because the IngestTMSSAdapter cannot be imported. Did you run cmake with BUILD_PACKAGES for both LTAIngest and TMSS?")

                        with IngestTMSSAdapter(exchange=self.tmp_exchange.address, broker=self.tmp_exchange.broker):
                            from lofar.sas.tmss.test.tmss_test_data_django_models import SubtaskTemplate_test_data, Subtask_test_data, \
                            TaskBlueprint_test_data, TaskTemplate_test_data, Dataproduct_test_data, \
                            SubtaskOutput_test_data, SubtaskInput_test_data
                            from lofar.sas.tmss.tmss.tmssapp import models
                            from lofar.common.json_utils import get_default_json_object_for_schema
                            from lofar.sas.tmss.test.test_utils import set_subtask_state_following_allowed_transitions

                            ####################################################
                            # setup: create observation and link an ingest to it.
                            ####################################################

                            obs_task_template = models.TaskTemplate.objects.create(**TaskTemplate_test_data(task_type_value='observation'))
                            obs_task = models.TaskBlueprint.objects.create(**TaskBlueprint_test_data(specifications_template=obs_task_template))
                            obs_subtask_template = models.SubtaskTemplate.objects.get(name='observation control')
                            obs_subtask = models.Subtask.objects.create(**Subtask_test_data(subtask_template=obs_subtask_template, task_blueprint=obs_task))
                            obs_subtask_output = models.SubtaskOutput.objects.create(**SubtaskOutput_test_data(subtask=obs_subtask))

                            # Create SAP
                            sap_template = models.SAPTemplate.objects.get(name="SAP")
                            specifications_doc = sap_template.get_default_json_document_for_schema()
                            sap = models.SAP.objects.create(specifications_doc=specifications_doc, specifications_template=sap_template)
                            sap.save()

                            feedback_template = models.DataproductFeedbackTemplate.objects.get(name='feedback')
                            feedback_doc = {'percentage_written': 100, 'frequency': {'subbands': [156], 'station_subbands': [42], 'central_frequencies': [33593750.0], 'channel_width': 6103.515625, 'channels_per_subband': 32}, 'time': {'start_time': '2013-02-16T17:00:00', 'duration': 5.02732992172, 'sample_width': 2.00278016}, 'antennas': {'set': 'HBA_DUAL', 'fields': [{'type': 'HBA', 'field': 'HBA0', 'station': 'CS001'}, {'type': 'HBA', 'field': 'HBA1', 'station': 'CS001'}]}, 'target': {'pointing': {'angle1': 0, 'angle2': 0, 'direction_type': 'J2000', "target": "target1"}}, 'samples': {'polarisations': ['XX', 'XY', 'YX', 'YY'], 'type': 'float', 'bits': 32, 'writer': 'standard', 'writer_version': '2.2.0', 'complex': True}}
                            feedback_doc = feedback_template.add_defaults_to_json_object_for_schema(feedback_doc)
                            obs_dataproduct = models.Dataproduct.objects.create(**Dataproduct_test_data(producer=obs_subtask_output, sap=sap, feedback_template=feedback_template, feedback_doc=feedback_doc))

                            ingest_task_template = models.TaskTemplate.objects.create(**TaskTemplate_test_data(task_type_value='ingest'))
                            ingest_task = models.TaskBlueprint.objects.create(**TaskBlueprint_test_data(scheduling_unit_blueprint=obs_subtask.task_blueprint.scheduling_unit_blueprint,
                                                                                                        specifications_template=ingest_task_template))
                            ingest_subtask_template = models.SubtaskTemplate.objects.create(**SubtaskTemplate_test_data(subtask_type_value='ingest'))
                            ingest_subtask = models.Subtask.objects.create(**Subtask_test_data(subtask_template=ingest_subtask_template, task_blueprint=ingest_task))
                            ingest_subtask_input = models.SubtaskInput.objects.create(**SubtaskInput_test_data(subtask=ingest_subtask, producer=obs_subtask_output))
                            ingest_subtask_input.dataproducts.set(models.Dataproduct.objects.filter(producer=obs_subtask_output).all())
                            ingest_subtask_input.save()

                            ingest_subtask_output = models.SubtaskOutput.objects.create(**SubtaskOutput_test_data(subtask=ingest_subtask))
                            ingest_output_dataproduct = models.Dataproduct.objects.create(**Dataproduct_test_data(producer=ingest_subtask_output))

                            models.DataproductTransform.objects.create(input=obs_dataproduct, output=ingest_output_dataproduct, identity=True)

                            # mimic running the observation
                            set_subtask_state_following_allowed_transitions(obs_subtask, models.SubtaskState.Choices.FINISHED.value)

                            ####################################################
                            # end of object setup
                            ####################################################

                            project_name = ingest_task.draft.scheduling_unit_draft.scheduling_set.project.name
                            obs_id = obs_subtask.id
                            dpname = 'L%s_SAP000_SB000_uv.MS' % obs_id
                            self.test_dir_path = os.path.join(os.getcwd(), 'testdir_%s' % uuid.uuid1(), dpname)

                            def stub_GetStorageTicket(project, filename, filesize, archive_id, job_id, obs_id, check_mom_id=True, id_source='TMSS'):
                                return { 'primary_uri_rnd': 'srm://some.site.name:8443/some/path/data/lofar/ops/projects/%s/%s/%s.tar' % (project, obs_id, dpname),
                                         'result': 'ok',
                                         'error': '',
                                         'ticket': '3E0A47ED860D6339E053B316A9C3BEE2'}
                            ltamock.GetStorageTicket.side_effect = stub_GetStorageTicket

                            os.makedirs(self.test_dir_path)
                            test_file_paths = []
                            for i in range(10):
                                test_file_path = os.path.join(self.test_dir_path, 'testfile_%s.txt' % i)
                                test_file_paths.append(test_file_path)
                                with open(test_file_path, 'w') as file:
                                    file.write(1000*'a')

                            job_xml = createJobXml(testname, obs_id, dpname, obs_dataproduct.global_identifier.unique_identifier,
                                                   'localhost:%s' % self.test_dir_path,
                                                   tmss_ingest_subtask_id=ingest_subtask.id, tmss_input_dataproduct_id=obs_dataproduct.id)
                            logger.info('job xml: %s', job_xml)
                            job = parseJobXml(job_xml)

                            pl = IngestPipeline(job, ltaClient=self.ltaclient, exchange=self.tmp_exchange.address)
                            pl.run()

                            # check SIP
                            with tmss_test_env.create_tmss_client() as tmss_client:
                                SIP = tmss_client.get_dataproduct_SIP(ingest_output_dataproduct.id, retry_count=10)
                                self.assertTrue("<storageTicket>3E0A47ED860D6339E053B316A9C3BEE2</storageTicket>" in SIP)

                            # check archive info
                            ingest_output_dataproduct.refresh_from_db()
                            self.assertEqual("3E0A47ED860D6339E053B316A9C3BEE2", ingest_output_dataproduct.archive_info.storage_ticket)
                except Exception as e:
                    self.assertTrue(False, 'Unexpected exception in pipeline: %s' % e)
                finally:
                    # the 'stub-transfered' file ended up in out local stub lta
                    # with the path: ltastubs._local_globus_file_path
                    #check extension
                    self.assertTrue('.tar' == os.path.splitext(ltastubs._local_globus_file_path)[-1])

                    #check tar contents
                    tar = subprocess.Popen(['tar', '--list', '-f', ltastubs._local_globus_file_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    tar_file_list, err = tuple(x.decode('ascii') for x in tar.communicate())
                    self.assertEqual(tar.returncode, 0)
                    logger.info('file list in tar:\n%s', tar_file_list)

                    for test_file_path in test_file_paths:
                        self.assertTrue(os.path.basename(test_file_path) in tar_file_list)
                    logger.info('all expected source files are in tar!')

                    for f in os.listdir(self.test_dir_path):
                        os.remove(os.path.join(self.test_dir_path, f))
                    os.removedirs(self.test_dir_path)



        if __name__ == '__main__':
            logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                                level=logging.DEBUG)
            unittest.main()

