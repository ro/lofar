import random
from datetime import datetime

from lofar.maintenance.monitoringdb.models.component import Component
from lofar.maintenance.monitoringdb.models.component_error import ComponentError
from lofar.maintenance.monitoringdb.models.rtsm import RTSMObservation, RTSMError
from lofar.maintenance.monitoringdb.models.station import Station
from lofar.maintenance.monitoringdb.models.station_test import StationTest
from lofar.maintenance.monitoringdb.serializers.rtsm import RTSMObservationSerializer

TEST_STATION_NAMES = ('CS001', 'CS002')
TEST_STATION_TYPE = ('C', 'R', 'I')

TEST_COMPONENT_IDS = [i for i in range(48)]
TEST_COMPONENT_ERROR_TYPES = ('HIGH_NOISE', 'LOW_NOISE', 'DOWN')

TEST_COMPONENT_TYPES = ('HBA', 'LBH')
TEST_MODES = (1, 2, 3, 4, 5, 6, 7)

TEST_START_DATETIME = datetime(2018, 5, 12, 0, 0, 0)
TEST_END_DATETIME = datetime(2018, 5, 12, 0, 0, 0)


def random_date_in_range(start, end):
    """
    Computes a random datetime between the specified interval
    :param start: start date of the interval
    :type start: datetime
    :param end: end date of the interval
    :type end: datetime
    :return: a random datetime in the specified interval
    :rtype: datetime
    """
    time = random.uniform(start.timestamp(), end.timestamp())
    return datetime.fromtimestamp(time)


def generate_test_component(sample_size, station, expected_component_ids=TEST_COMPONENT_IDS,
                            expected_component_types=TEST_COMPONENT_TYPES):
    """
    Generate a sample of [sample_size] components in a given station
    :param expected_component_ids: the component ids that have to be used for the generation
    :type expected_component_ids: tuple(int)
    :param expected_component_types: the component types that have to be used for the generation
    :type expected_component_types: tuple(str)
    :param sample_size: sample size
    :type sample_size: int
    :param station: generated components' station
    :type station: Station
    :return: a tuple of component entities
    :rtype: tuple(Component)
    """
    component_list = []
    for i in range(sample_size):
        test_component = Component()
        test_component.component_id = random.choice(expected_component_ids)
        test_component.station = station
        test_component.type = random.choice(expected_component_types)
        test_component.save()
        component_list.append(test_component)
    return tuple(component_list)


def generate_test_component_errors(sample_size, station_test,
                                   expected_component_error_types=TEST_COMPONENT_ERROR_TYPES,
                                   expected_component_types=TEST_COMPONENT_TYPES):
    """
    Generate a sample of [sample size] component errors found during a station test
    :param expected_component_types: the component types to be used for the generation
    :param expected_component_error_types: the error types to be used for the generation
    :type expected_component_error_types: tuple(str)
    :param sample_size: size of the sample
    :type sample_size: int
    :param station_test: component errors' station test
    :type station_test: StationTest
    :return: a tuple of component error instances
    """
    component_error_list = []
    for i in range(sample_size):
        component_entity, = generate_test_component(1, station_test.station,
                                                    expected_component_types=
                                                    expected_component_types)
        component_error = ComponentError()
        component_error.station_test = station_test
        component_error.component = component_entity
        component_error.type = random.choice(expected_component_error_types)
        component_error.save()
        component_error_list.append(component_error)
    return tuple(component_error_list)


def generate_test_station(sample_size):
    """
    Generate a sample of station to be used for testing
    :param sample_size: size of the sample
    :type sample_size: int

    :return: a tuple of stations
    :rtype: tuple(Station)
    """
    station_list = []
    for i in range(sample_size):
        station = Station()
        station.name = random.choice(TEST_STATION_NAMES)
        station.type = random.choice(TEST_STATION_TYPE)
        station.location = ''
        station.save()
        station_list.append(station)
    return station_list


def generate_test_station_test(sample_size, station, start_datetime=TEST_START_DATETIME,
                               end_datetime=TEST_END_DATETIME):
    """
    Generate a sample of station to be used for testing
    :param sample_size: size of the sample
    :type sample_size: int
    :param station: station test's station
    :type station: Station
    :param start_datetime: end date and time of the time interval in which the station test will be
     generated
    :type start_datetime: datetime
    :param end_datetime: end date and time of the time interval in which the station test will be
     generated
    :return: a tuple of station tests
    :rtype: tuple(StationTest)
    """
    station_test_list = []
    for i in range(sample_size):
        station_test = StationTest()
        station_test.station = station
        station_test.start_datetime = random_date_in_range(start_datetime, end_datetime)
        station_test.end_datetime = random_date_in_range(station_test.start_datetime, end_datetime)
        station_test.save()
        station_test_list.append(station_test)
    return tuple(station_test_list)


def generate_test_rtsm_error(sample_size, rtsm_observation, expected_rcu_ids=TEST_COMPONENT_IDS,
                             expected_error_types=TEST_COMPONENT_ERROR_TYPES,
                             expected_modes=TEST_MODES,
                             start_datetime=TEST_START_DATETIME,
                             end_datetime=TEST_END_DATETIME):
    """

    :param sample_size:
    :param rtsm_observation:
    :type rtsm_observation: RTSMObservation
    :param expected_rcu_ids:
    :param expected_error_types:
    :param expected_modes:
    :param start_datetime:
    :param end_datetime:
    :return:
    """
    for i in range(sample_size):
        rtsm_error = RTSMError()
        rtsm_error.observation = rtsm_observation
        rtsm_error.mode = random.choice(expected_modes)
        rtsm_error.rcu = random.choice(expected_rcu_ids)
        rtsm_error.start_frequency = 100
        rtsm_error.stop_frequency = 200
        rtsm_error.time = random_date_in_range(start_datetime, end_datetime)
        rtsm_error.error_type = random.choice(expected_error_types)
        rtsm_error.save()
        RTSMObservationSerializer.compute_summary(rtsm_observation)


def generate_test_rtsm_observation(sample_size, station, start_date=datetime(2018, 5, 12, 0, 0, 0),
                                   end_date=datetime(2018, 5, 12, 1, 0, 0)):
    """
    Generate a sample of RTSMObservation objects
    :param sample_size: size of the sample
    :type sample_size: int
    :param station: RTSM Observation's station
    :type station: Station
    :param start_date: end date and time of the time interval in which the station test will be
     generated
    :type start_date: datetime
    :param end_date: end date and time of the time interval in which the station test will be
    generated
    :return: a tuple of RTSM Observations
    :rtype: tuple(RTSMObservation)
    """
    rtsm_observation_list = []
    for i in range(sample_size):
        rtsm_observation = RTSMObservation()
        rtsm_observation.station = station
        rtsm_observation.observation_id = random.uniform(6000, 7000)
        rtsm_observation.start_datetime = random_date_in_range(start_date, end_date)
        rtsm_observation.end_datetime = random_date_in_range(rtsm_observation.start_datetime,
                                                             end_date)
        rtsm_observation.save()
        rtsm_observation_list.append(rtsm_observation)
    return rtsm_observation_list


def generate_random_test_data(number_of_station,
                              number_of_station_tests,
                              number_of_component_errors,
                              number_of_observations,
                              number_of_rtsm_errors,
                              expected_error_types=TEST_COMPONENT_ERROR_TYPES,
                              expected_component_types=TEST_COMPONENT_TYPES,
                              expected_modes=TEST_MODES):
    """
    Generates and insert in the test database the data to test
    the view response
    """
    stations = generate_test_station(number_of_station)
    for station in stations:
        station_tests = generate_test_station_test(number_of_station_tests, station)
        for station_test in station_tests:
            generate_test_component_errors(number_of_component_errors, station_test,
                                           expected_component_error_types=expected_error_types,
                                           expected_component_types=expected_component_types)
        rtsm_observations = generate_test_rtsm_observation(number_of_observations, station)
        for rtsm_observation in rtsm_observations:
            generate_test_rtsm_error(number_of_rtsm_errors,
                                     rtsm_observation,
                                     expected_error_types=expected_error_types,
                                     expected_modes=expected_modes)
