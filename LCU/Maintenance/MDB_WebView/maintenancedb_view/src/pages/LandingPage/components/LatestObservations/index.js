import React, { Component } from 'react';
import { Table } from 'reactstrap';
import AutoLoadWrapper from 'components/AutoLoadWrapper'

import ObsRow from './ObsRow'

// CSS
import styles from './styles.module.scss'


/**
 * TableRows: function component returning the table rows.
 * LatestObservations class.
 * Component showing a table with the latest observations and number of errors.
 */
function TableRows({data}) {
    return data.map( (stationData) =>
        <ObsRow key={stationData.observation_id} data={ stationData } />
    );
}


/**
 * LatestObservations class.
 * Component showing a table with the latest observations and number of errors.
 */
class LatestObservationsC extends Component {

    // Do not (re)render when data is loading (performance improvement)
    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.isLoading) {
            return false;
        }
        return true;
    }

    render() {
        return (
            <div className={styles.latest_obs_ctrl}>
                <Table size="sm" className="so-table">
                    <thead>
                      <tr>
                        <th>Observation</th>
                        <th>Start date</th>
                        <th>Stations</th>
                        <th>Stations with errors</th>
                        <th>Total errors</th>
                      </tr>
                    </thead>
                    <tbody>
                        <TableRows data={this.props.data} />
                    </tbody>
                </Table>
            </div>
        );
    }

}

/* Add some magic; use the AutoLoadWrapper to create a HOC that handles the
   auto-loading of the data for StationOverviewC.
 */
const LatestObservations = AutoLoadWrapper(LatestObservationsC);

export default LatestObservations;
