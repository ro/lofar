# service
lofar_add_bin_scripts(tbbservice)

# supervisord config files
lofar_add_sysconf_files(
  tbbservice.ini
  DESTINATION supervisord.d)
