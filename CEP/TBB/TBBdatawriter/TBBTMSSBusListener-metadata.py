#!/usr/bin/env python3

'''This module is a 'bin'-script to start/use the TBBTMSSBusListener for TBB datawriter use.
It (re)uses the common main function in lofar.cep.tbb.TBBdatawriterController.
The automatically generated queue names get the name of this module as 'program' part.'''

from lofar.cep.tbb.TBBTMSSBusListener import main

if __name__ == '__main__':
    main()

