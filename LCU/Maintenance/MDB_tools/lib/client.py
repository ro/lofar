import logging
import requests
import re
from .tests_validators import is_rtsm_test, is_station_test
logger = logging.getLogger(__name__)


def read_stationtest_rtsm_output_to_dict(file_path):
    """
    Reads the content of the stationtest or RTSM output file into a dict
    -> ex. {'content': [file_content]}.
    If the file contains a RTSM it also append the station name
    -> ex. {'content': [file_content], 'station_name': CS001C}
    :param file_path: path to the file
    :return: return a dict containing a content field if the file refers to a station test ->
    ex. {'content': [file_content]}
    or if the file_path refers to a RTSM it returns a dict containing the content of the file and the station name
    to which the test refers
    ex. {'content': [file_content], 'station_name'} or None if it cannot read the file
    """
    try:
        with open(file_path, 'r') as input_stream:
            rtsm_filename_pattern = '(?P<station_name>\w*)_\d+_(\d+|obsdone)\.dat$'
            result = dict(content=input_stream.read().strip('\n'), file_path=file_path)
            if re.search(rtsm_filename_pattern, file_path):  # Matches the filename to understand if it is a RTSM
                result.update(station_name=re.search(rtsm_filename_pattern, file_path).group('station_name'))
            return result
    except Exception as e:
        logging.error('cannot read file %s', file_path)
        logging.exception(e)
        return None


def send_stationtest_rtsm_content_to_address(address, content):
    """
    Send the stationtest RTSM content to the web site API at the given address
    :param address: url where the API is hosted
    :param content: content of the API call
    :return: True if the request was successful False otherwise
    """
    full_address = '/'.join([address, compose_api_url_for_given_test_type(content['content'])])
    file_path = content.pop('file_path')
    content.pop('content')
    logger.info('performing request to address %s', full_address)

    logger.debug('request content %s', content)
    with open(file_path, 'rb') as file_stream:
        response = requests.post(full_address, data=content, files=dict(upload_file=file_stream))
    logger.info('response acknowledged: status code is %s, reason %s, content %s', response.status_code,
                 response.reason,
                 response.content)
    if response.status_code == 200:
        return True
    else:
        logger.error('error sending request %s', response.reason)
        return False


def compose_api_url_for_given_test_type(content):
    """
    Create the url from the content type
    :return: the query needed to insert the raw results
    """
    if is_station_test(content):
        query = 'stationtests/raw/insert'
    elif is_rtsm_test(content):
        query = 'rtsm/raw/insert'
    else:
        raise ValueError('The path format must refer to either an RTSM or a station test.')
    return query