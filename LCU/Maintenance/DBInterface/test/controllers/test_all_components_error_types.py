from rest_framework.test import APIClient, APITestCase

from test_data_generators import generate_random_test_data


class AllComponentsErrorTypesTest(APITestCase):
    def setUp(self):
        self.expected_error_types = ['SN', 'RS', 'VF']
        self.apiClient = APIClient()
        generate_random_test_data(2, 3, 5, 0, 0, expected_error_types=self.expected_error_types)

    def ctrl_list_component_error_type_returns_all_error_types(self):
        response = self.apiClient.get('/api/view/ctrl_list_component_error_types')
        self.assertEqual(set(response.data), set(self.expected_error_types))
