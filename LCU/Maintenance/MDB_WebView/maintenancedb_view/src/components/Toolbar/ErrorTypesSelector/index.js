import React, {Component} from 'react';
import {connect} from "react-redux";
import {setErrorTypes} from "redux/actions/mainFiltersActions";

import MultiSelectDropdown from 'components/MultiSelectDropdown'

// CSS
import toolbarStyles from '../styles.module.scss'

/**
 * ErrorTypesSelector:
 * Renders a multi-select dropdown for selecting error types (HIGH_NOISE, LOW_NOISE, ..)
 */
class ErrorTypesSelectorC extends Component {

    onSelectionErrorTypes = (errorTypes) => {
        this.props.setErrorTypes(errorTypes)
    }

    render() {
        const errorTypes = this.props.errorTypes.map(item => ({value:item, label:item}))

        return (<div className={toolbarStyles.toolbar_ctrl}>
            <label>Error type</label>
            <MultiSelectDropdown
                className="form-input"
                placeHolder="All"
                options={errorTypes}
                selectedItems={this.props.selectedErrorTypes}
                onSelectionChange={this.onSelectionErrorTypes}
            />
        </div>);
    }
}

const mapStateErrorTypesSelector = state => {
    return {
        selectedErrorTypes: state.mainFilters.selectedErrorTypes,
        errorTypes: state.appInitData.errorTypes
    };
};

const mapDispatchErrorTypesSelector = {
    setErrorTypes
};

export const ErrorTypesSelector = connect(mapStateErrorTypesSelector, mapDispatchErrorTypesSelector)(ErrorTypesSelectorC);
