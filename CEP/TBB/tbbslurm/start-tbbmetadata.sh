#!/bin/bash
DATE=$(date +%Y%m%dT%H%M%S)
echo logfile=/data/log/tbbwriter/tbbmetadata-$DATE.log
exec sbatch --partition=cpu -N 1 --cpus-per-task=1 \
      --nice=0 \
      --job-name=run-tbbmetadata \
      --output=/data/log/tbbwriter/tbbmetadata-$DATE.log \
      --dependency=singleton --mail-user=veen@astron.nl \
      --mail-type=ALL \
      ./start-srun-tbbmetadata.sh

