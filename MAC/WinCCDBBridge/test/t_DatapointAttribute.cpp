#include "UnitTest++/UnitTest++.h"

#include "../src/DatapointAttribute.hpp"
#include "../src/DatapointRule.hpp"

#include <QDomElement>
#include <QDomDocument>


#include <QDebug>
TEST(test_formatValue)
{
    DatapointAttribute attr("datapointname", "alias_name", "STRING", "s", "12", 0, 15, true);
    CHECK_EQUAL("\'12\'", attr.formatValue().toStdString());

    attr.setType("INT");

    CHECK_EQUAL("12", attr.formatValue().toStdString());
}

TEST(test_toString)
{
    DatapointAttribute attr("datapointname", "alias_name", "STRING", "s", "12", 0, 15, true);

    CHECK_EQUAL("DatapointAttribute(datapointname, alias_name, STRING, s, 12)", attr.toString().toStdString());
}

TEST(test_isInRange)
{
    DatapointAttribute attr("datapointname", "alias_name", "STRING", "s", "12", 0, 15, true);
    CHECK_EQUAL(true, attr.isInRange("12"));

    CHECK_EQUAL(false, attr.isInRange("-1"));

    CHECK_EQUAL(false, attr.isInRange("18"));

    attr = DatapointAttribute{};
    attr.setUpperLimit(12);
    attr.setLowerLimit(0);

    CHECK_EQUAL(false, attr.isBoundariesCheckEnabled());
    CHECK_EQUAL(false, attr.isInRange("-1"));
    CHECK_EQUAL(false, attr.isInRange("15"));
    CHECK_EQUAL(false, attr.isInRange("5"));

}

TEST(test_URI){
    DatapointAttribute attr("datapointname", "alias_name", "STRING", "s", "12", 0, 15, true);

    DatapointRule rule("rule.", "ruleAlias", "mytestrule");

    attr.setParentRule(rule);

    CHECK_EQUAL("rule.datapointname", attr.URI().toStdString());
}

TEST(test_XMLParsing){
    QDomDocument doc = QDomDocument("someParentElement");
    QDomElement test = doc.createElement("attribute");
    test.setAttribute("name", "myname");
    test.setAttribute("alias", "alias");
    test.setAttribute("unit", "s");
    test.setAttribute("value", "5");

    test.setAttribute("max", "12");
    test.setAttribute("min", "12");

    DatapointAttribute attr = readAttribute(test);

    CHECK_EQUAL("myname", attr.name().toStdString());
    CHECK_EQUAL("5", attr.value().toStdString());
    CHECK_EQUAL("alias", attr.alias().toStdString());
    CHECK_EQUAL("s", attr.unit().toStdString());
    CHECK_EQUAL(12, attr.upperLimit());
    CHECK_EQUAL(12, attr.lowerLimit());
    CHECK_EQUAL(true, attr.isBoundariesCheckEnabled());


    test = doc.createElement("attribute");
    test.setAttribute("name", "myname");
    test.setAttribute("alias", "alias");
    test.setAttribute("unit", "s");

    attr = readAttribute(test);

    CHECK_EQUAL("myname", attr.name().toStdString());
    CHECK_EQUAL("", attr.value().toStdString());
    CHECK_EQUAL("alias", attr.alias().toStdString());
    CHECK_EQUAL("s", attr.unit().toStdString());
    CHECK_EQUAL(false, attr.isBoundariesCheckEnabled());

}

int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}
