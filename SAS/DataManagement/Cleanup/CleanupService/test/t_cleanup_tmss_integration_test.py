#!/usr/bin/env python3
import time
import unittest

import logging
logger = logging.getLogger('lofar.'+__name__)
logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

from lofar.sas.tmss.test.test_environment import TMSSTestEnvironment

from lofar.messaging.messagebus import TemporaryExchange, BusListenerJanitor
from lofar.common.test_utils import integration_test
try:
    from lofar.sas.datamanagement.cleanup.service import create_rpc_service
except ModuleNotFoundError:
    print("Skipping test. could not import cleanup service. Did you build it?")
    exit(3)

from lofar.sas.datamanagement.cleanup.rpc import CleanupRPC

from datetime import datetime, timedelta
from uuid import uuid4
import threading
import os
from unittest import mock

@integration_test
class TestCleanupTMSSIntegration(unittest.TestCase):
    TEST_DIR = '/tmp/cleanup_tmss_integration_test/' + str(uuid4()) + '/data'
    TEST_PROJECTS_DIR = TEST_DIR + '/test-projects'

    @classmethod
    def setUpClass(cls) -> None:
        cls.TEST_UUID = uuid4()

        cls.tmp_exchange = TemporaryExchange()
        cls.tmp_exchange.open()

        cls.tmss_test_env = TMSSTestEnvironment(exchange=cls.tmp_exchange.address, populate_schemas=True, start_postgres_listener=True, populate_test_data=False, enable_viewflow=False, start_dynamic_scheduler=False, start_subtask_scheduler=True, start_workflow_service=False)
        cls.tmss_test_env.start()

    def setUp(self) -> None:
        os.makedirs(self.TEST_DIR)

        from lofar.sas.tmss.tmss.tmssapp import models
        for fs in models.Filesystem.objects.all():
            fs.directory = self.TEST_PROJECTS_DIR
            fs.save()

        # mockpatch the ssh calls which are issued from the cleanup subtask normally to cep4.
        # in this test we just keep the original command without the ssh
        ssh_cmd_list_patcher = mock.patch('lofar.common.cep4_utils.ssh_cmd_list')
        self.addCleanup(ssh_cmd_list_patcher.stop)
        ssh_cmd_list_mock1 = ssh_cmd_list_patcher.start()
        ssh_cmd_list_mock1.side_effect = lambda host, user: []

        # we should not depend on "previous" data
        self.tmss_test_env.delete_scheduling_unit_drafts_cascade()

    def tearDown(self) -> None:
        import shutil
        shutil.rmtree(self.TEST_DIR, ignore_errors=True)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.tmss_test_env.stop()
        cls.tmp_exchange.close()

    def test(self):
        from lofar.sas.tmss.tmss.tmssapp import models
        from lofar.sas.tmss.tmss.tmssapp.tasks import create_scheduling_unit_blueprint_and_tasks_and_subtasks_from_scheduling_unit_draft, schedule_independent_subtasks_in_scheduling_unit_blueprint, update_task_graph_from_specifications_doc
        from lofar.sas.tmss.test.test_environment import create_scheduling_unit_blueprint_simulator
        from lofar.sas.tmss.test.tmss_test_data_django_models import SchedulingUnitDraft_test_data, SchedulingSet_test_data
        from lofar.common.json_utils import add_defaults_to_json_object_for_schema
        from lofar_tmss_client.tmss_bus_listener import TMSSEventMessageHandler, TMSSBusListener
        from lofar.sas.tmss.test.test_utils import set_subtask_state_following_allowed_transitions

        scheduling_set = models.SchedulingSet.objects.create(**SchedulingSet_test_data())
        scheduling_set.project.auto_ingest = False # for user granting permission (in this test the simulator does that for us)
        scheduling_set.project.auto_pin = True # all tasks should pin their output data by default
        scheduling_set.project.save()

        strategy_template = models.SchedulingUnitObservingStrategyTemplate.objects.get(name="IM LBA Survey - 3 Beams")
        scheduling_unit_spec = add_defaults_to_json_object_for_schema(strategy_template.template, strategy_template.scheduling_unit_template.schema)
        # slim down the number of SAP-subbands/QA for debugging reabality
        for sap_nr, SAP in enumerate(scheduling_unit_spec['tasks']['Combined Observation']['specifications_doc']['station_configuration']['SAPs']):
            SAP['subbands'] = [sap_nr]
        scheduling_unit_spec['tasks']['Combined Observation']['specifications_doc']['QA']['plots']['enabled'] = False
        scheduling_unit_spec['tasks']['Combined Observation']['specifications_doc']['QA']['file_conversion']['enabled'] = False

        scheduling_unit_draft = models.SchedulingUnitDraft.objects.create(**SchedulingUnitDraft_test_data(template=strategy_template.scheduling_unit_template,
                                                                                                          scheduling_set=scheduling_set,
                                                                                                          scheduling_constraints_template=models.SchedulingConstraintsTemplate.get_latest('constraints')))

        # we require explicit ingest permission
        # this is set in the TestEventHanler when the pipeline has finished
        scheduling_unit_draft.ingest_permission_required = True
        scheduling_unit_draft.save()

        scheduling_unit_draft = update_task_graph_from_specifications_doc(scheduling_unit_draft, specifications_doc=scheduling_unit_spec)
        cleanup_task_draft = scheduling_unit_draft.task_drafts.get(specifications_template__type__value=models.TaskType.Choices.CLEANUP.value)
        self.assertTrue(cleanup_task_draft.output_pinned)

        scheduling_unit = create_scheduling_unit_blueprint_and_tasks_and_subtasks_from_scheduling_unit_draft(scheduling_unit_draft)
        cleanup_task_blueprint = scheduling_unit.task_blueprints.get(specifications_template__type__value=models.TaskType.Choices.CLEANUP.value)
        self.assertTrue(cleanup_task_blueprint.output_pinned)

        # ensure/check the data dir is empty at the start
        self.assertEqual([], os.listdir(self.TEST_DIR))

        class TestEventhandler(TMSSEventMessageHandler):
            """This test-TMSSEventMessageHandler tracks the interesting subtask status changes and determines
            if the dataproducts were first written by the obs/pipeline and then deleted by the cleanuptask"""
            def __init__(self, sync_object:{}):
                self._sync_object = sync_object
                super().__init__()

            def onSubTaskStatusChanged(self, id: int, status: str):
                subtask = models.Subtask.objects.get(id=id)

                if status == 'scheduled':
                    if subtask.specifications_template.type.value == models.SubtaskType.Choices.CLEANUP.value:
                        assert(cleanup_subtask.input_dataproducts.count() > 0)
                        assert(all([dp.deleted_since is None for dp in cleanup_subtask.input_dataproducts.all()]))
                    else:
                        # proceed to next state
                        subtask.state = models.SubtaskState.objects.get(value=models.SubtaskState.Choices.STARTING.value)
                        subtask.save()

                elif status=='starting':
                    if subtask.specifications_template.type.value == models.SubtaskType.Choices.CLEANUP.value:
                        logger.info("subtask %s %s starting", id, subtask.specifications_template.type.value)

                        self._sync_object['cleanup_sees_written_files'] = subtask.input_dataproducts.count() > 0 and \
                                                                          all(os.path.exists(dp.filepath) and os.path.getsize(dp.filepath) > 0
                                                                              for dp in subtask.input_dataproducts.all())
                    else:
                        # proceed to next state
                        subtask.state = models.SubtaskState.objects.get(value=models.SubtaskState.Choices.STARTED.value)
                        subtask.save()
                elif status=='started':
                    if subtask.specifications_template.type.value != models.SubtaskType.Choices.CLEANUP.value:
                        # proceed to next state
                        subtask.state = models.SubtaskState.objects.get(value=models.SubtaskState.Choices.FINISHING.value)
                        subtask.save()
                elif status=='finishing':
                    if subtask.specifications_template.type.value != models.SubtaskType.Choices.CLEANUP.value:
                        for output_dp in subtask.output_dataproducts.all():
                            os.makedirs(output_dp.directory, exist_ok=True)
                            logger.info('writing 1KB test dataproduct for subtask id=%s %s', subtask.id, output_dp.filepath)
                            with open(output_dp.filepath, 'w') as file:
                                file.write(1024 * 'a')

                        # proceed to next state
                        subtask.state = models.SubtaskState.objects.get(value=models.SubtaskState.Choices.FINISHED.value)
                        subtask.save()
                elif status=='finished':
                    logger.info("subtask %s %s finished", id, subtask.specifications_template.type.value)

                    subtask_did_write_files = all(os.path.exists(dp.filepath) and os.path.getsize(dp.filepath) > 0
                                                  for dp in subtask.output_dataproducts.all())

                    if subtask.specifications_template.type.value == models.SubtaskType.Choices.OBSERVATION.value:
                        self._sync_object['observation_did_write_files'] = subtask_did_write_files
                    elif subtask.specifications_template.type.value == models.SubtaskType.Choices.PIPELINE.value:
                        self._sync_object['pipeline_did_write_files'] = subtask_did_write_files

                        scheduling_unit_blueprint = subtask.task_blueprint.scheduling_unit_blueprint
                        assert(scheduling_unit_blueprint.ingest_permission_required)
                        if scheduling_unit_blueprint.ingest_permission_granted_since is None:
                            # grant ingest permission. This triggers via event the ingest subtask to be scheduled->started->finished
                            logger.info("granting ingest permission for scheduling_unit_blueprint %s", scheduling_unit_blueprint.id)
                            scheduling_unit_blueprint.ingest_permission_granted_since = datetime.utcnow()
                            scheduling_unit_blueprint.save()
                    elif subtask.specifications_template.type.value == models.SubtaskType.Choices.INGEST.value:
                        for task in subtask.task_blueprint.scheduling_unit_blueprint.task_blueprints.all():
                            # check if task output is indeed pinned (which follows from project auto_pin)
                            assert(task.output_pinned)

                            logger.info("unpinning output data task id=%s", task.id)
                            task.output_pinned = False
                            task.save()
                    elif subtask.specifications_template.type.value == models.SubtaskType.Choices.CLEANUP.value:
                        self._sync_object['cleanup_deleted_written_files'] = not any(os.path.exists(dp.filepath) and os.path.getsize(dp.filepath) > 0
                                                                                     for dp in subtask.input_dataproducts.all())
                        # signal simulator and test-method that we are done
                        self._sync_object['stop_event'].set()

        # helper object to communicate events/results
        sync_object = {'observation_did_write_files': False,
                       'pipeline_did_write_files': False,
                       'cleanup_sees_written_files': False,
                       'cleanup_deleted_written_files': False,
                       'stop_event': threading.Event()}

        with BusListenerJanitor(TMSSBusListener(handler_type=TestEventhandler, exchange=self.tmp_exchange.address, handler_kwargs={'sync_object': sync_object})):
            # start a simulator, forcing the scheduling_unit to "run" the observations, pipelines, ingest....
            # and let the cleanup server act on the eventmessages.
            # as a result, the scheduling_unit should be finished at the end, and the dataproducts should be "cleaned up"

            # check that the cleanup task is defined and ready to be used
            cleanup_subtask = models.Subtask.objects.get(task_blueprint__scheduling_unit_blueprint__id=scheduling_unit.id, specifications_template__type__value=models.SubtaskType.Choices.CLEANUP.value)
            self.assertEqual("defined", cleanup_subtask.state.value)

            # check that the sync-results are in initial state.
            # nobody wrote any files yet, and nothing was deleted yet.
            self.assertFalse(sync_object['observation_did_write_files'])
            self.assertFalse(sync_object['pipeline_did_write_files'])
            self.assertFalse(sync_object['cleanup_sees_written_files'])
            self.assertFalse(sync_object['cleanup_deleted_written_files'])

            # start the objects-under-test: the cleanup service
            # this service should respond to subtask events, and take care of the cleanup at the right moment.
            from lofar.sas.datamanagement.cleanup.service import create_tmss_buslistener, create_rpc_service
            with create_rpc_service(exchange=self.tmp_exchange.address, tmss_dbcreds_id=self.tmss_test_env.client_credentials.dbcreds_id, mountpoint=self.TEST_DIR):
                with create_tmss_buslistener(exchange=self.tmp_exchange.address, tmss_dbcreds_id=self.tmss_test_env.client_credentials.dbcreds_id):
                    # schedule the observation, this will result in a scheduled_event,
                    # which is handled above and triggers consecutive events,
                    # resulting in the end that all observation, pipeline, ingest subtasks are finished,
                    # and the cleanup subtask (as object-under-test) starts running and finished as well.
                    schedule_independent_subtasks_in_scheduling_unit_blueprint(scheduling_unit, datetime.utcnow()+timedelta(minutes=1))

                    # wait until scheduling_unit including the cleanup task is done
                    # the actual tests are done in the TestEventhandler above, setting their results in the sync_object
                    self.assertTrue(sync_object['stop_event'].wait(300))

                    # check states
                    cleanup_subtask.refresh_from_db()
                    self.assertEqual("finished", cleanup_subtask.state.value)
                    scheduling_unit.refresh_from_db()
                    self.assertEqual("finished", scheduling_unit.status.value)

                    # check that the files were written and deleted
                    self.assertTrue(sync_object['observation_did_write_files'])
                    self.assertTrue(sync_object['pipeline_did_write_files'])
                    self.assertTrue(sync_object['cleanup_sees_written_files'])
                    self.assertTrue(sync_object['cleanup_deleted_written_files'])

                    # were the dataproducts all marked as deleted?
                    self.assertTrue(cleanup_subtask.input_dataproducts.count()>0)
                    self.assertTrue(all([dp.deleted_since is not None for dp in cleanup_subtask.input_dataproducts.all()]))

    def test_TMSS_1684_bugfix(self):
        from lofar.common.json_utils import add_defaults_to_json_object_for_schema
        from lofar.sas.tmss.test.test_utils import set_subtask_state_following_allowed_transitions
        from lofar.sas.tmss.test.tmss_test_data_django_models import Project_test_data, SchedulingSet_test_data
        from lofar.sas.tmss.tmss.tmssapp import models
        from lofar.sas.tmss.tmss.tmssapp.tasks import create_scheduling_unit_blueprint_and_tasks_and_subtasks_from_scheduling_unit_draft, update_task_graph_from_specifications_doc, mark_task_blueprint_as_obsolete
        from lofar.sas.tmss.tmss.tmssapp.subtasks import wait_for_subtask_status

        # Setup
        # Step 0: setup scheduling unit
        project = models.Project.objects.create(**Project_test_data(auto_ingest=True))
        scheduling_set = models.SchedulingSet.objects.create(**SchedulingSet_test_data(project=project))

        constraints_template = models.SchedulingConstraintsTemplate.get_version_or_latest(name="constraints")
        constraints = add_defaults_to_json_object_for_schema({}, constraints_template.schema)
        uc1_strategy_template = models.SchedulingUnitObservingStrategyTemplate.get_version_or_latest(name="IM HBA LoTSS - 2 Beams")
        scheduling_unit_spec = add_defaults_to_json_object_for_schema(uc1_strategy_template.template, uc1_strategy_template.scheduling_unit_template.schema)
        # limit target obs duration for demo data
        scheduling_unit_spec['tasks']['Calibrator Observation 1']['specifications_doc']['duration'] = 2 * 60
        scheduling_unit_spec['tasks']['Target Observation']['specifications_doc']['duration'] = 2 * 3600
        scheduling_unit_spec['tasks']['Calibrator Observation 2']['specifications_doc']['duration'] = 2 * 60

        # add the scheduling_unit_doc to a new SchedulingUnitDraft instance, and were ready to use it!
        su_draft = models.SchedulingUnitDraft.objects.create(name=str(uuid4()),
                                                             scheduling_set=scheduling_set,
                                                             specifications_template=uc1_strategy_template.scheduling_unit_template,
                                                             observation_strategy_template=uc1_strategy_template,
                                                             scheduling_constraints_doc=constraints,
                                                             scheduling_constraints_template=constraints_template)
        su_draft = update_task_graph_from_specifications_doc(su_draft, scheduling_unit_spec)
        su_blueprint = create_scheduling_unit_blueprint_and_tasks_and_subtasks_from_scheduling_unit_draft(su_draft)

        with self.tmss_test_env.create_tmss_client() as client:
            # Step 1: simulate obs run
            scheduling_unit_blueprint_ext = client.get_schedulingunit_blueprint(su_blueprint.id, extended=True, include_specifications_doc=True)

            tasks = scheduling_unit_blueprint_ext['task_blueprints']
            observation_tasks = [t for t in tasks if t['task_type'] == 'observation']  # 3 obs
            pipeline_tasks = [t for t in tasks if t['task_type'] == 'pipeline']  # 4 pipes
            ingest_task = next(t for t in tasks if t['task_type'] == 'ingest')  # 1 ingest

            cal_obs1_task = next(t for t in observation_tasks if t['name'] == 'Calibrator Observation 1')
            target_obs_task = next(t for t in observation_tasks if t['name'] == 'Target Observation')
            cal_obs2_task = next(t for t in observation_tasks if t['name'] == 'Calibrator Observation 2')
            cal_pipe2_task = next(t for t in pipeline_tasks if t['name'] == 'Calibrator Pipeline 2')

            # -------------------
            # schedule first calibrator obs
            cal_obs1_subtask = next(st for st in cal_obs1_task['subtasks'] if st['subtask_type'] == 'observation')
            cal_obs1_subtask = client.schedule_subtask(cal_obs1_subtask['id'])
            # "mimic" that the cal_obs1_subtask starts running
            set_subtask_state_following_allowed_transitions(cal_obs1_subtask['id'], 'started')
            # "mimic" that the cal_obs1_subtask finished (including qa subtasks)
            for subtask in cal_obs1_task['subtasks']:
                set_subtask_state_following_allowed_transitions(subtask['id'], 'finished')

            # -------------------
            # schedule target obs
            target_obs_subtask = next(st for st in target_obs_task['subtasks'] if st['subtask_type'] == 'observation')
            target_obs_subtask = client.schedule_subtask(target_obs_subtask['id'])
            # "mimic" that the target_obs_subtask starts running
            set_subtask_state_following_allowed_transitions(target_obs_subtask['id'], 'started')
            # "mimic" that the target_obs_subtask finished (including qa subtasks)
            for subtask in target_obs_task['subtasks']:
                set_subtask_state_following_allowed_transitions(subtask['id'], 'finished')

            # -------------------
            # schedule second calibrator obs
            cal_obs2_subtask = next(st for st in cal_obs2_task['subtasks'] if st['subtask_type'] == 'observation')
            cal_obs2_subtask = client.schedule_subtask(cal_obs2_subtask['id'])
            # "mimic" that the cal_obs2_subtask starts running
            set_subtask_state_following_allowed_transitions(cal_obs2_subtask['id'], 'started')
            # "mimic" that the cal_obs2_subtask finished (including qa subtasks)
            for subtask in cal_obs2_task['subtasks']:
                set_subtask_state_following_allowed_transitions(subtask['id'], 'finished')

            # -------------------
            # Step 2: cancel one of the pipelines
            # cancel second calibrator pipeline to reproduce behaviour
            cal_pipe2_subtask = next(st for st in cal_pipe2_task['subtasks'] if st['subtask_type'] == 'pipeline')
            # "mimic" that the cal_pipe2_subtask is cancelled
            client.cancel_subtask(cal_pipe2_subtask['id'])

            cancelled_pipe_task = models.TaskBlueprint.objects.get(status=models.TaskStatus.Choices.CANCELLED.value)
            self.assertEqual(models.TaskStatus.Choices.CANCELLED.value, cancelled_pipe_task.status.value)

            # Step 3: create a copy for failed tasks
            client.create_copies_of_failed_tasks_via_draft(su_blueprint.id)

            # wait for the copied pipeline until it is scheduled.
            # it should be scheduled automatically upon creation, because its predecessor obs is already finished
            copy_pipe_task = models.TaskBlueprint.objects.get(name='Calibrator Pipeline 2 (copy for rerun 01)')
            copy_pipe_subtask = models.Subtask.objects.get(task_blueprint=copy_pipe_task)
            client.wait_for_subtask_status(copy_pipe_subtask.id, 'scheduled')
            copy_pipe_subtask.refresh_from_db()
            self.assertEqual(models.SubtaskState.Choices.SCHEDULED.value, copy_pipe_subtask.state.value)

            # Step 4: mark the cancelled pipeline obsolete
            client.mark_task_blueprint_as_obsolete(cancelled_pipe_task.id)
            cancelled_pipe_task.refresh_from_db()
            self.assertTrue(cancelled_pipe_task.is_obsolete)

            # Step 5: simulate run and finishing of copied pipeline, and the other normal pipelines
            for pipeline_subtask in su_blueprint.subtasks.filter(specifications_template__type__value='pipeline').exclude(state__value='cancelled'):
                set_subtask_state_following_allowed_transitions(pipeline_subtask.id, 'finished')

            # Step 5.1: wait until ingest is scheduled...
            ingest_subtask = models.Subtask.objects.get(task_blueprint__id=ingest_task['id'])
            wait_for_subtask_status(ingest_subtask, 'scheduled')

            # end of long setup, we now have an SU with the observation and all but one pipeline finished
            # the one pipelined failed, and was set to obsolete.

            # now start the objects-under-test: the cleanup service's tmss_buslistener
            # this tmss_buslistener's event handler should respond to predecessor subtask events,
            # and take care of the cleanup at the right moment.
            # Note that we do not start the cleanup service itself, cause we're not testing for actual cleanup. Just for scheduling/queueing/starting the cleanup task.

            with unittest.mock.patch('lofar.sas.datamanagement.cleanup.rpc.CleanupRPC.removeTaskData') as mocked_removeTaskData:
                # import and start cleanup buslistener
                from lofar.sas.datamanagement.cleanup.service import create_tmss_buslistener
                with create_tmss_buslistener(exchange=self.tmp_exchange.address,
                                             tmss_dbcreds_id=self.tmss_test_env.client_credentials.dbcreds_id):

                    # ..."mimic" that the ingest_subtask runs and finishes
                    # as a result the cleanup subtask should be scheduled, and then queued, and then started.
                    set_subtask_state_following_allowed_transitions(ingest_subtask.id, 'finished')

                    # Step 6: wait and assert the cleanup task is started
                    cleanup_task = su_blueprint.task_blueprints.get(specifications_template__type__value=models.TaskType.Choices.CLEANUP.value)
                    cleanup_subtask = cleanup_task.subtasks.get(task_blueprint__id=cleanup_task.id)
                    client.wait_for_subtask_status(cleanup_subtask.id, 'finished')
                    cleanup_task.refresh_from_db()
                    self.assertEqual(models.TaskStatus.Choices.FINISHED.value, cleanup_task.status.value)

                    # mocked_removeTaskData should have been called 8 times, for each cleanup-predecessor
                    self.assertEqual(8, mocked_removeTaskData.call_count)

    def test_TMSS_1862_bugfix(self):
        from lofar.common.json_utils import add_defaults_to_json_object_for_schema
        from lofar.sas.tmss.test.test_utils import set_subtask_state_following_allowed_transitions
        from lofar.sas.tmss.test.tmss_test_data_django_models import Project_test_data, SchedulingSet_test_data
        from lofar.sas.tmss.tmss.tmssapp import models
        from lofar.sas.tmss.tmss.tmssapp.tasks import create_scheduling_unit_blueprint_and_tasks_and_subtasks_from_scheduling_unit_draft, update_task_graph_from_specifications_doc, mark_task_blueprint_as_obsolete
        from lofar.sas.tmss.tmss.tmssapp.subtasks import wait_for_subtask_status, schedule_subtask

        # Setup
        project = models.Project.objects.create(**Project_test_data(auto_ingest=True))
        scheduling_set = models.SchedulingSet.objects.create(**SchedulingSet_test_data(project=project))

        strategy_template = models.SchedulingUnitObservingStrategyTemplate.get_version_or_latest(name="BF Pulsar Timing")
        scheduling_unit_spec = add_defaults_to_json_object_for_schema(strategy_template.template, strategy_template.scheduling_unit_template.schema)
        su_draft = models.SchedulingUnitDraft.objects.create(name=str(uuid4()),
                                                             scheduling_set=scheduling_set,
                                                             specifications_template=strategy_template.scheduling_unit_template,
                                                             observation_strategy_template=strategy_template)
        su_draft = update_task_graph_from_specifications_doc(su_draft, scheduling_unit_spec)
        su_blueprint = create_scheduling_unit_blueprint_and_tasks_and_subtasks_from_scheduling_unit_draft(su_draft)

        with self.tmss_test_env.create_tmss_client() as client:
            scheduling_unit_blueprint_ext = client.get_schedulingunit_blueprint(su_blueprint.id, extended=True, include_specifications_doc=True)

            tasks = scheduling_unit_blueprint_ext['task_blueprints']
            obs_task = next(t for t in tasks if t['task_type'] == 'observation')
            pip_task = next(t for t in tasks if t['task_type'] == 'pipeline')
            ingest_task = next(t for t in tasks if t['task_type'] == 'ingest')

            # -------------------
            # schedule observation
            obs_subtask = next(st for st in obs_task['subtasks'] if st['subtask_type'] == 'observation')
            obs_subtask = client.schedule_subtask(obs_subtask['id'])
            # "mimic" that the obs_subtask starts running
            set_subtask_state_following_allowed_transitions(obs_subtask['id'], 'started')
            # "mimic" that the obs_subtask finished
            set_subtask_state_following_allowed_transitions(obs_subtask['id'], 'finished')

            # -------------------
            # cancel pipeline
            pip_subtask = next(st for st in pip_task['subtasks'] if st['subtask_type'] == 'pipeline')
            client.cancel_subtask(pip_subtask['id'])

            # check progress. In the bug report this seems to be the cause for the problem. Namely that a cancelled tasks cannot yield it's progress and returns an error 500
            with self.assertRaises(Exception) as context:
                client.get_subtask_progress(pip_subtask['id'])
            self.assertTrue('500 Internal Server Error - Could not get progress for subtask' in str(context.exception))

            # mark the cancelled pipeline obsolete
            # (this should trigger the ingest subtask to be scheduled in the background)
            client.mark_subtask_as_obsolete(pip_subtask['id'])

            cancelled_pipe_task = models.TaskBlueprint.objects.get(status=models.TaskStatus.Choices.CANCELLED.value)
            self.assertEqual(models.TaskStatus.Choices.CANCELLED.value, cancelled_pipe_task.status.value)
            cancelled_pipe_task.refresh_from_db()
            self.assertTrue(cancelled_pipe_task.is_obsolete)

            # -------------------
            # grab ingest (sub)task, and scheduled it
            ingest_subtask = models.Subtask.objects.get(task_blueprint__id=ingest_task['id'])
            ingest_subtask = schedule_subtask(ingest_subtask)
            self.assertEqual(models.SubtaskState.Choices.SCHEDULED.value, ingest_subtask.state.value)

            # "mimic" that the ingest_subtask runs and finishes
            # (this should trigger the cleanup subtask to be scheduled in the background)
            set_subtask_state_following_allowed_transitions(ingest_subtask, 'finished')

            # grab cleanup (sub)task, and wait until it was scheduled in the background
            cleanup_task = su_blueprint.task_blueprints.get(specifications_template__type__value=models.TaskType.Choices.CLEANUP.value)
            cleanup_subtask = cleanup_task.subtasks.get(task_blueprint__id=cleanup_task.id)
            wait_for_subtask_status(cleanup_subtask, models.SubtaskState.Choices.SCHEDULED.value)
            self.assertEqual(models.SubtaskState.Choices.SCHEDULED.value, cleanup_subtask.state.value)

            # end of big setup.
            # now start the cleanup rpc service and the cleanup tmss event handler/buslistener.
            # let them run and act on the scheduled cleanup subtask.
            # wait for expected status, and then check results.

            with mock.patch('lofar.sas.datamanagement.cleanup.service.CleanupHandler._removeTaskData') as patcher:
                # patch the cleanup-services remove task data method.
                # make it return what we want (and prevent wiping anything on our system)
                def patched_removeTaskData(**kwargs):
                    msg = "patched removeTaskData method was called with tmss_id=%s" % (kwargs.get('tmss_id'),)
                    logger.info(msg)
                    return {'deleted': True, # fake that we deleted the data
                            'message': msg}
                patcher.side_effect = patched_removeTaskData

                # start the actual cleanupservice (with the mocked remove task data)
                with create_rpc_service(exchange=self.tmp_exchange.address) as service:
                    # and start the cleanupservice <-> tmss event handler
                    from lofar.sas.datamanagement.cleanup.service import create_tmss_buslistener
                    with create_tmss_buslistener(exchange=self.tmp_exchange.address, tmss_dbcreds_id=self.tmss_test_env.client_credentials.dbcreds_id):

                        # wait until cleanup service executed the cleanup subtask
                        wait_for_subtask_status(cleanup_subtask, models.SubtaskState.Choices.FINISHED.value)
                # The cleanup task is done. exit scope of service and event handler.

            # check that the cleanupservice actually tried to delete the data for subtask obs and pipe
            patcher.assert_called()
            self.assertEqual(2, patcher.call_count)

            # TODO: make these asserts pass on build-runner. They do pass on my dev machine.
            # for id in (obs_subtask['id'], pip_subtask['id']):
            #     self.assertTrue(any([call.kwargs.get('tmss_id')==id for call in patcher.call_args_list]))

            # confirm su is finished
            su_blueprint.refresh_from_db()
            self.assertEqual(models.SchedulingUnitStatus.Choices.FINISHED.value, su_blueprint.status.value)

    def test_TMSS_2304_bugfix(self):
        from lofar.common.json_utils import add_defaults_to_json_object_for_schema
        from lofar.sas.tmss.test.test_utils import set_subtask_state_following_allowed_transitions
        from lofar.sas.tmss.test.tmss_test_data_django_models import Project_test_data, SchedulingSet_test_data
        from lofar.sas.tmss.tmss.tmssapp import models
        from lofar.sas.tmss.tmss.tmssapp.tasks import create_scheduling_unit_blueprint_and_tasks_and_subtasks_from_scheduling_unit_draft, create_cleanuptask_for_scheduling_unit_draft, schedule_independent_subtasks_in_scheduling_unit_blueprint
        from lofar.sas.tmss.tmss.tmssapp.subtasks import wait_for_subtask_status, schedule_subtask

        # Setup
        project = models.Project.objects.create(**Project_test_data(auto_ingest=True))
        scheduling_set = models.SchedulingSet.objects.create(**SchedulingSet_test_data(project=project))
        su_draft = models.SchedulingUnitDraft.objects.create(name=str(uuid4()),
                                                             scheduling_set=scheduling_set,
                                                             specifications_template=models.SchedulingUnitTemplate.get_latest(name='scheduling unit'))

        obs_template = models.TaskTemplate.get_latest(name='parallel calibrator target and beamforming observation')
        obs_spec = obs_template.get_default_json_document_for_schema()
        obs_spec['QA']['plots']['enabled'] = False
        obs_spec['QA']['file_conversion']['enabled'] = False
        obs_spec['beamformer']['pipelines'][0]['coherent']['SAPs'][0]['subbands']['list'] = obs_spec['station_configuration']['SAPs'][0]['subbands']
        obs_spec['beamformer']['pipelines'][0]['coherent']['SAPs'][0]['tab_rings']['count'] = 1
        models.TaskDraft.objects.create(name="IM+BF obs", specifications_template=obs_template, specifications_doc=obs_spec, scheduling_unit_draft=su_draft)
        su_draft = create_cleanuptask_for_scheduling_unit_draft(su_draft)

        su_blueprint = create_scheduling_unit_blueprint_and_tasks_and_subtasks_from_scheduling_unit_draft(su_draft)
        su_blueprint = schedule_independent_subtasks_in_scheduling_unit_blueprint(su_blueprint)
        su_blueprint = schedule_independent_subtasks_in_scheduling_unit_blueprint(su_blueprint, datetime.utcnow()+timedelta(minutes=5))
        self.assertEqual(2, su_blueprint.subtasks.count())

        obs_subtask = su_blueprint.subtasks.filter(specifications_template__name='observation control').first()
        cleanup_subtask = su_blueprint.subtasks.filter(specifications_template__name='cleanup').first()

        obs_subtask = set_subtask_state_following_allowed_transitions(obs_subtask, 'finished')
        cleanup_subtask = wait_for_subtask_status(cleanup_subtask, 'scheduled')

        # start the objects-under-test: the cleanup service
        # this service should respond to subtask events, and take care of the cleanup at the right moment.
        from lofar.sas.datamanagement.cleanup.service import create_tmss_buslistener, create_rpc_service
        with create_rpc_service(exchange=self.tmp_exchange.address,
                                tmss_dbcreds_id=self.tmss_test_env.client_credentials.dbcreds_id,
                                mountpoint=self.TEST_DIR):
            with create_tmss_buslistener(exchange=self.tmp_exchange.address,
                                         tmss_dbcreds_id=self.tmss_test_env.client_credentials.dbcreds_id):
                # wait until cleanup service executed the cleanup subtask
                wait_for_subtask_status(cleanup_subtask, (models.SubtaskState.Choices.FINISHED.value, models.SubtaskState.Choices.ERROR.value))
                # The cleanup task is done. exit scope of service and event handler.

        # expecting the cleanup to be finished.
        # Before the bug fix, the subtask went to ERROR. Now it FINISHes properly. Bug fixed.
        self.assertEqual(models.SubtaskState.Choices.FINISHED.value, cleanup_subtask.state.value)


if __name__ == '__main__':
    unittest.main()
