#ifndef DATAPOINT_ATTRIBUTE_H
#define DATAPOINT_ATTRIBUTE_H

#include <QString>
class DatapointRule;
class QDomElement;

/*! \class DatapointAttribute
    \brief Define an attribute for a given datapoint.
    The DatapointAttribute describes an attribute of a datapoint that could be for example a last modification date or a value.
    If it is a value it can be subject of jittering. For this reason, it can be possible to specify a range within which the value
    can be without triggering an update of the database.
    It has to be specified as in the following XML fragment (ex.
        <attribute name=":_original.._value" alias="value" type="STRING" unit=""/>
    )
*/


class DatapointAttribute
{
public:
    DatapointAttribute();
    DatapointAttribute(QString name, QString alias, QString type, QString unit, QString value, double minValue, double maxValue, bool check);

    const QString formatValue() const;
    const QString toString() const;

    const QString & URI() const;
    void setURI(const QString &uri);

    bool isBoundariesCheckEnabled();
    void enableBoundariesCheck();

    bool isInRange(QString value);

    const QString & name() const;
    void setName(const QString & name);

    const QString & alias() const;
    void setAlias(const QString & alias);

    const QString & type() const;
    void setType(const QString & type);

    const QString & unit() const;
    void setUnit(const QString & unit);

    const QString & value() const;
    void setValue(const QString & value);

    const double & upperLimit() const;
    void setUpperLimit(const double upperLimit);

    const double & lowerLimit() const;
    void setLowerLimit(const double lowerLimit);

    void setParentRule(DatapointRule & rule);

private:
    QString m_name;
    QString m_alias;
    QString m_type;
    QString m_unit;
    QString m_value;
    double m_max;
    double m_min;
    QString m_uri;
    QString composeDatapointURI(DatapointRule & rule);
    bool checkBoundaries;
friend DatapointAttribute readAttribute(const QDomElement &);;
};


DatapointAttribute readAttribute(const QDomElement &attributeEntry);

#endif // DATAPOINT_ATTRIBUTE_H
