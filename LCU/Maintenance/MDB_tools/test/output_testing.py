# coding=utf-8
import mock

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO


class compare_output_with(object):
    """
    This class purpose is to create a 'with' environment to assert that the output is equal to the one specified
    in the class constructor
    """

    def __init__(self, expected_stdout):
        """
        Class constructor takes a path to a text file that contains the expected std output
        :param expected_stdout: expected standard output
        :type expected_stdout: str
        """
        self.expected_stdout = expected_stdout

    def __enter__(self):
        self.stdout_patcher = mock.patch('sys.stdout', new=StringIO())
        self.stdout_patcher.start()

    def __exit__(self, exc_type, exc_val, exc_tb):
        stdout_produced = self.stdout_patcher.target.stdout.getvalue()
        self.stdout_patcher.stop()

        assert self.expected_stdout == stdout_produced, \
            "expected stdout {} differs from {}".format(repr(self.expected_stdout), repr(stdout_produced))


class compare_output_with_file_content(compare_output_with):
    """
        This class purpose is to create a 'with' environment to assert that the output is equal to the one specified
        in content of the file_path specified in the class constructor
    """

    def __init__(self, file_path):
        """
        Class constructor takes a path to a text file that contains the expected std output
        :param file_path: path to the file containing the expected standard output
        """
        try:
            with open(file_path, 'r') as fstream_in:
                content = fstream_in.read()
        except IOError as e:
            raise Exception('Cannot run test  - IOError on file {}: {}'.format(file_path, e))

        super(compare_output_with_file_content, self).__init__(content)


class save_output_to_file(compare_output_with):
    """
    This class is meant to catch through a with environment the standard output and save it to the specified file
    """
    def __init__(self, file_path):
        super(save_output_to_file, self).__init__('in')
        self.file_path = file_path

    def __exit__(self, exc_type, exc_val, exc_tb):
        stdout_produced = self.stdout_patcher.target.stdout.getvalue()
        with open(self.file_path, 'w') as fstream_out:
            fstream_out.write(stdout_produced)