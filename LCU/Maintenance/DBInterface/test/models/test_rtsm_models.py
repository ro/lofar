from django.test import TestCase
from lofar.maintenance.monitoringdb.models.rtsm import RTSMObservation
from lofar.maintenance.monitoringdb.models.station import Station
from datetime import datetime

class RTSMObservationTest(TestCase):
    def test_observation_insertion(self):
        test_station = Station()
        test_station.name = 'CS001C'
        test_station.location = None
        test_station.save()

        rtsm_observation = RTSMObservation()
        rtsm_observation.station = test_station
        rtsm_observation.observation_id = 123456
        rtsm_observation.samples = 12
        rtsm_observation.start_datetime = datetime(2018, 12, 12)
        rtsm_observation.end_datetime = datetime(2018, 12, 12)

        rtsm_observation.save()

        saved_entity = RTSMObservation.objects.get(observation_id=123456)
        self.assertEqual(saved_entity.pk, rtsm_observation.pk)