#include "DatapointRule.hpp"

#include "DatapointIndex.hpp"
#include "DatapointAttribute.hpp"

#include <QStringList>

#include <QSqlQuery>
#include <QTimer>

#include <QDebug>
#include <QDomElement>
#include <QDomDocument>

DatapointRule::DatapointRule(): statement{""}
{

}

std::vector<std::string> DatapointRule::getDatapointsURI()
{
    using std::vector;
    using std::string;

    vector<string> datapointURIs;
    for(auto & attr: m_attributes){
        datapointURIs.push_back(attr.second.URI().toStdString());
    }
    return datapointURIs;
}

DatapointRule::DatapointRule(QString name, QString alias, QString description):
    m_name{name}, m_alias{alias}, m_description{description}, statement{""}, checkBoundaries{false}
{

}

void DatapointRule::addAttribute(DatapointAttribute attribute){
    attribute.setParentRule(*this);

    checkBoundaries |= attribute.isBoundariesCheckEnabled();

    m_attributes[attribute.URI().toStdString()] = attribute;
}

const QString DatapointRule::toString() const {
     return "DatapointRule(" + QStringList({m_name, m_alias, m_description}).join(", ") + ")";
}

const QString& DatapointRule::name() const
{
    return m_name;
}

const QString& DatapointRule::alias() const
{
    return m_alias;
}

const QString& DatapointRule::description() const
{
    return m_description;
}

const QString& DatapointRule::table() const
{
    return m_table;
}

void DatapointRule::setName(const QString& name)
{
    m_name = name;
}

void DatapointRule::setAlias(const QString& alias)
{
    m_alias = alias;
}

void DatapointRule::setDescription(const QString& description)
{
    m_description = description;
}
void DatapointRule::setTable(const QString& table)
{
    m_table = table;
}


void DatapointRule::prepareStatement(){
    QStringList columnNames;
    QStringList valueNames;
    QStringList indexNames;

    for(const DatapointAttribute & att : index.keys()){
        columnNames << att.alias();
        valueNames << att.formatValue();
    }

    for(const auto & att : m_attributes){
        columnNames << att.second.alias();
        valueNames << ":" + att.second.alias();

    }

    statement = "INSERT INTO " + m_table + " ";
    statement += "(" + columnNames.join(", ") + ")";
    statement += " VALUES ";
    statement += "(" + valueNames.join(", ") + ")";
}

const QString & DatapointRule::sqlStatement(){
    if(statement.isEmpty() == true) prepareStatement();
    return statement;
}

void DatapointRule::bindValues(QSqlQuery & query, std::map<std::string, std::string> & values){
    qDebug() << "binding values";

    for(auto & uriValue: values){
        const QString attribute_alias = ":" + m_attributes[uriValue.first].alias();
        const QVariant attribute_value = QString::fromStdString(uriValue.second);
        qDebug() << "alias=" << attribute_alias << ", value=" << attribute_value;
        query.bindValue(attribute_alias, attribute_value);
    }
}

DatapointRule * readDatapointRule(const QDomElement & element){

    DatapointRule * dpRule{new DatapointRule};
    dpRule->m_description = element.firstChildElement("description").text();

    dpRule->m_checkpointInterval = std::chrono::seconds(element.firstChildElement("CheckpointInterval").attribute("value").toInt());
    dpRule->m_name = element.attribute("name");
    dpRule->m_alias = element.attribute("alias");

    dpRule->index = readIndexRule(element.firstChildElement("index"), dpRule->m_name);

    qDebug() << dpRule->index.toString();

    QDomNodeList attributes = element.firstChildElement("attributes").elementsByTagName("attribute");

    for(int i=0; i < attributes.size(); i++){
        DatapointAttribute attribute{readAttribute(attributes.item(i).toElement())};
        qDebug()<<attribute.toString();
        dpRule->addAttribute(attribute);
    }
    return dpRule;
}

bool DatapointRule::hasToTriggerUpdate(std::map<std::string, std::string> & values){
    if(checkBoundaries){
        qDebug() << "check values in range";
        bool inRange = true;
        for(const auto uriValue: values){
            DatapointAttribute & attribute = m_attributes[uriValue.first];
            inRange = inRange && attribute.isInRange(uriValue.second.c_str());
        }
        return !inRange;
    }else{
        return true;
    }
}

void DatapointRule::triggerRequestCheckpoint(){
    emit requestCheckpoint(this);
}

void DatapointRule::startCheckpointTimer(){
	triggerRequestCheckpoint();
    timer.reset(new QTimer(this));
    connect(timer.get(), &QTimer::timeout, this, &DatapointRule::triggerRequestCheckpoint);
    if(m_checkpointInterval.count() > 0){
        timer->setInterval(std::chrono::milliseconds(m_checkpointInterval).count());
        timer->start();
    }
}

std::vector<std::shared_ptr<DatapointRule>> readDatapointRules(QDomDocument & document){
    QDomElement table = document.firstChildElement("Table");
    QString tableName = table.attribute("name");

    qDebug() << "Rule file for table " << tableName;

    QDomElement element = table.firstChildElement("Datapoints")
                               .firstChildElement("Datapoint");
    std::vector<std::shared_ptr<DatapointRule>> rules;

    while(element.isNull() == false){

        DatapointRule * rule{readDatapointRule(element)};
        rule->m_table = tableName;
        rule->prepareStatement();
        rules.push_back(std::shared_ptr<DatapointRule>{rule});

        element = element.nextSiblingElement();
    }
    return rules;
}
