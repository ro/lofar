#!/usr/bin/env python

import logging

from lofar.maintenance.utils.cli.probe_mdb import main

logger = logging.getLogger('probe_mdb')

"""
This program is meant to load the station tests and RTSM present in a certain directory to the database
"""

if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=logging.WARN)
    main()