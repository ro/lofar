from rest_framework.serializers import ModelSerializer
from ..models.element import Element
from .log import ActionLogSerializer


class ElementSerializer(ModelSerializer):
    class Meta:
        model = Element
        fields = '__all__'
        depth = 1

    @staticmethod
    def insert_element(component, element):
        element_entry = Element.objects.filter(component=component,
                                               **element).first()
        if element_entry is None:
            element_entry = Element(component=component,
                                    **element)
            element_entry.save()
            # Log the action
            ActionLogSerializer.log_model_insert(element_entry)
        return element_entry