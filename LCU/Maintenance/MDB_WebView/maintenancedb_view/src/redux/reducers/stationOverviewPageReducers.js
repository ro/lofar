import {
    SET_CHILD_PANEL_DATA,
    UNPIN_CHILD_PANEL_DATA
} from '../actions/stationOverviewPageActions.js'

import { LOCATION_CHANGE } from "connected-react-router";

const initialState = {

    main_panel: {
        highlightData: null
    },

    // Child panel showing data on mouse hover
    child_panel: {
        data: null,
        isPinned: false
    }
};


export default function(state = initialState, action) {
    switch (action.type) {
        case SET_CHILD_PANEL_DATA:

            if (state.child_panel.isPinned && ! action.payload.doPin) {
                // action.payload.doPin is only true when right-mouse click is used, so
                // this is probably a mouse move event. Ignore it.
                return state;
            }
            let newState = {
              ...state,
              child_panel: {
                  data: action.payload.data,
                  isPinned: action.payload.doPin
              }
            };

            if (action.payload.doPin) {
                newState.main_panel = {
                    highlightData: action.payload.data
                };
            }
            return newState;

        case UNPIN_CHILD_PANEL_DATA:
        case LOCATION_CHANGE:
            // Note: LOCATION_CHANGE is triggered by changes in the main filters (e.g. selectedStation)
            // then also reset the child panel
            return {
              ...state,
              main_panel: {
                  highlightData: null
              },
              child_panel: {
                  data: null,
                  isPinned: false
              }
            };

        default:
            return state;
    }
}
