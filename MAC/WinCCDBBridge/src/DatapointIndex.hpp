#ifndef DATAPOINT_INDEX_H
#define DATAPOINT_INDEX_H
#include <QString>

class DatapointAttribute;
class QDomElement;

/*! \class DatapointIndex
    \brief Define a unique index for a given datapoint.
    The DatapointIndex is meant to reflect a DB table index.
    Such index is derived from the datapoint name through a regex to give the widest range of possibilities in the configuration file.
    In particular, the XML fragment parsed by the function readIndexRule and used to instantiate the DatapointIndex is for example:
        <index>
            <regex>
                <![CDATA[(?<system>\w*)\.((?<attribute>\w*))]]>
            </regex>
            <attribute name="system" alias="system" type="STRING"/>
            <attribute name="attribute" alias="attribute" type="STRING"/>
        </index>
    The index keys are picked up from the named regex capture group. Therefore, each capture group that has to be used as an index field
    has to be named.
*/

class DatapointIndex
{
public:
    DatapointIndex();

    void parseDPIndex(QString index);
    const QString toString() const;
    const std::vector<DatapointAttribute> & keys();
private:
    std::vector<DatapointAttribute> m_keys;
    QString pattern;
friend const DatapointIndex readIndexRule(const QDomElement &, const QString &);
};

const DatapointIndex readIndexRule(const QDomElement & element, const QString & dpName);

#endif // DATAPOINT_INDEX_H
