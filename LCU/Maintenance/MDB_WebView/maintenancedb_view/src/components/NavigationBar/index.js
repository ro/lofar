import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { Collapse,
        Nav,
        NavbarToggler,
        Navbar,
        NavbarBrand,
        NavItem,
        NavLink } from 'reactstrap';

// CSS
import styles from './styles.module.scss'

/**
 * NavigationBar
 * Creates the top navigation bar with app name and collapsable main menu.
 */
class NavigationBar extends PureComponent {

    state = {
        isOpen: false
    }

    toggle = () => {
        this.setState({
            isOpen:!this.state.isOpen
        });
    }

    check_status(item){
        if (this.props.active_page.pathname === item){
            return "active"
        }
    }

    render() {
        return (
          <Navbar className={styles.header_navbar} dark expand="lg">
            <NavbarBrand className={styles.header_brand}>
                <h1>
                    <em>LOFAR</em> Station monitor <span>v0.1</span>
                </h1>
            </NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className="ml-auto" navbar>
                    <NavItem className={this.check_status("/")}>
                        <NavLink tag={Link} to="/">
                            <h3>Dashboard</h3>
                        </NavLink>
                    </NavItem>
                    <NavItem className={this.check_status("/station_overview")}>
                        <NavLink tag={Link} to="/station_overview">
                            <h3>Station Overview</h3>
                        </NavLink>
                    </NavItem>
                    <NavItem className={this.check_status("/tiles")}>
                        <NavLink tag={Link} to="/tiles">
                            <h3>Tiles</h3>
                        </NavLink>
                    </NavItem>
                </Nav>
            </Collapse>
          </Navbar>);
    }
}

export default NavigationBar;
