#
# base
#

# Allow to specify a specific base image version.
ARG LOFAR_BASE_IMAGE_VERSION=latest
FROM lofar-base:${LOFAR_BASE_IMAGE_VERSION}
ENV LOFAR_BASE_IMAGE_VERSION=${LOFAR_BASE_IMAGE_VERSION}


# Allow to specify the LOFAR branch at build time of the image.
# Use master as the default if nothing is specified.
ARG LOFAR_VERSION=latest
ENV LOFAR_VERSION=${LOFAR_VERSION}


ENV BLITZ_VERSION=1.0.1 \
    LIBLAPACK_VERSION=3 \
    LIBLOG4CPLUS_VERSION=1.1-9 \
    LIBSIGCPP_VERSION=2.0 \
    LIBXMLPP_VERSION=2.6 \
    LIBGSL_VERSION=23 \
    LIBPQXX_VERSION=4.0v5 \
    DAL_VERSION=v3.3.1 \
    XMLRUNNER_VERSION=1.7.7 \
    MONETDB_VERSION=11.19.3.2


# Run-time dependencies
RUN aptitude install -y liblog4cplus-${LIBLOG4CPLUS_VERSION} libxml2-utils libpng-tools libsigc++-${LIBSIGCPP_VERSION}-0v5 libxml++${LIBXMLPP_VERSION}-2v5 libgsl${LIBGSL_VERSION} openssh-client gettext-base rsync python3-matplotlib ipython3 libhdf5-${LIBHDF5_VERSION} libcfitsio-bin libwcs5 && \
    aptitude clean && \
    aptitude autoclean

# Install
RUN export BUILD_PACKAGES="python3-pip python3-dev python3-setuptools liblog4cplus-dev libpng-dev libsigc++-${LIBSIGCPP_VERSION}-dev libxml++${LIBXMLPP_VERSION}-dev libgsl-dev libcfitsio-dev wcslib-dev libhdf5-dev" && \
    aptitude install -y ${BUILD_PACKAGES} && \
    pip3 install xmlrunner==${XMLRUNNER_VERSION} python-monetdb==${MONETDB_VERSION} && \
    aptitude purge -y ${BUILD_PACKAGES} && \
    aptitude clean && \
    aptitude autoclean

#
# Run-time dependencies
RUN aptitude install -y libboost-mpi-dev libboost-serialization${BOOST_VERSION}-dev libboost-serialization${BOOST_VERSION}.1 && \
    aptitude clean && \
    aptitude autoclean

RUN export BUILD_PACKAGES="git g++ gfortran autoconf automake make python" && \
    aptitude install -y ${BUILD_PACKAGES} && \
    mkdir -p ${INSTALLDIR}/blitz && \
    git clone --branch ${BLITZ_VERSION//latest/master} https://github.com/blitzpp/blitz.git ${INSTALLDIR}/blitz/blitz-${BLITZ_VERSION} && \
    cd ${INSTALLDIR}/blitz/blitz-${BLITZ_VERSION} && \
    autoreconf -fiv && ./configure --prefix=${INSTALLDIR}/blitz/ && \
    make -j ${J} lib && \
    make install && \
    rm -rf ${INSTALLDIR}/blitz/blitz-${BLITZ_VERSION} && \
    aptitude purge -y ${BUILD_PACKAGES} && \
    aptitude clean && \
    aptitude autoclean

ENV LD_LIBRARY_PATH=${INSTALLDIR}/blitz/lib:${LD_LIBRARY_PATH}
ENV PATH=${INSTALLDIR}/blitz/bin:${PATH}


#
# *******************
#   DAL
# *******************
#
#
# Run-time dependencies
RUN aptitude install -y libhdf5-${LIBHDF5_VERSION} python3 && \
    aptitude clean && \
    aptitude autoclean

RUN export BUILD_PACKAGES="git cmake g++ swig3.0 python3-setuptools python3-dev libhdf5-dev" && \
    aptitude install -y ${BUILD_PACKAGES} && \
    mkdir -p ${INSTALLDIR}/DAL/build && \
    git clone --branch ${DAL_VERSION//latest/master} https://github.com/nextgen-astrodata/DAL.git ${INSTALLDIR}/DAL/DAL.src && \
    cd ${INSTALLDIR}/DAL/build && \
    cmake -DPYTHON_INCLUDE_DIR=/usr/include/python${PYTHON_VERSION} -DPYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython${PYTHON_VERSION}m.so -DBUILD_TESTING=OFF -DCMAKE_CXX_FLAGS="${CXX_FLAGS} -fpermissive" -DCMAKE_INSTALL_PREFIX=${INSTALLDIR}/DAL ${INSTALLDIR}/DAL/DAL.src && \
    make -j ${J} && \
    make install && \
    bash -c "rm -rf ${INSTALLDIR}/DAL/{DAL.src,build}" && \
    aptitude purge -y ${BUILD_PACKAGES} && \
    aptitude clean && \
    aptitude autoclean

# Add build date to the environment - Moved, as it rebuilds from here. Normally on line 5
ARG DOCKER_IMAGE_BUILD_DATE="The build date has to be set by a --build-arg parameter!"
ENV DOCKER_IMAGE_BUILD_DATE=${DOCKER_IMAGE_BUILD_DATE}

#
# *******************
#   LOFAR
# *******************
#
# Run-time dependencies
RUN aptitude install -y libncurses${NCURSES_VERSION} liblog4cplus-${LIBLOG4CPLUS_VERSION} libhdf5-${LIBHDF5_VERSION} libboost-chrono${BOOST_VERSION}.1 libboost-program-options${BOOST_VERSION}.1 libboost-python${BOOST_VERSION}.1 libboost-regex${BOOST_VERSION}.1 python3 libxml2 libpng-tools liblapack${LIBLAPACK_VERSION} libfftw3-bin libxml++${LIBXMLPP_VERSION}-2v5 libgsl${LIBGSL_VERSION} libreadline${READLINE_VERSION} binutils libcfitsio-bin libwcs5 libopenblas-base libpqxx-${LIBPQXX_VERSION} libqpid-proton8 libqpid-proton-cpp8 python3-qpid-proton python3-pg python3-psycopg2 python3-requests && \
    aptitude clean && \
    aptitude autoclean

# Apply a finger print to force a rebuild if the source code changes. Supply a unique ID here to force a rebuild.
ARG LOFAR_FINGERPRINT=V2.2
ENV LOFAR_FINGERPRINT=${LOFAR_FINGERPRINT}

# Install
RUN export BUILD_PACKAGES="git cmake g++ gfortran python3-setuptools bison flex libncurses-dev liblog4cplus-dev libboost${BOOST_VERSION}-all-dev libboost-python${BOOST_VERSION}-dev python3-dev libxml2-dev pkg-config libpng-dev liblapack-dev libfftw3-dev libunittest++-dev libxml++${LIBXMLPP_VERSION}-dev libgsl-dev libreadline-dev binutils-dev libcfitsio-dev wcslib-dev libopenblas-dev libqpid-proton-dev libqpid-proton-cpp-dev libpqxx-dev libhdf5-dev" && \
    aptitude update && \
    aptitude install -y ${BUILD_PACKAGES} && \
    mkdir -p ${INSTALLDIR}/lofar/build/${LOFAR_BUILDVARIANT} && \
    git clone --branch ${LOFAR_VERSION//latest/master} https://git.astron.nl/ro/lofar.git ${INSTALLDIR}/lofar/src && \
    cd ${INSTALLDIR}/lofar/build/${LOFAR_BUILDVARIANT} && \
    sed -i "s/ABI=0/ABI=${CXX_ABI}/g" ../../src/CMake/variants/GNUCXX11.cmake && \
    cmake -DCMAKE_CXX_FLAGS="${CXX_FLAGS}" -DBUILD_PACKAGES="TBB_CEP" -DBUILD_TESTING=OFF -DCMAKE_INSTALL_PREFIX=${INSTALLDIR}/lofar/ -DPYTHON_EXECUTABLE=/usr/bin/python3 -DCASAREST_ROOT_DIR=${INSTALLDIR}/casarest/ -DCASACORE_ROOT_DIR=${INSTALLDIR}/casacore/ -DBLITZ_ROOT_DIR=${INSTALLDIR}/blitz -DAOFLAGGER_ROOT_DIR=${INSTALLDIR}/aoflagger/ -DBDSF_ROOT_DIR=${INSTALLDIR}/pybdsf/lib/python${PYTHON_VERSION}/site-packages/ -DQPID_ROOT_DIR=/opt/qpid/ -DUSE_OPENMP=True ${INSTALLDIR}/lofar/src/ && \
    make -j ${J} && \
    make install && \
    mkdir -p ${INSTALLDIR}/lofar/var/{log,run} && \
    chmod a+rwx  ${INSTALLDIR}/lofar/var/{log,run} && \
    bash -c "strip ${INSTALLDIR}/lofar/{bin,sbin,lib64}/* || true" && \
    aptitude purge -y ${BUILD_PACKAGES} && \
    aptitude clean && \
    aptitude autoclean

# install additional bashrc files
COPY ["bashrc.d",  "${INSTALLDIR}/bashrc.d/"]
RUN find /opt/ ! -perm -a+r -exec chmod a+r {} +

