import React, {
    Component
} from 'react';

import ReactTableContainer from "react-table-container";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import {setChildPanelData} from 'redux/actions/stationOverviewPageActions'
import classNames from "classnames";

import {componentStatusOK, componentStatusBeyondRepair} from 'utils/LOFARDefinitions';

import GenericTestRow from '../GenericTestRow';
import RTSMRows from '../RTSMRows';

// CSS
import styles from './styles.module.scss';

/**
 * RTSMSummaryLine: create one table row with percentages of errors per antenna.
 */
function TableHeader(props) {
    return (
        <tr>
            <th style={{textAlign: "left"}} scope="col"></th>
            {props.componentIds.map((id, key) => {
                let status = (props.status[id] && props.status[id].status) || null;
                let hasErrorStatus = !componentStatusOK(status);
                let isBeyondRepair = componentStatusBeyondRepair(status);

                let cls = classNames({
                    'status-non-operational': hasErrorStatus && !isBeyondRepair,
                    'status-beyond-repair': isBeyondRepair,
                    [styles.highlight]: (id === props.highlightId)
                });
                return (
                    <th scope="col"
                        id={"comp_"+id}
                        key={id}
                        className={cls}
                        data-has-problems={hasErrorStatus}
                        data-component-id={id}
                        data-data-idx="current">
                        {id}
                    </th>
                );
            }) }
        </tr>
    );
}

/**
 * ComponentType; renders a table of station tests and rtsm data for one component (HBA, RSP, LBH, etc.)
 *
 * Props:
 * station_type: C, R or I
 * type: component type
 * data: Data for this component. Object of { errors, current_status }
 */
class ComponentTypeC extends Component {

    computeComponentIDList(componentType) {
        let componentIDSet = new Set();
        this.props.data.errors && this.props.data.errors.forEach(test => {
            // Get the ID's of components with errors
            Object.keys(test.component_errors).forEach(id => componentIDSet.add(id));

            // Get the ID's of components with a non-operational status
            Object.keys(test.status).forEach(id => {
                let value = test.status[id];
                !componentStatusOK(value.status) && componentIDSet.add(id)
            });
        });

        // Get the ID's of components with a latest non-operational status
        this.props.data.current_status && Object.keys(this.props.data.current_status).forEach(id => {
            let value = this.props.data.current_status[id];
            !componentStatusOK(value.status) && componentIDSet.add(id)
        });

        // Numerical sort
        return Array.from(componentIDSet).sort((a, b) => a - b);
    }


    onMouseOver = (e) => {
        if (e.target.dataset.hasProblems === "true") {
            e.stopPropagation();
            this.onHover(e.target.dataset.dataIdx, e.target.dataset.componentId);
        }
    };

    onMouseOut = (e) => {
        if (e.target.dataset.hasProblems === "true") {
            e.stopPropagation();
            this.onHover(null, null);
        }
    };

    // left-click with mouse
    onClick = (e) => {
        if (e.target.dataset.hasOwnProperty('componentId')) {
            this.props.history.push(`/tiles?antenna_id=${e.target.dataset.componentId}&antenna_type=${this.props.type}&station=${this.props.station_name}`);
        }
    };

    // right-click with mouse
    onContextMenu = (e) => {
        e.preventDefault();
        if (e.target.dataset.hasProblems === "true") {
            this.onHover(e.target.dataset.dataIdx, e.target.dataset.componentId, true);
        }
    };

    // The mouse hovers over a component. Also used for the context menu event (right-click), then
    // doPin will be true
    onHover = (dataIdx, componentId, doPin=false) => {
        let data = {
            component_type: this.props.type,
            component_id: componentId,
            data_idx: dataIdx,
            start_date: null,
            errors: {},
            status: {}
        };

        if (dataIdx === null) {
            // Reset child panel on mouseOut
            data = null;
        }
        else if (dataIdx === "current") {
            // mouseOver header row
            data.status = this.props.data.current_status[componentId];
        }
        else {
            // Default: mouseOver test data row
            let testData = this.props.data.errors[dataIdx];
            data.errors = testData.component_errors[componentId];
            data.status = testData.status[componentId];
            data.test_type = testData.test_type;
            data.start_date = testData.start_date;
        }

        this.props.setChildPanelData(data, doPin);
    };


    renderGenericTestRow(key, data, component_ids) {
        return (<GenericTestRow key={key}
                          test_type="ST"
                          ordered_component_ids={component_ids}
                          component_type={this.props.type}
                          station_name={this.props.station_name}
                          station_type={this.props.station_type}
                          dataIdx={key}
                          data={data} />)
    }

    renderRTSMRows(key, data, component_ids) {
        return ( <RTSMRows key={key}
                           ordered_component_ids={component_ids}
                           component_type={this.props.type}
                           station_name={this.props.station_name}
                           station_type={this.props.station_type}
                           data={data}
                           update={this.updateIfComponentChanges} />)
    }

    renderTestRows(data, component_ids) {
        const rows = [];
        let tmp_rtsm_set = [],
            num_tests = data.length;

        for (let i = 0; i < num_tests; i++) {
            const current_item = data[i],
                  next_test = (i===num_tests-1 ? null : data[i + 1]);

            // Temporarily store RTSM lines
            if (current_item.test_type === 'R') {
                tmp_rtsm_set.push({
                    dataIdx: i,
                    details: current_item
                })

                // Push lines when next item is a station test or when we are at the last item
                if (next_test === null || next_test.test_type === 'S') {
                    rows.push(this.renderRTSMRows(i, tmp_rtsm_set, component_ids))
                    tmp_rtsm_set = []
                }
            }
            else if (current_item.test_type === 'S') {
                rows.push(this.renderGenericTestRow(i, current_item, component_ids));
            }
        }

        return rows
    }

    updateIfComponentChanges = () => {
        this.setState({ state: this.state });
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        // Only rerender when the tab containing this component type is active (for performance)
        if (!nextProps.active) {
            return false;
        }
        return true;
    }

    render() {
        const comp_ids = this.computeComponentIDList(this.props.type);
        let data = this.props.data;
        let highlightId = null;

        if (this.props.highlightData &&
            this.props.highlightData.data_idx === "current" &&
            this.props.highlightData.component_type === this.props.type) {
            highlightId = this.props.highlightData.component_id;
        }

        return (
            <ReactTableContainer width="100%" height={this.props.height+'px'}>
                <table className={styles.comp_type_table+" table-sm table-hover table-bordered"}
                       onContextMenu={this.onContextMenu}
                       onClick={this.onClick}
                       onMouseOver={this.onMouseOver}
                       onMouseOut={this.onMouseOut} >
                    <thead className={styles.comp_type_header}>
                        <TableHeader componentIds={comp_ids} status={data.current_status} highlightId={highlightId} />
                    </thead>
                    <tbody>
                        {this.renderTestRows(data.errors, comp_ids)}
                    </tbody>
                </table>
            </ReactTableContainer>
        );
    }
}

// ComponentType is connected to Redux store
const ComponentType = withRouter(connect(state => {
    return {
        ...state.station_page.main_panel
    };
}, {
    setChildPanelData
})(ComponentTypeC));

export default ComponentType;
