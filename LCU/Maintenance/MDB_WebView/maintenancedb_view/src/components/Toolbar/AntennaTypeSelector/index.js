import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, ButtonGroup} from 'reactstrap';
import {setAntennaType} from "redux/actions/mainFiltersActions";

// History handling
import { push } from 'connected-react-router';
import { store } from "redux/store.js";

// CSS
import toolbarStyles from '../styles.module.scss'


/**
 * Radio buttons for Antenna Type
 */
class AntennaTypeSelectorC extends Component {

    onAntennaTypeClick = (e) => {
        let type = e.currentTarget.innerHTML;
        //this.props.setAntennaType(type);

        store.dispatch(push(`?antenna_type=${type}`))
    }

    render() {
        return (<div className={toolbarStyles.toolbar_ctrl}>
            <label>Type</label>
            <ButtonGroup size="sm">
                <Button color="info" onClick={this.onAntennaTypeClick} active={this.props.antennaType === 'HBA'}>HBA</Button>
                <Button color="info" onClick={this.onAntennaTypeClick} active={this.props.antennaType === 'LBH'}>LBH</Button>
                <Button color="info" onClick={this.onAntennaTypeClick} active={this.props.antennaType === 'LBL'}>LBL</Button>
            </ButtonGroup>
        </div>);
    }
}

const mapStateAntennaTypeSelector = state => {
    return {
        antennaType: state.mainFilters.antennaType
    };
};

const mapDispatchAntennaTypeSelector = {
    setAntennaType
};

export const AntennaTypeSelector = connect(mapStateAntennaTypeSelector, mapDispatchAntennaTypeSelector)(AntennaTypeSelectorC);
