
let styleElement = null;
let curClassName = null;


/* Boiler plate for a Grid panel.
   Looks like a React component but is ordinary function! */
function highlightClass(className) {

    if (!styleElement) {
        styleElement = document.createElement('style')
        document.head.appendChild(styleElement)
    }

    for (let i=0; i<styleElement.sheet.cssRules.length; i++){
        styleElement.sheet.deleteRule(0);
    }

    // Insert a CSS Rule to the sheet at position 0.
    styleElement.sheet.insertRule(`.${className} { background-color: #8d8d8d; color: white; }`, 0);


    curClassName = className;
}

function unHighlightClass(className) {

    if (styleElement.sheet.cssRules.length > 0 && className === curClassName) {
        styleElement.sheet.deleteRule(0);

        curClassName = null;
    }
}

export { highlightClass, unHighlightClass };
