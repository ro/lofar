#! /usr/bin/env python

# This file list the start and stoptime of the most recent LOFAR observations

# import os and bfdata package
#import bfdata as bf
from datetime import datetime
from datetime import timedelta
from lofar_tmss_client.tmss_http_rest_client import TMSSsession

def currentobs(offset=0):
    """ Get's a task list with current tasks.
    * offset * offset in seconds. Use a negative value to get an observation in the past
    """
    d=timedelta(0,offset)
    with TMSSsession.create_from_dbcreds_for_ldap() as tmss_client:
        observation_subtasks = tmss_client.get_subtasks(subtask_type='observation',
                                                        scheduled_stop_time_greater_then=datetime.utcnow()+d,
                                                        scheduled_start_time_less_then=datetime.utcnow()+d)

        if len(observation_subtasks)==0:
            return "None"
        elif len(observation_subtasks)==1:
            return observation_subtasks[0]['id']
        elif len(observation_subtasks)>1:
            # change this to check for stations involved
            return observation_subtasks[0]['id']

def get_all_parameters_new(obsid, useFilename=False):
    if useFilename:
        print("Function absolute, please us obsid")
        assert False
    if type(obsid) == type(""):
        obsid=int(obsid.strip('L'))

    with TMSSsession.create_from_dbcreds_for_ldap() as tmss_client:
        parset = tmss_client.get_subtask_parset(obsid)
        # the tmss parset is a plain text string
        # tbb scripts expect a dict. convert it.
        parset = {parts[0].strip():parts[2].strip() for parts in [line.partition('=') for line in parset.splitlines()]}
        for k in list(parset.keys()):
            parset[k.replace("ObsSW.","")]=parset[k]
        return parset

if __name__ == '__main__':
    print(currentobs())
