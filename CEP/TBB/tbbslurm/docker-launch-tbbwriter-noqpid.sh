#!/bin/bash
#exec docker run \
#exec ~mol/docker-run-killable.sh \
exec docker-run-slurm.sh \
    --rm --net=host \
    --stop-signal=SIGKILL \
    -u `id -u` -e USER=$USER -e HOME=$HOME \
    -e SLURM_JOB_ID=$SLURM_JOB_ID \
    -v $HOME/.ssh:$HOME/.ssh:ro \
    -v /data/projects:/data/projects \
    -v /data/log:/data/log \
    -v /globaldata/tbb:/globaldata/tbb \
    --cap-add=sys_nice --cap-add=sys_admin \
    lofar-tbbwriter:IOprio /opt/lofarsoft/release/bin/TBBraw2h5 -P31664 -P31665 -P31666 -P31667 -P31668 -P31669 -P31670 -P31671 -P31672 -P31673 -P31674 -P31675 -K -V -M -R10 -O/data/projects/LT10_017/tbb/ --observer 'S. Buitink' --project LT10_017 --observationID L000000 --filterSelection LBA_10_90 --antennaSet LBA_OUTER

