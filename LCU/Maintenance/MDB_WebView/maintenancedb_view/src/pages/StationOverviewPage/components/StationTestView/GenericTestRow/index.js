import React, {
    Component
} from 'react';
import {connect} from "react-redux";
import classNames from "classnames";

import {formatDateUTC} from 'utils/utils'
import GenericStatusTd from '../GenericStatusTd'

// CSS
import styles from './styles.module.scss'


/**
 * GenericTestRow: renders a table row with the results of one station test or RTSM run.
 */
class GenericTestRowC extends Component {

    doHighlight = false;

    shouldHighlight(highlightData) {
        const props = this.props;

        return highlightData !== null &&
            highlightData.component_type === props.component_type &&
            highlightData.start_date === props.data.start_date;
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        // this.doHighlight will only be true for the previously and currently selected row
        if (nextProps.highlightData !== this.props.highlightData) {
            if (!this.doHighlight && !this.shouldHighlight(nextProps.highlightData))
                return false;
        }
        return true;
    }

    render() {
        let component_ids = this.props.ordered_component_ids;
        let componentErrors = this.props.data.component_errors;  // errors from the test
        let componentStatus = this.props.data.status;  // component status from WinCC
        let date = formatDateUTC(this.props.data.start_date);

        // Determine if this row needs to be highlighted
        this.doHighlight = this.shouldHighlight(this.props.highlightData);

        let cls = classNames({
            [styles.testrow]: true,
            [styles.highlight]: this.doHighlight
        });

        return (
            <tr className={cls}>
                <td className={styles.testrow_header}>{this.props.test_type} {date}</td>
                {component_ids.map((component_id, key) =>
                    <GenericStatusTd
                        key={key}
                        dataIdx={this.props.dataIdx}
                        doHighlight={this.doHighlight && component_id === this.props.highlightData.component_id}
                        component_id={component_id}
                        status={componentStatus.hasOwnProperty(component_id) ? componentStatus[component_id].status : null}
                        numErrors={componentErrors.hasOwnProperty(component_id) ? Object.keys(componentErrors[component_id]).length : 0} />
                )}
            </tr>
        );
    }
}

// TestLine is connected to Redux store
const GenericTestRow = connect(state => {
    return {
        ...state.station_page.main_panel
    };
}, null)(GenericTestRowC);


export default GenericTestRow;
