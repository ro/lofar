
var RESTAPI = {
   api_url:'/api',
   entity_map: {
      station : 'stationtests/station',
      station_test: 'stationtests/station_test'
   },

   format: '?format=json',

   composeBaseUrl(entity_name){
      return this.api_url + '/' + this.entity_map[entity_name] + '/' + this.format
   },

   composeQuery(parameters){
      let query_string = ''
      for (let key in parameters){
         if (parameters[key] !== ''){
            query_string += '&' + key + '=' + parameters[key]
         }
      }
      return query_string
   },

   get_url(entity_name, page, parameters){
      parameters['page'] = page;
      return this.composeBaseUrl(entity_name) +
        this.composeQuery(parameters);
   }
}

export default RESTAPI;
