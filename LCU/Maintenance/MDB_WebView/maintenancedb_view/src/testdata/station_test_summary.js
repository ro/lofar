const stdata = [
    {
        "station_name": "CS002C",
        "total_component_errors": 18,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:34Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "LBL": {
                "DOWN": 3,
                "HIGH_NOISE": 1
            },
            "HBA": {
                "JITTER": 3,
                "RF_FAIL": 3,
                "MODEM": 1,
                "HIGH_NOISE": 1
            },
            "LBH": {
                "RF_FAIL": 2,
                "DOWN": 2,
                "HIGH_NOISE": 2
            }
        }
    }, {
        "station_name": "CS004C",
        "total_component_errors": 11,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:24Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
        }
    }, {
        "station_name": "CS005C",
        "total_component_errors": 10,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:53:18Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "HIGH_NOISE": 3,
                "MODEM": 3,
                "RF_FAIL": 2
            },
            "LBH": {
                "FLAT": 1
            },
            "TBB": {
                "TEMPERATURE": 1
            }
        }
    }, {
        "station_name": "CS006C",
        "total_component_errors": 15,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:46:26Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "JITTER": 4,
                "MODEM": 3,
                "RF_FAIL": 2,
                "HIGH_NOISE": 2
            },
            "LBH": {
                "RF_FAIL": 2,
                "HIGH_NOISE": 1
            },
            "TBB": {
                "TEMPERATURE": 1
            }
        }
    }, {
        "station_name": "CS007C",
        "total_component_errors": 10,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:19Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "RF_FAIL": 2,
                "JITTER": 2,
                "HIGH_NOISE": 2,
                "MODEM": 1
            },
            "LBL": {
                "DOWN": 1
            },
            "LBH": {
                "DOWN": 1,
                "HIGH_NOISE": 1
            }
        }
    }, {
        "station_name": "CS021C",
        "total_component_errors": 20,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:25Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "SPURIOUS": 3,
                "OSCILLATION": 2,
                "RF_FAIL": 2,
                "HIGH_NOISE": 2,
                "MODEM": 2
            },
            "LBH": {
                "FLAT": 3,
                "RF_FAIL": 2,
                "LOW_NOISE": 1
            },
            "LBL": {
                "FLAT": 2,
                "LOW_NOISE": 1
            }
        }
    }, {
        "station_name": "CS026C",
        "total_component_errors": 23,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:32Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "RF_FAIL": 8,
                "MODEM": 3,
                "HIGH_NOISE": 3,
                "C_SUMMATOR": 1,
                "JITTER": 1,
                "SPURIOUS": 1
            },
            "LBH": {
                "HIGH_NOISE": 3,
                "LOW_NOISE": 1
            },
            "LBL": {
                "RF_FAIL": 1
            },
            "TBB": {
                "MEMORY": 1
            }
        }
    }, {
        "station_name": "CS028C",
        "total_component_errors": 36,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:21Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "RF_FAIL": 7,
                "HIGH_NOISE": 3,
                "SUMMATOR_NOISE": 3,
                "MODEM": 1,
                "OSCILLATION": 1,
                "C_SUMMATOR": 1
            },
            "LBL": {
                "FLAT": 5,
                "SPURIOUS": 2,
                "DOWN": 2,
                "RF_FAIL": 2,
                "SHORT": 1,
                "LOW_NOISE": 1
            },
            "LBH": {
                "DOWN": 2,
                "HIGH_NOISE": 2,
                "OSCILLATION": 1,
                "SPURIOUS": 1,
                "FLAT": 1
            }
        }
    }, {
        "station_name": "CS030C",
        "total_component_errors": 26,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:31Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "RF_FAIL": 4,
                "HIGH_NOISE": 3,
                "JITTER": 2,
                "MODEM": 2,
                "C_SUMMATOR": 1,
                "LOW_NOISE": 1,
                "SPURIOUS": 1
            },
            "LBH": {
                "LOW_NOISE": 2,
                "SHORT": 2,
                "FLAT": 1
            },
            "LBL": {
                "SHORT": 1,
                "FLAT": 1,
                "LOW_NOISE": 1
            },
            "RSP": {
                "TEMPERATURE": 1
            },
            "TBB": {
                "MEMORY": 1,
                "VERSION": 1,
                "VOLTAGE": 1
            }
        }
    }, {
        "station_name": "CS031C",
        "total_component_errors": 26,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:35Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "RF_FAIL": 6,
                "MODEM": 4,
                "HIGH_NOISE": 1,
                "LOW_NOISE": 1,
                "SPURIOUS": 1
            },
            "LBH": {
                "FLAT": 3,
                "SPURIOUS": 1,
                "DOWN": 1
            },
            "LBL": {
                "LOW_NOISE": 2,
                "DOWN": 1,
                "FLAT": 1
            },
            "TBB": {
                "MEMORY": 1,
                "TEMPERATURE": 1,
                "VERSION": 1,
                "VOLTAGE": 1
            }
        }
    }, {
        "station_name": "CS032C",
        "total_component_errors": 5,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:46:02Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "MODEM": 2,
                "JITTER": 1,
                "RF_FAIL": 1
            },
            "LBH": {
                "JITTER": 1
            }
        }
    }, {
        "station_name": "CS103C",
        "total_component_errors": 17,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:23Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "LBH": {
                "DOWN": 3,
                "RF_FAIL": 2,
                "FLAT": 1
            },
            "HBA": {
                "RF_FAIL": 3,
                "MODEM": 1,
                "C_SUMMATOR": 1
            },
            "LBL": {
                "DOWN": 2,
                "FLAT": 2,
                "RF_FAIL": 1,
                "LOW_NOISE": 1
            }
        }
    }, {
        "station_name": "CS302C",
        "total_component_errors": 26,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:17Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "RF_FAIL": 9,
                "MODEM": 7,
                "HIGH_NOISE": 3,
                "SUMMATOR_NOISE": 3,
                "JITTER": 2,
                "SPURIOUS": 1
            },
            "LBL": {
                "FLAT": 1
            }
        }
    }, {
        "station_name": "CS501C",
        "total_component_errors": 36,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:22Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "HIGH_NOISE": 10,
                "MODEM": 5,
                "RF_FAIL": 5,
                "JITTER": 2
            },
            "LBH": {
                "FLAT": 2,
                "DOWN": 1,
                "HIGH_NOISE": 1,
                "RF_FAIL": 1,
                "SPURIOUS": 1
            },
            "LBL": {
                "DOWN": 1,
                "FLAT": 1,
                "SHORT": 1
            },
            "RSP": {
                "TEMPERATURE": 1
            },
            "TBB": {
                "MEMORY": 1,
                "TEMPERATURE": 1,
                "VERSION": 1,
                "VOLTAGE": 1
            }
        }
    }, {
        "station_name": "DE601C",
        "total_component_errors": 18,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:23Z",
        "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 S7 E7 TV TBC TM",
        "component_error_summary": {
            "RSP": {
                "TEMPERATURE": 7
            },
            "LBH": {
                "LOW_NOISE": 2,
                "DOWN": 2,
                "FLAT": 2,
                "HIGH_NOISE": 1,
                "RF_FAIL": 1
            },
            "HBA": {
                "MODEM": 2,
                "HIGH_NOISE": 1
            }
        }
    }, {
        "station_name": "DE602C",
        "total_component_errors": 6,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:34Z",
        "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "MODEM": 3
            },
            "LBH": {
                "HIGH_NOISE": 2,
                "JITTER": 1
            }
        }
    }, {
        "station_name": "DE603C",
        "total_component_errors": 3,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:43:42Z",
        "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E5 TV TBC TM",
        "component_error_summary": {
            "RSP": {
                "TEMPERATURE": 2
            },
            "LBH": {
                "RF_FAIL": 1
            }
        }
    }, {
        "station_name": "DE604C",
        "total_component_errors": 1,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:50:07Z",
        "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E5 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "RF_FAIL": 1
            }
        }
    }, {
        "station_name": "DE605C",
        "total_component_errors": 11,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:28:50Z",
        "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E5 TV TBC TM",
        "component_error_summary": {
            "LBH": {
                "DOWN": 2,
                "RF_FAIL": 2,
                "HIGH_NOISE": 1
            },
            "HBA": {
                "SUMMATOR_NOISE": 2,
                "MODEM": 2,
                "OSCILLATION": 1
            },
            "RSP": {
                "TEMPERATURE": 1
            }
        }
    }, {
        "station_name": "DE609C",
        "total_component_errors": 6,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:39:15Z",
        "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E5 TV TBC TM",
        "component_error_summary": {
            "TBB": {
                "VERSION": 2,
                "VOLTAGE": 2,
                "MEMORY": 1
            },
            "LBH": {
                "RF_FAIL": 1
            }
        }
    }, {
        "station_name": "IE613C",
        "total_component_errors": 23,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:36:21Z",
        "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E5 TV TBC TM",
        "component_error_summary": {
            "LBH": {
                "RF_FAIL": 16,
                "DOWN": 4
            },
            "HBA": {
                "SPURIOUS": 2
            },
            "TBB": {
                "MEMORY": 1
            }
        }
    }, {
        "station_name": "PL610C",
        "total_component_errors": 56,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:42Z",
        "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "LBH": {
                "RF_FAIL": 28,
                "DOWN": 7,
                "LOW_NOISE": 1,
                "SPURIOUS": 1
            },
            "HBA": {
                "SPURIOUS": 19
            }
        }
    }, {
        "station_name": "PL611C",
        "total_component_errors": 54,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:47:53Z",
        "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "SPURIOUS": 47
            },
            "LBH": {
                "RF_FAIL": 2,
                "DOWN": 1
            },
            "TBB": {
                "MEMORY": 1,
                "VERSION": 1,
                "VOLTAGE": 1
            },
            "RSP": {
                "TEMPERATURE": 1
            }
        }
    }, {
        "station_name": "PL612C",
        "total_component_errors": 29,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:36Z",
        "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "SPURIOUS": 28
            },
            "LBH": {
                "RF_FAIL": 1
            }
        }
    }, {
        "station_name": "RS205C",
        "total_component_errors": 16,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:53:46Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "HIGH_NOISE": 4,
                "MODEM": 3,
                "RF_FAIL": 2,
                "SPURIOUS": 1,
                "JITTER": 1
            },
            "LBL": {
                "HIGH_NOISE": 3
            },
            "LBH": {
                "HIGH_NOISE": 1,
                "RF_FAIL": 1
            }
        }
    }, {
        "station_name": "RS210C",
        "total_component_errors": 9,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:28Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "RF_FAIL": 4,
                "HIGH_NOISE": 3,
                "MODEM": 1
            },
            "LBL": {
                "FLAT": 1
            }
        }
    }, {
        "station_name": "RS305C",
        "total_component_errors": 19,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:27Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "RF_FAIL": 5,
                "C_SUMMATOR": 1,
                "JITTER": 1,
                "HIGH_NOISE": 1
            },
            "TBB": {
                "MEMORY": 3,
                "VOLTAGE": 3,
                "VERSION": 3
            },
            "LBH": {
                "RF_FAIL": 2
            }
        }
    }, {
        "station_name": "RS306C",
        "total_component_errors": 41,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:46Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "HIGH_NOISE": 14,
                "RF_FAIL": 9,
                "MODEM": 7,
                "JITTER": 4,
                "SPURIOUS": 1,
                "C_SUMMATOR": 1
            },
            "LBL": {
                "FLAT": 2,
                "RF_FAIL": 1,
                "DOWN": 1
            },
            "LBH": {
                "RF_FAIL": 1
            }
        }
    }, {
        "station_name": "RS307C",
        "total_component_errors": 38,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:26Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "HIGH_NOISE": 10,
                "MODEM": 9,
                "JITTER": 7,
                "RF_FAIL": 4,
                "SPURIOUS": 1
            },
            "LBH": {
                "HIGH_NOISE": 1,
                "JITTER": 1,
                "LOW_NOISE": 1,
                "RF_FAIL": 1,
                "SHORT": 1,
                "DOWN": 1,
                "FLAT": 1
            }
        }
    }, {
        "station_name": "RS407C",
        "total_component_errors": 3,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:30Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "MODEM": 1
            },
            "LBL": {
                "HIGH_NOISE": 1
            },
            "TBB": {
                "TEMPERATURE": 1
            }
        }
    }, {
        "station_name": "RS409C",
        "total_component_errors": 17,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:51:06Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "SPURIOUS": 4,
                "RF_FAIL": 2,
                "SUMMATOR_NOISE": 2,
                "HIGH_NOISE": 1,
                "JITTER": 1,
                "OSCILLATION": 1
            },
            "LBH": {
                "SPURIOUS": 3,
                "FLAT": 1,
                "DOWN": 1
            },
            "TBB": {
                "MEMORY": 1
            }
        }
    }, {
        "station_name": "RS503C",
        "total_component_errors": 56,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:20Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "RF_FAIL": 17,
                "HIGH_NOISE": 16,
                "MODEM": 6,
                "JITTER": 3,
                "C_SUMMATOR": 1,
                "LOW_NOISE": 1,
                "SPURIOUS": 1
            },
            "LBH": {
                "RF_FAIL": 2,
                "DOWN": 1,
                "HIGH_NOISE": 1
            },
            "LBL": {
                "DOWN": 1,
                "FLAT": 1,
                "HIGH_NOISE": 1,
                "LOW_NOISE": 1,
                "RF_FAIL": 1,
                "SHORT": 1
            },
            "TBB": {
                "MEMORY": 1
            }
        }
    }, {
        "station_name": "RS508C",
        "total_component_errors": 2,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:16Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "LBL": {
                "FLAT": 1,
                "LOW_NOISE": 1
            }
        }
    }, {
        "station_name": "RS509C",
        "total_component_errors": 7,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:39Z",
        "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
        "component_error_summary": {
            "LBH": {
                "FLAT": 2,
                "LOW_NOISE": 1
            },
            "HBA": {
                "C_SUMMATOR": 1,
                "HIGH_NOISE": 1,
                "MODEM": 1
            },
            "LBL": {
                "FLAT": 1
            }
        }
    }, {
        "station_name": "SE607C",
        "total_component_errors": 24,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:48:55Z",
        "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 S7 E7 TV TBC TM",
        "component_error_summary": {
            "HBA": {
                "SPURIOUS": 21
            },
            "LBH": {
                "RF_FAIL": 1
            },
            "RSP": {
                "TEMPERATURE": 1
            },
            "TBB": {
                "MEMORY": 1
            }
        }
    }, {
        "station_name": "UK608C",
        "total_component_errors": 12,
        "date": "2018-10-10",
        "start_datetime": "2018-10-10T20:45:00Z",
        "end_datetime": "2018-10-11T00:52:29Z",
        "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 S7 E7 TV TBC TM",
        "component_error_summary": {
            "SPU": {
                "VOLTAGE": 4
            },
            "LBH": {
                "RF_FAIL": 3
            },
            "TBB": {
                "TEMPERATURE": 1,
                "VERSION": 1,
                "VOLTAGE": 1,
                "MEMORY": 1
            },
            "HBA": {
                "MODEM": 1
            }
        }
    }
];

export default stdata;
