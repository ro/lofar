#! /usr/bin/env python

# This file list the start and stoptime of the most recent LOFAR observations

# import os and bfdata package
import os
#import bfdata as bf
import time
import glob

def convertLatestParset(latestParset,derivelogdir=False):
    if not derivelogdir:
        logdir="/opt/lofar/tbb/parsets/"
    else:
        logdir=os.path.split(latestParset)[0]
    return os.path.join(logdir,os.path.split(os.path.realpath(latestParset))[1])

def get_all_parameters_new(obsid, useFilename=False):
    """Get the most important observation parameters. Returns a dictionary with these parameters.
    *obsid*        Observation id, f.e. L2010_08834 or D2009_16234. Only works on lofar cluster.
    *useFilename*  If set to true, obsid is filename of the parset.
    """
    if useFilename:
        parsetfilename=obsid
        if os.path.islink(parsetfilename) and 'latest' in parsetfilename:
            parsetfilename=convertLatestParset(parsetfilename)
    else:
        # What is the observation number? This determines the filename
        if '_' in obsid:
            obsnr=obsid.split('_')[1]
        else:
            obsnr=obsid.strip('L')
        # Name of the parset file
        parsetfilename='/opt/lofar/tbb/parsets/L'+obsnr+'.parset'
        if 'latest' == obsid:
            parsetfilename='/opt/lofar/tbb/parsets/latest.parset'
        if os.path.islink(parsetfilename) and 'latest' in parsetfilename:
            parsetfilename=convertLatestParset(parsetfilename)
        if not os.path.isfile(parsetfilename):
            parsetfilename='/opt/lofar/tbb/parsets/L'+obsnr+'/L'+obsnr+'.parset'
        if not os.path.isfile(parsetfilename):
            parsetfilename='/opt/lofar/tbb/parsets/'+obsid+'/RTCP.parset.0'
        if not os.path.isfile(parsetfilename):
            parsetfilename='/opt/lofar/tbb/parsets/latest'


    # Open the file
    parsetfile=open(parsetfilename,'r')
    parsetfile.seek(0)
    # Read in the parameters from the parset file
    allparameters={}
    for line in parsetfile:
        str2 = line.split('=',2)
        if len(str2)>=2:
            allparameters[str2[0].rstrip(' ')] = str2[1].strip('\n').lstrip(' ').rstrip('"').lstrip('"')
        else:
            allparameters[str2[0].rstrip(' ')] = "UNDEFINED"
        if  len(allparameters[str2[0].rstrip(' ')])<1:
            allparameters[str2[0].rstrip(' ')] = "UNDEFINED"

    return allparameters

def currentobs(prevobsid=None):
    # Directory of latest parset files
    #logdir='/opt/lofar/tbb/parsets/'
    #logdir='/home/veen/tbbdump/locustest/latest/'
    logdir='/opt/lofar/tbb/parsets/'
    parsetfilename=os.path.join(logdir,'latest')
    parsetfilename=convertLatestParset(parsetfilename)
    #for i in range(5):
    #    try:
    #        parsetfilename=glob.glob(logdir+"/*.parset")[-1]
    #        break
    #    except:
    #        time.sleep(5)
    p=get_all_parameters_new(parsetfilename,useFilename=True)

    # Get the files in this directory
    #for i in range(5):
    #    if os.path.isdir(logdir):
    #        obs=filter(os.path.isfile, glob.glob(logdir + "/*.parset"))
    #        break
    #    else:
    #        if i<4:
    #            time.sleep(60)
    #        else:
    #            print "Parset dir not present."
    #            return "None"

    # Put them in the right order
    #obs.sort(key=lambda x: os.path.getmtime(x), reverse=True)

    #if prevobsid != "None":
    #    if os.path.isfile(logdir+"/"+prevobsid+'.parset'):
    #        obs=[logdir+"/"+prevobsid+'.parset']+obs
    def time_to_second(timestring):
        if len(timestring)==19:
            year=int(timestring[0:4])
            month=int(timestring[5:7])
            day=int(timestring[8:10])
            hour=int(timestring[11:13])
            minute=int(timestring[14:16])
            second=int(timestring[17:19])
            return time.mktime((year,month,day,hour,minute,second,0,0,0))
        elif len(timestring)==21:
            year=int(timestring[1:5])
            month=int(timestring[6:8])
            day=int(timestring[9:11])
            hour=int(timestring[12:14])
            minute=int(timestring[15:17])
            second=int(timestring[18:20])
            return time.mktime((year,month,day,hour,minute,second,0,0,0))
        else:
            print("WRONG LENGTH FOR TIMESTRING",timestring)
            assert False



    # Loop over all files
    #for num,file in enumerate(obs):
        # There are also non parset files in the directory
    #    if os.path.getsize(file) > 0 and 'swp' not in file:
        # Print start and stoptime of the observations.
            #print "Opening",file
    if "Observation.startTime" in list(p.keys()):
        starttime=p["Observation.startTime"]
    else:
        print("Invalid logfile",logdir,file)
        return "None"
    if "Observation.stopTime" in list(p.keys()):
        stoptime=p["Observation.stopTime"]
    else:
        print("Invalid logfile",logdir,file)
        return "None"
    starttime=time_to_second(starttime)
    stoptime=time_to_second(stoptime)
    currenttime=time.time()
    if starttime  < currenttime  < stoptime:
        return "L"+p["Observation.ObsID"]

    return "None"

if __name__ == '__main__':
    print(currentobs())
