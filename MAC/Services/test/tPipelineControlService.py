import unittest

from unittest import mock
from lofar.mac.PipelineControl import PipelineControlServiceHandler


class TestPipelineControlServiceHandler(unittest.TestCase):
    def setUp(self):
        self.pipeline_control_handler = PipelineControlServiceHandler()
        self.pipeline_control_handler.slurm = mock.MagicMock()

    def test_returns_True_if_pipeline_not_running(self):
        self.pipeline_control_handler.slurm.isQueuedOrRunning.return_value = False
        
        canceled = self.pipeline_control_handler.cancel_pipeline(12345)

        self.assertTrue(canceled["canceled"])

    def test_returns_True_if_pipeline_is_running(self):
        self.pipeline_control_handler.slurm.isQueuedOrRunning.return_value = True
        
        canceled = self.pipeline_control_handler.cancel_pipeline(12345)

        self.assertTrue(canceled["canceled"])

    def test_slurm_cancel_is_not_called_if_pipeline_not_running(self):
        self.pipeline_control_handler.slurm.isQueuedOrRunning.return_value = False
        
        self.pipeline_control_handler.cancel_pipeline(12345)

        self.assertFalse(self.pipeline_control_handler.slurm.cancel.called)

    def test_slurm_cancel_is_called_if_pipeline_is_running(self):
        self.pipeline_control_handler.slurm.isQueuedOrRunning.return_value = True
        
        self.pipeline_control_handler.cancel_pipeline(12345)

        self.assertTrue(self.pipeline_control_handler.slurm.cancel.called)

    def test_slurm_cancels_abort_trigger_first_if_pipeline_is_running(self):
        self.pipeline_control_handler.slurm.isQueuedOrRunning.return_value = True
        
        self.pipeline_control_handler.cancel_pipeline(12345)

        self.assertEquals(
            self.pipeline_control_handler.slurm.cancel.call_args_list,
            [mock.call("12345-abort-trigger"), mock.call("12345")]
        )


if __name__ == '__main__':
    unittest.main()
