import time

# List with allowed stations. Can also be "CS" for all corestation (but not ["CS"]) or "all" for all stations.

brokenstations=[] # used for tests 20181016

allowedlist=['CS001C', 'CS002C', 'CS003C', 'CS004C',  'CS005C', 'CS006C', 'CS007C','CS011C', 'CS013C', 'CS017C', 'CS021C', 'CS024C', 'CS026C' ,'CS028C', 'CS030C',  'CS031C' , 'CS032C', 'CS101C', 'CS103C', 'CS201C', 'CS301C',  'CS302C', 'CS401C', 'CS501C' ]

# TRIGGERING is not allowed by default for the following projects
disallowedProjects=['Pulsars','Pulsars2'] # currently not functioning. Reading piggyback allowed key for these projects.

# BY DEFAULT TRIGGERING IS NOW ALLOWED.
# TRIGGERING is allowed by default in the following projects
allowedProjects=['CosmicRays','LEA076','MSSS','LEA052','LEA036','LC1_006']

# TO AUTOMATICALLY DISABLE TRIGGERING, set this key to False.
donotDumpTBBs=False

# AS AN EMERGENCY STOP, set this key to True. At the next trigger the program will stop running. DO NOT USE THIS LIGHTLY. To just not allow the dumping of TBBs, set the above key to True. TBBs will still receive a stop command then, but no data is send. Contact s.terveen@astro.ru.nl if you have shut down the program, to restart it.
stopRunning=False





# Allowed trigger sources, possible sources: LORA, manual, FRATS
allowedsources=['manual','validation','LORA','FRATS','FR606','Effelsberg','lightning'] #FRATS, 'lightning'
skipLORALOTAAS=True
maxidletime=150 # seconds. Maximum dumptime an RCU could take, dumping from this station is stopped if this takes too long. Normal dumptime per RCU for a full station is 30 seconds (or 120 seconds with the extended memory), but this can be slower. This timer is incase a station is hung. Default time: 600 seconds.
waittime=3900 # Time to wait for an answer, if requisted for manual dumps
notworkinglist=[]#['RS409C','RS307C']
newTBBDriverStations=["CS031C"] # obsolete, all stations have a new driver.
reroute=dict()
#reroute['RS106C']='cp001'
#reroute['CS032C']='locus037'
pagesDict=dict() # set pages for each project
waitforresponse=dict() # set of project needs a manual response
dumpmethod=dict() # set dump method for each project
stationlist=dict()
rcuDict=dict()
# Availables read methods:
# read (pages before and after timestamp),
# readall, (pages from end)
# readbefore (pages before timestamp )
# readafter ( pages after timestamp )
pagesDict['LORA']=200
pagesDict['FRATS']=350000 #00
pagesDict['manual']=200
pagesDict['validation']=2000 #pagesDict['LORA']
pagesDict['UK608']=0
pagesDict['FR606']=200
pagesDict['default']=200
pagesDict['Effelsberg']=900000
pagesDict['EffelsbergTest']=200 # 100 ms
pagesDict['lightning']=200000#4000000
stationlist['default']=allowedlist
stationlist['manual']=allowedlist
stationlist['lightning']='all'
stationlist['FR606']='all' #['RS106C']
stationlist['LORA']='CS'
stationlist['Effelsberg']=['CS001C', 'CS002C', 'CS003C', 'CS004C',  'CS005C', 'CS006C', 'CS007C','CS011C', 'CS013C', 'CS017C', 'CS021C', 'CS024C', 'CS026C' ,'CS028C', 'CS030C',  'CS031C' , 'CS032C', 'CS101C',  'CS201C', 'CS301C', 'CS302C', 'CS401C', 'CS501C' , 'CS103C' , 'RS106C', 'RS205C', 'RS208C', 'RS210C', 'RS305C', 'RS306C', 'RS307C', 'RS310C', 'RS406C', 'RS407C', 'RS409C', 'RS503C', 'RS508C', 'RS509C']
stationlist['EffelsbergTest']=['CS001C', 'CS002C', 'CS003C', 'CS004C',  'CS005C', 'CS006C', 'CS007C','CS011C', 'CS013C', 'CS017C', 'CS021C', 'CS024C', 'CS026C' ,'CS028C', 'CS030C',  'CS031C' , 'CS032C', 'CS101C',  'CS201C', 'CS301C', 'CS302C', 'CS401C', 'CS501C' , 'CS103C' , 'RS106C', 'RS205C', 'RS208C', 'RS210C', 'RS305C', 'RS306C', 'RS307C', 'RS310C', 'RS406C', 'RS407C', 'RS409C', 'RS503C', 'RS508C', 'RS509C']
rcuDict['default']=list(range(0,96))
rcuDict['lightning']=sorted(list(range(14,96,16))+list(range(15,96,16)))
rcuDict['manual']=list(range(0,96)) #sorted(range(14,96,16)+range(15,96,16))
dumpmethod['LORA']='read'
dumpmethod['validation']='readafter' #dumpmethod['LORA']
dumpmethod['FRATS']='readbefore'
dumpmethod['manual']='readbefore'
dumpmethod['default']='read'
dumpmethod['lightning']='read'
#dumpmethod['Effelsberg']='readbefore'
dumpmethod['Effelsberg']='readbefore'
dumpmethod['EffelsbergTest']='readbefore'
dumpmethod['FR606']='readbefore'
waitforresponse['LORA']=False
waitforresponse['validation']=False
waitforresponse['FRATS']=True
waitforresponse['manual']=False
waitforresponse['default']=False
waitforresponse['Effelsberg']=True
waitforresponse['EffelsbergTest']=True
waitforresponse['lightning']=False


def isallowed(p):
    """Define here under what conditions the observation is allowed, given a
    library with observation parameters p, that can be obtained from the
    currentobs.get_all_parameters_new(obsid) function"""
    if donotDumpTBBs:
        return False
    if stopRunning:
        quit()
    if not 'Observation.Campaign.name' in list(p.keys()):
        return False
    if p['triggersource'] not in allowedsources:
        return False
    if skipLORALOTAAS and p['triggersource']=="LORA" and p['Observation.Campaign.name'] in ["COMMISSIONING2015","2015LOFAROBS_new","LC6_009"]:
        # No LORA triggers during LOTAAS observations?
        return False
#    if p['Observation.Campaign.name'] in disallowedProjects:
#        return False

#allowedlist="all"
##### CAMPAIGN CONDITION. LEA076 should be in this list for VHECR observations
    if p['Observation.Campaign.name'] in allowedProjects:
        return True
    else:
        if p['Observation.ObsID']=='42350' or p['Observation.ObsID']=='61578':
            return True
        #else:
        #    return False
##### END CAMPAIGN CONDITION

##### TIME CONDITION AND ALL RCU REQUIREMEN
        starttime=time.mktime((2012,6,30,10,0,0,0,0,0))
        endtime=time.mktime((2012,6,30,20,0,0,0,0,0))

        if starttime <  time.time() < endtime:
            return True
    if 'Observation.tbbPiggybackAllowed' in list(p.keys()):
        if p['Observation.tbbPiggybackAllowed'] == 'true' and p['Observation.Campaign.name'] not in ['LEA036']: # Excluding FRATS for the moment
            return True
    if 'Observation.ObservationControl.StationControl.tbbPiggybackAllowed' in list(p.keys()):
        if p['Observation.ObservationControl.StationControl.tbbPiggybackAllowed'] == 'true' and p['Observation.Campaign.name'] not in ['LEA036']: # Excluding FRATS for the moment
            return True

    return False
###### END TIME CONDITION

###### OBSERVATION TYPE CONDITION
    #if p['Observation.processSubtype'] in ["'TBB (standalone)'"]:
    #    return True
    #else:
    #    return False

###### END OBSERVATION TYPE CONDITION


def time_to_second(timestring):
        year=int(timestring[1:5])
        month=int(timestring[6:8])
        day=int(timestring[9:11])
        hour=int(timestring[12:14])
        minute=int(timestring[15:17])
        second=int(timestring[18:20])
        return time.mktime((year,month,day,hour,minute,second,0,0,0))

