# - Setup the LOFAR CTest for python environments.

#  Copyright (C) 2008-2010
#  ASTRON (Netherlands Foundation for Research in Astronomy)
#  P.O.Box 2, 7990 AA Dwingeloo, The Netherlands, softwaresupport@astron.nl
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#  $Id$

include(LofarCTest)

# TO ENABLE THE PYTHON COVERAGE ANALYSIS EXECUTE IN THE CMAKE file
# enable_coverage(PYTHON_VERSION_YOU_WISH_TO_USE[option])
# then insert the following lines into the .run file
# --------------
# . coverage.sh
# python_coverage_test [path of the module to include] [file that contains the python test]
# --------------
macro(enable_coverage PYTHON_VERSION)
    set(PYTHON_VERSION ${PYTHON_VERSION})
    configure_file(${LOFAR_ROOT}/CMake/testscripts/test_python_with_coverage.run.in
               ${CMAKE_CURRENT_BINARY_DIR}/coverage.sh @ONLY)
endmacro(enable_coverage)
