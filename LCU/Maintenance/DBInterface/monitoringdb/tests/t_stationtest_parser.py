import unittest

from .common import *

class TestStationTestsParser(unittest.TestCase):
    def test_element_error_details_parsing(self):
        preparsed_element_error = ['20180508', 'HBA', '044', 'E_FAIL',
                                   {'x1': '99.5 140 101.6 99.8 140 102.4'},
                                   {'y1': '99.5 140 101.6 99.8 140 102.4'},
                                   {'ox1': '1'},
                                   {'oy1': '1'},
                                   {'spx1': '1'},
                                   {'spy1': '1'},
                                   {'lnx1': '67.3 2.031'},
                                   {'hnx1': '67.3 2.031'},
                                   {'lny1': '67.3 2.031'},
                                   {'hny1': '67.3 2.031'},
                                   {'jy1': '67.3'},
                                   {'jx1': '67.3'},
                                   ]
        expected_result = [
            {'component_error': {'component': {'type': 'HBA', 'component_id': 44},
                                 'type': 'RF_FAIL'}, 'element': {'element_id': 1},
             'type': 'RF_FAIL',
             'details': {'xval_full_delay': 140.0, 'xval_no_delay': 99.5, 'xsb_full_delay': 99.8,
                         'xsb_no_delay': 101.6, 'xref_full_delay': 102.4, 'xref_no_delay': 140.0,
                         'yval_full_delay': 140.0, 'yval_no_delay': 99.5, 'ysb_full_delay': 99.8,
                         'ysb_no_delay': 101.6, 'yref_full_delay': 102.4, 'yref_no_delay': 140.0}},
            {'component_error': {'component': {'type': 'HBA', 'component_id': 44},
                                 'type': 'OSCILLATION'}, 'element': {'element_id': 1},
             'type': 'OSCILLATION', 'details': {'x': True, 'y': True}},
            {'component_error': {'component': {'type': 'HBA', 'component_id': 44},
                                 'type': 'SPURIOUS'}, 'element': {'element_id': 1},
             'type': 'SPURIOUS', 'details': {'x': True, 'y': True}},
            {'component_error': {'component': {'type': 'HBA', 'component_id': 44},
                                 'type': 'LOW_NOISE'}, 'element': {'element_id': 1},
             'type': 'LOW_NOISE',
             'details': {'xval': 67.3, 'xdiff': 2.031, 'yval': 67.3, 'ydiff': 2.031}},
            {'component_error': {'component': {'type': 'HBA', 'component_id': 44},
                                 'type': 'HIGH_NOISE'}, 'element': {'element_id': 1},
             'type': 'HIGH_NOISE',
             'details': {'xval': 67.3, 'xdiff': 2.031, 'yval': 67.3, 'ydiff': 2.031}},
            {'component_error': {'component': {'type': 'HBA', 'component_id': 44},
                                 'type': 'JITTER'}, 'element': {'element_id': 1}, 'type': 'JITTER',
             'details': {'ydiff': 67.3, 'xdiff': 67.3}}
        ]
        parsed_element_errors = raw_parser.dicts_from_element_error(preparsed_element_error)

        self.assertEqual(parsed_element_errors, expected_result)


if __name__ == '__main__':
    unittest.main()
