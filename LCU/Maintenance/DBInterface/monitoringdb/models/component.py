from typing import Tuple

from django.db import models

from .station import Station

HBA_ELEMENT_ID_RANGE = range(0, 16, 1)

def antenna_type_antenna_id_to_component_type_component_id(station_name,
                                                           antenna_type,
                                                           antenna_id) -> Tuple[str, int]:
    if antenna_type == 'HBA':
        return 'HBA', antenna_id

    if station_name.startswith('CS') or station_name.startswith('RS'):
        if antenna_id >= 48:
            component_type = 'LBL'
            component_id = antenna_id
        else:
            component_type = 'LBH'
            component_id = antenna_id

        return component_type, component_id
    else:
        return 'LBH', antenna_id


class Component(models.Model):
    type = models.CharField(max_length=50)
    component_id = models.IntegerField()
    station = models.ForeignKey(Station, on_delete=models.DO_NOTHING, related_name='components')

    class Meta:
        unique_together = ('station', 'type', 'component_id')
