import {
  FETCH_ERRORTYPES_BEGIN,
  FETCH_ERRORTYPES_SUCCESS,
  FETCH_ERRORTYPES_FAILURE,
  FETCH_STATIONS_SUCCESS,
  FETCH_STATIONS_FAILURE
} from '../actions/appInitDataActions';


const initialState = {
    errorTypesLoaded: false,
    errorTypes: [],
    stationsLoaded: false,
    stations: []
};


export default function (state = initialState, action) {
  switch(action.type) {
    case FETCH_ERRORTYPES_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      return {
        ...state,
        errorTypesLoaded: false,
        errorTypes: []
      };

    case FETCH_ERRORTYPES_SUCCESS:
      // All done: set loading "false".
      return {
        ...state,
        errorTypesLoaded: true,
        errorTypes: action.payload.errorTypes
      };

    case FETCH_ERRORTYPES_FAILURE:
      // The request failed, but it did stop, so set loading to "false".
      return {
        ...state,
        errorTypesLoaded: false,
        // don't: errorTypes: []
        errorTypesError: action.payload.error
      };

    case FETCH_STATIONS_SUCCESS:
        // All done: set loading "false".
        return {
          ...state,
          stationsLoaded: true,
          stations: action.payload.stations
        };

    case FETCH_STATIONS_FAILURE:
        // The request failed, but it did stop, so set loading to "false".
        return {
          ...state,
          stationsLoaded: false,
          // don't: errorTypes: []
          stationsError: action.payload.error
        };

    default:
      // ALWAYS have a default case in a reducer
      return state;
  }
}
