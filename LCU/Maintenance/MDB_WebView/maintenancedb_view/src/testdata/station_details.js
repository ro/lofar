const data = [
    {
        "station_name": "CS004C",
        "station_tests": [
            {
                "total_component_errors": 11,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:24Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 4,
                        "MODEM": 2,
                        "OSCILLATION": 1,
                        "C_SUMMATOR": 1
                    },
                    "LBL": {
                        "LOW_NOISE": 1,
                        "RF_FAIL": 1
                    },
                    "TBB": {
                        "TEMPERATURE": 1
                    }
                }
            }
        ]
    }, {
        "station_name": "CS006C",
        "station_tests": [
            {
                "total_component_errors": 15,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:46:26Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "JITTER": 4,
                        "MODEM": 3,
                        "RF_FAIL": 2,
                        "HIGH_NOISE": 2
                    },
                    "LBH": {
                        "RF_FAIL": 2,
                        "HIGH_NOISE": 1
                    },
                    "TBB": {
                        "TEMPERATURE": 1
                    }
                }
            }
        ]
    }, {
        "station_name": "CS017C",
        "station_tests": [
            {
                "total_component_errors": 6,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:32Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "RF_FAIL": 3,
                        "HIGH_NOISE": 1,
                        "SPURIOUS": 1
                    },
                    "LBH": {
                        "LOW_NOISE": 1
                    }
                }
            }
        ]
    }, {
        "station_name": "CS028C",
        "station_tests": [
            {
                "total_component_errors": 9,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:21Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "RF_FAIL": 3,
                        "HIGH_NOISE": 1,
                        "OSCILLATION": 1
                    },
                    "LBH": {
                        "DOWN": 2
                    },
                    "LBL": {
                        "DOWN": 2
                    }
                }
            }
        ]
    }, {
        "station_name": "CS032C",
        "station_tests": [
            {
                "total_component_errors": 5,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:46:02Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "MODEM": 2,
                        "JITTER": 1,
                        "RF_FAIL": 1
                    },
                    "LBH": {
                        "JITTER": 1
                    }
                }
            }
        ]
    }, {
        "station_name": "CS103C",
        "station_tests": [
            {
                "total_component_errors": 17,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:23Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "LBH": {
                        "DOWN": 3,
                        "RF_FAIL": 2,
                        "FLAT": 1
                    },
                    "HBA": {
                        "RF_FAIL": 3,
                        "MODEM": 1,
                        "C_SUMMATOR": 1
                    },
                    "LBL": {
                        "DOWN": 2,
                        "FLAT": 2,
                        "RF_FAIL": 1,
                        "LOW_NOISE": 1
                    }
                }
            }
        ]
    }, {
        "station_name": "CS302C",
        "station_tests": [
            {
                "total_component_errors": 26,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:17Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "RF_FAIL": 9,
                        "MODEM": 7,
                        "HIGH_NOISE": 3,
                        "SUMMATOR_NOISE": 3,
                        "JITTER": 2,
                        "SPURIOUS": 1
                    },
                    "LBL": {
                        "FLAT": 1
                    }
                }
            }
        ]
    }, {
        "station_name": "DE602C",
        "station_tests": [
            {
                "total_component_errors": 6,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:34Z",
                "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "MODEM": 3
                    },
                    "LBH": {
                        "HIGH_NOISE": 2,
                        "JITTER": 1
                    }
                }
            }
        ]
    }, {
        "station_name": "DE605C",
        "station_tests": [
            {
                "total_component_errors": 11,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:28:50Z",
                "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E5 TV TBC TM",
                "component_error_summary": {
                    "LBH": {
                        "DOWN": 2,
                        "RF_FAIL": 2,
                        "HIGH_NOISE": 1
                    },
                    "HBA": {
                        "SUMMATOR_NOISE": 2,
                        "MODEM": 2,
                        "OSCILLATION": 1
                    },
                    "RSP": {
                        "TEMPERATURE": 1
                    }
                }
            }
        ]
    }, {
        "station_name": "PL610C",
        "station_tests": [
            {
                "total_component_errors": 56,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:42Z",
                "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "LBH": {
                        "RF_FAIL": 28,
                        "DOWN": 7,
                        "LOW_NOISE": 1,
                        "SPURIOUS": 1
                    },
                    "HBA": {
                        "SPURIOUS": 19
                    }
                }
            }
        ]
    }, {
        "station_name": "PL611C",
        "station_tests": [
            {
                "total_component_errors": 54,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:47:53Z",
                "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "SPURIOUS": 47
                    },
                    "LBH": {
                        "RF_FAIL": 2,
                        "DOWN": 1
                    },
                    "TBB": {
                        "MEMORY": 1,
                        "VERSION": 1,
                        "VOLTAGE": 1
                    },
                    "RSP": {
                        "TEMPERATURE": 1
                    }
                }
            }
        ]
    }, {
        "station_name": "RS205C",
        "station_tests": [
            {
                "total_component_errors": 16,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:53:46Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 4,
                        "MODEM": 3,
                        "RF_FAIL": 2,
                        "SPURIOUS": 1,
                        "JITTER": 1
                    },
                    "LBL": {
                        "HIGH_NOISE": 3
                    },
                    "LBH": {
                        "HIGH_NOISE": 1,
                        "RF_FAIL": 1
                    }
                }
            }
        ]
    }, {
        "station_name": "RS210C",
        "station_tests": [
            {
                "total_component_errors": 9,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:28Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "RF_FAIL": 4,
                        "HIGH_NOISE": 3,
                        "MODEM": 1
                    },
                    "LBL": {
                        "FLAT": 1
                    }
                }
            }
        ]
    }, {
        "station_name": "RS305C",
        "station_tests": [
            {
                "total_component_errors": 12,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:27Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "RF_FAIL": 2,
                        "HIGH_NOISE": 1,
                        "JITTER": 1,
                        "C_SUMMATOR": 1
                    },
                    "TBB": {
                        "MEMORY": 2,
                        "VERSION": 2,
                        "VOLTAGE": 2
                    },
                    "LBH": {
                        "RF_FAIL": 1
                    }
                }
            }
        ]
    }, {
        "station_name": "RS409C",
        "station_tests": [
            {
                "total_component_errors": 17,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:51:06Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "SPURIOUS": 4,
                        "RF_FAIL": 2,
                        "SUMMATOR_NOISE": 2,
                        "HIGH_NOISE": 1,
                        "JITTER": 1,
                        "OSCILLATION": 1
                    },
                    "LBH": {
                        "SPURIOUS": 3,
                        "FLAT": 1,
                        "DOWN": 1
                    },
                    "TBB": {
                        "MEMORY": 1
                    }
                }
            }
        ]
    }, {
        "station_name": "SE607C",
        "station_tests": [
            {
                "total_component_errors": 24,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:48:55Z",
                "checks": "RV SPU RBC SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 S7 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "SPURIOUS": 21
                    },
                    "LBH": {
                        "RF_FAIL": 1
                    },
                    "RSP": {
                        "TEMPERATURE": 1
                    },
                    "TBB": {
                        "MEMORY": 1
                    }
                }
            }
        ]
    }
];

export default data;
