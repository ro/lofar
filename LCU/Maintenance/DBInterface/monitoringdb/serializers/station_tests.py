from rest_framework.serializers import ModelSerializer
from ..models.station_test import StationTest
from ..models.station import Station
from .component_error import ComponentErrorSerializer
from .element_error import ElementErrorSerializer
from .log import ActionLogSerializer

import logging

logger = logging.getLogger(__name__)

class StationTestSerializer(ModelSerializer):
    component_errors = ComponentErrorSerializer(many=True, read_only=True, depth=0)
    class Meta:
        model = StationTest
        fields = '__all__'
        depth = 5

    @staticmethod
    def insert_station_test(station_test):
        """
        Insert the station test in the database and if necessary the component errors
        and the element errors
        :param station_test: json representation of the station test
        :type station_test: dict
        :return: the station test entry inserted
        :rtype: StationTest
        """
        station = station_test.pop('station')
        component_errors = station_test.pop('component_errors')
        element_errors = station_test.pop('element_errors')
        station_entry = Station.objects.filter(**station).first()

        if station_entry is None:
            logger.debug('Station is not present, inserting ...')
            station_entry = Station(**station)
            station_entry.save()
            # Log the action
            ActionLogSerializer.log_model_insert(station_entry)

        station_test_entry = StationTest.objects.filter(**station_test).first()
        if station_test_entry is None:
            logger.debug('Station test is not present, inserting ...')
            station_test_entry = StationTest(station=station_entry,
                                             **station_test)
            station_test_entry.save()
            # Log the action
            ActionLogSerializer.log_model_insert(station_test_entry)

        ComponentErrorSerializer.insert_component_errors(station_test_entry,
                                                         station_entry,
                                                         component_errors)

        ElementErrorSerializer.insert_element_errors(station_test_entry,
                              station_entry,
                              element_errors)

        return station_test_entry

