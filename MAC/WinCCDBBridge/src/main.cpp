#include <QCoreApplication>

#include <QCommandLineParser>
#include <QDebug>

#include "Program.hpp"

/**
Defines the Qt application properties.
*/
void defineApplication(){
    QCoreApplication::setApplicationName("WinCCDispatcher");

}

/**
Defines the command line options
*/
void defineCommandLineOptions(QCommandLineParser & parser){
    parser.setApplicationDescription("Stores the value directly in a chosen SQL database.\n");

    parser.addHelpOption();
    parser.addPositionalArgument("datapoint_config", "Configuration file containing the list of datapoints to connect to");
    parser.addPositionalArgument("configuration", "Configuration file containing the database connection info");
    parser.addPositionalArgument("WinCCProjectName", "Project name to connect to");
}


/**
Execute the main program
*/
void executeProgram(const QStringList arguments){
    const QString datapointXMLConfiguration = arguments.at(0);
    const QString configuration = arguments.at(1);

    const QString winccProjectName = arguments.at(2);

    Program program{nullptr, configuration, winccProjectName};
    program.loadDatapointsConfiguration(datapointXMLConfiguration);
    program.run();
}

/**
Main function. It execute in succession:
  * parsing the program options
  * handle required missing parameter
  * constructing the main program
  * executes the main loop
*/
int main(int argc, char * argv[]){
    QCoreApplication app{argc, argv};
    QCommandLineParser parser;

    defineApplication();

    defineCommandLineOptions(parser);

    parser.process(app);
    const QStringList arguments = parser.positionalArguments();

    if(arguments.size() == 3) {
        executeProgram(arguments);
    }else{
        qCritical()<< "Missing required arguments";
        parser.showHelp();
        exit(1);
    }

}
