import React, {Component} from 'react';
import {connect} from "react-redux";
import {setStationGroup} from "redux/actions/mainFiltersActions";

// CSS
import toolbarStyles from '../styles.module.scss'

/**
 * StationGroupSelector:
 * Select box for selecting a station group (all, core, remote, ilt)
 */
class StationGroupSelectorC extends Component {

    onStationGroupChange = (e) => {
        this.props.setStationGroup(e.target.value);
    }

    render() {
        return (<div className={toolbarStyles.toolbar_ctrl}>
            <label htmlFor="selected-group">Station group</label>
            <select className="form-control custom-select custom-select-sm" id="selected-group"
                    value={this.props.selectedStationGroup}
                    onChange={this.onStationGroupChange}
                    style={{ width: 'auto' }}>
                <option value="A">All stations</option>
                <option value="C">Core stations</option>
                <option value="R">Remote stations</option>
                <option value="I">ILT stations</option>
            </select>
        </div>);
    }
}

const mapStateStationGroupSelector = state => {
    return {
        selectedStationGroup: state.mainFilters.selectedStationGroup
    };
};

const mapDispatchStationGroupSelector = {
    setStationGroup
};

export const StationGroupSelector = connect(mapStateStationGroupSelector, mapDispatchStationGroupSelector)(StationGroupSelectorC);
