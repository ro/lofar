# Copyright (C) 2012-2015  ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

from functools import wraps
import inspect

def check_type_hints(func):
    """ Decorator that verifies the type hints of the decorated function.

        Raises a TypeError if the type is not met, that is, the parameters and/or return value
        that have a type hint are not given values that are of that type, or a subclass.

        Example usage:

        @check_type_hints
        def myfunc(i: int, j) -> str:
          return "%d %s" % (i,j)

        myfunc(1, 2)    # ok, type of i matches type hint
        myfunc(1, "2")  # ok, type of j is not checked, as it has no type hint
        myfunc("1", 2)  # throws TypeError, type i does not match type hint
    """

    def check_type(obj, cls):
      if isinstance(cls, list):
        return any((isinstance(obj, c) for c in cls))

      return isinstance(obj, cls)

    @wraps(func)
    def decorator(*args, **kwargs):
        argspec = inspect.getfullargspec(func)
        hints = argspec.annotations

        for i, (arg, argname) in enumerate(zip(args, argspec.args)):
            if argname in hints:
                argtype = hints[argname]
                if not check_type(arg, argtype):
                    raise TypeError("Positional parameter %d (named %s) must have type %s (has type %s)" % (i, argname, argtype, type(arg)))

        for argname, argtype in hints.items():
            if argname in kwargs:
                if not check_type(kwargs[argname], argtype):
                    raise TypeError("Parameter %s must have type %s (has type %s)" % (argname, argtype, type(kwargs[argname])))

        return_value = func(*args, **kwargs)
        if 'return' in hints:
            if not check_type(return_value, hints['return']):
                raise TypeError("Return value must have type %s (has type %s)" % (hints['return'], type(return_value)))

        return return_value

    return decorator
