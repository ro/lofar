NOTE ABOUT ORGANIZATION OF COMPONENTS
=====================================

Directory structure
-------------------

React does not prescribe a specific directory structure for organizing components, reducers, CSS, etc. The directory structure chosen for this project is based on this one: https://medium.com/@alexmngn/how-to-better-organize-your-react-applications-2fd3ea1920f1

Main principle is that each page and component has everything it needs to work on its own, such as its own styles, images, etc. This way a page or component becomes an independent piece of code that can be tested separately. Note that reducers and actions should also be made local the same way, this is not yet done however.

 The directory structure follows the following principles (see example structure below):
* Top-level components are stored in src/components and can be used anywhere in the app.
* Nested components can only be used by the parent component. E.g. the DateRangeSelector can be used by Toolbar but not by the Header, but Toolbar and Header can be used anywhere.
* Pages are stored in src/pages. Pages can contain private components in a sub directory 'components'. These can only be used on that page, not globally.
* Components in pages can contain nested, child components (e.g. ObsRow in LatestObservations). Here too, only the parent component is allowed to use the nested component.
* Too deep nesting is not useful. Nesting stops at level 5. So if ObsRow would have child components, just put them on the same level.


Example structure
-----------------

0...1...2...3...4...5    (nest level)

src/
├── components/
│   ├── Toolbar/
│   │   ├── DateRangeSelector/
│   │   │   ├── index.js
│   │   │   └── styles.module.scss
│   │   ├── TestTypeSelector/
│   │   │   ├── index.js
│   │   │   └── styles.module.scss
│   │   ├── index.js
│   │   └── styles.module.scss
│   │
│   └── Header/
│       ├── index.js
│       └── styles.module.scss
│
└── pages/
    ├── LandingPage/
    │   ├── components/
    │   │   ├── LatestObservations/
    │   │   │   ├── ObsRow/
    │   │   │   │   ├── index.js
    │   │   │   │   └── styles.module.scss
    │   │   │   ├── index.js
    │   │   │   └── styles.module.scss
    │   │   └── StationOverview/
    │   │       ├── index.js
    │   │       └── styles.module.scss
    │   ├── index.js
    │   └── styles.scss
    │
    ├── TilesPage/
    │   ├── components/
    │   │   ├── ..etc..


Naming of JS and CSS files
--------------------------

Note from the example directory structure above that all components have an index.js and styles.module.scss. Using index.js results in cleaner import code as you can write:

    import AppGlobalComponent from 'components/AppGlobalComponent'

instead of something like:

    import AppGlobalComponent from 'components/AppGlobalComponent/AppGlobalComponent.js'

Note that this also requires a .env file in your project directory (where package.json resides) with the following line:

    NODE_PATH='src/'

The styles.module.scss is a combination of *SASS* (.scss) and *CSS modules* (.module). Both aspects are recognized by *Webpack*. So the SASS compiler runs and local classnames are generated.



Boilerplate for a component JS file
-----------------------------------

import React, {Component} from 'react';                         // imported from node_modules
import NpmComponent from 'NpmComponent';                        // imported from node_modules
import AppGlobalComponent from 'components/AppGlobalComponent'  // imported from src/components
import MyLocalComponent from './MyLocalComponent';              // imported from current directory

// CSS
import styles from './styles.module.scss';                      // imported from current directory

/**
 * MyComponent class.
 * @prop {string} a_prop - this is a prop description in jsdoc style
 */
class MyComponent extends Component {
    ...
}

export default MyComponent;
