#!/usr/bin/env python3

import logging
from lofar.messaging.rpc import RPCClient, RPCClientContextManagerMixin, DEFAULT_BROKER, DEFAULT_BUSNAME, DEFAULT_RPC_TIMEOUT
from lofar.lta.ingest.server.config import DEFAULT_INGEST_SERVICENAME

logger = logging.getLogger(__name__)


class IngestRPC(RPCClientContextManagerMixin):
    def __init__(self, rpc_client: RPCClient = None):
        """Create an instance of the IngestRPC using the given RPCClient,
        or if None given, to a default RPCClient connecting to the DEFAULT_INGEST_SERVICENAME service"""
        super().__init__()
        self._rpc_client = rpc_client or RPCClient(service_name=DEFAULT_INGEST_SERVICENAME)

    @staticmethod
    def create(exchange=DEFAULT_BUSNAME, broker=DEFAULT_BROKER):
        """Create a IngestRPC connecting to the given exchange/broker on the default DEFAULT_INGEST_SERVICENAME service"""
        return IngestRPC(RPCClient(service_name=DEFAULT_INGEST_SERVICENAME,
                                   exchange=exchange, broker=broker, timeout=DEFAULT_RPC_TIMEOUT))

    def removeExportJob(self, export_group_id):
        return self._rpc_client.execute('RemoveExportJob', export_group_id=export_group_id)

    def setExportJobPriority(self, export_group_id, priority):
        return self._rpc_client.execute('SetExportJobPriority', export_id=export_group_id, priority=priority)

    def getStatusReport(self):
        return self._rpc_client.execute('GetStatusReport')

    def getJobStatus(self, job_id):
        return self._rpc_client.execute('GetJobStatus', job_id=job_id)[job_id]

    def getReport(self, export_group_id):
        return self._rpc_client.execute('GetReport', job_group_id=export_group_id)

    def getExportIds(self):
        return self._rpc_client.execute('GetExportIds')


class IngestTMSSRPC(RPCClientContextManagerMixin):
    def __init__(self, rpc_client: RPCClient = None):
        """Create an instance of the IngestTMSSRPC using the given RPCClient,
        or if None given, to a default RPCClient connecting to the IngestTMSSService service"""
        super().__init__()
        self._rpc_client = rpc_client or RPCClient(service_name='IngestTMSSService')

    @staticmethod
    def create(exchange=DEFAULT_BUSNAME, broker=DEFAULT_BROKER, timeout=DEFAULT_RPC_TIMEOUT):
        """Create a IngestTMSSRPC connecting to the given exchange/broker on the default DEFAULT_INGEST_SERVICENAME service"""
        return IngestTMSSRPC(RPCClient(service_name='IngestTMSSService',
                                       exchange=exchange, broker=broker, timeout=timeout))

    def get_SIP(self, dataproduct_id: int) -> str:
        return self._rpc_client.execute('get_SIP', dataproduct_id=dataproduct_id)

    def get_output_dataproduct_id(self, input_dataproduct_id: int, subtask_id: int) -> int:
        return self._rpc_client.execute('get_output_dataproduct_id', input_dataproduct_id=input_dataproduct_id, subtask_id=subtask_id)

    def store_archive_information(self, dataproduct_id: int, size: int, filepath: str, storage_ticket: str, md5_hash: str, adler32_hash: str):
        return self._rpc_client.execute('store_archive_information', dataproduct_id=dataproduct_id, storage_ticket=storage_ticket, size=size, filepath=filepath, md5_hash=md5_hash, adler32_hash=adler32_hash)


    def delete_hashes_and_archive_information(self, dataproduct_id: int):
        return self._rpc_client.execute('delete_hashes_and_archive_information', dataproduct_id=dataproduct_id)


if __name__ == '__main__':
    logging.basicConfig()
    import pprint
    with IngestRPC() as rpc:
        pprint.pprint(rpc.getStatusReport())
