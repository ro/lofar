# LOFAR Software Repository 

The LOFAR repository contains some of the software packages to operate
the LOFAR radio telescope and process its measurement output.
LOFAR is operated by ASTRON, the Netherlands Institute for Radio Astronomy.
For more information, see http://www.astron.nl/ and http://www.lofar.org/

Source code is available at https://git.astron.nl/ro/lofar.git

## Top-level LOFAR Project Content
 
**Folders**
```
CAL/                    Calibration
CEP/			CEntral Processing software
CEP/Calibration/	Calibration: BBS, antenna and station responses
CEP/DP3/		Pre-processing: flagging, demixing, averag., calibration
CEP/Imager/		Imager
CEP/LAPS/		LOFAR Automated Processing System:
                          Image processing on the cluster/grid/archive
CEP/Pipeline/		Self-calibration / imaging pipeline
CEP/PyBDSM/		Source finder
CMake/			CMake configuration & build helper scripts
CMake/variants/		compiler- and hostname-specific build configurations
Docker/			Docker container templates and build scripts
EmbraceStMan            ??
LCS/			LOFAR Common Software: frequently used LOFAR libraries
LCS/LofarStMan/		LOFAR Storage Manager for casacore MeasurementSets
LCU/			Local Controller Unit: station software tools
LTA/			Long-Term Archive: archive ingest tools
MAC/			Monitoring and Control software, static metadata
MAC/Deployment/data/StaticMetaData/ Static metadata: names, positions, delays...
RTCP/			Real-Time Central Processing: correlator and beamformer
RTCP/Cobalt/		COrrelator and BeAmformer for the Lofar Telescope
SAS/			Submission And Scheduling. Including TMSS.
SubSystems/		Build targets for deployment and some integration tests
doc/                    Doxygen documentation
support/                ??
```
More info about [TMSS](SAS/TMSS/README.md)

**Files**
```
.gitattributes          Text file that gives attributes to path names
.gitignore              Git will not take attention to untracked files
.gitlab-ci.yml          Gitlab Continuous Integration Pipeline
CMakeLists.txt		Top-level CMake configuration & build script
COPYING			License text
CTestConfig.cmake       CTest settings
CtestCustom.cmake.in    CTest custom settings
INSTALL			Build and installation instructions
jenkins_make            Build file for Jenkins (obsolete?)
LICENCE                 Empty License file
LOFAR.md                This file (Ref.1)
lofar_config.h.cmake    
lofarinit.csh.in        Define the LOFAR environement (C shell)
lofarinit.sh.in         Define the LOFAR environement (Bourne-like shell
README			The 'old' README file (can be removed)
```

Ref.1: See [Mark Down Style Guide](https://about.gitlab.com/handbook/engineering/ux/technical-writing/markdown-guide/)

## Contact Information

For questions about LOFAR software installation, usage, or bug reports, please
contact the ASTRON Radio Observatory software group via:

	softwaresupport AT astron DOT nl


For questions about observing with LOFAR or your data product, please contact
the ASTRON Radio Observatory "Science Operations & Support" group via:

	sos AT astron DOT nl

