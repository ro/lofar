#!/bin/bash
# setstorage.sh
#ssh -o BatchMode=yes -o ConnectTimeout=2 head01.cep4.control.lofar ./tbb-hostnames.sh > tbb-hostnames-short.txt
ssh -o BatchMode=yes -o ConnectTimeout=2 lofarsys@head01.cep4.control.lofar /home/veen/tbb-hostnames.sh > tbb-hostnames-short.txt
head -n 24 tbb-hostnames-short.txt > tbb-hostnames.txt
head -n 24 tbb-hostnames-short.txt >> tbb-hostnames.txt
paste stations.txt tbb-hostnames.txt | grep C | sed 's:CS:ssh -o BatchMode=yes -o ConnectTimeout=2 CS:g' | sed 's:RS:ssh -o BatchMode=yes -o ConnectTimeout=2 RS:g' | sed 's:cpu:tbbctl --storage=cpu:g' | sed 's:\t: :g' | grep tbb > reroute.sh
./reroute.sh
#echo No CEP4 reroute possible at the moment. This freezes the TBB boards.
