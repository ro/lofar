from django.db import models
from .element import Element
from django.contrib.postgres.fields import JSONField

from .component_error import ComponentError


class ElementError(models.Model):
    element = models.ForeignKey(Element, on_delete=models.DO_NOTHING)
    component_error = models.ForeignKey(ComponentError,
                                        on_delete=models.DO_NOTHING,
                                        related_name='failing_elements')

    type = models.CharField(max_length=50)
    details = JSONField(default=dict)

    class Meta:
        unique_together = ('component_error', 'element', 'type')
