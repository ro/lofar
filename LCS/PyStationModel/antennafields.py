def antenna_fields(station: str, antenna_set: str) -> tuple:
    """ Return the tuple of antenna fields for a certain station, for a certain antenna set. """

    if antenna_set in ["HBA_DUAL", "HBA_DUAL_INNER"] and station.startswith("CS"):
        return (station + "HBA0", station + "HBA1")

    if antenna_set.startswith("LBA"):
        return (station + "LBA",)

    if antenna_set.startswith("HBA"):
        return (station + "HBA",)

    raise ValueError("Cannot parse antennaset name: %s" % antenna_set)


def antennafields_for_antennaset_and_station(antennaset:str, station:str) -> list:
    """
    convert an antennaset to a list of antennafields
    :param antennaset: A string identifier for an antennaset, like 'HBA_DUAL'
    :param station: A string identifier for a station, like 'CS001'
    :return: a list of antennafields that the station uses for the given antennaset ['HBA0', 'HBA1']
    """
    if antennaset.startswith('LBA'):
        fields = ['LBA']
    elif antennaset.startswith('HBA') and not station.startswith('CS'):
        fields = ['HBA']
    elif antennaset.startswith('HBA_DUAL'):
        fields = ['HBA0', 'HBA1']
    elif antennaset.startswith('HBA_ZERO'):
        fields = ['HBA0']
    elif antennaset.startswith('HBA_ONE'):
        fields = ['HBA1']
    else:
        raise ValueError('Cannot determine antennafields for station=%s antennaset=%s' % (station, antennaset))

    return fields

