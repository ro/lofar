import json
import logging
import unittest
from datetime import datetime

from mock import patch, Mock

from lofar.maintenance.utils.cli.probe_mdb import get_url_string_to_select_timespan, TestType, \
    get_url_string_to_select_test_type, CACHING_SIZE, get_url_string_to_select_station_name, \
    retrieve_station_tests, retrieve_rtsm, reformat_datetime, sort_component_errors, \
    rtsm_sort_errors, query_api_for_stationtest_rtsm_results, \
    print_out_station_test_summary, print_out_component_errors_summary, \
    print_out_component_errors, \
    print_out_rtsm_summary, print_out_rtsm, print_out_rtsm_and_stationtests, \
    parse_specified_datetime
from output_testing import compare_output_with_file_content, save_output_to_file

logger = logging.getLogger(__name__)


def read_api_station_test_output():
    """
    reads the mock output of the api for a station test
    :return: a mocked station test
    :type: dict
    """
    with open('t_probe_mdb.in.stationtest_rest_output.json') as fstream_in:
        test_station_test = json.load(fstream_in)
    return test_station_test


def read_api_rtsm_test_output():
    """
    reads the mock output of the api for a rtsm
    :return: a mocked rtsm
    :type: dict
    """
    with open('t_probe_mdb.in.rtsm_rest_output.json') as fstream_in:
        test_rtsm = json.load(fstream_in)
    return test_rtsm


class TESTProbeMDB(unittest.TestCase):
    @patch('lofar.maintenance.utils.cli.probe_mdb.requests')
    def test_query_api_for_stationtest_rtsm_results_success(self, requests_mock):
        address = "testaddress"
        query_string = "test_query_string"

        response = Mock(status_code=200)
        response.json.return_value = dict(
            results=[dict(key='value')],
            next=None)
        requests_mock.get.return_value = response
        result = query_api_for_stationtest_rtsm_results(query_string, address)

        requests_mock.get.assert_called()
        requests_mock.get.assert_called_with('testaddress/test_query_string')

        self.assertEqual(result, [dict(key='value')])

    @patch('lofar.maintenance.utils.cli.probe_mdb.requests')
    def test_query_api_for_stationtest_rtsm_success_next_url(self, requests_mock):
        address = "testaddress"
        query_string = "test_query_string"
        next_url = "next_url"
        response = Mock(status_code=200)
        response.json.side_effect = [dict(results=[dict(key='value')],
                                          next=next_url),
                                     dict(results=[dict(key='value')],
                                          next=None)]
        requests_mock.get.return_value = response

        response = query_api_for_stationtest_rtsm_results(query_string, address)

        requests_mock.get.assert_called()
        requests_mock.get.assert_called_with('next_url')
        self.assertEqual(requests_mock.get.call_count, 2)
        self.assertEqual(response, [dict(key='value'), dict(key='value')])

    @patch('lofar.maintenance.utils.cli.probe_mdb.requests')
    def test_query_api_for_stationtest_rtsm_error_raise_exception(self, requests_mock):
        address = "testaddress"
        query_string = "test_query_string"
        response = Mock(status_code=300)
        requests_mock.get.return_value = response
        with self.assertRaises(Exception):
            response = query_api_for_stationtest_rtsm_results(query_string, address)

    def test_parse_specified_datetime(self):
        from_date_str = "2018-12-13"
        from_date = datetime(2018, 12, 13)
        result = parse_specified_datetime(from_date_str)
        self.assertEqual(result, from_date)

        result = parse_specified_datetime(from_date)

        self.assertEqual(result, from_date)

        from_date_str = "2018-12-13-M1"
        with self.assertRaises(ValueError):
            result = parse_specified_datetime(from_date_str)

    def test_get_url_string_to_select_timespan_from_date_success(self):
        from_date = datetime(2018, 12, 13)
        response = get_url_string_to_select_timespan(from_date, None)
        self.assertEqual(response,
                         '&start_time__year__gte=2018&'
                         'start_time__month__gte=12&'
                         'start_time__day__gte=13')

    def test_get_url_string_to_select_timespan_to_date_success(self):
        to_date = datetime(2018, 12, 13)
        response = get_url_string_to_select_timespan(None, to_date)
        self.assertEqual(response,
                         '&start_time__year__lte=2018&'
                         'start_time__month__lte=12&'
                         'start_time__day__lte=13')

    def test_get_query_string_none_specified(self):
        self.assertEqual(get_url_string_to_select_timespan(None, None), '')

    def test_get_query_string_both_specified(self):
        date = datetime(2018, 12, 13)
        expected_result = '&start_time__year__gte=2018&' \
                          'start_time__month__gte=12&' \
                          'start_time__day__gte=13&' \
                          'start_time__year__lte=2018&' \
                          'start_time__month__lte=12&s' \
                          'tart_time__day__lte=13'
        self.assertEqual(get_url_string_to_select_timespan(date, date), expected_result)

    def test_get_query_string_per_type_rtsm(self):
        result = get_url_string_to_select_test_type(TestType.RTSM)
        self.assertEqual(result, 'rtsm/?page_size={}'.format(CACHING_SIZE))

    def test_get_query_string_per_type_station_test(self):
        result = get_url_string_to_select_test_type(TestType.STATION_TEST)
        self.assertEqual(result, 'stationtests/?page_size={}'.format(CACHING_SIZE))

    def test_get_query_string_per_type_wrong_selection(self):
        with self.assertRaises(ValueError):
            get_url_string_to_select_test_type("WRONG")

    def test_get_url_string_to_select_station_name(self):
        station_name = 'JIMMY'
        result = get_url_string_to_select_station_name(station_name)
        self.assertEqual('&station_name={}'.format(station_name), result)

    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_test_type')
    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_timespan')
    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_station_name')
    @patch('lofar.maintenance.utils.cli.probe_mdb.query_api_for_stationtest_rtsm_results')
    def test_retrieve_station_tests_all_set(self, query_api_for_stationtest_rtsm_results_mock,
                                        get_url_string_to_select_station_name_mock,
                                        get_url_string_to_select_timespan_mock,
                                        get_url_string_to_select_test_type_mock):
        from_time = "2013-12-12"
        to_time = "2013-12-11"
        address = "test_address"
        station_name = "WILLY"
        retrieve_station_tests(address, from_time, to_time, station_name=station_name)

        get_url_string_to_select_station_name_mock.assert_called()
        get_url_string_to_select_timespan_mock.assert_called()
        get_url_string_to_select_test_type_mock.assert_called()
        query_api_for_stationtest_rtsm_results_mock.assert_called()

    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_test_type')
    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_timespan')
    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_station_name')
    @patch('lofar.maintenance.utils.cli.probe_mdb.query_api_for_stationtest_rtsm_results')
    def test_retrieve_station_tests_time_not_set(self,
                                                 query_api_for_stationtest_rtsm_results_mock,
                                                 get_url_string_to_select_station_name_mock,
                                                 get_url_string_to_select_timespan_mock,
                                                 get_url_string_to_select_test_type_mock):
        from_time = None
        to_time = None
        address = "test_address"
        station_name = "WILLY"
        retrieve_station_tests(address, from_time, to_time, station_name=station_name)

        get_url_string_to_select_station_name_mock.assert_called()
        get_url_string_to_select_timespan_mock.assert_not_called()
        get_url_string_to_select_test_type_mock.assert_called()
        query_api_for_stationtest_rtsm_results_mock.assert_called()

    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_test_type')
    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_timespan')
    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_station_name')
    @patch('lofar.maintenance.utils.cli.probe_mdb.query_api_for_stationtest_rtsm_results')
    def test_retrieve_station_tests_station_name_not_set(self,
                                                         query_api_for_stationtest_rtsm_results_mock,
                                                         get_url_string_to_select_station_name_mock,
                                                         get_url_string_to_select_timespan_mock,
                                                         get_url_string_to_select_test_type_mock):
        from_time = "2013-05-01"
        to_time = "2013-05-01"
        address = "test_address"
        retrieve_station_tests(address, from_time, to_time)

        get_url_string_to_select_station_name_mock.assert_not_called()
        get_url_string_to_select_timespan_mock.assert_called()
        get_url_string_to_select_test_type_mock.assert_called()
        query_api_for_stationtest_rtsm_results_mock.assert_called()

        get_url_string_to_select_station_name_mock.reset()
        get_url_string_to_select_timespan_mock.reset()
        get_url_string_to_select_test_type_mock.reset()
        query_api_for_stationtest_rtsm_results_mock.reset()

        from_time = "2013-05-01"
        to_time = "2013-05-01"
        address = "test_address"
        retrieve_station_tests(address, from_time, to_time, station_name="")

        get_url_string_to_select_station_name_mock.assert_not_called()
        get_url_string_to_select_timespan_mock.assert_called()
        get_url_string_to_select_test_type_mock.assert_called()
        query_api_for_stationtest_rtsm_results_mock.assert_called()

    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_test_type')
    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_timespan')
    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_station_name')
    @patch('lofar.maintenance.utils.cli.probe_mdb.query_api_for_stationtest_rtsm_results')
    def test_retrieve_rtsm_all_set(self, query_api_for_stationtest_rtsm_results_mock,
                                   get_url_string_to_select_station_name_mock,
                                   get_url_string_to_select_timespan_mock,
                                   get_url_string_to_select_test_type_mock):
        from_time = "2013-12-12"
        to_time = "2013-12-11"
        address = "test_address"
        station_name = "WILLY"
        retrieve_rtsm(address, from_time, to_time, station_name=station_name)

        get_url_string_to_select_station_name_mock.assert_called()
        get_url_string_to_select_timespan_mock.assert_called()
        get_url_string_to_select_test_type_mock.assert_called()
        query_api_for_stationtest_rtsm_results_mock.assert_called()

    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_test_type')
    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_timespan')
    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_station_name')
    @patch('lofar.maintenance.utils.cli.probe_mdb.query_api_for_stationtest_rtsm_results')
    def test_retrieve_rtsm_time_not_set(self, query_api_for_stationtest_rtsm_results_mock,
                                        get_url_string_to_select_station_name_mock,
                                        get_url_string_to_select_timespan_mock,
                                        get_url_string_to_select_test_type_mock):
        from_time = None
        to_time = None
        address = "test_address"
        station_name = "WILLY"
        retrieve_rtsm(address, from_time, to_time, station_name=station_name)

        get_url_string_to_select_station_name_mock.assert_called()
        get_url_string_to_select_timespan_mock.assert_not_called()
        get_url_string_to_select_test_type_mock.assert_called()
        query_api_for_stationtest_rtsm_results_mock.assert_called()

    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_test_type')
    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_timespan')
    @patch('lofar.maintenance.utils.cli.probe_mdb.get_url_string_to_select_station_name')
    @patch('lofar.maintenance.utils.cli.probe_mdb.query_api_for_stationtest_rtsm_results')
    def test_retrieve_rtsm_station_name_not_set(self, query_api_for_stationtest_rtsm_results_mock,
                                             get_url_string_to_select_station_name_mock,
                                             get_url_string_to_select_timespan_mock,
                                             get_url_string_to_select_test_type_mock):
        from_time = "2013-05-01"
        to_time = "2013-05-01"
        address = "test_address"
        retrieve_rtsm(address, from_time, to_time)

        get_url_string_to_select_station_name_mock.assert_not_called()
        get_url_string_to_select_timespan_mock.assert_called()
        get_url_string_to_select_test_type_mock.assert_called()
        query_api_for_stationtest_rtsm_results_mock.assert_called()

        get_url_string_to_select_station_name_mock.reset()
        get_url_string_to_select_timespan_mock.reset()
        get_url_string_to_select_test_type_mock.reset()
        query_api_for_stationtest_rtsm_results_mock.reset()

        from_time = "2013-05-01"
        to_time = "2013-05-01"
        address = "test_address"
        retrieve_rtsm(address, from_time, to_time, station_name="")

        get_url_string_to_select_station_name_mock.assert_not_called()
        get_url_string_to_select_timespan_mock.assert_called()
        get_url_string_to_select_test_type_mock.assert_called()
        query_api_for_stationtest_rtsm_results_mock.assert_called()

    def test_reformat_values(self):
        date = datetime(2013, 12, 12, 11, 11, 11)
        self.assertEqual(reformat_datetime(date), "2013 12 12 11:11:11")

        date = '20/07/2012'
        self.assertEqual(reformat_datetime(date), date)

    def test_sort_component_errors(self):
        component_errors = [dict(component_type="HBA", component_id=13),
                            dict(component_type="HBA", component_id=12),
                            dict(component_type="LBA", component_id=15),
                            dict(component_type="LBA", component_id=12),
                            dict(component_type="HBA", component_id=11),
                            ]
        expected_result = [dict(component_type="HBA", component_id=11),
                           dict(component_type="HBA", component_id=12),
                           dict(component_type="HBA", component_id=13),
                           dict(component_type="LBA", component_id=12),
                           dict(component_type="LBA", component_id=15),
                           ]
        sorted_component_errors = sort_component_errors(component_errors)
        self.assertEqual(sorted_component_errors, expected_result)

    def test_rtsm_sort_errors(self):
        time1 = datetime(2018, 12, 12, 11, 11, 11)
        time2 = datetime(2018, 12, 12, 11, 11, 12)
        time3 = datetime(2018, 12, 12, 11, 11, 13)

        rtsm_component_errors = [dict(error_type="HBA", rcu=13, time=time1),
                                 dict(error_type="HBA", rcu=12, time=time3),
                                 dict(error_type="LBA", rcu=15, time=time2),
                                 dict(error_type="LBA", rcu=12, time=time1),
                                 dict(error_type="HBA", rcu=11, time=time2),
                                 ]
        expected_result = [dict(error_type="HBA", rcu=11, time=time2),
                           dict(error_type="HBA", rcu=12, time=time3),
                           dict(error_type="LBA", rcu=12, time=time1),
                           dict(error_type="HBA", rcu=13, time=time1),
                           dict(error_type="LBA", rcu=15, time=time2),
                           ]
        sorted_component_errors = rtsm_sort_errors(rtsm_component_errors)
        self.assertEqual(sorted_component_errors, expected_result)

    def test_print_out_station_test_summary(self):
        test_station_test = read_api_station_test_output()[0]
        with compare_output_with_file_content('t_probe_mdb.in.'
                                              'print_out_station_test_summary'
                                              '_expected_output.txt'):
            print_out_station_test_summary(test_station_test, 1)

    def test_print_out_component_error_summary(self):
        test_station_test = read_api_station_test_output()[0]
        with compare_output_with_file_content('t_probe_mdb.in.'
                                              'print_out_component_errors_summary'
                                              '_expected_output.txt'):
            print_out_component_errors_summary(test_station_test)

    def test_print_out_component_errors(self):
        test_station_test = read_api_station_test_output()[0]

        with compare_output_with_file_content('t_probe_mdb.in.'
                                              'print_out_component_errors'
                                              '_expected_output.txt'):
            print_out_component_errors(test_station_test)

    def test_print_out_rtsm_summary(self):
        test_rtsm = read_api_rtsm_test_output()
        with compare_output_with_file_content('t_probe_mdb.in.'
                                              'print_out_rtsm_summary'
                                              '_expected_output.txt'):
            print_out_rtsm_summary(test_rtsm)

    def test_print_out_rtsm(self):
        test_rtsm = read_api_rtsm_test_output()
        with compare_output_with_file_content('t_probe_mdb.in.'
                                              'print_out_rtsm_'
                                              'expected_output.txt'):
            print_out_rtsm(test_rtsm)

    def test_print_out_rtsm_and_stationtests(self):
        test_rtsm = read_api_rtsm_test_output()
        test_station_test = read_api_station_test_output()
        with save_output_to_file('../t_probe_mdb.in.'
                                              'print_out_rtsm_and_stationtests_'
                                              'expected_output.txt'):

            print_out_rtsm_and_stationtests(test_station_test, test_rtsm)
        with compare_output_with_file_content('t_probe_mdb.in.'
                                              'print_out_rtsm_and_stationtests_'
                                              'expected_output.txt'):
            print_out_rtsm_and_stationtests(test_station_test, test_rtsm)


if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=logging.DEBUG)
    unittest.main()

