STATION_TYPES = (
        ('c', 'core'),
        ('i', 'international'),
        ('r', 'remote')
        )
COMPONENT_TYPES = (
        ('RSP', 'RSP'),
        ('TBB', 'Temporary Buffered Board'),
        ('RCU', 'RCU'),
        ('LBL', 'LBL'),
        ('LBH', 'LBH'),
        ('LBA', 'LBA'),
        ('HBA', 'HBA'),
        )
ERROR_TYPES = (
        ('VERSION', 'Not expected hardware version'),
        ('MEMORY', 'Memory error'),
        ('BROKEN', 'Broken antennas connected to this RCU'),
        ('NOSIGNAL', 'Signal not detected'),
        ('TOOLOW', 'Average signal too low to perform test'),
        ('DOWN', 'Antenna fallen down'),
        ('RF_FAIL', 'Signal too high/low in tested subband'),
        ('LOW_NOISE', 'Average signal in subband is low compared to the other subbands'),
        ('HIGH_NOISE', 'Average signal in subband is high compared to the other subbands'),
        ('JITTER', 'Jitter error'),
        ('OSCILLATION', 'Oscillating antenna'),
        ('MODEM', 'Failure with tile connection'),
        ('SPURIOUS', 'Spurious signal found'),
        ('P_SUMMATOR', 'Too low signals on all tiles'),
        ('C_SUMMATOR', 'No communication with elements possible'),
        ('SUMMATOR_NOISE', 'oscillating summator'),
        ('TESTSIGNAL', 'Test signal used in RF test'),
        )
POLARIZATIONS = ('X', 'Y')
LBA_TYPES = ('H', 'L')