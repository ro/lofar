#!/usr/bin/env bash

source /opt/lofar/lofarinit.sh
celery -b rabbitmq-broker -A lofar.maintenance.django_postgresql worker  -l info -c $N_WORKERS
