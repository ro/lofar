import React, {Component} from 'react';
import NavigationBar from 'components/NavigationBar'

import StationOverview from './components/StationOverview';
import StationTestSummary from './components/StationTestSummary';
import LatestObservations from './components/LatestObservations';
import StationStatistics from './components/StationStatistics';

import {Responsive, WidthProvider} from 'react-grid-layout';

import * as moment from 'moment';
import { connect } from "react-redux";
import { setNewLayout } from "redux/actions/landingPageActions";
import { composeQueryString } from 'utils/utils.js';
import { createGridPanel } from 'utils/grid.js';
import { Toolbar, StationGroupSelector, ErrorTypesSelector, ErrorsOnlySelector, PeriodSelector } from 'components/Toolbar'

import 'react-grid-layout/css/styles.css';
import 'react-resizable/css/styles.css';

const ResponsiveGridLayout = WidthProvider(Responsive);


/**
 * LandingPage
 * The 'dashboard' or home page with responsive grid and panels.
 */
class LandingPageC extends Component {

    getStationOverviewURL() {
        const url = '/api/view/ctrl_stationoverview';

        const parametersString = composeQueryString({
            format: 'json',
            station_group: this.props.selectedStationGroup,
            errors_only: this.props.errorsOnly,
            n_station_tests: 4,
            n_rtsm: 6,
            error_types: this.props.selectedErrorTypes
        });

        return `${url}?${parametersString}`;
    }

    getStationTestSummaryURL() {
        const url = '/api/view/ctrl_stationtestsummary';

        const parametersString = composeQueryString({
            // ---- Mandatory parameters
            format: 'json',
            lookback_time: this.props.period,
            // ---- Optional parameters
            station_group: this.props.selectedStationGroup,
            errors_only: this.props.errorsOnly,
            error_types: this.props.selectedErrorTypes
        });

        return `${url}?${parametersString}`;
    }

    getLatestObservationURL() {
        let nDaysAgo = moment().add(-this.props.period, 'days');
        let nDaysAgo_String = nDaysAgo.format('YYYY-MM-DD');
        const url = '/api/view/ctrl_latest_observation?format=json'
        const parameters = {}
        // ---- Mandatory parameters
        parameters.from_date = nDaysAgo_String
        // ---- Optional parameters
        parameters.station_group = this.props.selectedStationGroup
        parameters.errors_only = this.props.errorsOnly
        parameters.error_types = this.props.selectedErrorTypes
        const parametersString = composeQueryString(parameters)

        return `${url}&${parametersString}`;
    }

    getStationStatisticsURL() {
        let nDaysAgo = moment().add(-this.props.period, 'days').format('YYYY-MM-DD');
        let now = moment().format('YYYY-MM-DD');
        let averaging_interval = this.props.station_statistics.averaging_window
        let test_type = this.props.station_statistics.test_type
        let url = '/api/view/ctrl_stationtest_statistics?format=json'

        const parameters = {}
        // ---- Mandatory parameters
        // select from
        parameters.from_date = nDaysAgo
        // select to
        parameters.to_date = now
        // select averaging interval
        parameters.averaging_interval = averaging_interval
        // ---- Optional parameters
        parameters.errors_only = this.props.errorsOnly
        parameters.station_group = this.props.selectedStationGroup
        parameters.test_type = test_type
        parameters.error_types = this.props.selectedErrorTypes
        const parametersString = composeQueryString(parameters)

        return `${url}&${parametersString}`;
    }

    render() {
        return (<div>
            <NavigationBar active_page={this.props.location}/>
            <Toolbar>
                <StationGroupSelector />
                <ErrorsOnlySelector />
                <PeriodSelector />
                <ErrorTypesSelector />
            </Toolbar>
            <ResponsiveGridLayout className="layout" layouts={this.props.layout.panels} measureBeforeMount={true}
                                  breakpoints={this.props.layout.breakpoints} cols={this.props.layout.cols}
                                  onResizeStop={this.props.setNewLayout}>
                {createGridPanel({key: "ul", renderHeader: true, title: "Station overview", body: <StationOverview addLoaderDiv={true} url={this.getStationOverviewURL()}/>})}
                {createGridPanel({key: "ur", renderHeader: true, title: "Latest observations", body: <LatestObservations url={this.getLatestObservationURL()}/>})}
                {createGridPanel({key: "bl", renderHeader: true, title: "Station test summary", body: <StationTestSummary url={this.getStationTestSummaryURL()}/>})}
                {createGridPanel({key: "br", renderHeader: false, body: <StationStatistics url={this.getStationStatisticsURL()}/>})}
            </ResponsiveGridLayout>
        </div>);
    }
}

const LandingPage = connect(state => {
    return {
        ...state.mainFilters,
        ...state.landing_page
    };
}, {setNewLayout})(LandingPageC);

export default LandingPage;
