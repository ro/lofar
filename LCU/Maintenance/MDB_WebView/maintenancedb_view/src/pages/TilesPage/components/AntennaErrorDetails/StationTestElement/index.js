import React, {
    Component
} from 'react';
import {Badge} from 'reactstrap';
import TreeviewFromObject from 'components/TreeviewFromObject'

// CSS
//import styles from './styles.module.scss'


class StationTestElement extends Component {

    getElementData() {
        let data = this.props.data;
        let element_id = data.itemValue;
        let element_data = {};

        Object.keys(data.content.component_errors).forEach((error_type) => {
            let value = data.content.component_errors[error_type]
            if (value.element_errors && value.element_errors[element_id]) {
                element_data[error_type] = value.element_errors[element_id]
            }
        })

        return element_data
    }

    render() {
        let element_data = this.getElementData();

        return (
            <div>
                <Badge color="info">Element {this.props.data.itemValue}</Badge>
                <TreeviewFromObject label="Errors" objdata={element_data} />
            </div>
        );
    }
}


export default StationTestElement;
