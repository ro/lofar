import React from 'react';
import axios from 'axios';

import {Alert} from 'reactstrap';

// CSS
import styles from './styles.module.scss'


/**
 * Spinner: simple component to show a CSS Spinner
 */
function Spinner() {
    return <div className={styles.autoloader_loading}></div>;
}

/**
 * ErrorAlert: simple component to show a error message
 * @arg {string} message - The message text to display
 */
function ErrorAlert({message}) {
    return  <Alert className="py-1" color="danger">
                <strong>Error:</strong>&nbsp;{message}
            </Alert>
}

/**
 * AutoLoadWrapper: HOC for automatic reloading of data at a specified interval.
 * See defaultProps fro accepted props, other props are passed through to then
 * WrappedComponent.
 * @param {class} WrappedComponent - The component to wrap.
 * @returns {class} The wrapped component.
 * @example
 * const MyComponentWithLoader = AutoLoadWrapper(MyComponent);
 */
function AutoLoadWrapper(WrappedComponent) {

  // Note: returns another component
  return class extends React.Component {

    _intervalId = null;
    _source = undefined;

    state = {
        data: [],
        isLoading: false,  // A request is currenly pending (true/false)
        hasError: false,   // A request got an unexpected error (true/false)
        strError: "",      // When hasError=true, this item contains the error text
        prevUrl: null      // The last URL that has been loaded, used to detect changes
    }

    static defaultProps = {
        addLoaderDiv: false,  // wrap component in another <div>
        reloadInterval: 60000,
        url: ""               // When url = "" no request will be issued and data will be set to []
    }

    /* Called when props changed, before the render  phase */
    static getDerivedStateFromProps(props, state) {
        // Store prevUrl in state so we can compare when props change.
        if (props.url !== state.prevUrl) {
            let newState = {
                prevUrl: props.url,
                isLoading: true
            };

            if (props.url === "") {
                newState.isLoading = false;
                newState.data = [];
            }

            return newState;
        }

        // No state update necessary
        return null;
    }

    fetchData() {

        if (this.props.url === "") {
            return;
        }

        // Set loading state
        if (! this.state.isLoading) {
            this.setState({
                isLoading: true
            });
        }

        // Create new cancellation token
        this.cancelFetchData('Operation canceled due to new request.');
        this._source = axios.CancelToken.source();

        // Handle the request
        axios.get(this.props.url, {
            cancelToken: this._source.token
        }).then(res => {
            this.setState({
                data: res.data,
                isLoading: false,
                hasError: false
            });
        }).catch(error => {
            if (axios.isCancel(error)) {
                console.log('Request canceled: ', error);
            }
            else {
                console.log(error);
                this.setState({
                    isLoading: false,
                    hasError: true,
                    strError: error.message,
                    data: []
                });
            }
        }).then(() => {
            // do always
            this._source = undefined;
        });
    }

    cancelFetchData(reason) {
        if (typeof this._source !== typeof undefined) {
            this._source.cancel(reason)
            this._source = undefined;
        }
    }

    componentDidMount() {
        this.fetchData();
        this._intervalId = setInterval(() => this.fetchData(), this.props.reloadInterval);
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.url !== this.props.url) {
            this.fetchData();
        }
    }

    componentWillUnmount() {
        clearInterval(this._intervalId);
        this.cancelFetchData('Component is unmounting');
    }

    render() {
        const { addLoaderDiv, ...otherProps } = this.props;

        let body =  <React.Fragment>
                        {this.state.isLoading && <Spinner />}
                        {this.state.hasError  && <ErrorAlert message={this.state.strError} />}
                        <WrappedComponent data={this.state.data} isLoading={this.state.isLoading} {...otherProps} />
                    </React.Fragment>

        // Wrap in <div> when requested
        if (addLoaderDiv) {
            return(
                <div className={styles.autoloader_container}>
                    {body}
                </div>
            );
        }

        return body;
    }
  };
}


export default AutoLoadWrapper;
