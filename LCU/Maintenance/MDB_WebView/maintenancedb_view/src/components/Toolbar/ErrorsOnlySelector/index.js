import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button} from 'reactstrap';
import {setErrorsOnly} from "redux/actions/mainFiltersActions";

// CSS
import toolbarStyles from '../styles.module.scss'


/*
 * Toggle button for "errorsOnly"
 */
class ErrorsOnlySelectorC extends Component {

    onErrorsOnlyClick = () => {
        this.props.setErrorsOnly(!this.props.errorsOnly);
    }

    render() {
        return (
            <div className={toolbarStyles.toolbar_ctrl}>
                <Button color="info" size="sm" onClick={this.onErrorsOnlyClick} active={this.props.errorsOnly}>
                    Errors only
                </Button>
            </div>);
    }
}

const mapStateErrorsOnlySelector = state => {
    return {
        errorsOnly: state.mainFilters.errorsOnly
    };
};

const mapDispatchErrorsOnlySelector = {
    setErrorsOnly
};

export const ErrorsOnlySelector = connect(mapStateErrorsOnlySelector, mapDispatchErrorsOnlySelector)(ErrorsOnlySelectorC);
