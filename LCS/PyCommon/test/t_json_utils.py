#!/usr/bin/env python3

# Copyright (C) 2012-2015  ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

from lofar.common.test_utils import exit_with_skipped_code_if_skip_unit_tests
exit_with_skipped_code_if_skip_unit_tests()

import logging
logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s %(process)s %(threadName)s %(levelname)s %(message)s', level=logging.DEBUG)

import unittest
import threading
import json
from lofar.common.json_utils import get_default_json_object_for_schema, replace_host_in_urls, resolved_remote_refs, resolved_local_refs, get_sub_schema, raise_on_self_refs, resolved_refs

class TestJSONUtils(unittest.TestCase):
    def setUp(self) -> None:
        # enable full diffs on large schemas
        self.maxDiff = None

    def test_empty_schema_yields_empty_object(self):
        schema = {}
        json = get_default_json_object_for_schema(schema)
        self.assertEqual({}, json)

    def test_schema_for_object_with_plain_properties(self):
        schema = {"type":"object",
                  "default": {},
                  "properties": {
                    "prop_a": { "type": "integer", "default": 42},
                    "prop_b": {"type": "number", "default": 3.14}
                    }
                  }

        json = get_default_json_object_for_schema(schema)
        self.assertEqual({"prop_a": 42,
                          "prop_b": 3.14}, json)

    def test_schema_for_object_with_nested_objects(self):
        schema = {"type":"object",
                  "default": {},
                  "properties": {
                      "sub_a": {"type": "object",
                       "properties": {
                           "prop_a": {"type": "integer", "default": 42},
                           "prop_b": {"type": "number", "default": 3.14}
                       }
                       },
                    "prop_a": { "type": "integer", "default": 42},
                    "prop_b": {"type": "number", "default": 3.14}
                    }
                  }

        json = get_default_json_object_for_schema(schema)
        self.assertEqual({"sub_a": {"prop_a": 42,
                          "prop_b": 3.14},
                          "prop_a": 42,
                          "prop_b": 3.14}, json)

    def test_get_sub_schema(self):
        test_schema = { "one": { "two": { "three": "value" }, "foo": "bar" }, "foo": "bar" }

        # resolve root
        self.assertEqual(test_schema, get_sub_schema(test_schema, "/"))
        self.assertEqual(test_schema, get_sub_schema(test_schema, ""))

        # resolve deeper
        self.assertEqual(test_schema["one"], get_sub_schema(test_schema, "/one"))
        self.assertEqual(test_schema["one"]["two"], get_sub_schema(test_schema, "/one/two"))
        self.assertEqual("value", get_sub_schema(test_schema, "/one/two/three"))

        # check variants
        self.assertEqual("value", get_sub_schema(test_schema, "/one/two/three/"))
        self.assertEqual("value", get_sub_schema(test_schema, "one/two/three"))

    def test_resolved_local_refs(self):
        '''test if $refs to #/definitions are properly resolved'''

        user_schema = {"definitions": {
                             "timestamp": {
                                 "description": "A timestamp defined in UTC",
                                 "type": "string",
                                 "pattern": "\\d{4}-[01]\\d-[0-3]\\dT[0-2]\\d:[0-5]\\d:[0-5]\\d(\\.\\d+)?Z?",
                                 "format": "date-time"
                             },
                             "email": {
                                 "type": "string",
                                 "format": "email",
                                 "pattern": "@example\\.com$" },
                             "account": {
                                 "type": "object",
                                 "properties": {
                                     "email_address": {
                                         "$ref": "#/definitions/email"
                                     },
                                     "creation_at": {
                                         "$ref": "#/definitions/timestamp"
                                     }
                                 }
                             },
                             "accounts": {
                                 "type": "array",
                                 "items": {
                                     "$ref": "#/definitions/account"
                                 }
                             }
                         },
                       "type": "object",
                       "default": {},
                       "properties": {
                           "name": {
                               "type": "string",
                               "minLength": 2 },
                           "email": {
                               "$ref": "#/definitions/email",
                               "extra_prop": "very important"
                           },
                           "other_accounts": {
                                   "$ref": "#/definitions/accounts"
                           } } }

        expected_resolved_user_schema = { "type": "object",
                                          "default": {},
                                          'definitions': {'account': {'properties': {'creation_at': {'description': 'A '
                                                                                                                    'timestamp '
                                                                                                                    'defined '
                                                                                                                    'in '
                                                                                                                    'UTC',
                                                                                                     'format': 'date-time',
                                                                                                     'pattern': '\\d{4}-[01]\\d-[0-3]\\dT[0-2]\\d:[0-5]\\d:[0-5]\\d(\\.\\d+)?Z?',
                                                                                                     'type': 'string'},
                                                                                     'email_address': {
                                                                                         'format': 'email',
                                                                                         'pattern': '@example\\.com$',
                                                                                         'type': 'string'}},
                                                                      'type': 'object'},
                                                          'accounts': {'items': {
                                                              'properties': {'creation_at': {'description': 'A '
                                                                                                            'timestamp '
                                                                                                            'defined '
                                                                                                            'in '
                                                                                                            'UTC',
                                                                                             'format': 'date-time',
                                                                                             'pattern': '\\d{4}-[01]\\d-[0-3]\\dT[0-2]\\d:[0-5]\\d:[0-5]\\d(\\.\\d+)?Z?',
                                                                                             'type': 'string'},
                                                                             'email_address': {'format': 'email',
                                                                                               'pattern': '@example\\.com$',
                                                                                               'type': 'string'}},
                                                              'type': 'object'},
                                                                       'type': 'array'},
                                                          'email': {'format': 'email',
                                                                    'pattern': '@example\\.com$',
                                                                    'type': 'string'},
                                                          'timestamp': {'description': 'A timestamp defined in UTC',
                                                                        'format': 'date-time',
                                                                        'pattern': '\\d{4}-[01]\\d-[0-3]\\dT[0-2]\\d:[0-5]\\d:[0-5]\\d(\\.\\d+)?Z?',
                                                                        'type': 'string'}},
                                          "properties": {
                                            "name": {
                                              "type": "string",
                                              "minLength": 2
                                            },
                                            "email": {
                                              "type": "string",
                                              "format": "email",
                                              "pattern": "@example\\.com$",
                                              "extra_prop": "very important"
                                            },
                                            "other_accounts": {
                                              "type": "array",
                                              "items": {
                                                "type": "object",
                                                "properties": {
                                                    "email_address": {
                                                        "type": "string",
                                                        "format": "email",
                                                        "pattern": "@example\\.com$"
                                                    },
                                                    "creation_at": {
                                                        "description": "A timestamp defined in UTC",
                                                        "type": "string",
                                                        "pattern": "\\d{4}-[01]\\d-[0-3]\\dT[0-2]\\d:[0-5]\\d:[0-5]\\d(\\.\\d+)?Z?",
                                                        "format": "date-time"
                                                    }
                                                }
                                              }
                                            }
                                          }
                                        }

        resolved_user_schema = resolved_local_refs(user_schema)
        self.assertEqual(expected_resolved_user_schema, resolved_user_schema)

        self.assertNotEqual(user_schema['properties']['email'], resolved_user_schema['properties']['email'])
        for key,value in user_schema['definitions']['email'].items():
            self.assertEqual(value, resolved_user_schema['properties']['email'][key])
        self.assertTrue('extra_prop' in resolved_user_schema['properties']['email'])
        self.assertEqual('very important', resolved_user_schema['properties']['email']['extra_prop'])


    def test_raise_on_self_refs(self):
        base_url = "http://127.0.0.1/schemas/"

        # create a schema where one of the properties remote references to this schema via http. (That is forbidden.)
        base_schema = { "$id": base_url + "/base_schema.json",
                        "$schema": "http://json-schema.org/draft-06/schema#",
                         "definitions": {
                             "my_prop_definition": {
                                 "type": "string",
                                 "minLength": 2}
                         },
                         "properties": {
                             "my_self_referencing_prop": {
                                 "$ref": base_url + "/base_schema.json" + "/#/definitions/my_prop_definition"
                                 }
                             }
                         }

        # check that we do raise on this forbidden kind of self reference
        with self.assertRaises(Exception) as context:
            raise_on_self_refs(base_schema)
        self.assertTrue('contains a $ref to itself' in str(context.exception))

    def test_resolved_remote_refs(self):
        '''test if $refs to URL's are properly resolved'''
        import http.server
        import socketserver
        from lofar.common.util import find_free_port

        port = find_free_port(8000, allow_reuse_of_lingering_port=False)
        host = "127.0.0.1"
        host_port = "%s:%s" % (host, port)
        base_url = "http://"+host_port

        base_schema = { "$id": base_url + "/base_schema.json",
                        "$schema": "http://json-schema.org/draft-06/schema#",
                         "definitions": {
                             "timestamp": {
                                 "description": "A timestamp defined in UTC",
                                 "type": "string",
                                 "pattern": "\\d{4}-[01]\\d-[0-3]\\dT[0-2]\\d:[0-5]\\d:[0-5]\\d(\\.\\d+)?Z?",
                                 "format": "date-time"
                             },
                             "email": {
                                 "type": "string",
                                 "format": "email",
                                 "pattern": "@example\\.com$" },
                             "account": {
                                 "type": "object",
                                 "properties": {
                                     "email_address": {
                                         "$ref": "#/definitions/email"
                                     },
                                     "creation_at": {
                                         "$ref": "#/definitions/timestamp"
                                     }
                                 }
                             },
                             "accounts": {
                                 "type": "array",
                                 "items": {
                                     "$ref": "#/definitions/account"
                                 }
                             }
                         } }

        user_schema = {"$id": base_url + "/user_schema.json",
                       "$schema": "http://json-schema.org/draft-06/schema#",
                       "type": "object",
                       "default": {},
                       "definitions": {
                           "user_schema_local_prop": {
                               "type": "boolean"
                           }
                       },
                       "properties": {
                           "name": {
                               "type": "string",
                               "minLength": 2 },
                           "user_account": {
                               "$ref": base_url + "/base_schema.json" + "#/definitions/account",
                               "extra_prop": "very important"
                           },
                           "other_emails": {
                               "type": "array",
                               "items": {
                                   "$ref": base_url + "/base_schema.json" + "#/definitions/email"
                               }
                           },
                           "other_accounts": {
                               "$ref": base_url + "/base_schema.json" + "#/definitions/accounts"
                           }
                       } }

        class TestRequestHandler(http.server.BaseHTTPRequestHandler):
            '''helper class to serve the schemas via http. Needed for resolving the $ref URLs'''
            def send_json_response(self, json_object):
                self.send_response(http.HTTPStatus.OK)
                self.send_header("Content-type", "application/json")
                self.end_headers()
                self.wfile.write(json.dumps(json_object, indent=2).encode('utf-8'))

            def do_GET(self):
                try:
                    if self.path == "/base_schema.json":
                        self.send_json_response(base_schema)
                    elif self.path == "/user_schema.json":
                        self.send_json_response(user_schema)
                    else:
                        self.send_error(http.HTTPStatus.NOT_FOUND, "No such resource")
                except Exception as e:
                    self.send_error(http.HTTPStatus.INTERNAL_SERVER_ERROR, str(e))

        with socketserver.TCPServer((host, port), TestRequestHandler) as httpd:
            thread = threading.Thread(target=httpd.serve_forever)
            thread.start()

            try:
                # the method-under-test
                resolved_user_schema = resolved_remote_refs(user_schema)

                logger.info('base_schema: %s', json.dumps(base_schema, indent=2))
                logger.info('user_schema: %s', json.dumps(user_schema, indent=2))
                logger.info('resolved_user_schema: %s', json.dumps(resolved_user_schema, indent=2))



                expected_resolved_user_schema = r'''{
  "$id": "%s/user_schema.json/ref_resolved",
  "$schema": "http://json-schema.org/draft-06/schema#",
  "type": "object",
  "default": {},
  "definitions": {
    "user_schema_local_prop": {
      "type": "boolean"
    },
    "timestamp": {
      "description": "A timestamp defined in UTC",
      "type": "string",
      "pattern": "\\d{4}-[01]\\d-[0-3]\\dT[0-2]\\d:[0-5]\\d:[0-5]\\d(\\.\\d+)?Z?",
      "format": "date-time"
    },
    "email": {
      "type": "string",
      "format": "email",
      "pattern": "@example\\.com$"
    },
    "accounts": {"items": {"$ref": "#/definitions/account"},
                 "type": "array"},
    "account": {
      "type": "object",
      "properties": {
        "email_address": {
          "$ref": "#/definitions/email"
        },
        "creation_at": {
          "$ref": "#/definitions/timestamp"
        }
      }
    }
  },
  "properties": {
    "name": {
      "type": "string",
      "minLength": 2
    },
    "user_account": {
      "$ref": "#/definitions/account",
      "extra_prop": "very important"
    },
    "other_accounts": {"$ref": "#/definitions/accounts"},
    "other_emails": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/email"
      }
    }
  }
}
''' % (base_url,)

                self.assertEqual(json.loads(expected_resolved_user_schema), resolved_user_schema)

            finally:
                httpd.shutdown()
                thread.join(timeout=2)
                self.assertFalse(thread.is_alive())


    def test_ambiguous_remote_refs_raises(self):
        '''test if ambiguous $refs raise'''
        import http.server
        import socketserver
        from lofar.common.util import find_free_port

        port = find_free_port(8000, allow_reuse_of_lingering_port=False)
        host = "127.0.0.1"
        host_port = "%s:%s" % (host, port)
        base_url = "http://"+host_port

        # create 2 schemas with the a different definition for "my_prop"
        base_schema1 = { "$id": base_url + "/base_schema1.json",
                         "$schema": "http://json-schema.org/draft-06/schema#",
                         "definitions": {
                             "my_prop": {
                                 "type": "string"
                             }
                           }
                         }
        base_schema2 = { "$id": base_url + "/base_schema2.json",
                         "$schema": "http://json-schema.org/draft-06/schema#",
                         "definitions": {
                             "my_prop": {
                                 "type": "int"
                             }
                           }
                         }

        # use both in one schema...
        user_schema = {"$id": base_url + "/user_schema.json",
                       "$schema": "http://json-schema.org/draft-06/schema#",
                       "type": "object",
                       "properties": {
                           "abc": {
                               "$ref": base_url + "/base_schema1.json" + "#/definitions/my_prop"
                           },
                           "xyz": {
                               "$ref": base_url + "/base_schema2.json" + "#/definitions/my_prop"
                           }
                         }
                       }

        # run an http server to host them...
        class TestRequestHandler(http.server.BaseHTTPRequestHandler):
            '''helper class to serve the schemas via http. Needed for resolving the $ref URLs'''
            def send_json_response(self, json_object):
                self.send_response(http.HTTPStatus.OK)
                self.send_header("Content-type", "application/json")
                self.end_headers()
                self.wfile.write(json.dumps(json_object, indent=2).encode('utf-8'))

            def do_GET(self):
                try:
                    if self.path == "/base_schema1.json":
                        self.send_json_response(base_schema1)
                    elif self.path == "/base_schema2.json":
                        self.send_json_response(base_schema2)
                    elif self.path == "/user_schema.json":
                        self.send_json_response(user_schema)
                    else:
                        self.send_error(http.HTTPStatus.NOT_FOUND, "No such resource")
                except Exception as e:
                    self.send_error(http.HTTPStatus.INTERNAL_SERVER_ERROR, str(e))

        with socketserver.TCPServer((host, port), TestRequestHandler) as httpd:
            thread = threading.Thread(target=httpd.serve_forever)
            thread.start()

            try:
                with self.assertRaises(Exception) as context:
                    resolved_remote_refs(user_schema)

                self.assertTrue('ambiguity while resolving remote references' in str(context.exception))

            finally:
                httpd.shutdown()
                thread.join(timeout=2)
                self.assertFalse(thread.is_alive())


    def test_replace_host_in_ref_urls(self):
        base_host = "http://foo.bar.com"
        path = "/my/path"

        schema = {"$id": base_host + path + "/user_schema.json",
                  "$schema": "http://json-schema.org/draft-06/schema#",
                  "type": "object",
                  "default": {},
                  "properties": {
                      "name": {
                          "type": "string",
                          "minLength": 2 },
                      "email": {
                          "$ref": base_host + path + "/base_schema.json" + "#/definitions/email"  },
                       "other_emails": {
                           "type": "array",
                           "items": {
                                "$ref": base_host + path + "/base_schema.json" + "#/definitions/email"
                            }
                       }
                  } }

        new_base_host = 'http://127.0.0.1'
        url_fixed_schema = replace_host_in_urls(schema, new_base_host)

        logger.info('schema: %s', json.dumps(schema, indent=2))
        logger.info('url_fixed_schema: %s', json.dumps(url_fixed_schema, indent=2))

        self.assertEqual(new_base_host+path+"/user_schema.json", url_fixed_schema['$id'])
        self.assertEqual(new_base_host+path+"/base_schema.json" + "#/definitions/email", url_fixed_schema['properties']['email']['$ref'])
        self.assertEqual(new_base_host+path+"/base_schema.json" + "#/definitions/email", url_fixed_schema['properties']['other_emails']['items']['$ref'])
        self.assertEqual("http://json-schema.org/draft-06/schema#", url_fixed_schema['$schema'])
        self.assertEqual(json.dumps(schema, indent=2).replace(base_host, new_base_host), json.dumps(url_fixed_schema, indent=2))

if __name__ == '__main__':
    unittest.main()
