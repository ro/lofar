import unittest

from mock import patch, MagicMock, Mock, call

from lofar.maintenance.mdb.client import *

logger = logging.getLogger(__name__)


def load_test_file(file_name):
    with open(file_name, 'r') as f_read_stream:
        content = f_read_stream.read().strip('\n')
    return content


def load_rtsm_test():
    file_name = 't_client.in.test_rtsm.data'
    return load_test_file(file_name)


def load_station_test():
    file_name = 't_client.in.test_stationtest.data'
    return load_test_file(file_name)


class TESTMDBClient(unittest.TestCase):
    def test_is_station_test_right_content(self):
        station_test_content = load_station_test()
        self.assertTrue(is_station_test(station_test_content))

    def test_is_station_test_wrong_content(self):
        station_test_content = load_rtsm_test()
        self.assertFalse(is_station_test(station_test_content))

    def test_is_rtsm_test_right_content(self):
        rtsm_test_content = load_rtsm_test()
        self.assertTrue(is_rtsm_test(rtsm_test_content))

    def test_is_rtsm_test_wrong_content(self):
        rtsm_test_content = load_station_test()
        self.assertFalse(is_rtsm_test(rtsm_test_content))

    @patch('requests.post')
    def test_send_stationtest_rtsm_content_to_address_success(self, mocked_post):
        station_test_content = load_station_test()
        address = 'http://testaddress'
        response = Mock(status_code=200, reason='ALL GOOD', content='YEAH')
        mocked_post.return_value = response

        full_address = '/'.join([address, compose_api_url_for_given_test_type(station_test_content)])
        result = send_stationtest_rtsm_content_to_address('http://testaddress', dict(content=station_test_content))
        self.assertTrue(mocked_post.called)
        mocked_post.assert_called_with(full_address, data=dict(content=station_test_content))
        self.assertTrue(result)

    @patch('requests.post')
    def test_send_stationtest_rtsm_content_to_address_failure(self, mocked_post):
        station_test_content = load_station_test()
        address = 'http://testaddress'
        response = Mock(status_code=400, reason='Bad Request', content='Sorry')
        mocked_post.return_value = response

        full_address = '/'.join([address, compose_api_url_for_given_test_type(station_test_content)])
        result = send_stationtest_rtsm_content_to_address('http://testaddress', dict(content=station_test_content))
        self.assertTrue(mocked_post.called)
        mocked_post.assert_called_with(full_address, data=dict(content=station_test_content))
        self.assertFalse(result)

    def test_compose_api_url_for_given_test_type_is_rtsm(self):
        rtsm_test_content = load_rtsm_test()
        self.assertEqual(compose_api_url_for_given_test_type(rtsm_test_content), 'rtsm/raw/insert')

    def test_compose_api_url_for_given_test_type_is_station_test(self):
        station_test_content = load_station_test()
        self.assertEqual(compose_api_url_for_given_test_type(station_test_content), 'stationtests/raw/insert')

    def test_compose_api_url_for_given_test_type_raises_content_unrecognized(self):
        with self.assertRaises(ValueError):
            compose_api_url_for_given_test_type('Neque porro quisquam est qui dolorem ipsum quia'
                                                ' dolor sit amet'
                                                'consectetur, adipisci velit...')


    @patch('lofar.maintenance.mdb.client.open', create=True)
    def test_read_stationtest_rtsm_output_to_dict_rtsm_success(self, open_mock):
        file_name = 'CS001C_12345_20180305.dat'
        file_content = 'testline1\ntestline2'
        open_mock.return_value = MagicMock()
        file_handle = open_mock.return_value.__enter__.return_value

        file_handle.read.return_value = file_content

        result = read_stationtest_rtsm_output_to_dict(file_name)
        open_mock.assert_called_with(file_name, 'r')
        file_handle.read.assert_called()
        self.assertEqual(result, dict(content=file_content, station_name='CS001C'))

    @patch('lofar.maintenance.mdb.client.open', create=True)
    def test_read_stationtest_rtsm_output_to_dict_rtsm_failure(self, open_mock):
        file_name = 'CS001C_12345_20180305.dat'
        open_mock.return_value = MagicMock()

        file_handle = open_mock.return_value.__enter__.return_value

        file_handle.read.side_effect = IOError('Cannot read file')

        result = read_stationtest_rtsm_output_to_dict(file_name)
        open_mock.assert_called_with(file_name, 'r')
        file_handle.read.assert_called()
        self.assertIsNone(result)

    @patch('lofar.maintenance.mdb.client.open', create=True)
    def test_read_stationtest_rtsm_output_to_dict_station_test_success(self, open_mock):
        file_name = 'CS031C_L2_StationTestHistory.csv'
        file_content = 'testline1\ntestline2'
        open_mock.return_value = MagicMock()
        file_handle = open_mock.return_value.__enter__.return_value

        file_handle.read.return_value = file_content

        result = read_stationtest_rtsm_output_to_dict(file_name)
        open_mock.assert_called_with(file_name, 'r')
        file_handle.read.assert_called()
        self.assertEqual(result, dict(content=file_content))


if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=logging.DEBUG)
    unittest.main()
