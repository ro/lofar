import React, { Component } from 'react';

//Place on top to get the correct order of loading
import './App.css';

import LandingPage from 'pages/LandingPage'
import StationOverviewPage from 'pages/StationOverviewPage'
import TilesPage from 'pages/TilesPage'

import { connect } from "react-redux";
import { fetchErrorTypes, fetchStations } from "redux/actions/appInitDataActions.js";

import {
  //BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import { ConnectedRouter as Router } from 'connected-react-router';
import { history } from "redux/store.js";



class AppC extends Component {

  componentDidMount( ) {
      // Load initial application data
      this.props.dispatch(fetchErrorTypes());
      this.props.dispatch(fetchStations());
  }

  render(){
    return (
        <Router history={history}>
            <div>
                <Switch>
                    <Route exact path="/" component={LandingPage}/>
                    <Route path="/station_overview/:name?" component={StationOverviewPage}/>
                    <Route exact path="/tiles/:antenna?" component={TilesPage}/>
                </Switch>
            </div>
        </Router>
    );
    }
}

const App = connect()(AppC);

export default App;
