import React, {
    PureComponent
} from 'react';
import {
    Badge,
    Card,
    CardBody,
    Table
} from 'reactstrap';
import {formatDateUTC} from "utils/utils";

// CSS
//import styles from './styles.module.scss'


class StatusDetails extends PureComponent {

    getStatusData() {
        let data = this.props.data;
        let statusData = {};

        if (data.dataIdx === "current") {
            statusData = data.content
        } else if (data.content && data.content.status) {
            statusData = data.content.status[data.antenna_id];
        }
        return statusData
    }

    render() {
        let statusData = this.getStatusData();

        return (
            <Card>
                <CardBody>
                    <Table borderless size="sm">
                       <tbody>
                           <tr><td>Component status:</td><td><Badge color="danger">{statusData.status}</Badge></td></tr>
                           <tr><td>Code:</td><td>{statusData.status_code}</td></tr>
                           <tr><td>Modified:</td><td>{formatDateUTC(statusData.last_modified)}</td></tr>
                           <tr><td>Reason:</td><td>{statusData.reason}</td></tr>
                       </tbody>
                   </Table>
                </CardBody>
            </Card>
        );
    }
}


export default StatusDetails;
