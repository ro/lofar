cmake_minimum_required(VERSION 2.8)

#setup cpack variables and configuration
set(CPACK_PACKAGE_NAME "lofar")
set(CPACK_PACKAGE_FILE_NAME "lofar")
set(CPACK_PACKAGE_VENDOR "Astron")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "A lofar rpm package. <TODO: add summary>")
set(CPACK_RPM_PACKAGE_DESCRIPTION "A lofar rpm package. <TODO: add description>")
set(CPACK_RPM_PACKAGE_GROUP "radio astronomy")
set(CPACK_RPM_PACKAGE_LICENSE "GPL3")

# these lofar software version numbers are applied to each rpm package
set(CPACK_PACKAGE_VERSION_MAJOR "3")
set(CPACK_PACKAGE_VERSION_MINOR "1")
set(CPACK_PACKAGE_VERSION_PATCH "0")

set(CPACK_GENERATOR "RPM")
set(CPACK_RPM_COMPONENT_INSTALL ON)
set(CPACK_SET_DESTDIR ON)
set(CPACK_PACKAGE_RELOCATABLE FALSE)
set(CPACK_RPM_PACKAGE_RELOCATABLE FALSE)
set(CPACK_COMPONENTS_GROUPING ONE_PER_GROUP) #creates one package file per component group (one package file per lofar package)
set(CPACK_RPM_PACKAGE_AUTOREQ OFF) # AUTOREQ is a nice feaure, but it also detects 'packages' like casa, for which we do not have rpm's (yet). So currently disabled.

#if we install in "/opt/lofar" or any other path starting with default unix directories
# we do not want the install the /opt (or other default unix) dir, because that will conflict with rpm package "filesystem", which setsup the default linux filesystem
set(CPACK_RPM_EXCLUDE_FROM_AUTO_FILELIST "/opt;/home;/usr;/tmp")



# fire up cmake cpack module.
include(CPack)

