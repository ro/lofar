from currentobs import currentobs
from currentobs import get_all_parameters_new

import time
nogui=True
#f=open('/home/veen/logs/rundatawriter.txt','w')
prevobsid="None"
datawriterrunning=False
proclist=[]
import TBBdatawriterController as TBBc
import subprocess
import os, sys

print("Starting to run datawriter, but need an observation first")

fCurrentlyDumping="/home/veen/datawriter/nowdumping.py"

if os.access(fCurrentlyDumping,os.R_OK):
    exec(compile(open(fCurrentlyDumping, "rb").read(), fCurrentlyDumping, 'exec'))
else:
    print("WARNING unable to open ",fCurrentlyDumping)
    bCurrentlyDumping=False

if len(sys.argv)>1:
    offset=int(sys.argv[1])
else:
    offset=0

while 1:
    obsid=currentobs(offset)
    if not obsid or obsid == "None" and time.time() < 1344345492+3600: # start test on august 7
        obsid="L63176"
    #obsid="L42479"
    if not obsid or obsid == "None" or obsid==prevobsid:
        time.sleep(20)
    else:
        p=dict()
        if(obsid=='L57197'):
            p=get_all_parameters_new('/home/veen/tbbdump/locustest/latest/L57197.parset',True)
        else:
            p=get_all_parameters_new(obsid)
#        observer=p['Observation.ObserverName']
        observer=p['Observation.Campaign.PI']
        antennaSet=p['Observation.antennaSet']
#        project=p['Observation.ProjectName']
        project=p['Observation.Campaign.name']
        observationID="L"+p['Observation.tmssID']
        filterSelection=p['Observation.bandFilter']
        print('Observation ',obsid,'Start time ', p['Observation.startTime'],' Stop time: ',p['Observation.stopTime']+'\n')
        if datawriterrunning:
            print('Stop datawriter for obs ',prevobsid,' at ',time.strftime('%Y-%m-%d %H:%M:%S')+'\n')
            if os.access(fCurrentlyDumping,os.R_OK):
                exec(compile(open(fCurrentlyDumping, "rb").read(), fCurrentlyDumping, 'exec'))
                while(bCurrentlyDumping):
                    time.sleep(1)
                    exec(compile(open(fCurrentlyDumping, "rb").read(), fCurrentlyDumping, 'exec'))
            TBBc.stopdatawriter(proclist)
            try:
                proc=subprocess.Popen(["python","/home/veen/datawriter/addmetadata.py","/data/TBB/VHECRtest",prevobsid])
            except:
                pass
            #os.system("chmod 664 /data/TBB/VHECRtest/*.h5")
            datawriterrunning=False
            time.sleep(15)
        else: # datawriter not running
            print('Start datawriter for obs ',obsid,' at ',time.strftime('%Y-%m-%d %H:%M:%S')+'\n')
            proclist=TBBc.startdatawriter(filename="",timeouttime=20,observer=observer,antennaSet=antennaSet,project=project,observationID=observationID,filterSelection=filterSelection)
            datawriterrunning=True
            prevobsid=obsid

