import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, ButtonGroup} from 'reactstrap';
import {setTestType} from "redux/actions/mainFiltersActions";

// CSS
import toolbarStyles from '../styles.module.scss'


/**
 * Radio buttons for Test Type
 */
class TestTypeSelectorC extends Component {

    onTestTypeClick(type) {
        this.props.setTestType(type);
    }

    render() {
        return (<div className={toolbarStyles.toolbar_ctrl}>
            <label>Type</label>
            <ButtonGroup size="sm">
                <Button color="info" onClick={() => this.onTestTypeClick('B')} active={this.props.testType === 'B'}>BOTH</Button>
                <Button color="info" onClick={() => this.onTestTypeClick('S')} active={this.props.testType === 'S'}>ST-TEST</Button>
                <Button color="info" onClick={() => this.onTestTypeClick('R')} active={this.props.testType === 'R'}>RTSM</Button>
            </ButtonGroup>
        </div>);
    }
}

const mapStateTestTypeSelector = state => {
    return {
        testType: state.mainFilters.testType
    };
};

const mapDispatchTestTypeSelector = {
    setTestType
};

export const TestTypeSelector = connect(mapStateTestTypeSelector, mapDispatchTestTypeSelector)(TestTypeSelectorC);
