#!/usr/bin/env python3

# Copyright (C) 2012-2015    ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

# $Id$
import os, sys
import uuid

import logging

logger = logging.getLogger(__name__)

from lofar.common.dbcredentials import Credentials, DBCredentials

class TemporaryCredentials():
    ''' A helper class which creates/destroys dbcredentials automatically.
    Best used in a 'with'-context so the server is destroyed automagically.
    '''
    def __init__(self, user: str = 'test', password: str='test', dbcreds_id: str=None) -> None:
        self.dbcreds_id = dbcreds_id or str(uuid.uuid4())
        self.dbcreds = Credentials()
        self.dbcreds.user = user
        self.dbcreds.password = password
        self._dbcreds_path = None
        self._delete_on_exit = True

    def __enter__(self):
        '''calls create (and calls destroy in the __exit__ function)'''
        try:
            self.create_if_not_existing()
        except Exception as e:
            logger.error(e)
            self.destroy_if_not_existing_upon_creation()
            raise
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        '''calls destroy the temporary credentials file'''
        self.destroy_if_not_existing_upon_creation()

    def create_if_not_existing(self):
        '''create the the temporary credentials and store them in a file'''
        self._dbcreds_path = os.path.expanduser('~/.lofar/dbcredentials/%s.ini' % (self.dbcreds_id,))

        if os.path.exists(self._dbcreds_path):
            existing_creds = DBCredentials().get(self.dbcreds_id)
            if existing_creds != self.dbcreds:
                raise RuntimeWarning("Cannot reuse existing dbcreds in '%s' because they are different from the temporary creds.\n"\
                                     "file: %s\n"\
                                     "temp: %s" % (self._dbcreds_path, existing_creds.stringWithHiddenPassword(), self.dbcreds.stringWithHiddenPassword()))

            logger.info('reusing tmp dbcreds file from existing file\'%s\': %s', self._dbcreds_path, self.dbcreds.stringWithHiddenPassword())
            self._delete_on_exit = False
        else:
            logger.info('saving tmp dbcreds file \'%s\': %s', self._dbcreds_path, self.dbcreds.stringWithHiddenPassword())

            if not os.path.exists(os.path.dirname(self._dbcreds_path)):
                os.makedirs(os.path.dirname(self._dbcreds_path))

            with open(self._dbcreds_path, 'w+') as file:
                file.write("[database:%s]\nhost=%s\nuser=%s\npassword=%s\ntype=%s\nport=%d\ndatabase=%s\n" %
                           (self.dbcreds_id,
                            self.dbcreds.host,
                            self.dbcreds.user,
                            self.dbcreds.password,
                            self.dbcreds.type,
                            self.dbcreds.port,
                            self.dbcreds.database))
                self._delete_on_exit = True

        # make the credentials user-rw only
        try:
            os.chmod(self._dbcreds_path, 0o600)
        except Exception as e:
            logger.error('Error: Could not change permissions on %s: %s' % (self._dbcreds_path, str(e)))

    def destroy_if_not_existing_upon_creation(self):
        '''destroy the temporary credentials file if the file was not existing upon creation'''
        try:
            if self._delete_on_exit and self._dbcreds_path and os.path.exists(self._dbcreds_path):
                logger.info('removing tmp dbcreds file \'%s\'', self._dbcreds_path)
                os.remove(self._dbcreds_path)
        except Exception as e:
            logger.error("Could not remove temporary credentials file '%s': %s", self._dbcreds_path, e)

__all__ = ['TemporaryCredentials']