import random
import string


def free_random_port():
    import socket
    with socket.socket() as sock:
        sock.bind(('', 0))
        port = sock.getsockname()[1]
    return port


def test_database_configuration():
    return {
        # Postgres:
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'test_mdb',
        'USER': ''.join(random.sample(string.ascii_lowercase, 10)),
        'PASSWORD': ''.join(random.sample(string.ascii_letters, 10)),
        'HOST': 'localhost',
        'PORT': free_random_port()
    }