const data = [
    {
        "station_name": "CS001C",
        "rtsm": [
            {
                "observation_id": 672048,
                "start_datetime": "2018-10-14T19:55:00Z",
                "end_datetime": "2018-10-14T20:15:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671992,
                "start_datetime": "2018-10-14T15:25:00Z",
                "end_datetime": "2018-10-14T15:55:00Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 1,
                    "LOW_NOISE": 1
                }
            }, {
                "observation_id": 671980,
                "start_datetime": "2018-10-14T13:44:00Z",
                "end_datetime": "2018-10-14T14:04:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671972,
                "start_datetime": "2018-10-14T13:10:00Z",
                "end_datetime": "2018-10-14T13:30:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671968,
                "start_datetime": "2018-10-14T12:59:00Z",
                "end_datetime": "2018-10-14T13:09:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671964,
                "start_datetime": "2018-10-14T12:48:00Z",
                "end_datetime": "2018-10-14T12:58:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671952,
                "start_datetime": "2018-10-14T10:28:00Z",
                "end_datetime": "2018-10-14T10:38:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671948,
                "start_datetime": "2018-10-14T08:49:00Z",
                "end_datetime": "2018-10-14T09:09:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671940,
                "start_datetime": "2018-10-14T08:17:00Z",
                "end_datetime": "2018-10-14T08:37:00Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 671936,
                "start_datetime": "2018-10-14T08:06:00Z",
                "end_datetime": "2018-10-14T08:16:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671932,
                "start_datetime": "2018-10-14T07:44:00Z",
                "end_datetime": "2018-10-14T08:04:00Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 671928,
                "start_datetime": "2018-10-14T07:33:00Z",
                "end_datetime": "2018-10-14T07:43:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671924,
                "start_datetime": "2018-10-14T07:20:00Z",
                "end_datetime": "2018-10-14T07:30:00Z",
                "mode": [5],
                "total_component_errors": 3,
                "error_summary": {
                    "HIGH_NOISE": 1,
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 671920,
                "start_datetime": "2018-10-14T07:09:00Z",
                "end_datetime": "2018-10-14T07:19:00Z",
                "mode": [5],
                "total_component_errors": 3,
                "error_summary": {
                    "HIGH_NOISE": 1,
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 671904,
                "start_datetime": "2018-10-14T05:09:00Z",
                "end_datetime": "2018-10-14T05:29:00Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 671896,
                "start_datetime": "2018-10-14T04:28:00Z",
                "end_datetime": "2018-10-14T04:38:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671880,
                "start_datetime": "2018-10-14T02:56:00Z",
                "end_datetime": "2018-10-14T03:06:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671840,
                "start_datetime": "2018-10-13T22:22:00Z",
                "end_datetime": "2018-10-13T22:32:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671792,
                "start_datetime": "2018-10-13T18:40:00Z",
                "end_datetime": "2018-10-13T18:50:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671772,
                "start_datetime": "2018-10-13T17:14:00Z",
                "end_datetime": "2018-10-13T17:34:00Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "LOW_NOISE": 2
                }
            }, {
                "observation_id": 671768,
                "start_datetime": "2018-10-13T16:43:00Z",
                "end_datetime": "2018-10-13T17:13:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "LOW_NOISE": 1
                }
            }, {
                "observation_id": 671756,
                "start_datetime": "2018-10-13T15:55:00Z",
                "end_datetime": "2018-10-13T16:05:00Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "LOW_NOISE": 2
                }
            }, {
                "observation_id": 671752,
                "start_datetime": "2018-10-13T15:34:00Z",
                "end_datetime": "2018-10-13T15:54:00Z",
                "mode": [5],
                "total_component_errors": 3,
                "error_summary": {
                    "LOW_NOISE": 2,
                    "HIGH_NOISE": 1
                }
            }, {
                "observation_id": 671748,
                "start_datetime": "2018-10-13T15:03:00Z",
                "end_datetime": "2018-10-13T15:33:00Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "LOW_NOISE": 2
                }
            }, {
                "observation_id": 671740,
                "start_datetime": "2018-10-13T14:33:00Z",
                "end_datetime": "2018-10-13T14:43:00Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 1,
                    "LOW_NOISE": 1
                }
            }, {
                "observation_id": 672574,
                "start_datetime": "2018-10-13T12:11:00Z",
                "end_datetime": "2018-10-13T12:21:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672566,
                "start_datetime": "2018-10-13T08:15:00Z",
                "end_datetime": "2018-10-13T08:30:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672562,
                "start_datetime": "2018-10-13T08:02:00Z",
                "end_datetime": "2018-10-13T08:12:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672886,
                "start_datetime": "2018-10-13T02:21:00Z",
                "end_datetime": "2018-10-13T06:55:00Z",
                "mode": [1],
                "total_component_errors": 2,
                "error_summary": {
                    "LOW_NOISE": 1,
                    "FLAT": 1
                }
            }, {
                "observation_id": 672878,
                "start_datetime": "2018-10-13T02:05:00Z",
                "end_datetime": "2018-10-13T02:20:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672666,
                "start_datetime": "2018-10-12T22:36:00Z",
                "end_datetime": "2018-10-12T22:46:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672662,
                "start_datetime": "2018-10-12T22:20:00Z",
                "end_datetime": "2018-10-12T22:35:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672882,
                "start_datetime": "2018-10-12T20:45:00Z",
                "end_datetime": "2018-10-12T22:15:00Z",
                "mode": [1],
                "total_component_errors": 2,
                "error_summary": {
                    "LOW_NOISE": 1,
                    "FLAT": 1
                }
            }, {
                "observation_id": 672598,
                "start_datetime": "2018-10-12T16:12:00Z",
                "end_datetime": "2018-10-12T16:22:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672594,
                "start_datetime": "2018-10-12T16:01:00Z",
                "end_datetime": "2018-10-12T16:11:00Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 672862,
                "start_datetime": "2018-10-12T05:00:00Z",
                "end_datetime": "2018-10-12T07:55:00Z",
                "mode": [1],
                "total_component_errors": 1,
                "error_summary": {
                    "FLAT": 1
                }
            }, {
                "observation_id": 672864,
                "start_datetime": "2018-10-12T04:49:00Z",
                "end_datetime": "2018-10-12T04:59:00Z",
                "mode": [1],
                "total_component_errors": 1,
                "error_summary": {
                    "FLAT": 1
                }
            }, {
                "observation_id": 672840,
                "start_datetime": "2018-10-12T04:37:00Z",
                "end_datetime": "2018-10-12T04:47:00Z",
                "mode": [1],
                "total_component_errors": 1,
                "error_summary": {
                    "FLAT": 1
                }
            }, {
                "observation_id": 672838,
                "start_datetime": "2018-10-12T04:26:00Z",
                "end_datetime": "2018-10-12T04:36:00Z",
                "mode": [3],
                "total_component_errors": 1,
                "error_summary": {
                    "FLAT": 1
                }
            }, {
                "observation_id": 672690,
                "start_datetime": "2018-10-12T04:00:00Z",
                "end_datetime": "2018-10-12T04:15:00Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 1,
                    "HIGH_NOISE": 1
                }
            }, {
                "observation_id": 672590,
                "start_datetime": "2018-10-12T03:24:00Z",
                "end_datetime": "2018-10-12T03:34:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672750,
                "start_datetime": "2018-10-12T03:12:00Z",
                "end_datetime": "2018-10-12T03:22:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672734,
                "start_datetime": "2018-10-11T19:11:00Z",
                "end_datetime": "2018-10-12T03:11:00Z",
                "mode": [5],
                "total_component_errors": 4,
                "error_summary": {
                    "HIGH_NOISE": 1,
                    "SUMMATOR_NOISE": 3
                }
            }, {
                "observation_id": 672730,
                "start_datetime": "2018-10-11T19:00:00Z",
                "end_datetime": "2018-10-11T19:10:00Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 672854,
                "start_datetime": "2018-10-11T18:17:00Z",
                "end_datetime": "2018-10-11T18:27:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672470,
                "start_datetime": "2018-10-11T01:19:16Z",
                "end_datetime": "2018-10-11T05:19:16Z",
                "mode": [5],
                "total_component_errors": 4,
                "error_summary": {
                    "HIGH_NOISE": 1,
                    "SUMMATOR_NOISE": 3
                }
            }, {
                "observation_id": 672466,
                "start_datetime": "2018-10-11T01:08:16Z",
                "end_datetime": "2018-10-11T01:18:16Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 670310,
                "start_datetime": "2018-10-10T12:06:40Z",
                "end_datetime": "2018-10-10T20:06:40Z",
                "mode": [5],
                "total_component_errors": 5,
                "error_summary": {
                    "HIGH_NOISE": 2,
                    "SUMMATOR_NOISE": 3
                }
            }, {
                "observation_id": 670306,
                "start_datetime": "2018-10-10T11:55:40Z",
                "end_datetime": "2018-10-10T12:05:40Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 672786,
                "start_datetime": "2018-10-10T08:45:05Z",
                "end_datetime": "2018-10-10T09:15:05Z",
                "mode": [5],
                "total_component_errors": 3,
                "error_summary": {
                    "SUMMATOR_NOISE": 3
                }
            }, {
                "observation_id": 672420,
                "start_datetime": "2018-10-10T07:15:28Z",
                "end_datetime": "2018-10-10T07:25:28Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 672484,
                "start_datetime": "2018-10-10T01:10:11Z",
                "end_datetime": "2018-10-10T05:10:11Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 672480,
                "start_datetime": "2018-10-10T00:59:11Z",
                "end_datetime": "2018-10-10T01:09:11Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672708,
                "start_datetime": "2018-10-09T23:07:07Z",
                "end_datetime": "2018-10-09T23:17:07Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 672698,
                "start_datetime": "2018-10-09T18:55:07Z",
                "end_datetime": "2018-10-09T19:05:07Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672722,
                "start_datetime": "2018-10-09T18:07:49Z",
                "end_datetime": "2018-10-09T18:17:49Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672716,
                "start_datetime": "2018-10-09T14:06:49Z",
                "end_datetime": "2018-10-09T18:06:49Z",
                "mode": [5],
                "total_component_errors": 17,
                "error_summary": {
                    "LOW_NOISE": 3,
                    "SUMMATOR_NOISE": 2,
                    "HIGH_NOISE": 12
                }
            }, {
                "observation_id": 672712,
                "start_datetime": "2018-10-09T13:55:49Z",
                "end_datetime": "2018-10-09T14:05:49Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 672402,
                "start_datetime": "2018-10-09T12:44:21Z",
                "end_datetime": "2018-10-09T12:54:21Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672410,
                "start_datetime": "2018-10-08T20:20:14Z",
                "end_datetime": "2018-10-09T04:20:14Z",
                "mode": [5],
                "total_component_errors": 6,
                "error_summary": {
                    "HIGH_NOISE": 3,
                    "SUMMATOR_NOISE": 3
                }
            }, {
                "observation_id": 672406,
                "start_datetime": "2018-10-08T20:09:14Z",
                "end_datetime": "2018-10-08T20:19:14Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 667914,
                "start_datetime": "2018-10-08T08:00:00Z",
                "end_datetime": "2018-10-08T16:00:00Z",
                "mode": [2],
                "total_component_errors": 1,
                "error_summary": {
                    "FLAT": 1
                }
            }, {
                "observation_id": 672090,
                "start_datetime": "2018-10-07T17:01:00Z",
                "end_datetime": "2018-10-07T17:11:00Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 2
                }
            }, {
                "observation_id": 672192,
                "start_datetime": "2018-10-07T04:53:00Z",
                "end_datetime": "2018-10-07T07:47:00Z",
                "mode": [1],
                "total_component_errors": 2,
                "error_summary": {
                    "LOW_NOISE": 1,
                    "FLAT": 1
                }
            }, {
                "observation_id": 672076,
                "start_datetime": "2018-10-07T04:47:00Z",
                "end_datetime": "2018-10-07T04:52:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672072,
                "start_datetime": "2018-10-06T22:56:00Z",
                "end_datetime": "2018-10-07T04:46:00Z",
                "mode": [5],
                "total_component_errors": 3,
                "error_summary": {
                    "SUMMATOR_NOISE": 3
                }
            }, {
                "observation_id": 672068,
                "start_datetime": "2018-10-06T22:50:00Z",
                "end_datetime": "2018-10-06T22:55:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 672060,
                "start_datetime": "2018-10-05T22:56:00Z",
                "end_datetime": "2018-10-06T04:46:00Z",
                "mode": [5],
                "total_component_errors": 4,
                "error_summary": {
                    "HIGH_NOISE": 1,
                    "SUMMATOR_NOISE": 3
                }
            }, {
                "observation_id": 672056,
                "start_datetime": "2018-10-05T22:50:00Z",
                "end_datetime": "2018-10-05T22:55:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }
        ]
    }
];

export default data;
