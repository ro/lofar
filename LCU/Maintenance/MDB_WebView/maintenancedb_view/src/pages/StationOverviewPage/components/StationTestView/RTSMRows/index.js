import React, {
    Component
} from 'react';
import {IoMdArrowDropdown as DropDownIcon} from 'react-icons/io';
import {renderDateRange} from 'utils/utils'

import GenericTestRow from '../GenericTestRow'

// CSS
import rtsmStyles from 'themes/rtsm_collapsable.module.scss';


/**
 * RTSMSummaryLine: create one table row with percentages of errors per antenna.
 */
function RTSMSummaryLine(props) {
    const data = props.data;

    const cols = props.ordered_component_ids.map((item, key) => {
        if (data[item] > 0) {
            let perc = Math.ceil(data[item]);
            return (<td key={key} className={rtsmStyles.rtsm_summary_badge}>{perc + '%'} </td>);
        } else {
            return <td key={key}></td>;
        }
    });

    const dropdownAdditionStyles = props.isExpanded ? " "+rtsmStyles.dropdownbutton_up : "";

    return (
        <tr className={rtsmStyles.rtsm_summary_row}>
            <td className={rtsmStyles.row_header} onClick={props.onClick}>
                RT {props.dateRange}
                <DropDownIcon className={rtsmStyles.dropdownbutton + dropdownAdditionStyles} color="black"/>
            </td>
            {cols}
        </tr>
    );
}

/**
 * RTSMRows: create summary line + expandable data rows
 */
class RTSMRows extends Component {

    state = {
        displaySingleTests: false
    }

    computeSummary() {
        let summary = {};
        let n_tests = this.props.data.length;
        const component_id_list = this.props.ordered_component_ids;
        component_id_list.forEach(component_id => summary[component_id] = 0);

        this.props.data.forEach((item, key) => {
            Object.keys(item.details.component_errors).forEach((component_id) => {
                summary[component_id] += 1
            })
        });

        Object.keys(summary).forEach(item => summary[item] /= n_tests / 100.);
        return summary
    }

    toggleDisplaySingleTests = (e) => {
        this.setState({
            displaySingleTests: !this.state.displaySingleTests
        });
        this.props.update();
    };

    render() {
        let summaryData = this.computeSummary();

        // RTSM data rows, only shown when expanded
        let all_rtsm = this.state.displaySingleTests ? this.props.data : [] ;

        return (
            <React.Fragment>
                <RTSMSummaryLine onClick={this.toggleDisplaySingleTests}
                                 isExpanded={this.state.displaySingleTests}
                                 data={summaryData}
                                 ordered_component_ids={this.props.ordered_component_ids}
                                 dateRange={renderDateRange(this.props.data)} />
                {   // All RTSM lines in this block (expanded or folded)
                    all_rtsm.map((item, key) =>
                        <GenericTestRow className="collapse open"
                            key={key}
                            test_type="RT"
                            ordered_component_ids={this.props.ordered_component_ids}
                            component_type={this.props.component_type}
                            station_name={this.props.station_name}
                            station_type={this.props.station_type}
                            dataIdx={item.dataIdx}
                            data={item.details}/>
                    )
                }
            </React.Fragment>
        );
    }
}


export default RTSMRows;
