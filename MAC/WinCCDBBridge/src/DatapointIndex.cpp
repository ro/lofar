#include "DatapointIndex.hpp"
#include "DatapointAttribute.hpp"

#include <QRegularExpression>
#include <QDomElement>
#include <QDebug>

DatapointIndex::DatapointIndex()
{

}

const QString DatapointIndex::toString() const {
     return "DatapointIndex(\"" + QStringList({pattern}).join(", ") + "\")";
}

void DatapointIndex::parseDPIndex(QString index)
{
    QRegularExpression regexParser{pattern};
    QRegularExpressionMatch matched = regexParser.match(index);


    if(matched.hasMatch()){
        for(DatapointAttribute & key: m_keys){
            const QString index_id{key.name()};
            key.setValue(matched.captured(index_id));
        }
    }

}

const std::vector<DatapointAttribute>& DatapointIndex::keys()
{
    return m_keys;
}



const DatapointIndex readIndexRule(const QDomElement & element, const QString & dpName){
    DatapointIndex dpIndex;

    dpIndex.pattern = element.firstChildElement("regex").text();
    QDomNodeList attributes = element.elementsByTagName("attribute");
    for(int i=0; i < attributes.size(); i++){
        DatapointAttribute attribute{readAttribute(attributes.item(i).toElement())};
        dpIndex.m_keys.push_back(attribute);
    }

    dpIndex.parseDPIndex(dpName);

    return dpIndex;
}
