# - Template for script MakeSphinxDoc.cmake used to generate code documentation.
#
# The MakeSphinxDoc.cmake script is used to generate source code documentation
# using Sphinx. The configured script will be invoked when the custom target
# 'doc' is (re)created, i.e. by doing a 'make doc'.

#  $Id$
#
#  Copyright (C) 2010
#  ASTRON (Netherlands Foundation for Research in Astronomy)
#  P.O.Box 2, 7990 AA Dwingeloo, The Netherlands, softwaresupport@astron.nl
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

message("Running Sphinx Apidoc to produce *.rst files for current LOFAR build ...")
message("@SPHINX_APIDOC_EXECUTABLE@ -o ${SPHINX_SOURCE_DIR} ${PYTHON_BUILD_DIR}")
execute_process(
  COMMAND "@SPHINX_APIDOC_EXECUTABLE@" -o "${SPHINX_SOURCE_DIR}" "${PYTHON_BUILD_DIR}"
  RESULT_VARIABLE _result)

if(NOT _result EQUAL 0)
  message(SEND_ERROR "Sphinx apidoc returned with error: ${_result}")
endif(NOT _result EQUAL 0)


message("Copying over Sphinx Makefile and config file and prepared *.rst files to build dir ...")
execute_process(
  COMMAND cp "${LOFARROOT}/doc/sphinx/Makefile" "${CMAKE_BINARY_DIR}/doc/sphinx/"
  RESULT_VARIABLE _result)

if(NOT _result EQUAL 0)
  message(SEND_ERROR "copy Sphinx Makefile returned with error: ${_result}")
endif(NOT _result EQUAL 0)

execute_process(
  COMMAND cp "${LOFARROOT}/doc/sphinx/source/conf.py" "${CMAKE_BINARY_DIR}/doc/sphinx/source/"
  RESULT_VARIABLE _result)

if(NOT _result EQUAL 0)
  message(SEND_ERROR "copy Sphinx config returned with error: ${_result}")
endif(NOT _result EQUAL 0)

FILE(GLOB RSTFiles "${LOFARROOT}/doc/sphinx/source/*.rst")
execute_process(
  COMMAND cp "${RSTFiles}" "${CMAKE_BINARY_DIR}/doc/sphinx/source/"
  RESULT_VARIABLE _result)

if(NOT _result EQUAL 0)
  message(SEND_ERROR "copy prepared *.rst files returned with error: ${_result}")
endif(NOT _result EQUAL 0)


message("Executing Sphinx Makefile to create html documentation for current build ...")
execute_process(
  COMMAND make SPHINXBUILD=@SPHINX_BUILD_EXECUTABLE@ html
  WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/doc/sphinx/"
  RESULT_VARIABLE _result)

if(NOT _result EQUAL 0)
  message(SEND_ERROR "Sphinx make returned with error: ${_result}")
endif(NOT _result EQUAL 0)
