#!/usr/bin/env python3

# LTACP Python module for transferring data from localhost to a remote SRM
#
# Source data can be individual files or directories. Directories will be tar-ed.
#
# md5 and adler32 checksums are computed on the streamed data to ensure integrity of the file between localhost and the SRM.

from optparse import OptionParser
from subprocess import Popen, PIPE
import socket
import os, sys, getpass
import time
import re
import random
import math
import atexit
from datetime import datetime, timedelta
from lofar.common.util import humanreadablesize
from lofar.common.datetimeutils import totalSeconds
from lofar.common.subprocess_utils import PipeReader, communicate_returning_strings
from lofar.lta.ingest.common.config import hostnameToIp
from lofar.lta.ingest.server.config import GLOBUS_TIMEOUT
from lofar.lta.ingest.common.srm import *
from lofar.common.subprocess_utils import communicate_returning_strings

from prometheus_client import Counter

import logging
logger = logging.getLogger(__name__)

class LtacpException(Exception):
     def __init__(self, value):
         self.value = value
     def __str__(self):
         return str(self.value)

class LtacpDestinationExistsException(LtacpException):
     def __init__(self, value):
         super(LtacpDestinationExistsException, self).__init__(value)

class LtaCp:
    metric_nr_bytes_sent = Counter("ingest_ltacp_bytes_sent", "Number of bytes sent by LtaCp towards the LTA")
    metric_nr_transfers = Counter("ingest_ltacp_transfers", "Number of LtaCp transfers towards the LTA")

    def __init__(self,
                 src_path,
                 dst_surl,
                 globus_timeout=GLOBUS_TIMEOUT,
                 progress_callback=None):
        """
        Create an LtaCp instance so you can start transferring data from a given src_path at localhost to the
        given dst_url in the LTA.
        Simple Example:
          ltacp = LtaCp('localhost', '/data/projects/LC8_001/L654321', 'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/test/test123.tar')
          md5, a32, num_bytes = ltacp.transfer()

        Example with progress:
          def print_progress(percentage_done, current_speed, total_bytes_transfered):
            print percentage_done, current_speed, total_bytes_transfered

          ltacp = LtaCp('localhost', '/data/projects/LC8_001/L654321', 'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/test/test123.tar',
                        progress_callback=print_progress)
          md5, a32, num_bytes = ltacp.transfer()

        :param src_path: either a string with a path to a file or directory, or a list paths to multiple files.
                         All files (either the one file, all files in the directory, or all files in the list of file_paths) are tarred togheter
                         and transfered to the dst_surl
        :param dst_surl: a string with an srm url (like: 'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/test/test123.tar')
        :param globus_timeout: timeout in seconds to wait for the destination host to finish
        :param progress_callback: function with parameters percentage_done, current_speed, total_bytes_transfered which is called during transfer to report on progress.
        """

        self.src_path = src_path.rstrip('/') if isinstance(src_path, str) else [sp.rstrip('/') for sp in src_path]
        self.dst_surl = dst_surl
        self.globus_timeout = globus_timeout
        self.progress_callback = progress_callback
        if isinstance(src_path, str):
            self.logId = os.path.basename(self.src_path)
        else:
            #src_path is a list of paths, pick filename of first as logId
            self.logId = os.path.basename(self.src_path[0])
        self.started_procs = {}
        self.fifos = []

    def path_exists(self, path):
        cmd = ['ls', path]
        logger.info('ltacp %s: checking if source exists. executing: %s' % (self.logId, ' '.join(cmd)))
        proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
        self.started_procs[proc] = cmd

        # block until find is finished
        communicate_returning_strings(proc)
        del self.started_procs[proc]

        logger.info('ltacp %s: source %s %s' % (self.logId, path, 'exists' if proc.returncode==0 else 'does not exist'))
        return proc.returncode==0

    def source_exists(self):
        if isinstance(self.src_path, str):
            return self.path_exists(self.src_path)
        else:
            #self.src_path is a list, check each item and combine
            return all([self.path_exists(p) for p in self.src_path])

    def is_soure_single_file(self):
        if isinstance(self.src_path, str):
            src_dirname = os.path.dirname(self.src_path)
            src_basename = os.path.basename(self.src_path)

            # get input filetype
            cmd_source_filetype = ['stat', '-L', '-c', '%F', os.path.join(src_dirname, src_basename)]
            logger.info('ltacp %s: determining source type. executing: %s' % (self.logId, ' '.join(cmd_source_filetype)))
            p_source_filetype = Popen(cmd_source_filetype, stdout=PIPE, stderr=PIPE)
            self.started_procs[p_source_filetype] = cmd_source_filetype

            # block until find is finished
            output_source_filetype = communicate_returning_strings(p_source_filetype)
            del self.started_procs[p_source_filetype]
            if p_source_filetype.returncode != 0:
                raise LtacpException('ltacp %s: determining source type failed: \nstdout: %s\nstderr: %s' % (self.logId,
                                                                                                             output_source_filetype[0],
                                                                                                             output_source_filetype[1]))

            for line in output_source_filetype[0].split('\n'):
                if 'regular file' in line.strip():
                    logger.info('ltacp %s: source path is a file' % (self.logId,))
                    return True

            logger.info('ltacp %s: source path is a directory' % (self.logId))
            return False
        else:
            #self.src_path is a list of files/dirs, so it is not a single file
            logger.info('ltacp %s: remote path is a list of files/dirs' % self.logId)
            return False



    # transfer file/directory from given src to SRM location with given turl
    def transfer(self, force=False, dereference=False):
        starttime = datetime.utcnow()
        self.metric_nr_transfers.inc()

        # for cleanup
        self.started_procs = {}
        self.fifos = []

        try:
            if not self.source_exists():
                raise LtacpException("ltacp %s: source path %s does not exist" % (self.logId, self.src_path))

            # determine if input is file
            input_is_file = self.is_soure_single_file()

            if not input_is_file:
                # make sure the file extension is .tar or .tar.gz
                missing_suffix = ""
                if not self.dst_surl.endswith(".tar"):
                    missing_suffix = ".tar"

                if missing_suffix:
                    self.dst_surl += missing_suffix
                    logger.info("ltacp %s: appending missing suffix %s to surl: %s", self.logId, missing_suffix, self.dst_surl)

            dst_turl = convert_surl_to_turl(self.dst_surl)
            logger.info('ltacp %s: initiating transfer of %s to surl=%s turl=%s' % (self.logId, self.src_path, self.dst_surl, dst_turl))

            # get input datasize
            cmd_source_du = ['du', '-b'] + (["--dereference"] if dereference else []) + ['--max-depth=0'] + ([self.src_path] if isinstance(self.src_path, str) else self.src_path)
            logger.info('ltacp %s: getting datasize of source. executing: %s' % (self.logId, ' '.join(cmd_source_du)))
            p_source_du = Popen(cmd_source_du, stdout=PIPE, stderr=PIPE)
            self.started_procs[p_source_du] = cmd_source_du

            # block until du is finished
            output_source_du = communicate_returning_strings(p_source_du)
            del self.started_procs[p_source_du]
            if p_source_du.returncode != 0:
                raise LtacpException('ltacp %s: source du failed: \nstdout: %s\nstderr: %s' % (self.logId, output_source_du[0], output_source_du[1]))

            # compute various parameters for progress logging
            if input_is_file:
                input_datasize = int(output_source_du[0].split()[0])
            else:
                input_datasize = sum([int(line.strip().split()[0]) for line in output_source_du[0].split('\n') if line.strip()])

            logger.info('ltacp %s: input datasize: %d bytes, %s' % (self.logId, input_datasize, humanreadablesize(input_datasize)))
            estimated_tar_size = 512*(input_datasize // 512) + 3*512 #512byte header, 2*512byte ending, 512byte modulo data
            logger.info('ltacp %s: estimated_tar_size: %d bytes, %s' % (self.logId, estimated_tar_size, humanreadablesize(estimated_tar_size)))

            # start the actual transfer...
            # we need a filehandle to /dev/null as "input" for the source-data-stream-proc
            with open(os.devnull, 'r') as devnull:
                # prepare cmd to send source data to a stdout stream
                # construct cmdline based on type of input (file/dir)
                if input_is_file:
                    cmd_source_data = ['cat', self.src_path]
                    src_path_parent, _ = os.path.split(self.src_path)
                else:
                    if isinstance(self.src_path, str):
                        #src_path is dir
                        src_path_parent, src_path_child = os.path.split(self.src_path)
                    else:
                        #src_path is list if paths
                        dirs = set([os.path.dirname(p) for p in self.src_path])

                        if len(dirs) > 1:
                            raise LtacpException('ltacp %s: cannot combine multiple files from different directories in one tarbal' % self.logId)

                        files = set([os.path.basename(p) for p in self.src_path])
                        src_path_parent = list(dirs)[0]
                        src_path_child = sorted(list(files))


                    cmd_source_data = ['tar', 'c'] + ([src_path_child] if isinstance(src_path_child, str) else src_path_child) + ['-O']
                    if dereference:
                        cmd_source_data += ['--dereference']

                logger.info('ltacp %s: starting transfer of source data. executing: %s' % (self.logId, ' '.join(cmd_source_data)))
                p_source_data = Popen(cmd_source_data, stdin=devnull, stdout=PIPE, stderr=PIPE, cwd=src_path_parent)
                self.started_procs[p_source_data] = cmd_source_data


                # prepare a fifo into which md5a32bc can write
                self.hashed_source_data_fifo = '/tmp/ltacp_datapipe_%s' % (self.logId,)

                logger.info('ltacp %s: creating data fifo: %s' % (self.logId, self.hashed_source_data_fifo))
                # remove any potentially dangling fifo from previous run(s)
                if os.path.exists(self.hashed_source_data_fifo):
                    os.remove(self.hashed_source_data_fifo)
                os.mkfifo(self.hashed_source_data_fifo)
                if not os.path.exists(self.hashed_source_data_fifo):
                    raise LtacpException("ltacp %s: Could not create fifo: %s" % (self.logId, self.hashed_source_data_fifo))

                # transfer source data stream via md5a32bc to compute md5, adler32 and byte_count
                # data is written to hashed_source_data_fifo, which is then later fed into globus-url-copy
                # on stdout we can monitor progress
                # set progress message step 0f 0.5% of estimated_tar_size
                cmd_md5a32bc = ['md5a32bc', '-p', str(min(1000000, estimated_tar_size//200)), self.hashed_source_data_fifo]
                logger.info('ltacp %s: processing data stream for md5, adler32 and byte_count. executing: %s' % (self.logId, ' '.join(cmd_md5a32bc),))
                p_md5a32bc = Popen(cmd_md5a32bc, stdin=p_source_data.stdout, stdout=PIPE, stderr=PIPE, universal_newlines=True)
                self.started_procs[p_md5a32bc] = cmd_md5a32bc

                # stream hashed_source_data_fifo into globus-url-copy
                guc_options = ['-cd', #create remote directories if missing
                               '-p 4', #number of parallel ftp connections
                               '-bs 131072', #buffer size
                               '-b', # binary
                               '-nodcau', # turn off data channel authentication for ftp transfers
                               ]
                cmd_data_out = ['/bin/bash', '-c', 'globus-url-copy %s file://%s %s' % (' '.join(guc_options), self.hashed_source_data_fifo, dst_turl) ]
                logger.info('ltacp %s: copying data stream into globus-url-copy. executing: %s' % (self.logId, ' '.join(cmd_data_out)))
                p_data_out = Popen(cmd_data_out, stdout=PIPE, stderr=PIPE)
                self.started_procs[p_data_out] = cmd_data_out

                # ok, then complete chain has been set up. Data should start flowing.
                # waiting for output, comparing checksums, etc.
                logger.info('ltacp %s: transfering... waiting for progress...' % self.logId)
                transfer_start_time = datetime.utcnow()
                prev_progress_time = datetime.utcnow()
                prev_bytes_transfered = 0
                prev_bytes_transfered_for_metric = 0

                with PipeReader(p_md5a32bc.stdout, self.logId) as pipe_reader:
                    # wait and poll for progress while all processes are runnning
                    while len([p for p in list(self.started_procs.keys()) if p.poll() is not None]) == 0:
                        try:
                            current_progress_time = datetime.utcnow()
                            elapsed_secs_since_prev = totalSeconds(current_progress_time - prev_progress_time)

                            if elapsed_secs_since_prev > 900:
                                raise LtacpException('ltacp %s: transfer stalled for 15min.' % (self.logId))

                            # read and process md5a32bc stdout lines to create progress messages
                            lines = pipe_reader.readlines(1)
                            nextline = lines[-1].strip() if lines else ''

                            if len(nextline) > 0:
                                try:
                                    logger.debug('ltacp %s: transfering... %s', self.logId, nextline)
                                    total_bytes_transfered = int(nextline.split()[0].strip())

                                    self.metric_nr_bytes_sent.inc(total_bytes_transfered - prev_bytes_transfered_for_metric)
                                    prev_bytes_transfered_for_metric = total_bytes_transfered

                                    percentage_done = (100.0*float(total_bytes_transfered))/float(estimated_tar_size)
                                    elapsed_secs_since_start = totalSeconds(current_progress_time - transfer_start_time)
                                    if percentage_done > 0 and elapsed_secs_since_start > 0 and elapsed_secs_since_prev > 0:
                                        avg_speed = total_bytes_transfered / elapsed_secs_since_start
                                        current_bytes_transfered = total_bytes_transfered - prev_bytes_transfered
                                        current_speed = current_bytes_transfered / elapsed_secs_since_prev
                                        if elapsed_secs_since_prev > 120 or current_bytes_transfered > 0.333*estimated_tar_size:
                                            prev_progress_time = current_progress_time
                                            prev_bytes_transfered = total_bytes_transfered
                                            percentage_to_go = 100.0 - percentage_done
                                            time_to_go = elapsed_secs_since_start * percentage_to_go / percentage_done

                                            try:
                                                if self.progress_callback:
                                                    self.progress_callback(percentage_done=percentage_done, current_speed=current_speed, total_bytes_transfered=total_bytes_transfered)
                                            except Exception as e:
                                                logger.error(e)

                                            logger.info('ltacp %s: transfered %s %.1f%% in %s at avgSpeed=%s (%s) curSpeed=%s (%s) to_go=%s to %s' % (self.logId,
                                                                                                                    humanreadablesize(total_bytes_transfered),
                                                                                                                    percentage_done,
                                                                                                                    timedelta(seconds=int(round(elapsed_secs_since_start))),
                                                                                                                    humanreadablesize(avg_speed, 'Bps'),
                                                                                                                    humanreadablesize(avg_speed*8, 'bps'),
                                                                                                                    humanreadablesize(current_speed, 'Bps'),
                                                                                                                    humanreadablesize(current_speed*8, 'bps'),
                                                                                                                    timedelta(seconds=int(round(time_to_go))),
                                                                                                                    dst_turl))
                                except Exception as e:
                                    msg = 'ltacp %s: error while parsing md5a32bc loglines: error=%s. line=%s' % (self.logId, e, nextline)
                                    logger.error(msg)
                                    self.cleanup()
                                    raise LtacpException(msg)
                            time.sleep(0.05)
                        except KeyboardInterrupt:
                            self.cleanup()
                        except LtacpException as e:
                            logger.error('ltacp %s: %s' % (self.logId, str(e)))
                            self.cleanup()
                            raise
                        except Exception as e:
                            logger.error('ltacp %s: %s' % (self.logId, str(e)))

                def waitForSubprocess(proc, timeout=timedelta(seconds=60), proc_log_name='', loglevel=logging.DEBUG):
                    logger.log(loglevel, 'ltacp %s: waiting at most %s for %s to finish...', self.logId, timeout, proc_log_name)
                    start_wait = datetime.now()
                    while datetime.now() - start_wait < timeout:
                        if proc.poll() is not None:
                            break;
                        time.sleep(1)

                    if proc.poll() is None:
                        raise LtacpException('ltacp %s: %s did not finish within %s.' % (self.logId, proc_log_name, timeout))

                waitForSubprocess(p_data_out, timedelta(seconds=self.globus_timeout), 'globus-url-copy', logging.INFO)
                output_data_out = communicate_returning_strings(p_data_out)
                if p_data_out.returncode != 0:
                    if 'file exist' in output_data_out[1].lower():
                        raise LtacpDestinationExistsException('ltacp %s: data transfer via globus-url-copy to LTA failed, file already exists. turl=%s.' % (self.logId, dst_turl))
                    raise LtacpException('ltacp %s: transfer via globus-url-copy to LTA failed. turl=%s error=%s' % (self.logId,
                                                                                                                     dst_turl,
                                                                                                                     output_data_out[0].strip()+output_data_out[1].strip()))
                logger.info('ltacp %s: data transfer via globus-url-copy to LTA complete.' % self.logId)

                waitForSubprocess(p_source_data, timedelta(seconds=60), 'source data transfer')
                output_source_data = communicate_returning_strings(p_source_data)
                if p_source_data.returncode != 0:
                    raise LtacpException('ltacp %s: Error in remote data transfer: %s' % (self.logId, output_source_data[1]))
                logger.debug('ltacp %s: source data transfer finished...' % self.logId)


                waitForSubprocess(p_md5a32bc, timedelta(seconds=60), 'local computation of md5 adler32 and byte_count')
                output_md5a32bc_local = communicate_returning_strings(p_md5a32bc)
                if p_md5a32bc.returncode != 0:
                    raise LtacpException('ltacp %s: Error while computing md5 adler32 and byte_count: %s' % (self.logId, output_md5a32bc_local[1]))
                logger.debug('ltacp %s: computed local md5 adler32 and byte_count.' % self.logId)

                # process local md5 adler32 and byte_count
                try:
                    items = output_md5a32bc_local[1].splitlines()[-1].split(' ')
                    md5_checksum_local = items[0].strip()
                    a32_checksum_local = items[1].strip().zfill(8)
                    byte_count = int(items[2].strip())
                except Exception as e:
                    logger.error('ltacp %s: error while parsing md5 adler32 and byte_count outputs: %s' % (self.logId, output_md5a32bc_local[0]))
                    raise

                # check transfered number of bytes
                if(byte_count == 0 and input_datasize > 0):
                    raise LtacpException('ltacp %s: did not transfer any bytes of the expected %s. Something is wrong in the datastream setup.' % (self.logId, humanreadablesize(estimated_tar_size)))

                logger.info('ltacp %s: byte count of datastream is %d %s' % (self.logId, byte_count, humanreadablesize(byte_count)))

            logger.info('ltacp %s: fetching adler32 checksum from LTA...' % self.logId)
            srm_ok, srm_file_size, srm_a32_checksum = get_srm_size_and_a32_checksum(self.dst_surl, 'ltacp %s:' % self.logId)

            if not srm_ok:
                raise LtacpException('ltacp %s: Could not get srm adler32 checksum for: %s'  % (self.logId, self.dst_surl))

            if srm_a32_checksum != a32_checksum_local:
                raise LtacpException('ltacp %s: adler32 checksum reported by srm (%s) does not match original data checksum (%s)' % (self.logId,
                                                                                                                                     srm_a32_checksum,
                                                                                                                                     a32_checksum_local))

            logger.info('ltacp %s: adler32 checksums are equal: %s' % (self.logId, a32_checksum_local,))

            if int(srm_file_size) != int(byte_count):
                raise LtacpException('ltacp %s: file size reported by srm (%s) does not match datastream byte count (%s)' % (self.logId,
                                                                                                                             srm_file_size,
                                                                                                                             byte_count))

            logger.info('ltacp %s: srm file size and datastream byte count are equal: %s bytes (%s)' % (self.logId,
                                                                                                        srm_file_size,
                                                                                                        humanreadablesize(srm_file_size)))
            logger.info('ltacp %s: transfer to LTA completed successfully.' % (self.logId))

        except LtacpDestinationExistsException as e:
            logger.log(logging.WARN if force else logging.ERROR, str(e))
            if force:
                self.cleanup()
                srmrm(self.dst_surl, 'ltacp %s ' % self.logId)
                return self.transfer(force=False)
            else:
                # re-raise the exception to the caller
                raise
        except Exception as e:
            # Something went wrong
            logger.error('ltacp %s: Error in transfer: %s' % (self.logId, str(e)))
            # re-raise the exception to the caller
            raise
        finally:
            # cleanup
            self.cleanup()

        total_time = max(1, round(10*totalSeconds(datetime.utcnow() - starttime))/10)
        logger.info('ltacp %s: successfully completed transfer of %s to %s in %ssec for %s at avg speed of %s or %s', self.logId,
                                                                                                                      self.src_path,
                                                                                                                      self.dst_surl,
                                                                                                                      total_time,
                                                                                                                      humanreadablesize(int(srm_file_size)),
                                                                                                                      humanreadablesize(int(srm_file_size)/total_time, 'Bps'),
                                                                                                                      humanreadablesize(8*int(srm_file_size)/total_time, 'bps'))

        return (md5_checksum_local, a32_checksum_local, str(byte_count))

    def cleanup(self):
        logger.debug('ltacp %s: cleaning up' % (self.logId))

        # remove local fifos
        for fifo in self.fifos:
            if os.path.exists(fifo):
                logger.info('ltacp %s: removing local fifo: %s' % (self.logId, fifo))
                os.remove(fifo)
        self.fifos = []

        # cancel any started running process, as they should all be finished by now
        running_procs = dict((p, cl) for (p, cl) in list(self.started_procs.items()) if p.poll() == None)

        if len(running_procs):
            logger.warning('ltacp %s: terminating %d running subprocesses...' % (self.logId, len(running_procs)))
            for p,cl in list(running_procs.items()):
                if isinstance(cl, list):
                    cl = ' '.join(cl)
                logger.warning('ltacp %s: terminated running process pid=%d cmdline: %s' % (self.logId, p.pid, cl))
                p.terminate()
            logger.info('ltacp %s: terminated %d running subprocesses...' % (self.logId, len(running_procs)))
        self.started_procs = {}

        logger.debug('ltacp %s: finished cleaning up' % (self.logId))

# limited standalone mode for testing:
# usage: ltacp.py <source-path> <surl>
def main():
    # Check the invocation arguments
    parser = OptionParser("%prog [options] <source_path> <lta-detination-srm-url>",
                          description='copy a file/directory from <source_path> (on localhost) to the LTA <lta-detination-srm-url>')
    parser.add_option('-t', '--timeout', dest='globus_timeout', type='int', default=GLOBUS_TIMEOUT, help='number of seconds (default=%default) to wait for globus-url-copy to finish after the transer is done (while lta-site is computing checksums)')
    parser.add_option('-f', '--force', dest='force', action='store_true', help='force file transfer/copy, even if destination exists (overwrite destanation)')
    parser.add_option('-d', '--dereference', dest='dereference', action='store_true', help='''dereference (follow) all symlinks.''')
    parser.add_option('-V', '--verbose', dest='verbose', action='store_true', help='verbose logging')
    (options, args) = parser.parse_args()

    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=logging.DEBUG if options.verbose else logging.INFO)

    if len(args) != 2:
        parser.print_help()
        sys.exit(1)

    try:
        cp = LtaCp(args[0], args[1], options.globus_timeout)

        # make sure that all subprocesses and fifo's are cleaned up when the program exits
        atexit.register(cp.cleanup)

        cp.transfer(options.force, options.dereference)
        sys.exit(0)
    except LtacpException as e:
        logger.error("ltacp transfer raised an exception: %s", e)
        sys.exit(1)

if __name__ == '__main__':
    main()
