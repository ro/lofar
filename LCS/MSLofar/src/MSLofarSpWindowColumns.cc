//# MSLofarSpWindowColumns.cc: provides easy access to LOFAR's MSSpectralWindow columns
//# Copyright (C) 2021
//# ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$
//#
//# @author Jan David Mol

#include <lofar_config.h>
#include <MSLofar/MSLofarSpWindowColumns.h>
#include <MSLofar/MSLofarSpectralWindow.h>

using namespace casacore;

namespace LOFAR {

  ROMSLofarSpWindowColumns::ROMSLofarSpWindowColumns
  (const MSLofarSpectralWindow& msLofarSpectralWindow)
  {
    attach (msLofarSpectralWindow);
  }

  ROMSLofarSpWindowColumns::ROMSLofarSpWindowColumns()
  {}

  void ROMSLofarSpWindowColumns::attach
  (const MSLofarSpectralWindow& msLofarSpectralWindow)
  {
    ROMSSpWindowColumns::attach (msLofarSpectralWindow);
    roTopocentricFrequencyCorrectionApplied_p.attach (msLofarSpectralWindow, "LOFAR_TOPOCENTRIC_FREQUENCY_CORRECTION_APPLIED");
  }


  MSLofarSpWindowColumns::MSLofarSpWindowColumns
  (MSLofarSpectralWindow& msLofarSpectralWindow)
  {
    attach (msLofarSpectralWindow);
  }

  MSLofarSpWindowColumns::~MSLofarSpWindowColumns()
  {}

  MSLofarSpWindowColumns::MSLofarSpWindowColumns()
  {}

  void MSLofarSpWindowColumns::attach
  (MSLofarSpectralWindow& msLofarSpectralWindow)
  {
    MSSpWindowColumns::attach (msLofarSpectralWindow);
    // Readonly.
    roTopocentricFrequencyCorrectionApplied_p.attach (msLofarSpectralWindow, "LOFAR_TOPOCENTRIC_FREQUENCY_CORRECTION_APPLIED");
    // Read/write
    rwTopocentricFrequencyCorrectionApplied_p.attach (msLofarSpectralWindow, "LOFAR_TOPOCENTRIC_FREQUENCY_CORRECTION_APPLIED");
  }

} //# end namespace
