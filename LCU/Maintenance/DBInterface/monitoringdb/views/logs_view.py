from rest_framework.viewsets import ReadOnlyModelViewSet

from lofar.maintenance.monitoringdb.serializers.log import ActionLogSerializer, ActionLog


class ActionLogViewSet(ReadOnlyModelViewSet):
    """
    Action event log line

    """
    queryset = ActionLog.objects.all()
    serializer_class = ActionLogSerializer
    filter_fields = '__all__'
