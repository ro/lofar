import React, {
    Component
} from 'react';

import classNames from "classnames";
import {componentStatusOK, componentStatusBeyondRepair} from 'utils/LOFARDefinitions';

// CSS
import styles from './styles.module.scss';


/**
 * GenericStatusTd; render a <td> showing the number of errors for an component.
 * Show the error details in the detail panel on the right on mouse hover.
 */
class GenericStatusTd extends Component {

    getClass() {
        let cls = classNames({
            [styles.status]: true,
            [styles.highlight]: this.props.doHighlight,
            'hilite-serious': this.props.numErrors > 0,
            'hilite-good': this.props.numErrors === 0,
            'status-non-operational': !componentStatusOK(this.props.status) && !componentStatusBeyondRepair(this.props.status),
            'status-beyond-repair-dark': componentStatusBeyondRepair(this.props.status)
        });

        return cls;
    }

    hasProblems = () => {
        return this.props.numErrors > 0 || !componentStatusOK(this.props.status)
    }

    render() {
        const content = (this.props.numErrors===0 ? ' ' : this.props.numErrors);

        return (
            <td className={this.getClass()}
                data-has-problems={this.hasProblems()}
                data-component-id={this.props.component_id}
                data-data-idx={this.props.dataIdx} >
                {content}
            </td>
        );
    }
}


export default GenericStatusTd;
