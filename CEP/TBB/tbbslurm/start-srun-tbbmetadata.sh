#!/bin/bash
DATE=$(date +%Y%m%dT%H%M%S)
echo logfile /data/log/tbbwriter-$DATE.log
sleep 60
./start-tbbmetadata.sh
exec srun -N 1 \
    --partition=cpu \
    --cpus-per-task=1 \
    --job-name="tbbmetadata" \
    --output=/data/log/tbbwriter/tbbmetadata-$DATE-%N.log \
    /data/home/lofarsys/tbb-writer/run/docker-launch-tbbmetadata.sh


