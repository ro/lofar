import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "./reducers/index.js";
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history';
import { connectRouter, routerMiddleware } from 'connected-react-router'


// Needed for the chrome Redux DevTools extension
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const history = createBrowserHistory();

const store = createStore(
    connectRouter(history)(rootReducer),
    /* preloadedState, */
    composeEnhancers(
        applyMiddleware(
            routerMiddleware(history),
            thunk
        )
    )
);

export { store, history };
