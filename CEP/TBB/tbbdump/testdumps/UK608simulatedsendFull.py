#! /usr/bin/env python
# UDP server example
import socket
import struct
import time

port1=31662
port2=31662

client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

DM=12.44
referenceFrequency=150195132.5 #MHz
sourceID='\x68' #UK608 
importance=5

#sendadress="10.135.252.101" #lhn001, inside LOFAR
sendadress="portal.lofar.eu" # outside LOFAR
#sendadress="localhost"
packets=[]
rectime=[]

fmt='>cciiffic'
# DATASTRUCTURE
# 8 bit character: \x99
# 8 bit character: \xA0
# 32 bit int: time in full seconds
# 32 bit int: rest of time in nanosecond
# 32 bit float: DM
# 32 bit float: referenceFrequency, at the time stamp, the signal is at this frequency
# 32 bit int: importance, How significant is this signal
# 8 bit character: sourcID, which source is this? \x68 for UK608, \x69 for FR606



def getStopTime(rcu=0):
    t=time.time()
    sec=int(t)
    nsec=int(1e9*(t-sec))
    return (sec,nsec)



(sec,nsec)=getStopTime()    
senddata=struct.pack(fmt,'\x99','\xA0',sec,nsec,DM,referenceFrequency,importance,sourceID)
client_socket.sendto(senddata, (sendadress,port1))
print("sending trigger",sec,nsec)

