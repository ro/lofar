import React, { Component } from 'react';
import { connect } from "react-redux";
import { Table } from 'reactstrap';
import { unique_id } from 'utils/utils.js'
import { componentErrorTypes } from 'utils/constants.js'
import AutoLoadWrapper from 'components/AutoLoadWrapper'

import StationTestRow from './StationTestRow'

// CSS
import styles from './styles.module.scss'


/**
 * ColHeaders: function component returning the column headers.
 */
function ColHeaders({errorTypes}) {
    return (
        <tr>
            <th>Date</th>
            <th>Time</th>
            <th>Station</th>
            <th>Comp.</th>
            { errorTypes.map((err) =>
                <th key={err} title={err}>
                    { componentErrorTypes[err] ? componentErrorTypes[err] : err }
                </th>
            )}
        </tr>
    );
}


/**
 * TableRows: function component returning the table rows.
 */
function TableRows({data, errorTypes}) {
    let prevDate = null;
    let rows;

    rows = data.map( (stationData) => {
        let date = (stationData.date !== prevDate ? stationData.date : "");
        let components = Object.keys(stationData.component_error_summary).sort();
        let station = stationData.station_name;
        let time = (stationData.start_datetime.match(/T(.*):..Z/))[1];

        prevDate = stationData.date;

        if (components.length === 0) {
            return <StationTestRow key={ unique_id() } date={date} time={time} component={"-"} station={station} data={stationData} errorTypes={errorTypes}/>
        }
        else {
            return components.map( (component) => {
                let row = <StationTestRow key={ unique_id() } date={date} time={time} component={component} station={station} data={stationData} errorTypes={errorTypes}/>
                date = station = time = "";
                return row;
            });
        }
    });

    return rows;
}


/**
 * StationOverview class.
 */
class StationTestSummaryC extends Component {

    activeErrorTypes = [];  // Result of filtering state.errorTypes

    filterErrorTypes() {
        let typesFound = {},
            retTypes = [];

        if (this.props.errorTypes.length === 0 || this.props.data.length === 0){
            return [];
        }

        // Create index object for all error types in the data
        this.props.data.forEach( (stationData) => {
            let esummary = stationData.component_error_summary;
            let key;
            for(key in esummary){
                let errors = Object.keys(esummary[key]);
                errors.forEach((e) => typesFound[e] = 1);
            }
        });

        // Loop over all error types and check which ones are present in the data
        this.props.errorTypes.forEach((t) => {
            if (typesFound[t]) {
                retTypes.push(t);
            }
        })

        return retTypes;
    }

    setActiveErrorTypes() {
        this.activeErrorTypes = this.filterErrorTypes();
    }

    // Do not (re)render when data is loading (performance improvement)
    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.isLoading) {
            return false;
        }
        return true;
    }

    render() {
        this.setActiveErrorTypes();

        return (
            <Table bordered hover size="sm" className={styles.sts_table}>
                <thead>
                    <ColHeaders errorTypes={this.activeErrorTypes} />
                </thead>
                <tbody>
                    <TableRows errorTypes={this.activeErrorTypes} data={this.props.data} />
                </tbody>
            </Table>
        );
    }

}

// Map the appInitData from store for the error types
const mapStateToProps = state => {
    return { ...state.appInitData };
};

/* Add some magic; use the AutoLoadWrapper to create a HOC that handles the
   auto-loading of the data for StationOverviewC.
 */
const StationTestSummary = AutoLoadWrapper( connect(mapStateToProps)(StationTestSummaryC) );

export default StationTestSummary;
