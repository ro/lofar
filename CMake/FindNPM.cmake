# - Try to find NPM.
# Variables used by this module:
#  NPM_ROOT_DIR     - NPM root directory
# Variables defined by this module:
#  NPM_FOUND        - system has NPM
#  NPM_ROOT         - hint to the path where npm is located
#  NPM_EXECUTABLE - path to the npm executable

# Copyright (C) 2009
# ASTRON (Netherlands Institute for Radio Astronomy)
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
#
# $Id: FindALGLIB.cmake 21886 2012-09-04 11:57:26Z mol  $

if(NOT NPM_FOUND)
	find_program(NPM_EXECUTABLE NAMES npm HINTS ENV/npm ${NPM_ROOT}/npm)
    include(FindPackageHandleStandardArgs)

    find_package_handle_standard_args (NPM REQUIRED_VARS NPM_EXECUTABLE)
endif(NOT NPM_FOUND)
