from django.db import models
from .fixed_types import STATION_TYPES


class Station(models.Model):
    location = models.CharField(max_length=50, null=True, blank=True, help_text='where the station is located')
    name = models.CharField(max_length=10, help_text='name of the station', unique=True)
    type = models.CharField(max_length=1, choices=STATION_TYPES, help_text='station type one of [Core[C], Remote[R], International[I]]')
