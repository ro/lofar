import { SELECT_ANTENNA_ERROR,
         UNPIN_ANTENNA_ERROR } from '../actions/antennaOverviewPageActions'

import { LOCATION_CHANGE } from "connected-react-router";


const initialState = {
    main_panel : {
        highlightData: null
    },
    child_panel : {
        data: null,
        isPinned: false
    }
};


export default function(state = initialState, action) {
    let newState =  {...state};
    switch (action.type) {

        case SELECT_ANTENNA_ERROR:
            if (state.child_panel.isPinned && ! action.payload.doPin) {
                // action.payload.doPin is only true when right-mouse click is used, so
                // this is probably a mouse move event. Ignore it.
                return state;
            }

            //newState.main_panel = action.payload.data;
            newState.child_panel.data = action.payload.data;
            newState.child_panel.isPinned = action.payload.doPin;

            if (action.payload.doPin) {
                newState.main_panel.highlightData = action.payload.data;
            }

            return newState;

        case UNPIN_ANTENNA_ERROR:
        case LOCATION_CHANGE:
            // Note: LOCATION_CHANGE is triggered by changes in the main filters (e.g. selectedStation)
            // then also reset the child panel
            newState.main_panel.highlightData = null;
            newState.child_panel.data = null;
            newState.child_panel.isPinned = false;
            return newState;

        default:
            return newState
    }
}
