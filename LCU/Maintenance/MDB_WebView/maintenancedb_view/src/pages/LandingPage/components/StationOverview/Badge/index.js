import React, {PureComponent} from 'react';
import classNames from "classnames";

// CSS
import styles from './styles.module.scss'

/**
 * Badge; class to render a badge with label and pill.
 */
class Badge extends PureComponent {

    getClass() {
        let cnt = this.props.count;

        let cls = classNames({
            [styles.so_pill]: true,
            'hilite-serious': cnt > 10,
            'hilite-alarming': cnt > 5 && cnt <=10,
            'hilite-warning': cnt > 0 && cnt <= 5,
            'hilite-good': cnt === 0
        });
        
        return cls;
    }

    render() {
        let props = this.props;
        return (<div id={props.myid} className={styles.so_badge+" "+this.props.className} onMouseOver={props.togglePopOver} onMouseOut={props.togglePopOver}>
            {props.label}
            <span className={this.getClass()}>{props.count}</span>
        </div>);
    }
}

export default Badge;
