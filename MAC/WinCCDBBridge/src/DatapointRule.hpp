#ifndef DATAPOINT_RULE_H
#define DATAPOINT_RULE_H

#include <vector>
#include <map>
#include <memory>

#include <QString>
#include <QObject>
#include <chrono>
class QTimer;
class QSqlQuery;
class QDomElement;
class QDomDocument;

#include "DatapointAttribute.hpp"
#include "DatapointIndex.hpp"

class DatapointRule: public QObject
{
    Q_OBJECT
public:
    DatapointRule();
    DatapointRule(QString name, QString alias, QString description);
    std::vector<std::string> getDatapointsURI();

    std::chrono::seconds m_checkpointInterval;

    DatapointIndex index;

    const QString toString() const;
    void addAttribute(DatapointAttribute attribute);

    const QString & sqlStatement();
    void bindValues(QSqlQuery & query, std::map<std::string, std::string> & values);

    bool hasToTriggerUpdate(std::map<std::string, std::string> & values);

    void prepareStatement();

    const QString & name() const;
    void setName(const QString & name);

    const QString & alias() const;
    void setAlias(const QString & alias);

    const QString & description() const;
    void setDescription(const QString & description);

    const QString & table() const;
    void setTable(const QString & table);
private:
    QString m_name;
    QString m_alias;
    QString m_description;
    QString m_table;

    QString statement;
    std::map<std::string, DatapointAttribute> m_attributes;
    std::shared_ptr<QTimer> timer;
    bool checkBoundaries;
signals:
    void requestCheckpoint(DatapointRule *);

public slots:
    void triggerRequestCheckpoint();
    void startCheckpointTimer();

friend DatapointRule * readDatapointRule(const QDomElement &);
friend std::vector<std::shared_ptr<DatapointRule>> readDatapointRules(QDomDocument &);
};

DatapointRule * readDatapointRule(const QDomElement & element);
std::vector<std::shared_ptr<DatapointRule>> readDatapointRules(QDomDocument & document);

#endif // DATAPOINT_RULE_H

