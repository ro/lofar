import React, {Component} from 'react';
import {connect} from "react-redux";
import Autosuggest from 'react-autosuggest';
import { Input, InputGroup, InputGroupAddon, Button } from 'reactstrap';
import {
    IoMdCloseCircleOutline as CloseIcon,
    IoIosArrowBack as BackIcon,
    IoIosArrowForward as ForwardIcon
} from 'react-icons/io';

// History handling
import { push } from 'connected-react-router';
import { store } from "redux/store.js";

// CSS
import toolbarStyles from '../styles.module.scss'
import './styles.scss'


/**
 * AutoCompleteC; class to render an input field for station name with auto-completion.
 *
 * The parent component is notified about a new station name (through the onChange callback)
 * when the user presses 'Enter' in the input field or when an item from the list of
 * suggestions is chosen.
 *
 * When using this component, in most cases you want to add a key with the value of the current
 * selected station. This forces a new instance instead of only a rerendering when the
 * selected station was changed outside this component. In a new instance the state.value is
 * set to the selectedStation prop.
 *
 * Usage: <AutoComplete key={station} onChange={onchange} stations={stations} selectedStation={selectedStation}/>
 */
class AutoComplete extends Component {

    // The DOM node of the input
    inputRef = null;

    // Timeout for station loading when back/forward button is used
    timeoutId = null;

    // Autosuggest is a controlled component.
    // However the input value is decoupled from Redux state but gets its
    // initial value from the props.
    state = {
      suggestions: this.props.stations,
      value: this.props.selectedStation
    };

    // Get list of suggestions based on user input (while typing)
    getSuggestions = value => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;

        return inputLength === 0 ? this.props.stations : this.props.stations.filter(obj =>
            //obj.name.toLowerCase().slice(0, inputLength) === inputValue
            obj.name.toLowerCase().indexOf(inputValue) > -1
        );
    };

    // Get value to show in the input field when a suggestion is chosen
    getSuggestionValue = suggestion => suggestion.name;

    // Change handler for input
    onChange = (event, { newValue, method }) => {
        this.setState({
          value: newValue.toUpperCase()
        });
    };

    onKeyPress = (event) => {
        if (event.key === "Enter" && this.state.value !== this.props.selectedStation) {
            // Call the callback provided by parent component to announce the change
            this.props.onChange(this.state.value);
        }
    };

    // Autosuggest calls this function when the list of suggestions need to be updated
    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };

    // Autosuggest will call this function when the suggestions need to be cleared.
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    // onSuggestionSelected(event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method })
    onSuggestionSelected = (event, { suggestionValue }) => {
        // Call the callback provided by parent component to announce the change
        this.props.onChange(suggestionValue);
    }

    // Always show suggestions, also when input gets the initial focus
    shouldRenderSuggestions = (value) => {
        return true;
    };

    getInput = (input) => {
        this.inputRef = input
    }

    // Clear button handler for input
    clearInput = () => {
        this.setState(
            { value: "" },
            () => this.inputRef.focus()
        );
    };

    inputNotification = (bgcolor, text) => {
        this.inputRef.style.backgroundColor = bgcolor;
        this.inputRef.value = text;
        window.setTimeout(() => {
            if (this.inputRef) {
                this.inputRef.style.backgroundColor = null;
                this.inputRef.value = this.state.value;
            }
        }, 300)

    }

    backForward = (e) => {
        const mode = e.currentTarget.id;
        const v = this.state.value;
        // Find index of selected station in array
        let idx = this.props.stations.findIndex(obj => obj.name === v);
        if (idx === -1) {
            this.inputNotification('#f9abab', 'NOT FOUND');
            return;
        }
        idx = (mode === "btn-forward" ? idx+1 : idx-1);
        if (idx < 0 || idx >= this.props.stations.length) {
            // At first station (mode=back) or at last station (mode=forward)
            this.inputNotification('#a2d6a2', mode==="btn-forward" ? 'LAST' : 'FIRST');
            return;
        }
        // Update display value and reload after 500ms (prevents reloading on fast clicking)
        window.clearTimeout(this.timeoutId);
        this.setState({
          value: this.props.stations[idx].name
        });
        this.timeoutId = window.setTimeout(() => this.props.onChange(this.props.stations[idx].name), 500);
    }

    // Render a suggestion
    renderSuggestion = suggestion => (
      <div>
        {suggestion.name}
      </div>
    );

    renderInputComponent = inputProps => (
        <InputGroup size="sm">
          <InputGroupAddon addonType="prepend">
              <Button id="btn-back" color="info" onClick={this.backForward}><BackIcon /></Button>
          </InputGroupAddon>
          <div className="btn-group">
            <Input innerRef={this.getInput} {...inputProps} />
            <CloseIcon onClick={this.clearInput} className="btn-clear"/>
        </div>
          <InputGroupAddon addonType="append">
              <Button id="btn-forward" color="info" onClick={this.backForward}><ForwardIcon /></Button>
          </InputGroupAddon>
        </InputGroup>
    );


    render() {

        // Autosuggest will pass through all these props to the input.
        const inputProps = {
            placeholder: 'Type station name..',
            value: this.state.value,
            onChange: this.onChange,
            onKeyPress: this.onKeyPress,
            className: 'form-control form-control-sm react-autosuggest__input'
        };

        return (
            <Autosuggest
                suggestions={this.state.suggestions}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                onSuggestionSelected={this.onSuggestionSelected}
                shouldRenderSuggestions={this.shouldRenderSuggestions}
                getSuggestionValue={this.getSuggestionValue}
                renderSuggestion={this.renderSuggestion}
                renderInputComponent={this.renderInputComponent}
                inputProps={inputProps}
            />
        );
    }
}



class StationAutoCompleteC extends Component {


    // Callback for StationAutoComplete
    onStationChange = (station) => {
        store.dispatch(push(`?station=${station}`))
    }

    render() {
        // The key on AutoComplete is important, see the desc of AutoComplete
        // That's the main reason why this wrapper exists.
        return(
            <div className={toolbarStyles.toolbar_ctrl}>
                <AutoComplete key={this.props.selectedStation}
                              stations={this.props.stations}
                              selectedStation={this.props.selectedStation}
                              onChange={this.onStationChange}
                />
            </div>
        );
    }
}

// Get full list of stations from redux
const mapStateToPropsToolBar = state => {
    return {
        stations: state.appInitData.stations,
        selectedStation: state.mainFilters.selectedStation
    };
};

const StationAutoComplete = connect(mapStateToPropsToolBar)(StationAutoCompleteC);


export { StationAutoComplete };
