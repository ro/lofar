import React, {
    Component
} from 'react';
import 'react-treeview/react-treeview.css';
import {
    Button,
} from 'reactstrap';
import { IoMdCloseCircleOutline as CloseIcon} from 'react-icons/io';
import {unpinAntennaError} from 'redux/actions/antennaOverviewPageActions';
import {LOFARTESTS} from 'utils/LOFARDefinitions';
import {formatDateUTC} from 'utils/utils';
import {connect} from "react-redux";
import RTSMDetails from './RTSMDetails'
import StationTestErrorType from './StationTestErrorType'
import StationTestElement from './StationTestElement'
import StatusDetails from './StatusDetails'

// CSS
import styles from './styles.module.scss'


/**
 * UnpinButton: simple component to optionally display an unpin button
 */
function UnpinButton({doDisplay, onClick}) {
    return ( !doDisplay ? null :
        <Button title="Click to unpin the error details"
                color="secondary"
                size="xs"
                className={styles.unpin_button}
                onClick={onClick}>
            <CloseIcon/>&nbsp;unpin
        </Button>
    );
}


class AntennaErrorDetailsC extends Component {

    composeTitle() {
        let data = this.props.data;

        if (!this.props.data){
            return "Error details";
        }

        if (data.dataIdx === 'current') {
            return `Current status, antenna ${data.antenna_id}`
        }

        let test_type = LOFARTESTS[data.content.test_type];
        let date = formatDateUTC(data.content.start_date)

        return `${test_type} ${date}, antenna ${this.props.data.antenna_id}`;
    }


    unpinPanel = () => {
        this.props.unpinAntennaError();
    };

    renderEmptyContent(){
        return (
            <div>
                <i>Hover the mouse over an error to view the details. Right-click on the error to pin it on this panel.</i>
            </div>);
    }

    render(){
        const title = this.composeTitle();
        let content = null

        if (this.props.data === null) {
            // Nothing selected
            content = this.renderEmptyContent();
        } else if (this.props.data.itemType === 'STATUS') {
            // A status field selected
            content = <StatusDetails data={this.props.data} />
        } else if (this.props.data.itemType === 'ELEMENT') {
            // Element of a station test selected
            content = <StationTestElement data={this.props.data} />
        } else if (this.props.data.content.test_type === 'S') {
            // Error type in a station test selected (eg. RF_FAIL)
            content = <StationTestErrorType data={this.props.data} />
        } else if (this.props.data.content.test_type === 'R') {
            // Error type of a RTSM selected (eg. RF_FAIL)
            content = <RTSMDetails data={this.props.data} />
        }

        return (
            <div className={styles.child_view_container}>
                <div className={styles.header}>
                    {title}
                    <UnpinButton doDisplay={this.props.isPinned} onClick={this.unpinPanel}/>
                </div>
                {content}
            </div>)
    }
}

const AntennaErrorDetails = connect(state => {
    return {
        ...state.antenna_page.child_panel
    };
},{
    unpinAntennaError
})(AntennaErrorDetailsC);


export default AntennaErrorDetails;
