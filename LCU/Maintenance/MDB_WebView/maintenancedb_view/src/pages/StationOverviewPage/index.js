import React, { Component } from 'react';
import {connect} from "react-redux";
import {Alert, Container, Row, Col} from 'reactstrap';
import moment from 'moment';

import NavigationBar from 'components/NavigationBar'
import StationTestView from './components/StationTestView';
import StationTestChildView from './components/StationTestChildView';
import { composeQueryString } from 'utils/utils.js';
import { Toolbar, StationAutoComplete, DateRangeSelector, TestTypeSelector, ErrorTypesSelector } from 'components/Toolbar'

/*
 * Display an Alert
 */
function ErrorAlert({doDisplay, message}){

    if (! doDisplay) {
        return null;
    }

    // The 10px is the margin that ResponsiveGridLayout uses
    return (
        <Alert style={{margin: '10px'}} color="warning">
            {message}
        </Alert>
    );
}

/*
 * Display the page body (below the header)
 */
function PageBody({doDisplay, url}) {

    if (! doDisplay) {
        return null;
    }

    return (
        <Container fluid={true}>
            <Row>
                <Col md="8" className="col-padding">
                    <StationTestView url={url} />
                </Col>
                <Col md="4" className="col-padding">
                    <StationTestChildView />
                </Col>
            </Row>
        </Container>
    );
}


class StationOverviewPageC extends Component {

    getStationSummaryURL() {

        if (this.isParameterMissing()) {
            return '';
        }

        const parameters = {
            format: 'json',
            station_name: this.props.selectedStation,
            test_type: this.props.testType,
            from_date: moment(this.props.startDate).format('YYYY-MM-DD'),
            to_date: moment(this.props.endDate).format('YYYY-MM-DD'),
            error_types: this.props.selectedErrorTypes
        };

        const baseURL = '/api/view/ctrl_station_component_errors';
        const queryString = composeQueryString(parameters);

        return `${baseURL}?${queryString}`
    }

    isParameterMissing(){
        const stationName = this.props.selectedStation;
        return stationName === undefined || stationName === "";
    }

    render() {
        const parmMissing = this.isParameterMissing();

        return (
            <React.Fragment>
                <NavigationBar active_page={this.props.location} />
                <Toolbar>
                    <StationAutoComplete />
                    <TestTypeSelector />
                    <DateRangeSelector />
                    <ErrorTypesSelector />
                </Toolbar>
                <ErrorAlert doDisplay={parmMissing} message="Please select a station" />
                <PageBody doDisplay={!parmMissing} url={this.getStationSummaryURL()} />
            </React.Fragment>
    );
  }
}


const StationOverviewPage = connect(state => {
    return {
        ...state.mainFilters
    };
})(StationOverviewPageC);

export default StationOverviewPage;
