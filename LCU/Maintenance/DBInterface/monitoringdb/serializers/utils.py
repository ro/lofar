from rest_framework.serializers import ModelSerializer
from collections import OrderedDict


class NotNullModelSerializer(ModelSerializer):
    def filter_null_values(self, result, items):
        for key, value in items:
            if isinstance(value, dict):
                result[key] = self.filter_null_values(OrderedDict(), value.items())
            else:
                if value is not None:
                    result[key] = value
        return result

    def to_representation(self, instance):
        pre_result = super(NotNullModelSerializer, self).to_representation(instance)

        filtered_result = self.filter_null_values(OrderedDict(), pre_result.items())

        return filtered_result