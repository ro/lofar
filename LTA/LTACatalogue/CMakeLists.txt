lofar_package(LTACatalogue 1.0 DEPENDS PyCommon)

include(FindPythonModule)
find_python_module(cx_Oracle REQUIRED)            # pip3 install cx_Oracle

python_install(lta_catalogue_db.py DESTINATION lofar/lta)
