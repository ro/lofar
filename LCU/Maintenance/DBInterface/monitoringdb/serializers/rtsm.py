from ..models.rtsm import *
from rest_framework import serializers
import logging
from django.db.transaction import atomic
from django.conf import settings
from ..exceptions import ItemAlreadyExists
from .log import ActionLogSerializer
from .station import Station
import os

logger = logging.getLogger('serializers')


class RTSMErrorSampleSerializer(serializers.ModelSerializer):

    class Meta:
        model = RTSMErrorSample
        fields = '__all__'


class RTSMErrorSerializer(serializers.ModelSerializer):
    observation = serializers.PrimaryKeyRelatedField(read_only=True)
    samples = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = RTSMError
        fields = '__all__'


class RTSMObservationSerializer(serializers.ModelSerializer):
    errors = RTSMErrorSerializer(many=True)

    class Meta:
        model = RTSMObservation
        fields = '__all__'


class FilePathSerializer(serializers.Field):
    def __init__(self, path, *args, **kwargs):
        self.path = path
        super(FilePathSerializer, self).__init__(*args, **kwargs)

    def to_representation(self, value):
        full_path = os.path.join(self.path, value)
        return full_path

    def to_internal_value(self, data):
        file_name = os.path.relpath(self.path, data)
        return file_name


class RTSMSummaryPlotSerializer(serializers.ModelSerializer):
    uri = FilePathSerializer(settings.URL_TO_STORE_RTSM_PLOTS)
    class Meta:
        model = RTSMSummaryPlot
        fields = '__all__'
