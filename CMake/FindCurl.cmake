# - Try to find libcurl, a library for doing http calls
# Variables used by this module:
#  CURL_ROOT_DIR     - curl root directory
# Variables defined by this module:
#  CURL_FOUND        - system has curl
#  CURL_INCLUDE_DIR  - the curl include directory (cached)
#  CURL_INCLUDE_DIRS - the curl include directories
#                          (identical to CURL_INCLUDE_DIR)
#  CURL_LIBRARY      - the curl library (cached)
#  CURL_LIBRARIES    - the curl library

# Copyright (C) 2009
# ASTRON (Netherlands Institute for Radio Astronomy)
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
#
# $Id$

if(NOT CURL_FOUND)

  find_path(CURL_INCLUDE_DIR curl/curl.h
    HINTS ${CURL_ROOT_DIR} PATH_SUFFIXES include)  #  curl headers

  find_library(CURL_LIBRARY curl)   # libcurl
  mark_as_advanced(CURL_INCLUDE_DIR CURL_LIBRARY CURL_LIBRARY)

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(curl DEFAULT_MSG
    CURL_LIBRARY CURL_INCLUDE_DIR)

  set(CURL_INCLUDE_DIRS ${CURL_INCLUDE_DIR})
  set(CURL_LIBRARIES ${CURL_LIBRARY})

endif(NOT CURL_FOUND)
