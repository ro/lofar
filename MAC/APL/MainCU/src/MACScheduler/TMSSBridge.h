//  TMSSBridge.h: Implementation of the TMSS Bridge, interface between MAC Scheduler and TMSS
//
//  Copyright (C) 2020
//  ASTRON (Netherlands Foundation for Research in Astronomy)
//  P.O.Box 2, 7990 AA Dwingeloo, The Netherlands, softwaresupport@astron.nl
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//  $Id$

#ifndef TMSSBRIDGE_H
#define TMSSBRIDGE_H

#include <Common/LofarTypes.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <jsoncpp/json/json.h>

namespace LOFAR {
	namespace MainCU {

class TMSSBridge
{
public:
    TMSSBridge (const std::string &hostname, int port, const std::string &username, const std::string &password);
    ~TMSSBridge ();

    Json::Value getSubTask(int subtask_id);

    Json::Value getObservationSubTasksStartingInThreeMinutes();
    Json::Value getActiveObservationSubTasks();
    Json::Value getFinishingObservationSubTasks();
    std::string getParsetAsText(int subtask_id);
    bool setSubtaskState(int subtask_id, const std::string& state);

    // Actually the next method are private, make it public to be able to use in UnitTest++
    std::vector<std::string> translateHttpResultToSortedUrlList(Json::Value result);

protected:
    // http request to TMSS
    bool httpQuery(const std::string& target, std::string &result, const std::string& query_method="GET", const std::string& data="");
    bool httpGETAsJson(const std::string& target, Json::Value &result);

private:
    // Copying is not allowed
    TMSSBridge(const TMSSBridge&);
    TMSSBridge& operator=(const TMSSBridge&);

    std::string    itsUser;
    std::string    itsPassword;
    std::string    itsHost;
    int       itsPort;
};

  };//MainCU
};//LOFAR
#endif
