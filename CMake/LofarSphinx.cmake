# - Generate script to configure and run the code documentation tool Sphinx.

# $Id$
#
# Copyright (C) 2010
# ASTRON (Netherlands Institute for Radio Astronomy)
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

include(LofarMacros)
include(FindPythonModule)

# Only process this file when we're ate the top-level source directory.
if("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_SOURCE_DIR}")


    # Locate the sphinx programs.
    find_package(Sphinx)

    # Set Sphinx input directory, containing the *.rst files that are used to create the HTML documenation.
    # These files are first created using the sphinx-apidoc utility on the python module created by make.
    if(NOT SPHINX_SOURCE_DIR)
      set(SPHINX_SOURCE_DIR "${CMAKE_BINARY_DIR}/doc/sphinx/source" CACHE PATH
        "Directory where Sphinx will read the source rst files from to generate documentation")
    endif(NOT SPHINX_SOURCE_DIR)
    file(MAKE_DIRECTORY "${SPHINX_SOURCE_DIR}")


    # Generate the CMake script that will be invoked by 'make doc'.
    configure_file(
      "${CMAKE_SOURCE_DIR}/CMake/docscripts/MakeSphinxDoc.cmake.in"
      "${CMAKE_BINARY_DIR}/MakeSphinxDoc.cmake" @ONLY)

    # Define custom target 'doc'.
    add_custom_target(doc
      COMMAND "${CMAKE_COMMAND}"
      -D SPHINX_SOURCE_DIR="${SPHINX_SOURCE_DIR}"
      -D PYTHON_BUILD_DIR="${PYTHON_BUILD_DIR}"
      -D LOFARROOT="${CMAKE_SOURCE_DIR}"
      -P "${CMAKE_BINARY_DIR}/MakeSphinxDoc.cmake"
      COMMENT "Defining target 'doc' ...")


endif("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_SOURCE_DIR}")
