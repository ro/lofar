import React, {
    Component
} from 'react';

import { IoMdArrowDropdown as DropDownIcon} from 'react-icons/io';
import {renderDateRange} from 'utils/utils';
import AntennaTestRow from '../AntennaTestRow'

// CSS
import rtsmStyles from 'themes/rtsm_collapsable.module.scss';



class AntennaRTSMRows extends Component {

    state = {
        isCollapsed: true
    }

    dropdownClick = () => {
        this.setState({isCollapsed: !this.state.isCollapsed})
        this.props.update()
    }

    computeRTSMSummary(errors, errorTypes) {
        const totalNumberOfErrors = errors.length;
        const summary = {};

        for (const type of errorTypes) {
            for (const error of errors) {
                if (error.component_errors.hasOwnProperty(type)) {
                    if (!summary.hasOwnProperty(type)) {
                        summary[type] = 0.;
                    }
                    summary[type] += 1.;
                }
            }
        }

        const summaryItems = errorTypes.map(item => {
            let value = "";
            if (summary.hasOwnProperty(item)) {
                value = (summary[item] * 100 / totalNumberOfErrors).toFixed(0)+"%"
            }
            return {item, value}
        });

        return summaryItems;
    }

    renderRTSMSummaryRow() {
        const timeSpan = renderDateRange(this.props.data);
        const summaryItems = this.computeRTSMSummary(this.props.data, this.props.errorTypes);
        const dropdownAdditionStyles = !this.state.isCollapsed ? " "+rtsmStyles.dropdownbutton_up : "";

        return (
            <tr className={rtsmStyles.rtsm_summary_row}>
                <td>R</td>
                <td onClick={this.dropdownClick}>
                    {timeSpan}
                    <DropDownIcon className={rtsmStyles.dropdownbutton + dropdownAdditionStyles}/>
                </td>
                <td></td>
                {summaryItems.map( obj => {
                    let c = obj.value !== "" ? rtsmStyles.rtsm_summary_badge : ""
                    return (
                        <td key={obj.item} className={c}>
                            {obj.value}
                        </td>
                    )
                }) }
            </tr>);
    }

    render() {

        // RTSM; return summary row and data rows if expanded
        return (
            <React.Fragment>
                {this.renderRTSMSummaryRow()}
                {!this.state.isCollapsed && this.props.data.map((test, id) =>
                    <AntennaTestRow data={test} antennaId={this.props.antennaId} errorTypes={this.props.errorTypes} key={id} />
                )}
            </React.Fragment>
        );
    }
}


export default AntennaRTSMRows;
