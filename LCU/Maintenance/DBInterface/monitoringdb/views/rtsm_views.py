import os
from datetime import datetime

from .common import *
from rest_framework import viewsets
from django.http import Http404
from django.shortcuts import get_object_or_404

from lofar.maintenance.mdb.tests_validators import is_rtsm_test
from lofar.maintenance.monitoringdb.tasks.generate_plots import check_error_summary_plot
from lofar.maintenance.monitoringdb.tasks.insert_raw_tests import insert_rtsm_test
from ..models.log import ActionLog
from ..models.rtsm import RTSMObservation
from ..serializers.rtsm import RTSMObservationSerializer, RTSMErrorSerializer, \
    RTSMSummaryPlotSerializer, RTSMError, RTSMErrorSample, RTSMErrorSampleSerializer

logger = logging.getLogger('views')


class RTSMErrorsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = RTSMError.objects.all()
    serializer_class = RTSMErrorSerializer
    filter_fields = '__all__'


class RTSMObservationViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = RTSMObservation.objects.all()
    serializer_class = RTSMObservationSerializer
    filter_fields = '__all__'


class RTSMErrorSampleViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = RTSMErrorSample.objects.all()
    serializer_class = RTSMErrorSampleSerializer
    exclude_fields = 'bad_spectrum', 'average_spectrum'


class RTSMSummaryPlot(viewsets.ViewSet):
    """
    Get the summary plot associated to the error summary given
    """
    queryset = RTSMError.objects.all()

    def retrieve(self, _, pk=None):
        try:
            entity = get_object_or_404(self.queryset, pk=pk)
            summary_plot = entity.summary_plot.first()

            if summary_plot is None:
                raise ObjectDoesNotExist()

            uri = RTSMSummaryPlotSerializer(summary_plot).data['uri']

        except ObjectDoesNotExist:
            check_error_summary_plot.delay(pk)
            raise Http404()

        if uri and os.path.exists(uri) and os.path.isfile(uri):
            with open(uri, 'rb') as f_stream:
                image = f_stream.read()
            return HttpResponse(image, status=status.HTTP_200_OK, content_type='image/gif')
        else:
            check_error_summary_plot.delay(pk)

            raise Http404()


def handle_rtsm_insert(content, remote_addr):
    action_log = ActionLog(who=remote_addr,
                           what='INSERT_REQUESTED',
                           when=datetime.now(),
                           model_name='RTSM',
                           entry_id=-1)

    action_log.save()

    insert_rtsm_test.delay(action_log.pk, content)

    return action_log.pk


@api_view(['POST'])
def insert_raw_rtsm_test(request: HttpRequest):
    """
    This function is meant to parse a request of the form
    {
    "content": "[RTSM TEST RAW TEXT]" station_name: "station_name"
    }
    parse the content field and create all the rtsm_test entity related into the database
    :param request: HTTP request
    :return:
    """
    if request.method == 'POST':
        remote_addr = request.META['REMOTE_ADDR']
        logs_pk = []

        if 'content' in request.data:
            content = request.data

            logs_pk += [handle_rtsm_insert(content, remote_addr)]
        elif request.FILES:
            files = request.FILES
            for station_name, file in zip(request.data['station_name'], files.values()):
                content_data = file.read().decode('UTF-8')
                if not is_rtsm_test(content_data):
                    return Response(exception=True,
                                    data="the attached file is not a RTSM %s" % file.name,
                                    status=status.HTTP_400_BAD_REQUEST)

                content = dict(station_name=request.data['station_name'],
                               content=content_data)
                logs_pk += [handle_rtsm_insert(content, remote_addr)]
        else:
            return Response(exception=True,
                            data="the post message is not correct." +
                                 " It has to be of the form \{'content':[RAWSTRING]\}",
                            status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK,
                        data='RTSM tests insert acknowledged.'
                             ' Check log for id %s' % logs_pk)
