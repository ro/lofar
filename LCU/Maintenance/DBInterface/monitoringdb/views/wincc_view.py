from rest_framework import viewsets
from lofar.maintenance.monitoringdb.models.wincc import WinCCAntennaStatus
from lofar.maintenance.monitoringdb.serializers.wincc import WinCCAntennaStatusSerializer


class WinCCAntennaStatusViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = WinCCAntennaStatus.objects.all()
    serializer_class = WinCCAntennaStatusSerializer
    filter_fields = '__all__'
