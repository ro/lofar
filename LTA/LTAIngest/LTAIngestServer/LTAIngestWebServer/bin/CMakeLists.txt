
lofar_add_bin_scripts(ingestwebserver)

# supervisord config files
lofar_add_sysconf_files(ingestwebserver.ini
                        DESTINATION supervisord.d)

