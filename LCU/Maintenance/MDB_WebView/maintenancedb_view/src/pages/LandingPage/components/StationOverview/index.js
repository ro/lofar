import React from 'react';
import {Table} from 'reactstrap';
import AutoLoadWrapper from 'components/AutoLoadWrapper'

import SORow from './SORow'

// CSS
import styles from './styles.module.scss'


/**
 * TableRows: function component returning the table rows.
 */
function TableRows({data}) {
    return data.map((stationData) =>
        <SORow key={stationData.station_name} data={stationData} />
    );
}

/**
 * StationOverview class.
 */
function StationOverviewC(props) {

        return (
            <div className={styles.station_overview_ctrl}>
                <Table size="sm" className={styles.so_table}>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Station tests</th>
                            <th>Latest observations</th>
                        </tr>
                    </thead>
                    <tbody>
                        <TableRows data={props.data} />
                    </tbody>
                </Table>
            </div>
        );
}

/* Add some magic; use the AutoLoadWrapper to create a HOC that handles the
   auto-loading of the data for StationOverviewC.
 */
const StationOverview = AutoLoadWrapper(StationOverviewC);

export default StationOverview;
