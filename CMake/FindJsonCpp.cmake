# - Try to find libjson, a library processing json blobs
# Variables used by this module:
#  JSONCPP_ROOT_DIR     - JsonCpp root directory
# Variables defined by this module:
#  JSONCPP_FOUND        - system has JsonCpp
#  JSONCPP_INCLUDE_DIR  - the JsonCpp include directory (cached)
#  JSONCPP_INCLUDE_DIRS - the JsonCpp include directories
#                          (identical to JSONCPP_INCLUDE_DIR)
#  JSONCPP_LIBRARY      - the JsonCpp library (cached)
#  JSONCPP_LIBRARIES    - the JsonCpp library

# Copyright (C) 2009
# ASTRON (Netherlands Institute for Radio Astronomy)
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
#
# $Id$

if(NOT JSONCPP_FOUND)

  find_path(JSONCPP_INCLUDE_DIR jsoncpp/json/json.h
    HINTS ${JSONCPP_ROOT_DIR} PATH_SUFFIXES include)
  find_library(JSONCPP_LIBRARY jsoncpp
    HINTS ${JSONCPP_ROOT_DIR} PATH_SUFFIXES lib)
  mark_as_advanced(JSONCPP_INCLUDE_DIR JSONCPP_LIBRARY)

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(JsonCpp DEFAULT_MSG
    JSONCPP_LIBRARY JSONCPP_INCLUDE_DIR)

  set(JSONCPP_INCLUDE_DIRS ${JSONCPP_INCLUDE_DIR})
  set(JSONCPP_LIBRARIES ${JSONCPP_LIBRARY})

endif(NOT JSONCPP_FOUND)
