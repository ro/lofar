#!/bin/bash
DATE=$(date +%Y%m%dT%H%M%S)
echo logfile /data/log/tbbwriter-$DATE.log
echo starting in 60 seconds
sleep 60
./start-tbbwriter.sh
exec srun -N 24 \
    --partition=cpu \
    --cpus-per-task=1 \
    --job-name="tbbwriter" \
    --output=/data/log/tbbwriter/tbbwriter-$DATE-%N.log \
    /data/home/lofarsys/tbb-writer/run/docker-launch-tbbwriter.sh "$@"


