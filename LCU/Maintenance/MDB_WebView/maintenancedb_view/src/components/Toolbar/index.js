/** @module Toolbar */

import React from 'react';

// CSS
import styles from './styles.module.scss'


/**
 * Toolbar: the wrapper for all toolbar components. Use as:
 * <Toolbar>
 *   <StationAutoComplete />
 *   <TestTypeSelector />
 *   <..>
 * </Toolbar>
 */
export function Toolbar(props) {

    return (<div className={styles.toolbar_top}>
                {props.children}
            </div>);
}


export { AntennaIdSelector } from './AntennaIdSelector';
export { AntennaTypeSelector } from './AntennaTypeSelector';
export { DateRangeSelector } from './DateRangeSelector';
export { ErrorTypesSelector } from './ErrorTypesSelector';
export { ErrorsOnlySelector } from './ErrorsOnlySelector';
export { PeriodSelector } from './PeriodSelector';
export { StationAutoComplete } from './StationAutoComplete';
export { StationGroupSelector } from './StationGroupSelector';
export { TestTypeSelector } from './TestTypeSelector';
