import React from 'react';

/* Boiler plate for a Grid panel.
   Looks like a React component but is ordinary function! */
function createGridPanel(props) {
    let body = props.body;
    if (props.renderHeader) {
        body = <React.Fragment>
            <h5 className="react-grid-item-header">{props.title}</h5>
            <div className="react-grid-item-body">
                {props.body}
            </div>
        </React.Fragment>;
    }
    return (<div key={props.key}>
        {body}
    </div>);
}


export { createGridPanel };
