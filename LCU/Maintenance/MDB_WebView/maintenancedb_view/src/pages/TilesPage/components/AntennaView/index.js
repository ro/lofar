import React, {
    Component
} from 'react';

import ReactTableContainer from 'react-table-container';
import AutoLoadWrapper from 'components/AutoLoadWrapper';
import {connect} from 'react-redux';
import {selectAntennaError} from 'redux/actions/antennaOverviewPageActions';

import {componentErrorTypes} from "utils/constants.js";
import {componentStatusOK, componentStatusBeyondRepair} from 'utils/LOFARDefinitions';
import classNames from "classnames";
import FillViewportHeightDiv from 'components/FillViewportHeightDiv';
import AntennaTestRow from './AntennaTestRow'
import AntennaRTSMRows from './AntennaRTSMRows'


// CSS
import styles from './styles.module.scss';


/**
 * TableHeader: renders table header for AntennaTable.
 *
 * @prop {string} status - current compponent status
 * @prop {array} errorTypes - array of all error types (from appInitData reducer)
 */
function TableHeader({status, doHighlightStatus, errorTypes}) {
    let statusOk = componentStatusOK(status)
    let isBeyondRepair = componentStatusBeyondRepair(status);

    let cls = classNames({
        'status': true,
        'status-non-operational': !statusOk && !isBeyondRepair,
        'status-beyond-repair': isBeyondRepair,
        [styles.highlight]: doHighlightStatus
    });

    return <tr>
        <th title="Test type">T</th>
        <th style={{width: "10em"}}>Date</th>
        <th title="Component status"
            className={cls}
            data-has-problems={!statusOk}
            data-data-idx="current"
            data-item-type="STATUS"
            data-item-value={status} >
            S
        </th>
        {errorTypes.map((error_type, key) =>
            <td key={key} title={error_type}>{componentErrorTypes[error_type]}</td>
        )}
        <th></th>
        {[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16].map((el, key) =>
            <td key={key} title={"Element "+el}>{el}</td>
        )}
    </tr>;
}


/**
 * AntennaTable: component with a chronological table of station test and RTSM
 * results for a selected antenna.
 *
 * @prop {object} data - the data object
 * @prop {array} errorTypes - array of all error types (from appInitData reducer)
 * @prop {integer} height - height in pixels of the containing <div>
 */
class AntennaTable extends Component {

    static groupData(data) {
        let grouped = [];
        let group = [];

        data.forEach( (test, idx) => {
            test.dataIdx = idx.toString()   // Add the array index for future reference
            if (test.test_type !== 'R') {
                if (group.length > 0) {
                    grouped.push(group);
                    group = [];
                }
                grouped.push(test)
            } else {
                group.push(test)
            }
        })

        if (group.length > 0) grouped.push(group);

        return grouped
    }

    updateIfContentChanges = () => {
        this.setState({state:this.state})
    }

    getCurrentStatus = () => {
        let data = this.props.data;
        if (data.current_status && data.current_status[this.props.antennaId]) {
            return data.current_status[this.props.antennaId].status
        }
        return null;
    }

    // Check the event node itself or its parent (some TD's have a child element that may trigger the mouseOver)
    getNode = (e) => {
        let node = e.target;
        return (node.dataset.hasProblems === "true" ? node :
                node.parentNode.hasProblems === "true" ? node.parentNode : null);
    }

    currentElem = null;

    onMouseOver = (e) => {
        if (this.currentElem) {
          // before entering a new element, the mouse always leaves the previous one
          // if we didn't leave <td> yet, then we're still inside it, so can ignore the event
          return;
        }

        // Check if this node or its parent is a TD
        let node = this.getNode(e);
        if (node) {
            this.currentElem = e.target;
            this.onHover(node.dataset, false);
        }
    }

    onMouseOut = (e) => {
        if (!this.currentElem) {
            return;
        }

        // we're leaving the element -- but maybe to a child element
        if (e.relatedTarget) { // possible: relatedTarget = null
            // check if we're still inside this.currentElem
            // if so, it is an internal transition -- ignore it
            if (e.relatedTarget === this.currentElem ||
                e.relatedTarget.parentNode === this.currentElem) {
                return;
            }
        }

        // we really left the element
        this.onHover(null, false);
        this.currentElem = null;
    }

    onContextMenu = (e) => {
        e.preventDefault();
        let node = this.getNode(e);
        if (node) {
            this.onHover(node.dataset, true);
        }
    }


    onHover = (ev_dataset, doPin=false) => {

        if (ev_dataset === null) {
            this.props.selectAntennaError(null);
            return
        }

        // Data to pass on to the child panel
        let childPanelData = {
            antenna_id: this.props.antennaId,
            dataIdx: ev_dataset.dataIdx,
            itemType: ev_dataset.itemType,
            itemValue: ev_dataset.itemValue,
            content: {}
        };

        // Special case: when dataIdx is current, it means the header row with the status fieldwas hovered
        if (ev_dataset.dataIdx === "current") {
            childPanelData.content = this.props.data.current_status[this.props.antennaId]
        } else {
            // Get the data of the selected test
            childPanelData.content = this.props.data.errors[ev_dataset.dataIdx];
        }

        this.props.selectAntennaError(childPanelData, doPin);
    }


    onHoverORG = (ev_dataset, doPin=false) => {

        if (ev_dataset === null) {
            this.props.selectAntennaError(null);
            return
        }

        // Data to pass on to the child panel
        let childPanelData = {
            antenna_id: this.props.antennaId,
            test_type: "",
            start_date: "",
            end_date:  "",
            error_type: "",
            content: {}
        };

        // Special case: when dataIdx is current, it means the header row with the status fieldwas hovered
        if (ev_dataset.dataIdx === "current") {
            let statusData = this.props.data.current_status[this.props.antennaId]
            childPanelData.test_type = "ST"
            childPanelData.start_date = "current"
            childPanelData.error_type = 'STATUS'
            childPanelData.content = statusData

        } else {

            // Get the data of the selected test
            let data = this.props.data.errors[ev_dataset.dataIdx];

            // Set common fields valid for all
            childPanelData.test_type = data.test_type
            childPanelData.start_date = data.start_date
            childPanelData.end_date = data.end_date

            // Set fields itemType
            if (ev_dataset.itemType === 'ERROR_TYPE') {
                childPanelData.error_type = ev_dataset.itemValue
                childPanelData.content = data.component_errors[ev_dataset.itemValue]
            }
            else if (ev_dataset.itemType === 'ELEMENT') {
                let element_id = ev_dataset.itemValue;
                let content = { details: {}}
                Object.keys(data.component_errors).forEach((error_type) => {
                    let value = data.component_errors[error_type]
                    if (value.element_errors && value.element_errors[element_id]) {
                        content.details[error_type] = value.element_errors[element_id]
                    }
                })

                childPanelData.element_id = element_id
                childPanelData.error_type = ""
                childPanelData.content = content
            }
            else if (ev_dataset.itemType === 'STATUS') {
                childPanelData.error_type = 'STATUS'
                childPanelData.content = data.status[this.props.antennaId]
            }
            else {
                return
            }
        }

        this.props.selectAntennaError(childPanelData, doPin);
    }

    render() {
        let rowData = [];
        if (this.props.data.hasOwnProperty('errors')) {
            rowData = AntennaTable.groupData(this.props.data.errors);
        }

        return (
            <ReactTableContainer width="100%" height={this.props.height+'px'}>
                <table  className={styles.antenna_table+" table-sm table-hover table-bordered"}
                        onContextMenu={this.onContextMenu}
                        onMouseOver={this.onMouseOver}
                        onMouseOut={this.onMouseOut} >
                    <thead>
                        <TableHeader status={this.getCurrentStatus()}
                                     doHighlightStatus={this.props.highlightData && this.props.highlightData.dataIdx === "current"}
                                     errorTypes={this.props.errorTypes} />
                    </thead>
                    <tbody>
                    {rowData.map((testData, id) =>
                        testData.hasOwnProperty('test_type')
                            ? <AntennaTestRow key={id} data={testData} antennaId={this.props.antennaId} errorTypes={this.props.errorTypes} />
                            : <AntennaRTSMRows key={id} data={testData} antennaId={this.props.antennaId} errorTypes={this.props.errorTypes} update={this.updateIfContentChanges}/>
                    )}
                    </tbody>
                </table>
            </ReactTableContainer>
        );
    }
}

/**
 * AntennaViewC; antenna view component to display the test results for a selected antenna.
 *
 * It gets the props from the mainFilters and appInitData reducers.
 */
const AntennaViewC = function(props){
    return (
        <FillViewportHeightDiv className="border-right">
           { ({height}) => <AntennaTable  {...props} height={height} />}
        </FillViewportHeightDiv>
    )
}

const AntennaView = AutoLoadWrapper( connect(state => {
    return {
        ...state.mainFilters,
        ...state.antenna_page.main_panel,
        ...state.appInitData
    };
}, {
    selectAntennaError
})(AntennaViewC) );


export default AntennaView;
