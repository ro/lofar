export const SET_STATION_GROUP = "SET_STATION_GROUP";
export const SET_STATION = "SET_STATION";
export const SET_ERRORS_ONLY = "SET_ERRORS_ONLY";
export const SET_PERIOD = "SET_PERIOD";
export const SET_DATE_RANGE = "SET_DATE_RANGE";
export const SET_TEST_TYPE = "SET_TEST_TYPE";
export const SET_ERROR_TYPES = "SET_ERROR_TYPES";
export const SET_ANTENNA_TYPE = "SET_ANTENNA_TYPE";
export const SET_ANTENNA_ID = "SET_ANTENNA_ID";

export const setAntennaID = antennaId => ({
  type: SET_ANTENNA_ID,
  payload: { antennaId }
});

export const setAntennaType = antennaType => ({
  type: SET_ANTENNA_TYPE,
  payload: { antennaType }
});

export const setStationGroup = stationGroup => ({
  type: SET_STATION_GROUP,
  payload: { stationGroup: stationGroup }
});

export const setStation = station => ({
  type: SET_STATION,
  payload: { station }
});

export const setErrorsOnly = errorsOnly => ({
  type: SET_ERRORS_ONLY,
  payload: { errorsOnly }
});

export const setPeriod = period => ({
  type: SET_PERIOD,
  payload: { period }
});

export const setDateRange = rangeObj => ({
  type: SET_DATE_RANGE,
  payload: { ...rangeObj }
});

export const setTestType = type => ({
  type: SET_TEST_TYPE,
  payload: { type }
});

export const setErrorTypes = type_list => ({
    type: SET_ERROR_TYPES,
    payload: { type_list }
});
