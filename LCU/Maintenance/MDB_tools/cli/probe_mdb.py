#! /usr/bin/env python
from __future__ import print_function
import argparse
import logging
import sys
from collections import Counter
from datetime import datetime, timedelta
from operator import itemgetter

import beautifultable
import blessings
import requests

logger = logging.getLogger('probe_mdb')

"""
This program is meant to probe from the command line the station tests and RTSM in a certain period
 of time and on a certain station name
"""
CACHING_SIZE = 100


def setup_argument_parser():
    """
    Setup the argument parser with the possible options
    :return: an instance of the argument parser with all the probe_mdb options
    :rtype: argparse.ArgumentParser
    """
    parser = argparse.ArgumentParser(prog='probe_mdb',
                                     description="probe_mdb is a tool which simplify the queries" +
                                                 " to the maintenance database. Specifically, it"
                                                 " helps to gather past test output performed on a "
                                                 " specific station, in a specific time range"
                                                 " or a specific class of stations")
    parser.add_argument('--address', help='address of the server. default [localhost]:8000',
                        default='http://localhost:8000')
    parser.add_argument('--from_date', help='selects tests from date [YYYY-MM-DD]', default=None)
    parser.add_argument('--to_date', help='selects tests to date [YYYY-MM-DD]', default=None)
    parser.add_argument('--station', help='selects a specific station. ex. CS001C', default=None)
    parser.add_argument('--station_type', help='selects a specific station type. ex. I',
                        choices=['C', 'R', 'I'])
    parser.add_argument('--last-month', help='selects the last month results', action='store_true')
    parser.add_argument('--request-size',
                        help='change the request size. Current default size is {}'.
                        format(CACHING_SIZE))
    parser.add_argument('-v', help='verbose output', action="store_false")

    # TODO implement it
    # parser.add_argument('--to_csv', help='s results into a csv file', default=None)

    return parser


def query_api_for_stationtest_rtsm_results(query_string, address, next_url=None):
    """
    Execute the query string to the specified address
    :param query_string: the query string to be done
    :param address: the address at which it will be done
    :param next_url: if set it uses directly next_url as address. This is meant for recursive lookup.
    :return: a list of dicts or an empty list
    """
    if next_url is None:
        full_address = '/'.join([address, query_string])
    else:
        full_address = next_url
    logging.info('performing query %s', full_address)
    response = requests.get(full_address)
    logging.info('response acknowledged: status code is %s', response.status_code)
    if response.status_code == 200:
        parsed_content = response.json()

        extracted_content = parsed_content['results']
        next_url = parsed_content['next']
        if next_url is not None:
            return extracted_content + query_api_for_stationtest_rtsm_results(query_string, address,
                                                                              next_url)
        else:
            return extracted_content
    else:
        logging.error('error querying %s: http response code %d - %s, %s',
                      full_address, response.status_code,
                      response.reason, response.content)
        raise Exception('Error querying {}'.format(address))


def get_url_string_to_select_timespan(from_date, to_date):
    """
    Create the query string to query the data in a certain time stamp

    :param from_date: date from which the results are queried
    :type from_date: Union[datetime, int]
    :param to_date: date to which the results are queries
    :type to_date: Union[datetime, int]
    :return: the query string
    """
    query = ''
    if from_date:
        query += '&start_time__year__gte=%d' % (from_date.year,)
        query += '&start_time__month__gte=%d' % (from_date.month,)
        query += '&start_time__day__gte=%d' % (from_date.day,)

    if to_date:
        query += '&start_time__year__lte=%d' % (to_date.year,)
        query += '&start_time__month__lte=%d' % (to_date.month,)
        query += '&start_time__day__lte=%d' % (to_date.day,)

    return query


def get_url_string_to_select_test_type(test_type):
    """
    Create the query string to probe a certain station test type choices are RTSM, STATION_TEST
    :param test_type: it can be RTSM or STATION_TEST
    :type test_type: str
    :return: the url for the API call
    :rtype: str
    """

    if test_type == 'RTSM':
        query = 'rtsm/?'
    elif test_type == 'STATION_TEST':
        query = 'stationtests/?'
    else:
        logger.error('please specify a test type=[RTSM|STATION_TEST]')
        raise ValueError('please specify an test type=[RTSM|STATION_TEST]')
    query += 'page_size={}'.format(CACHING_SIZE)
    return query


def get_url_string_to_select_station_name(station_name):
    """
    Return the query string to filter the results to a specific station name
    :param station_name: the name of the station
    :return: the part of the url to the API call to select a specific station
    :rtype: str
    """
    query = '&station_name={}'.format(station_name.strip(' \'\"'))
    return query


class TestType:
    """
    Enum class
    """

    STATION_TEST = 'STATION_TEST'
    RTSM = 'RTSM'


def retrieve_station_tests(address, from_time, to_time, station_name=""):
    """
    Queries the station test for a given time span and a station name
    :param address: address where the MDB REST api is hosted
    :type address: str
    :param from_time: date time from which interrogate the MDB REST API
    :type from_time: Union[str, datetime]
    :param to_time: date time to which interrogate the MDB REST API
    :type to_time: Union[str, datetime]
    :param station_name: name of the station to query for
    :type station_name: str
    :return: a dict with the results of the query to the REST API
    """
    query_string = get_url_string_to_select_test_type(TestType.STATION_TEST)

    if from_time or to_time:
        query_string += get_url_string_to_select_timespan(from_time, to_time)
    if station_name:
        query_string += get_url_string_to_select_station_name(station_name)

    return query_api_for_stationtest_rtsm_results(query_string, address)


def retrieve_rtsm(address, from_time, to_time, station_name=""):
    """
    Queries the RTSM for a given time span and a station name
     :param address: address where the MDB REST api is hosted
    :type address: str
    :param from_time: date time from which interrogate the MDB REST API
    :type from_time: Union[str, datetime]
    :param to_time: date time to which interrogate the MDB REST API
    :type to_time: Union[str, datetime]
    :param station_name: name of the station to query for
    :type station_name: str
    :return: a dict with the results of the query to the REST API
   """
    query_string = get_url_string_to_select_test_type(TestType.RTSM)
    if from_time or to_time:
        query_string += get_url_string_to_select_timespan(from_time, to_time)
    if station_name:
        query_string += get_url_string_to_select_station_name(station_name)

    return query_api_for_stationtest_rtsm_results(query_string, address)


def reformat_datetime(item):
    """
    Reformats some objects to better  them out
    :param item: the object to be formatted
    :return: the formatted string
    """
    if isinstance(item, datetime):
        return str(item).replace('T', ' ').replace('-', ' ')
    else:
        return str(item)


def print_out_station_test_summary(station_test, idx):
    """
    Prints the summary of the station test
    :param station_test: dict containing the results of the station test
    :param idx: index of the current station test
    """
    summary_fields = ['station_name', 'station_type', 'start_time', 'end_time', 'performed_checks']
    summary_header = ['Name', 'Type', 'Start', 'End', 'Checks']
    values = [reformat_datetime(station_test[item]) for item in summary_fields]
    print()
    print('Station test #{}'.format(idx))
    table_width = 200
    terminal = blessings.Terminal()
    if terminal.does_styling:
        table_width = terminal.width
    table = beautifultable.BeautifulTable(max_width=table_width)
    table.column_headers = summary_header
    table.append_row(values)
    table[0]['Start'] = table[0]['Start'].replace('-', '/').replace('T', ' ')
    table[0]['End'] = table[0]['End'].replace('-', '/').replace('T', ' ')

    print(table)


def sort_component_errors(component_errors):
    """
    Sort the component errors by
        component_type ascending
        component_id ascending
    :param component_errors:
    :return: returns the sorted component errors
    """
    sorted_components = sorted(component_errors, key=itemgetter('component_type', 'component_id'))
    return sorted_components


def print_out_component_errors_summary(station_test):
    """
    Prints out on terminal a summary of the component errors
    :param station_test: the dict that contains the station_test
    """
    summary_fields = ['component_type', 'component_id', 'error_type']

    table = beautifultable.BeautifulTable()
    table.column_headers = summary_fields
    for error in sort_component_errors(station_test['all_errors']):
        values = [error.get(item, '') for item in summary_fields]
        table.append_row(values)
    print()
    print('Component Error Summary')
    print(table)


def print_out_component_errors(station_test):
    """
    Prints out the output of the component errors
    :param station_test: the dict that contains the station_test
    """
    excluded_fields = {'parent', 'resourcetype', 'polymorphic_ctype', 'id', 'station_test_id',
                       'tile_errors',
                       'parent_component_error'}
    summary_fields = ['component_type', 'component_id', 'error_type']
    tile_summary_fields = ['tile_id']

    component_error_indentation = ' ' * 3
    tile_error_indentation = ' ' * 5

    print()
    print('=' * 5 + 'Details' + '=' * 5)

    for i, error in enumerate(sort_component_errors(station_test['all_errors'])):
        component_error_string = 'Component error #{}'.format(i)
        print(component_error_indentation + component_error_string)
        print(component_error_indentation + '-' * len(component_error_string))

        for key in summary_fields:
            value = str(error[key])
            print(component_error_indentation + key + ' ' + value)

        details_field = set(error.keys()) - set(summary_fields) - excluded_fields
        details = []

        for key in details_field:
            value = error[key]
            if value is None:
                continue
            details.append((key, value))

        if details:
            for key, value in details:
                print(component_error_indentation + key + ' ' + str(value))

        tile_errors = error.get('tile_errors', None)
        if tile_errors:
            for k, tile_error in enumerate(tile_errors):
                tile_error_string = 'Element error #{} on element id {}'.format(k, tile_error['tile_id'])
                print(tile_error_indentation + tile_error_string)
                print(tile_error_indentation + '-' * len(tile_error_string))

                tile_keys = set(tile_error.keys()) - set(tile_summary_fields) - excluded_fields
                tile_values = [tile_error[key] for key in tile_keys]
                for tile_key, tile_value in zip(tile_keys, tile_values):
                    print(tile_error_indentation + tile_key + ' ' + str(tile_value))
                print()
        print('\n')


def rtsm_sort_errors(errors):
    """
    Sort the rtsm errors by
        rcu
        error_type
        time

    :param errors: RTSM errors to be sorted
    :return: the sorted RTSM errors
    """
    sorted_components = sorted(errors, key=itemgetter('rcu', 'error_type', 'time'))
    return sorted_components


def print_out_rtsm_summary(rtsm_results):
    """
    It prints out on a terminal the RTMS summary
    :param rtsm_results: A list of dicts containing the RTSM results
    """
    table_width = 200
    terminal = blessings.Terminal()
    if terminal.does_styling:
        table_width = terminal.width
    table = beautifultable.BeautifulTable(max_width=table_width)
    header = ['observation_id', 'station_name', 'start_time', 'end_time',
              'component_id', 'error_type', 'start_frequency', 'stop_frequency',
              'mode', 'percentage']
    table.column_headers = header

    for rtsm_result in sorted(rtsm_results, key=itemgetter('observation_id')):
        obs_id = rtsm_result['observation_id']
        station_name = rtsm_result['station_name']
        start_time = rtsm_result['start_time'].replace('T', ' ')
        end_time = rtsm_result['end_time'].replace('T', ' ')
        samples = rtsm_result['samples']

        table = beautifultable.BeautifulTable(max_width=table_width)
        table.column_headers = ['observation_id', 'station_name', 'start_time', 'end_time',
                                'component_id', 'error_type', 'start_frequency', 'stop_frequency',
                                'mode', 'percentage']

        sorted_rtsm = rtsm_sort_errors(rtsm_result['errors_summary'])
        rtsm_errors = Counter()
        for error in sorted_rtsm:
            rcu_id = error['rcu']
            start_frequency = error['start_frequency']
            stop_frequency = error['stop_frequency']

            mode = error['mode']
            error_type = error['error_type']
            rtsm_errors[(rcu_id, error_type, start_frequency, stop_frequency, mode)] += 1
        for key, value in rtsm_errors.items():
            table.append_row(
                [obs_id, station_name, start_time, end_time] + list(key) + [value * 100 / samples])
        print(table)
        print()


def print_out_rtsm(rtsm_results):
    """
    Prints the results of the RTSM
    :param rtsm_results:
    """
    table_width = 200
    terminal = blessings.Terminal()
    if terminal.does_styling:
        table_width = terminal.width
    table = beautifultable.BeautifulTable(max_width=table_width)
    table.column_headers = ['obs_id',
                            'station',
                            'time',
                            'rcu',
                            'mode',
                            'error_type',
                            'start frequency',
                            'stop frequency'
                            ]

    for rtsm_result in rtsm_results:
        obs_id = rtsm_result['observation_id']
        station_name = rtsm_result['station_name']
        for error in rtsm_sort_errors(rtsm_result['errors_summary']):
            rcu_id = error['rcu']
            start_frequency = error['start_frequency']
            stop_frequency = error['stop_frequency']
            time = error['time']
            mode = error['mode']
            error_type = error['error_type']
            table.append_row([obs_id, station_name, time, rcu_id, mode, error_type, start_frequency,
                              stop_frequency])
    print(table)


def print_out_rtsm_and_stationtests(station_test_results, rtsm_results):
    """
    Prints out the formatted station test results and RTSM results
    :param station_test_results: dict that contains the stationtests output
    :param rtsm_results: dict that contains the rtsm tests output
    """

    for i, station_test_result in enumerate(station_test_results):
        print_out_station_test_summary(station_test_result, i)
        print_out_component_errors_summary(station_test_result)
        print_out_component_errors(station_test_result)
        print('=' * 10)
        print()
    print('RTSM')
    print_out_rtsm(rtsm_results)
    print_out_rtsm_summary(rtsm_results)


def parse_specified_datetime(specified_datetime):
    """
    Parses the input datetime and coverts it to datetime object
    the expected datetime format '%Y-%m-%d'
    :param specified_datetime: specified datetime from command line
    :return: a datetime object
    """
    try:
        if isinstance(specified_datetime, datetime):
            date_str = specified_datetime
        else:
            date_str = datetime.strptime(specified_datetime, '%Y-%m-%d')
    except ValueError:
        logger.error('format of to date not valid; YYYY-MM-DD es. 2016-15-12')
        raise
    return date_str


def probe_mdb():
    """
    Main entry for the script
    """
    parser = setup_argument_parser()
    args = parser.parse_args()
    if args is None:
        sys.exit(1)

    if args.v:
        logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=logging.DEBUG)
    from_date = parse_specified_datetime(args.from_date)
    to_date = parse_specified_datetime(args.to_date)

    if args.last_month:
        from_date = datetime.now() - timedelta(days=30)

    station_test_results = retrieve_station_tests(args.address, from_date, to_date, args.station)

    rtsm_results = retrieve_rtsm(args.address, from_date, to_date, args.station)

    print_out_rtsm_and_stationtests(station_test_results, rtsm_results)


def main():
    probe_mdb()
