import React, {
    Component
} from 'react';

import {componentStatusOK, componentStatusBeyondRepair} from 'utils/LOFARDefinitions';

import classNames from "classnames";

// CSS
import styles from './styles.module.scss';


/**
 * GenericStatusTd; render a <td> showing the status for an error. Show the error
 * details in the detail panel on the right on mouse hover.
 *
 * @prop {bool} isGood - indicates if this item has errors
 * @prop {bool} isPinned - indicates if this item is pinned in the child panel, if so highlight it
 * @prop {object} data - the data object
 * @prop {integer} dataIdx - index of data.errors in JSON from server
 * @prop {string} itemType - ERROR_TYPE or ELEMENT
 * @prop {string} itemValue - The error_type or element id
 */
class GenericStatusTd extends Component {

    renderStationTest() {
        return 'X';
    }

    renderRTSM() {
        let presentErrors = [];

        if (this.props.hasX) {
            presentErrors.push(<div className={styles.x_pol} key="X">X</div>);
        }
        if (this.props.hasY) {
            presentErrors.push(<div className={styles.y_pol} key="Y">Y</div>);
        }

        return presentErrors;
    }

    renderError() {

        // Status error
        if (this.props.itemType === 'STATUS') {
            return "";
        }
        // RTSM error
        if (this.props.testType === 'R') {
            return this.renderRTSM();
        }
        // Station test error
        if (this.props.testType === 'S') {
            return this.renderStationTest();
        }
        return ""
    }

    render() {
        let classes = {
            [styles.status]: true,
            [styles.highlight]: this.props.doHighlight
        }

        if (this.props.itemType !== 'STATUS') {
            classes['hilite-good'] = this.props.isGood
            classes['hilite-serious'] = !this.props.isGood
        } else {
            let status = this.props.itemValue;
            let isBeyondRepair = componentStatusBeyondRepair(status);
            classes['status-non-operational'] = (!componentStatusOK(status) && !isBeyondRepair)
            classes['status-beyond-repair'] = isBeyondRepair
        }

        let cls = classNames(classes)

        let label = this.props.isGood ? "" : this.renderError();

        return (
            <td className={cls}
                data-has-problems={!this.props.isGood}
                data-data-idx={this.props.dataIdx}      // index of data.errors in JSON from server
                data-item-type={this.props.itemType}    // ERROR_TYPE, ELEMENT or STATUS
                data-item-value={this.props.itemValue}  // The error_type, element id or status
            >
                {label}
            </td>);
    }
}


export default GenericStatusTd;
