import React, {
    Component
} from 'react';
import {connect} from "react-redux";
import {formatDateUTC} from "utils/utils";

import {componentStatusOK} from 'utils/LOFARDefinitions';
import classNames from "classnames";
import GenericStatusTd from '../GenericStatusTd'

// CSS
import styles from './styles.module.scss'


class AntennaTestRowC extends Component {

    doHighlight = false;

    getStatus = () => {
        let data = this.props.data;
        if (data.status && data.status[this.props.antennaId]) {
            return data.status[this.props.antennaId].status
        }
        return null;
    }

    shouldHighlight(highlightData) {
        const props = this.props;

        return highlightData !== null &&
               highlightData.antenna_id === props.antennaId &&
               highlightData.dataIdx === props.data.dataIdx;
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        // If highlight data changes, a new item was selected. Only update the old and new
        // highlighted row. this.doHighlight will only be true for the old row,
        // this.shouldHighlight() will be true for the new row.
        if (nextProps.highlightData !== this.props.highlightData) {
            if (!this.doHighlight && !this.shouldHighlight(nextProps.highlightData)) {
                return false
            }
        }
        return true;
    }

    renderGenericTestRow(data, key) {
        let line = [];
        let element = {};
        let component_errors;

        component_errors = data.component_errors;

        // Error types
        for (let i = 0; i < this.props.errorTypes.length; i++) {
            const error_type = this.props.errorTypes[i];

            let hasError = component_errors.hasOwnProperty(error_type)
            let hasX = false
            let hasY = false

            if (hasError) {
                const errors = component_errors[error_type];
                hasX = errors.hasOwnProperty('X')
                hasY = errors.hasOwnProperty('Y')

                if (errors.hasOwnProperty('element_errors')) {
                    for (let element_id of Object.keys(errors.element_errors)) {
                        element[element_id] = true
                    }
                }
            }

            line.push(<GenericStatusTd  key={i}
                                        dataIdx={data.dataIdx}
                                        itemType="ERROR_TYPE"
                                        itemValue={error_type}
                                        testType={data.test_type}
                                        hasX={hasX}
                                        hasY={hasY}
                                        doHighlight={this.doHighlight && error_type === this.props.highlightData.error_type}
                                        isGood={!hasError}  />)
        }


        // Elements
        if (this.props.data.test_type === 'S') {
            line.push(<td key="space"></td>);

            for (let i = 1; i <= 16; i++) {
                line.push(<GenericStatusTd  key={`element${i}`}
                                            dataIdx={data.dataIdx}
                                            itemType="ELEMENT"
                                            itemValue={i}
                                            isGood={!element.hasOwnProperty(i)}
                                            // eslint-disable-next-line eqeqeq
                                            doHighlight={this.doHighlight && i == this.props.highlightData.element_id}
                                            testType={data.test_type} />);
            }
        }

        return line;
    }


    render() {
        const data = this.props.data;
        let status = this.getStatus()

        // Determine if this row needs to be highlighted
        this.doHighlight = this.shouldHighlight(this.props.highlightData);

        let columns = this.renderGenericTestRow(data, "");

        let cls = classNames({
            [styles.highlight]: this.doHighlight
        });

        return (
            <tr className={cls}>
                <th>{data.test_type}</th>
                <th>{formatDateUTC(data.start_date)}</th>
                <GenericStatusTd dataIdx={data.dataIdx}
                                itemType="STATUS"
                                itemValue={status}
                                isGood={componentStatusOK(status)}
                                doHighlight={this.doHighlight && this.props.highlightData.itemType === 'STATUS'}
                                testType="ST" />
                {columns}
            </tr>
        );

    }
}

// TestLine is connected to Redux store
const AntennaTestRow = connect(state => {
    return {
        highlightData: state.antenna_page.main_panel.highlightData
    };
}, null)(AntennaTestRowC);

export default AntennaTestRow;
