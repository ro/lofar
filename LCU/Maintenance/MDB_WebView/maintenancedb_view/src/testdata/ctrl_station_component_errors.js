// /view/ctrl_station_component_errors
// parameters:
//   station_name: str #required
//   from_date: date #required
//   to_date: date #required
//   test_type: [R,S,B] #optional default B
// response:
// {
//   COMPONENT_TYPE: [
//    {test_type: R,S,
//     start_date: end_date
//     end_date: start_date
//     component_errors: {
//       rcu_id: [{ error_type: whatever,
//                     details:{...}}]
//     }
//    }
//     ,...
//   ]
// }

export const data = {
    "HBA": [
        {
            "test_type": 'R',
            "start_datetime": "2018-10-28T17:30:00Z",
            "end_datetime": "2018-10-28T20:27:17Z",
            "component_errors": {
        //       rcu_id: [{ error_type: whatever,
        //                     details:{...}}]
        //     }
            }
        }
    ]
};
