#!/usr/bin/env python

import logging
from lofar.maintenance.utils.cli.mdb_loader import main
import argparse
import sys
logger = logging.getLogger('mdb_loader')

"""
This program is meant to load the station tests and RTSM present in a certain directory to the database
"""


if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=logging.DEBUG)
    main()
