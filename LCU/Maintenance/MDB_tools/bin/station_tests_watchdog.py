#!/usr/bin/env python

import logging

from lofar.maintenance.utils.cli.station_tests_watchdog import main

logger = logging.getLogger('station test watchdog')

"""
This program is meant to load the station tests and RTSM present in a certain directory to the database if a new
test output file is created
"""

if __name__ == '__main__':
    main()