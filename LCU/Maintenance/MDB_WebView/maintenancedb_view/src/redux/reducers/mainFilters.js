import moment from 'moment';
import * as a from "../actions/mainFiltersActions";
import { LOCATION_CHANGE } from "connected-react-router";
import queryString from 'query-string';

const initialState = {
    selectedStationGroup: 'A',  // A, C, R, I
    selectedStation: '',   // note: not necessarily in sync with selectedStationGroup, used at different pages
    testType: 'B',         // B, S, R
    errorsOnly: false,
    period: 7,             // days
    startDate: moment().subtract(7, 'days'),
    endDate: moment(),
    selectedErrorTypes: [],
    antennaId: 0,
    antennaType: 'HBA',
};


export default function(state = initialState, action) {

    switch (action.type) {
        case LOCATION_CHANGE: {
            // Location changed, check is a station parameter is present
            let newState = {
                ...state
            };
            const values = queryString.parse(action.payload.location.search);
            if ('station' in values) {
                newState.selectedStation = values.station;
            }
            if ('antenna_id' in values){
                newState.antennaId = values.antenna_id;
            }
            if('antenna_type' in values){
                newState.antennaType = values.antenna_type;
            }
            return newState;
        }
        case a.SET_ANTENNA_ID: {
            let newState = {...state};
            newState.antennaId = action.payload.antennaId;
            return newState;
        }
        case a.SET_ANTENNA_TYPE: {
            let newState = {...state};
            newState.antennaType = action.payload.antennaType;
            return newState;
        }
        case a.SET_STATION_GROUP: {
            const { stationGroup }  = action.payload;
            return {
                ...state,
                selectedStationGroup: stationGroup
            };
        }
        case a.SET_STATION: {
            const { station }  = action.payload;
            return {
                ...state,
                selectedStation: station
            };
        }
        case a.SET_ERRORS_ONLY: {
            const { errorsOnly }  = action.payload;
            return {
                ...state,
                errorsOnly: errorsOnly
            };
        }
        case a.SET_PERIOD: {
            const { period } = action.payload;
            return {
                ...state,
                period: period
            };
        }
        case a.SET_DATE_RANGE: {
            const { startDate, endDate } = action.payload;
            return {
                ...state,
                startDate,
                endDate
            };
        }
        case a.SET_TEST_TYPE: {
            const { type } = action.payload;
            return {
                ...state,
                testType: type
            };
        }
        case a.SET_ERROR_TYPES: {
            const { type_list }  = action.payload;

            return {
                ...state,
                selectedErrorTypes: type_list
            }
        }
        default:
            return state;
    }
}
