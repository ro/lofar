#include "UnitTest++/UnitTest++.h"

#include "../src/DatapointAttribute.hpp"
#include "../src/DatapointIndex.hpp"
#include "../src/DatapointRule.hpp"


int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}
