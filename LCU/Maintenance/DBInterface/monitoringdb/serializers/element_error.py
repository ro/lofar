from rest_framework.serializers import ModelSerializer

from .component_error import ComponentErrorSerializer
from .element import ElementSerializer
from ..models.element_error import ElementError
from .log import ActionLogSerializer
import logging


logger = logging.getLogger(__name__)


class ElementErrorSerializer(ModelSerializer):
    class Meta:
        model = ElementError
        fields = '__all__'
        depth = 1

    @staticmethod
    def insert_element_error(station_test_entry,
                             station_entry,
                             element_error):
        component_error = element_error.pop('component_error')

        component_error_entry = ComponentErrorSerializer.insert_component_error(station_test_entry,
                                                         station_entry,
                                                         component_error)
        component = component_error_entry.component

        element = element_error.pop('element')
        element_error_details = element_error.pop('details')

        element_entry = ElementSerializer.insert_element(component, element)

        element_error_entry = ElementError.objects.filter(element=element_entry,
                                                          component_error=component_error_entry,
                                                          **element_error).first()
        if element_error_entry is None:
            element_error_entry = ElementError(element=element_entry,
                                               details=element_error_details,
                                               component_error=component_error_entry,
                                               **element_error)

            element_error_entry.save()
            # Log the action
            ActionLogSerializer.log_model_insert(element_error_entry)

        return element_error_entry

    @staticmethod
    def insert_element_errors(station_test_entry,
                              station_entry,
                              element_errors):
        results = []
        for element_error in element_errors:
            try:
                results.append(ElementErrorSerializer.insert_element_error(station_test_entry,
                                                                           station_entry,
                                                                           element_error))
            except Exception as e:
                logger.exception(
                    'Cannot insert element error %s'
                    ' for station_test_entry %s and station_entry %s: %s',
                    element_error, station_test_entry, station_entry, e)
                raise e
