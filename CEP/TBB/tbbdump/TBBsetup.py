# DO NOT CHANGE FOR THIS WEEKS OBSERVATIONS
stationname="CS031C"    # string, LOFAR name of station
station="CS031C" # string, LCU IP address or alias accessible by ssh
#stationname="CS002C"    # string, LOFAR name of station
#station="CS002C" # string, LCU IP address or alias accessible by ssh
storageip="lse005" # string, Storage IP adress or "this" if you run this program from the storage
#stationname="CS401C"    # string, LOFAR name of station
#station="CS401C" # string, LCU IP address or alias accessible by ssh
#storageip="locus043" # string, Storage IP adress or "this" if you run this program from the storage
rcus=list(range(0,96))    # list, RCUs to get data from (range(0,96) for dutch stations)
#rcus=range(0,192)     # list, RCUs to get data from (range(0,96) for dutch stations)
timeouttime=20        # int, Time before
datawriter="/home/veen/lus/src/code/release/bin/TBBraw2h5"    # string, Location of TBBraw2h5 program
datadir="/data/TBB/test/"
filename="LORA"     # Filename of datafile. Use date to get the current day in the filename.
logdir="/opt/lofar/tbb/logs/"
