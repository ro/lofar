export const SET_CHILD_PANEL_DATA = "SET_CHILD_PANEL_DATA";
export const UNPIN_CHILD_PANEL_DATA = "UNPIN_CHILD_PANEL_DATA";

export const setChildPanelData = (data, doPin = false) => ({
  type: SET_CHILD_PANEL_DATA,
  payload: { data, doPin }
});

export const unpinChildPanelData = () => ({
  type: UNPIN_CHILD_PANEL_DATA,
  payload: null
});
