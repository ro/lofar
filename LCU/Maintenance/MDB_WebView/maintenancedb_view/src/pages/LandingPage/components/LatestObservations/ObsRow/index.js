import React, { Component } from 'react';
import { Table, Tooltip } from 'reactstrap';
import { unique_id } from 'utils/utils.js'
import * as moment from 'moment';
import { datetime_format } from 'utils/constants'
import { highlightClass, unHighlightClass } from 'utils/highlightClass'
import ObservationInspectTag from 'components/ObservationInspectTag'

// CSS
import styles from './styles.module.scss'


/**
 * ObsRow:
 * Class to render the (RTSM) row for an observation.
 */
class ObsRow extends Component {

    constructor(props){
        super(props);
        this.state = {popoverOpen: false};
        this.id = unique_id();
        this.togglePopover = this.togglePopover.bind(this);
    }


    renderObservationID() {
        return this.props.data.observation_id;
    }

    getStationInvolvedList(){
        var result = [];
        if(this.props.data !== undefined){
            const station_involved = this.props.data.station_involved;
            for (var key in station_involved){
                if(station_involved.hasOwnProperty(key)){
                    result.push(station_involved[key]);
                }
            }
        }
        return result;
    }

    renderStationsWithProblems(station_involved_list){
        return station_involved_list.filter(rtsm_observation => rtsm_observation.n_errors > 0).length;
    }

    togglePopover(){
        this.setState({popoverOpen: !this.state.popoverOpen});
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.popoverOpen !== prevState.popoverOpen) {
            if (this.state.popoverOpen) {
                highlightClass('obs-'+this.props.data.observation_id);
            } else {
                unHighlightClass('obs-'+this.props.data.observation_id);
            }
        }
    }

    render() {
        const data = this.props.data;
        const station_involved_list = this.getStationInvolvedList();

        const {total_component_errors, start_datetime, end_datetime, mode} = data;
        const stations_and_errors = station_involved_list.map((station) =>
            <tr key={station.station_name}>
                <th scope="row">{station.station_name}</th>
                <td>{station.n_errors}</td>
            </tr>);

        return (
            <tr id={this.id}  className={styles.hoverable}>

                <td><ObservationInspectTag observationId={this.props.data.observation_id} /></td>
                <td>{ moment.utc(start_datetime).format(datetime_format) }</td>
                <td>{ station_involved_list.length }</td>
                <td>{ this.renderStationsWithProblems(station_involved_list) }</td>
                <td>{ total_component_errors }</td>

                <Tooltip placement="auto"
                        isOpen={this.state.popoverOpen}
                        target={this.id }
                        toggle={this.togglePopover}
                        style={{backgroundColor: "white", color:"black", opacity: "1"}}
                        autohide={false}>
                  <div className='popover-header'>
                      {data.observation_id}
                  </div>
                  <div>
                    <strong>Start:</strong> { moment.utc(start_datetime).format(datetime_format) }<br/>
                    <strong>End:</strong> { moment.utc(end_datetime).format(datetime_format) }<br/>
                    <strong>Mode:</strong> { mode.join(',') }<br/>
                    <Table size="sm" className={styles.table_wrapper}>
                        <thead><tr><th>Station name</th><th>errors</th></tr></thead>
                        <tbody>{stations_and_errors}</tbody>
                    </Table>
                  </div>

              </Tooltip>
            </tr>
        );
    }
}

export default ObsRow;
