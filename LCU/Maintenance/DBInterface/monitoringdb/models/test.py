import logging

from django.db import models

from .station import Station
from .wincc import StationStatus

logger = logging.getLogger(__name__)


class GenericTest(models.Model):
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()

    station = models.ForeignKey(Station, on_delete=models.DO_NOTHING, related_name='tests')

    station_status = models.OneToOneField(StationStatus,
                                          on_delete=models.SET_NULL,
                                          null=True)
