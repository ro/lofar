import unittest

from mock import patch, MagicMock, Mock, call

from lofar.maintenance.utils.cli.mdb_loader import *

logger = logging.getLogger(__name__)


def load_test_file(file_name):
    with open(file_name, 'r') as f_read_stream:
        content = f_read_stream.read().split('\n')
    return content


def load_rtsm_test():
    file_name = 't_mdb_loader.in.test_rtsm.data'
    return load_test_file(file_name)


def load_station_test():
    file_name = 't_mdb_loader.in.test_stationtest.data'
    return load_test_file(file_name)


class TESTMDBLoader(unittest.TestCase):

    @patch('lofar.maintenance.utils.cli.mdb_loader.path')
    @patch('lofar.maintenance.utils.cli.mdb_loader.glob')
    def test_obtain_file_path_list(self, glob_mock, path_mock):
        path = 'test'
        glob_return_values = ['test', 'test1', 'test2']
        glob_mock.return_value = glob_return_values
        obtain_file_list(path)
        glob_mock.assert_called_with(path)
        self.assertEqual(path_mock.isfile.call_count, 3)
        path_mock.isfile.assert_called_with('test2')

    @patch('lofar.maintenance.utils.cli.mdb_loader.logger')
    @patch('lofar.maintenance.utils.cli.mdb_loader.obtain_file_list')
    @patch('lofar.maintenance.utils.cli.mdb_loader.read_stationtest_rtsm_output_to_dict')
    @patch('lofar.maintenance.utils.cli.mdb_loader.send_stationtest_rtsm_content_to_address')
    def test_obtain_stationtest_file_list_and_process_all_working(self, send_content_to_address_mock,
                                                                  read_tests_mock, obtain_file_list_mock, logger_mock):
        path_list = ['path1', 'path2', 'path3']
        obtain_file_list_mock.return_value = path_list
        address = 'test_address'
        file_pattern = 'data/*history*.csv'
        mock_file_content = {'content': [{'item': 'key'}]}
        read_tests_mock.return_value = mock_file_content

        send_content_to_address_mock.return_value = True

        obtain_stationtest_file_list_and_process(file_pattern, address)
        obtain_file_list_mock.assert_any_call(file_pattern)

        for file_path in path_list:
            read_tests_mock.assert_any_call(file_path)
            send_content_to_address_mock.assert_any_call(address, mock_file_content)
            logger_mock.info.assert_any_call('file %s processed', file_path)

    @patch('lofar.maintenance.utils.cli.mdb_loader.logger')
    @patch('lofar.maintenance.utils.cli.mdb_loader.obtain_file_list')
    @patch('lofar.maintenance.utils.cli.mdb_loader.read_stationtest_rtsm_output_to_dict')
    @patch('lofar.maintenance.utils.cli.mdb_loader.send_stationtest_rtsm_content_to_address')
    def test_obtain_stationtest_file_list_and_process_found_irregular_test_file(self, send_content_to_address_mock,
                                                                                read_tests_mock, obtain_file_list_mock,
                                                                                logger_mock):
        path_list = ['path1', 'path2', 'path3']
        obtain_file_list_mock.return_value = path_list
        address = 'test_address'
        file_pattern = 'data/*history*.csv'
        mock_file_content0 = {'content': [{'item0': 'key0'}]}
        mock_file_content1 = {'content': [{'item1': 'key1'}]}

        read_side_effect = [mock_file_content0, None, mock_file_content1]
        read_tests_mock.side_effect = read_side_effect

        send_content_to_address_mock.return_value = True

        obtain_stationtest_file_list_and_process(file_pattern, address)
        obtain_file_list_mock.assert_any_call(file_pattern)

        for file_path, read_return_value in zip(path_list, read_side_effect):
            read_tests_mock.assert_any_call(file_path)

        send_content_to_address_mock.assert_has_calls([call(address, mock_file_content0),
                                                       call(address, mock_file_content1)])
        logger_mock.info.assert_has_calls([call('file %s processed', path_list[0]),
                                           call('file %s processed', path_list[2])])

        logger_mock.error.assert_has_calls([call('error processing file %s', path_list[1])])

    @patch('lofar.maintenance.utils.cli.mdb_loader.logger')
    @patch('lofar.maintenance.utils.cli.mdb_loader.obtain_file_list')
    @patch('lofar.maintenance.utils.cli.mdb_loader.read_stationtest_rtsm_output_to_dict')
    @patch('lofar.maintenance.utils.cli.mdb_loader.send_stationtest_rtsm_content_to_address')
    def test_obtain_stationtest_file_list_and_process_sending_content_failure(self, send_content_to_address_mock,
                                                                                read_tests_mock, obtain_file_list_mock,
                                                                                logger_mock):
        path_list = ['path1', 'path2', 'path3']
        obtain_file_list_mock.return_value = path_list
        address = 'test_address'
        file_pattern = 'data/*history*.csv'
        mock_file_content0 = {'content': [{'item0': 'key0'}]}

        read_side_effect = [mock_file_content0]
        read_tests_mock.side_effect = read_side_effect

        send_content_to_address_mock.return_value = False
        with self.assertRaises(SystemExit):
            obtain_stationtest_file_list_and_process(file_pattern, address)
        obtain_file_list_mock.assert_any_call(file_pattern)

        read_tests_mock.assert_called_once_with(path_list[0])

        send_content_to_address_mock.assert_called_once_with(address, mock_file_content0)


if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=logging.DEBUG)
    unittest.main()
