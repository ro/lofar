#!/usr/bin/env python3
#
# Copyright (C) 2016
# ASTRON (Netherlands Institute for Radio Astronomy)
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
#
# $Id$

from lofar.messaging import DEFAULT_BUSNAME, DEFAULT_BROKER, RPCException
from lofar.messaging import ServiceMessageHandler
from lofar.parameterset import parameterset, PyParameterValue
from lofar.common import isProductionEnvironment
from lofar.common.subprocess_utils import communicate_returning_strings
from lofar_tmss_client.tmss_http_rest_client import TMSSsession
from lofar_tmss_client.tmss_bus_listener import TMSSEventMessageHandler, TMSSBusListener

import subprocess
import pipes
import os
import re
from socket import getfqdn
import logging

logger = logging.getLogger(__name__)

# NDPPP seems to like to have 2 cores.
DEFAULT_NUMBER_OF_CORES_PER_TASK = 2
# This needs to match what's in SLURM
NUMBER_OF_NODES = 40
NUMBER_OF_CORES_PER_NODE = 24
# We /4 because we can then run 4 pipelines, and -2 to reserve cores for TBBwriter
DEFAULT_NUMBER_OF_TASKS = (NUMBER_OF_NODES // 4) * (NUMBER_OF_CORES_PER_NODE - 2) // DEFAULT_NUMBER_OF_CORES_PER_TASK

def runCommand(cmdline, input=None):
    logger.info("runCommand starting: %s", cmdline)

    # Start command
    proc = subprocess.Popen(
        cmdline,
        stdin=subprocess.PIPE if input else None,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
        universal_newlines=True
    )

    # Feed input and wait for termination
    logger.debug("runCommand input: %s", input)
    stdout, stderr = communicate_returning_strings(proc, input)
    logger.debug("runCommand output: %s", stdout)
    if stderr:
        logger.warning("runCommand stderr output: %s", stderr)

    # Check exit status, bail on error
    if proc.returncode != 0:
        logger.warning("runCommand(%s) had exit status %s with output: %s", cmdline, proc.returncode, stdout)
        raise subprocess.CalledProcessError(proc.returncode, cmdline)

    # Return output
    return stdout.strip()


""" Prefix that is common to all parset keys, depending on the exact source. """
PARSET_PREFIX="ObsSW."


class Parset(dict):
    def predecessors(self):
        """ Extract the list of predecessor obs IDs from the given parset. """

        key = PARSET_PREFIX + "Observation.Scheduler.predecessors"
        strlist = PyParameterValue(str(self[key]), True).getStringVector()

        # Key contains "Lxxxxx" values, we want to have "xxxxx" only
        result = [int(list(filter(str.isdigit,x))) for x in strlist]

        return result

    def isObservation(self):
        return self[PARSET_PREFIX + "Observation.processType"] == "Observation"

    def isPipeline(self):
        return not self.isObservation()

    def processingCluster(self):
        return self[PARSET_PREFIX + "Observation.Cluster.ProcessingCluster.clusterName"] or "CEP2"

    def processingPartition(self):
        result = self[PARSET_PREFIX + "Observation.Cluster.ProcessingCluster.clusterPartition"] or "cpu"
        if '/' in result:
            logger.error('clusterPartition contains invalid value: %s. Defaulting clusterPartition to \'cpu\'', result)
            return 'cpu'
        return result

    def processingNumberOfCoresPerTask(self):
        try:
            result = int(self[PARSET_PREFIX + "Observation.Cluster.ProcessingCluster.numberOfCoresPerTask"]) or None
            if not result:
                logger.warning('Invalid Observation.Cluster.ProcessingCluster.numberOfCoresPerTask: %s, defaulting to %i',
                            result, DEFAULT_NUMBER_OF_CORES_PER_TASK)
                result = DEFAULT_NUMBER_OF_CORES_PER_TASK
            return result
        except:
            return DEFAULT_NUMBER_OF_CORES_PER_TASK

    def processingNumberOfTasks(self):
        """ Parse the number of nodes to allocate from
        "Observation.Cluster.ProcessingCluster.numberOfTasks" """

        try:
            result = int(self[PARSET_PREFIX + "Observation.Cluster.ProcessingCluster.numberOfTasks"].strip()) or None

            # apply bound
            if not result or result <= 0 or result > NUMBER_OF_NODES * NUMBER_OF_CORES_PER_NODE:
                logger.warning('Invalid Observation.Cluster.ProcessingCluster.numberOfTasks: %s, defaulting to %s',
                                result, DEFAULT_NUMBER_OF_TASKS)
                result = DEFAULT_NUMBER_OF_TASKS
            return result
        except:
            return DEFAULT_NUMBER_OF_TASKS

    @staticmethod
    def dockerRepository():
        return "nexus.cep4.control.lofar:18080"

    @staticmethod
    def defaultDockerImage():
        return "lofar-pipeline"

    @staticmethod
    def defaultDockerTag():
        if isProductionEnvironment():
            # "latest" refers to the current /production/ image
            return "latest"
        else:
            # test/dev environments want to use their specific version, since they
            # share images with the production environment
            return runCommand("docker-template", "${LOFAR_TAG}")

    def dockerImage(self):
        # Return the version set in the parset, and fall back to our own version.
        image = self[PARSET_PREFIX + "Observation.ObservationControl.PythonControl.softwareVersion"]

        if not image:
            image = self.defaultDockerImage()

        if ":" in image:
            return image

        # Insert our tag by default
        return "%s:%s" % (image, self.defaultDockerTag())

    def otdbId(self):
        return int(self[PARSET_PREFIX + "Observation.otdbID"])

    def description(self):
        return "%s - %s" % (self.get(PARSET_PREFIX + "Observation.Campaign.name", 'unknown'),
                            self.get(PARSET_PREFIX + "Observation.Scheduler.taskName", 'unknown'))
    

class Slurm(object):
    def __init__(self, headnode="head.cep4.control.lofar"):
        self.headnode = headnode

    def _runCommand(self, cmdline, input=None):
        cmdline = "ssh %s %s" % (self.headnode, cmdline)
        return runCommand(cmdline, input)

    def submit(self, jobName, cmdline, sbatch_params=None):
        if sbatch_params is None:
            sbatch_params = []

        script = """#!/bin/bash -v
    {cmdline}
    """.format(cmdline = cmdline)

        stdout = self._runCommand("sbatch --job-name=%s %s" % (jobName, " ".join(sbatch_params)), script)

        # Returns "Submitted batch job 3" -- extract ID
        match = re.search("Submitted batch job (\d+)", stdout)
        if not match:
            return None

        return match.group(1)

    def cancel(self, jobName):
        self._runCommand("scancel --jobname %s" % (jobName,))

    def isQueuedOrRunning(self, jobName):
        # ask SLURM for all the states of this job and its subjobs.
        stdout = self._runCommand("sacct --starttime=2022-01-01 --noheader --parsable2 --format=state --name=%s" % (jobName,))

        # the states in which we consider a (sub)job running
        running_states = [
            "PENDING",
            "RUNNING",
            "REQUEUED",
            "RESIZING",
            "SUSPENDED",
        ]

        # we filter the output by hand, because using sacct's state filtering does not work! f.e., using
        # --state=RUNNING will not return the running (sub)jobs. For example:
        #
        # $ sacct --starttime=2020-01-01 -j 477234 -X --state=RUNNING
        #        JobID    JobName  Partition    Account  AllocCPUS      State ExitCode
        # ------------ ---------- ---------- ---------- ---------- ---------- --------
        # $ sacct --starttime=2020-01-01 -j 477234 -X
        #        JobID    JobName  Partition    Account  AllocCPUS      State ExitCode
        # ------------ ---------- ---------- ---------- ---------- ---------- --------
        # 477234       inspectio+        cpu     system         98    RUNNING      0:0`

        for running_state in running_states:
            if running_state in stdout:
                return True

        return False


class PipelineControlTMSSHandler(TMSSEventMessageHandler):

    def __init__(self, tmss_client_credentials_id: str=None):
        super().__init__()

        self.slurm = Slurm()
        self.tmss_client = TMSSsession.create_from_dbcreds_for_ldap(tmss_client_credentials_id)

    def start_handling(self):
        self.tmss_client.open()

    def stop_handling(self):
        self.tmss_client.close()

    def check_scheduled_pipelines(self):
        """
        In the old OTDB, PipelineControl determines itself if a pipeline can be started (scheduled and predecessors
        have finished). The predecessor check now happens within TMSS, so PipelineControl can simply start any pipeline
        that is scheduled.
        """
        try:
            logger.info("Checking for already scheduled pipelines in TMSS...")

            scheduled_subtasks = self.tmss_client.get_subtasks(state="scheduled", subtask_type="pipeline")
            scheduled_pipeline_subtask_ids = []
            for subtask in scheduled_subtasks:
                try:
                    subtask_id = subtask['id']
                    scheduled_pipeline_subtask_ids.append(subtask_id)
                except Exception as e:
                    logger.error(e)

            logger.info("Checking %s scheduled pipelines if they can start.", len(scheduled_pipeline_subtask_ids))

            for subtask_id in scheduled_pipeline_subtask_ids:
                logger.info("Checking if scheduled pipeline subtask_id=%s can start.", subtask_id)
                try:
                    subtask = self.tmss_client.get_subtask(subtask_id)
                    subtask_template = self.tmss_client.get_url_as_json_object(subtask['specifications_template'])
                    if 'pipeline' not in subtask_template['type_value']:
                        logger.info("skipping scheduled subtask id=%s of non-pipeline type '%s'", subtask_id, subtask_template['type_value'])
                        continue

                    logger.debug("getting parset for scheduled subtask id=%s of type '%s'", subtask_id, subtask_template['type_value'])
                    parset = self.tmss_client.get_subtask_parset(subtask_id)
                    logger.info("retreived parset for scheduled subtask id=%s of type '%s'\n%s", subtask_id, subtask_template['type_value'], parset)
                    parset = parameterset.fromString(parset)
                    parset = Parset(parset.dict())
                    if not parset or not self._shouldHandle(parset):
                        continue
                    self._startPipeline(subtask_id, parset)
                except Exception as e:
                    logger.error(e)
        except Exception as e:
            logger.error(e)

    def onSubTaskStatusChanged(self, id: int, status: str):
        '''Handle TMSS subtask status changes'''
        logger.info("subtask id=%s status changed to %s", id, status)

        if status == "scheduled":
            try:
                subtask = self.tmss_client.get_subtask(id)
                subtask_template = self.tmss_client.get_url_as_json_object(subtask['specifications_template'])
                if 'pipeline' not in subtask_template['type_value']:
                    logger.info("skipping scheduled subtask id=%s of non-pipeline type '%s'", id, subtask_template['type_value'])
                    return

                logger.debug("getting parset for scheduled subtask id=%s of type '%s'", id, subtask_template['type_value'])
                parset = self.tmss_client.get_subtask_parset(id)
                logger.info("retreived parset for scheduled subtask id=%s of type '%s'\n%s", id, subtask_template['type_value'], parset)
                parset = parameterset.fromString(parset)
                parset = Parset(parset.dict())
                if parset and self._shouldHandle(parset):
                    self._startPipeline(id, parset)
            except Exception as e:
                logger.error(e)


    @staticmethod
    def _shouldHandle(parset):
        try:
            if not parset.isPipeline():
                logger.info("Not processing tree: is not a pipeline")
                return False

            if parset.processingCluster() == "CEP2":
                logger.info("Not processing tree: is a CEP2 pipeline")
                return False
        except KeyError as e:
            # Parset not complete
            logger.error("Parset incomplete, ignoring: %s", e)
            return False

        return True

    @staticmethod
    def _jobName(subtask_id):
        return str(subtask_id)

    def _startPipeline(self, subtask_id, parset):
        """
          Schedule "docker-runPipeline.sh", which will fetch the parset and run the pipeline within
          a SLURM job.
        """

        # Avoid race conditions by checking whether we haven't already sent the job
        # to SLURM. Our QUEUED status update may still be being processed.
        # if self.slurm.isQueuedOrRunning(subtask_id):
        #     logger.info("Pipeline %s is already queued or running in SLURM.", subtask_id)
        #     return

        self.tmss_client.set_subtask_status(subtask_id, "queueing", retry_count=5)

        logger.info("***** START Subtask ID %s *****", subtask_id)

        # Determine SLURM parameters
        sbatch_params = [
            # Only run job if all nodes are ready
            "--wait-all-nodes=1",

            # Enforce the dependencies, instead of creating lingering jobs
            "--kill-on-invalid-dep=yes",

            # Annotate the job
            "--comment=%s" % pipes.quote(pipes.quote(parset.description())),

            # Lower priority to drop below inspection plots
            "--nice=1000",

            "--partition=%s" % parset.processingPartition(),
            "--ntasks=%s" % parset.processingNumberOfTasks(),
            "--cpus-per-task=%s" % parset.processingNumberOfCoresPerTask(),

            # Define better places to write the output
            os.path.expandvars("--output=/data/log/pipeline-%s-%%j.log" % (subtask_id,)),
        ]

        def setStatus_cmdline(status, error_reason=None):
            if error_reason is not None:
                reason = f"-e '{error_reason}'"
            else:
                reason = ""
            return (
                "ssh {myhostname} '"
                "source {lofarroot}/lofarinit.sh && "
                "tmss_set_subtask_state {reason} -r 5 {subtaskid} {status}"
                "'"
                    .format(
                    myhostname=getfqdn(),
                    lofarroot=os.environ.get("LOFARROOT", ""),
                    subtaskid=subtask_id,
                    status=status,
                    reason=reason
                ))

        def waitForStatus_cmdline(status):
            return (
                "ssh {myhostname} '"
                "source {lofarroot}/lofarinit.sh && "
                "tmss_wait_for_subtask_state -t 60 {subtaskid} {status}"
                "'"
                    .format(
                    myhostname=getfqdn(),
                    lofarroot=os.environ.get("LOFARROOT", ""),
                    subtaskid=subtask_id,
                    status=status
                ))

        def getParset_cmdline():
            return (
                "ssh {myhostname} '"
                "source {lofarroot}/lofarinit.sh && "
                "tmss_get_subtask_parset {subtaskid}'"
                    .format(
                    myhostname=getfqdn(),
                    lofarroot=os.environ.get("LOFARROOT", ""),
                    subtaskid=subtask_id,
                ))

        try:
            logger.info("Handing over pipeline %s to SLURM", subtask_id)

            # Schedule runPipeline.sh
            slurm_job_id = self.slurm.submit(self._jobName(subtask_id),
                                             """
                                     # Run a command, but propagate SIGINT and SIGTERM
                                     function runcmd {{
                                     trap 'kill -s SIGTERM $PID' SIGTERM
                                     trap 'kill -s SIGINT  $PID' SIGINT
                             
                                     "$@" &
                                     PID=$!
                                     wait $PID # returns the exit status of "wait" if interrupted
                                     wait $PID # returns the exit status of $PID
                                     CMDRESULT=$?
                             
                                     trap - SIGTERM SIGINT
                             
                                     return $CMDRESULT
                                     }}
                             
                                     # print some info
                                     echo Running on $SLURM_NODELIST
                             
                                     # wait for status queued (set by pipeline control directly after job submision) before advancing
                                     runcmd {waitForStatus_queued}
                             
                                     # notify TMSS that we're starting
                                     runcmd {setStatus_starting}
                             
                                     # notify ganglia
                                     wget -O - -q "http://ganglia.control.lofar/ganglia/api/events.php?action=add&start_time=now&summary=Pipeline {obsid} ACTIVE&host_regex="
                             
                                     # fetch parset
                                     runcmd {getParset} > {parset_file}
                             
                                     # notify TMSS that we're (almost) running (should be called from within the pipeline...)
                                     runcmd {setStatus_started}
                                     
                                     # run the pipeline
                                     runcmd docker-run-slurm.sh --rm --net=host \
                                         -e LOFARENV={lofarenv} \
                                         -v $HOME/.ssh:$HOME/.ssh:ro \
                                         -e SLURM_JOB_ID=$SLURM_JOB_ID \
                                         -v /data:/data \
                                         {image} \
                                     runPipeline.sh -o {obsid} -c /opt/lofar/share/pipeline/pipeline.cfg.{cluster} -P {parset_dir} -p {parset_file}
                                     RESULT=$?
                             
                            
                                     if [ $RESULT -eq 0 ]; then
                                         # if we reached this point, the pipeline ran succesfully, and TMSS will set it to finished once it processed the feedback
                                         # notify ganglia
                                         # !!! TODO Is TMSS supposed to inform Ganglia in future? 
                                         wget -O - -q "http://ganglia.control.lofar/ganglia/api/events.php?action=add&start_time=now&summary=Pipeline {obsid} FINISHED&host_regex="
                                     else
                                         # notify TMSS that some error occured
                                         runcmd {setStatus_error}
                                     fi
                             
                                     # report status back to SLURM
                                     echo "Pipeline exited with status $RESULT"
                                     exit $RESULT
                                         """.format(
                                                 lofarenv=os.environ.get("LOFARENV", ""),
                                                 obsid=subtask_id,
                                                 parset_dir="/data/parsets",
                                                 parset_file="/data/parsets/Observation%s.parset" % (subtask_id,),
                                                 repository=parset.dockerRepository(),
                                                 image=parset.dockerImage(),
                                                 cluster=parset.processingCluster(),

                                                 getParset=getParset_cmdline(),
                                                 setStatus_starting=setStatus_cmdline("starting"),
                                                 setStatus_started=setStatus_cmdline("started"),
                                                 setStatus_finished=setStatus_cmdline("finished"),
                                                 setStatus_error=setStatus_cmdline("error"),
                                                 waitForStatus_queued=waitForStatus_cmdline("queued")
                                             ),

                                             sbatch_params=sbatch_params
                                             )
            logger.info("Scheduled SLURM job %s for subtask_id=%s", slurm_job_id, subtask_id)

            # Schedule pipelineAborted.sh
            logger.info("Scheduling SLURM job for pipelineAborted.sh")
            slurm_cancel_job_id = self.slurm.submit("%s-abort-trigger" % self._jobName(subtask_id),
                                                    """
                                                # notify TMSS
                                                {setStatus_error}
                                                
                                                # notify ganglia
                                                wget -O - -q "http://ganglia.control.lofar/ganglia/api/events.php?action=add&start_time=now&summary=Pipeline {obsid} ABORTED&host_regex="
                                                    """
                                                    .format(
                                                        obsid=subtask_id,
                                                        setStatus_error=setStatus_cmdline('error', 'Pipeline aborted via SLURM job')
                                                    ),

                                                    sbatch_params=[
                                                        "--partition=%s" % parset.processingPartition(),
                                                        "--cpus-per-task=1",
                                                        "--ntasks=1",
                                                        "--dependency=afternotok:%s" % slurm_job_id,
                                                        "--kill-on-invalid-dep=yes",
                                                        "--requeue",
                                                        "--output=/data/log/abort-trigger-%s.log" % (subtask_id,),
                                                    ]
                                                    )
            logger.info("Scheduled SLURM job %s for abort trigger for subtask_id=%s", slurm_cancel_job_id, subtask_id)

            logger.info("Handed over pipeline %s to SLURM, setting status to QUEUED", subtask_id)
            self.tmss_client.set_subtask_status(subtask_id, "queued", retry_count=5)
        except Exception as e:
            logger.error(str(e))
            self.tmss_client.set_subtask_status(subtask_id, "error", f'Error when starting pipeline: {e}', retry_count=5)


class PipelineControlTMSS(TMSSBusListener):
    def __init__(self, handler_kwargs: dict = None,
                 exchange: str = DEFAULT_BUSNAME, broker: str = DEFAULT_BROKER):

        super().__init__(handler_type=PipelineControlTMSSHandler, handler_kwargs=handler_kwargs,
                         exchange=exchange, broker=broker,
                         num_threads=1)

    def start_listening(self):
        # HACK: create a temporary extra handler which is not connected to this listener,
        # and hence not responding to incoming messages,
        # and use this extra handler to initially check all already scheduled pipelines
        with self._create_handler() as helper_handler:
            helper_handler.check_scheduled_pipelines()

        # everything has been check, now start_listening, and let the normal handlers respond to events
        super().start_listening()


class PipelineControlServiceHandler(ServiceMessageHandler):
    def __init__(self):
        super(PipelineControlServiceHandler, self).__init__()
        self.register_service_method("CancelPipeline", self.cancel_pipeline)
        self.slurm = Slurm()

    def _cancel_pipeline(self, control_id):
        # Cancel corresponding SLURM job, but first the abort-trigger
        # to avoid setting ABORTED as a side effect.
        # to be cancelled as well.

        if not self.slurm.isQueuedOrRunning(control_id):
            logger.info("_cancel_pipeline: Job %s not running", control_id)
            return True

        def cancel(jobName):
            logger.info("Cancelling job %s", jobName)
            self.slurm.cancel(jobName)

        jobName = str(control_id)
        cancel("%s-abort-trigger" % jobName)
        cancel(jobName)

        return True

    def cancel_pipeline(self, control_id):
        """ canceling a pipeline with a given control id """
        canceled = self._cancel_pipeline(control_id)

        return {'canceled': canceled}
