//# MSLofarSpectralWindow.cc: MS SPECTRAL_WINDOW subtable with LOFAR extensions
//# Copyright (C) 2021
//# ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$
//#
//# @author Jan David Mol

#include <lofar_config.h>
#include <MSLofar/MSLofarSpectralWindow.h>

using namespace casacore;

namespace LOFAR {

  MSLofarSpectralWindow::MSLofarSpectralWindow()
  {}

  MSLofarSpectralWindow::MSLofarSpectralWindow (const String& tableName,
                                  Table::TableOption option) 
    : MSSpectralWindow (tableName, option)
  {}

  MSLofarSpectralWindow::MSLofarSpectralWindow (SetupNewTable& newTab, uInt nrrow,
                                  Bool initialize)
    : MSSpectralWindow (newTab, nrrow, initialize)
  {}

  MSLofarSpectralWindow::MSLofarSpectralWindow (const Table& table)
    : MSSpectralWindow (table)
  {}

  MSLofarSpectralWindow::MSLofarSpectralWindow (const MSLofarSpectralWindow& that)
    : MSSpectralWindow (that)
  {}

  MSLofarSpectralWindow& MSLofarSpectralWindow::operator= (const MSLofarSpectralWindow& that)
  {
    MSSpectralWindow::operator= (that);
    return *this;
  }

  TableDesc MSLofarSpectralWindow::requiredTableDesc()
  {
    TableDesc td (MSSpectralWindow::requiredTableDesc());
    MSLofarTable::addColumn (td, "LOFAR_TOPOCENTRIC_FREQUENCY_CORRECTION_APPLIED", TpBool,
                             "Topocentric frequency correction was applied");
    return td;
  }

} //# end namespace
