import os
from .. import station_test_raw_parser as raw_parser


def read_station_test_data(filename):
    with open(os.path.join(os.path.dirname(__file__) , filename)) as data:
        return [line for line in data.read().split('\n') if line]


def split_rows_as_expected(line):
    if "CHECKS" in line:
        values = [value for value in line.split(',')]
    else:
        values = [raw_parser.parse_key_value_pairs(value) if ('=' in value) else value for value in line.split(',')]
    return values