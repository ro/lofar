# UDP server example
# Script to dump LOFAR from a LORA trigger
# Also storage nodes can be assigned at the start of each observation.
import socket
import struct
import time
import shutil
from TBBcontroller import *
from currentobsCOBALT import currentobs
from currentobsCOBALT import get_all_parameters_new
import subprocess
import os
import signal

# This handler is used as a time-out when manual dumps are requested
def handler(signum, frame):
    print('Signal handler called with signal', signum)
    print('no action taken, waiting for next trigger')
    abortDump=True
    answer='no'
    raise ValueError('no input given')

signal.signal(signal.SIGALRM, handler)

# This file contains the setup of the current trigger system
exec(compile(open("allowconditionsAdv.py", "rb").read(), "allowconditionsAdv.py", 'exec'))
# CURRENTLY RUNS TILL 2021, it could be stopped earlier if needed
endtime=time.mktime((2021,1,10,11,0,0,0,0,0))
maxidletime=600 # 600 seconds is the maximum dumptime of an RCU, will be overwritten by allowconditions

# These files are used to indicate the state of the instrument such that new TBB writers are not stopped/started during a dump.
fNotDumping="/globaldata/tbb/notdumping.py"
fDumping="/globaldata/tbb/amdumping.py"
fCurrentlyDumping="/globaldata/tbb/nowdumping.py"
abortDump=False

# Obtain the station list used.
def get_stations(obsid,useFilename=False,allowedlist="CS"):
    from currentobsCOBALT import get_all_parameters_new
#   load parset
    p=get_all_parameters_new(obsid,useFilename)
#   this keyname gives the stationlist in the parset
    keyname='Observation.ObservationControl.StationControl._hostname'
#   read stations from the parset.
    if keyname in list(p.keys()):
        stations=p[keyname].strip('[]').split(',')
#   add a "C" as this is the hostname used for ssh commands
        stations=[st+"C" for st in stations]
        print("stations in observation",stations)
#   in kis001/station parsets there is an ObsSW in front of the key
    elif 'ObsSW'+keyname in list(p.keys()):
        stations=p['ObsSW'+keyname].strip('[]').split(',')
        stations=[st+"C" for st in stations]
        print("stations in observation",stations)
#   Check which stations data should be read from
    if allowedlist=="all":
        stations=[st for st in stations if "CS" in st or "RS" in st]
    elif allowedlist=="CS": # only corestations
        stations=[st for st in stations if "CS" in st]
    elif allowedlist=="RS": # only corestations
        stations=[st for st in stations if "RS" in st]
    else:
        stations=[st for st in stations if st in allowedlist]
#   return the stationlist
    return stations

def checkfiles(f1=fNotDumping,f2=fDumping,f3=fCurrentlyDumping):
    """ Check if files that indicate dumping are actually in place. """
    if os.access(f1, os.R_OK) and os.access(f2, os.R_OK) and (os.access(fCurrentlyDumping,os.W_OK) or (not os.path.isfile(fCurrentlyDumping) and os.access(os.path.split(fCurrentlyDumping)[0],os.W_OK))):
        return True
    else:
        return False


if not checkfiles():
    print("WARNING, cannot access one or more of the files",fNotDumping,fDumping,fCurrentlyDumping,"needed to stop the datawriter only at the appropriate time")

hostname=socket.gethostname()
if hostname != 'lcs155.control.lofar':
    answer=input("WARNING, this program should run on lcs155. Are you sure you want to run it on "+str(hostname)+" instead ? [y/n] ")
    if answer not in ["y","n","Y","N"]:
        answer=input("Please answer the question with \"y\" or \"n\"]")
    elif answer == "n":
        quit()





from time import sleep
obstimes=[]
# Observation times can be entered here
# WARNING OBSERVATION STOPTIME YEAR IS 2015
# This is not used anymore.
obstimes.append((time.mktime((2011,10,12,14,10,0,0,0,0)),time.mktime((2011,10,12,14,15,0,0,0,0)))) # test writing to locus

# Listen at port 31660 . Ports 0x7BA0-0x7BAF are assigned for trigger protocols
server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#server_socket.bind(("", 31662)) # test
server_socket.bind(("", 31660)) # LORA
packets=[]
rectime=[]
fmt='>cciic'
# Log files used for keeping track of triggers. DO NOT OVERWRITE THESE FILES, but always append
f=open('/opt/lofar/tbb/logs/LORAtime4','a')
f2=open('/opt/lofar/tbb/logs/LORAreceived','a')
f3=open('/opt/lofar/tbb/logs/EffReceived','a')
pages=200
#fulldumprcus=range(20,21)

# No longer used
def myStorage(stations):
    from TBBcontroller import setstorage
    for num, station in enumerate(stations):
        print(station)
        #setstorage(station,nodes[num%len(nodes)])

# Freeze TBB memory at the stations
def stopall(stations):
    print("stopping stations")
    proclist=[]
    for station in stations:
        proclist.append(stop(station))
    for proc in proclist:
        proc.wait()
    print("stations stopped")

# Restart recording at the stations
def recordall(stations):
    print("recording starting at  stations")
    proclist=[]
    for station in stations:
        proclist.append(record(station))
    for proc in proclist:
        proc.wait()
    print("recording started")

# Check at what time the RCUs are stopped
def getStopTime(stations,rcu=48):
    station=stations[0]
    rcunr,startsample,maxsample=findtimerange2(rcu,station=station)
    (tstart,tend)=findtimerange(rcu,startsample,maxsample,200,station=station)
    sec=tend[0]
    nsec=5*tend[1]
    return (sec,nsec)

newrun=True
#print"UDPServer Waiting for client on port 31661" #test
print("UDPServer Waiting for client on port 31660") #LORA
stoptimes=[]
#for (starttime,stoptime) in obstimes:
#    while time.time() < starttime:
#        print "observation not allowed ... waiting ... until ", time.strftime("%D %T",time.localtime(starttime))
#        sleep(30)
#        newrun=True
#    while time.time() < stoptime:
#        if newrun:
#            #myStorage()
#            sleep(5)
#            newrun=False
#while time.time()<time.mktime((2011,5,17,6,0,0,0,0,0)):
dumptype=False
obsid="None"
f2.writelines(["new observation started at "+str(time.time())+" "+str(time.strftime("%D %T"))+"\n"])
f3.writelines(["new observation started at "+str(time.time())+" "+str(time.strftime("%D %T"))+"\n"])
# Listen for triggers
prevdata=''
data=''
while time.time()<endtime:
        print("Waiting for trigger ...")
        prevdata=data
        data, address = server_socket.recvfrom(11)

        # get current time
        time1=time.time()
        # get current observation id
        obsid=currentobs(obsid)
        # unpack trigger message
        d2=struct.unpack(fmt,data)
        # Map trigger source id to trigger source name
        print(d2,d2=='A')
        if d2[4]=='\x66':
            dumptype="LORA"
        elif d2[4]=='\x67':
            dumptype="manual"
        elif d2[4]=='\x65':
            dumptype="FRATS"
        elif d2[4]=='\x68':
            dumptype="UK608"
        elif d2[4]=='\x69':
            dumptype="FR606"
        elif d2[4]=='\x64':
            dumptype="validation"
        elif d2[4]=='\x50':
            dumptype="Effelsberg"
        elif d2[4]=='\x51':
            dumptype="EffelsbergTest"
        elif d2[4]=='\x52':
            dumptype="lightning"
        else:
            print("wrong message send")
            dumptype="corrupt"
        if prevdata==data and dumptype=="LORA":
            f2.writelines([str(d2[2])+" "+str(d2[3])+" same trigger "+dumptype+" "+str(time.time())+"\n"])
            continue
        print("... trigger received from ",address)
        # Only accept FRATS triggers if they arrive within 8 seconds after arriving at the lowest frequency
        dumpsec=d2[2]
        dumpsam=d2[3]/5
        if dumptype=='FRATS':
            if d2[2]+d2[3]*1e-9+8.<time1:
                f2.writelines([str(d2[2])+" "+str(d2[3])+" too_late "+dumptype+" "+str(time.time())+"\n"])
                f2.flush()
                continue
        if 'L' in obsid:
            # Get observation info
            p=get_all_parameters_new(obsid)
            # Set the trigger source
            p['triggersource']=dumptype
            # Load trigger conditions
            exec(compile(open("allowconditionsAdv.py", "rb").read(), "allowconditionsAdv.py", 'exec'))
            # Check if this trigger is allowed, based on project, triggersource, TBBpiggybackAllowed
            if not isallowed(p):
                f2.writelines([str(d2[2])+" "+str(d2[3])+" not allowed "+obsid+" "+dumptype+"\n"])
                f2.flush()
                if 'Effelsberg' in dumptype:
                    f3.writelines([str(d2[2])+" "+str(d2[3])+" not allowed "+obsid+" "+dumptype+"; received at "+str(time1)+"\n"])
                    f3.flush()
                print("Dumping not allowed according to the conditions in allowconditions.py",time.time()-time1)
                continue
        else:
            print("no observation running, processing time",time.time()-time1)
            f2.writelines([str(d2[2])+" "+str(d2[3])+" no observation "+dumptype+"\n"])
            f3.writelines([str(d2[2])+" "+str(d2[3])+" no observation "+dumptype+" received "+str(int(time1))+" "+str(int((time1%1)*1e3))+" processed "+str(time.time())+"\n"])
            f3.flush()
            f2.flush()
            continue
        if dumptype in list(rcuDict.keys()):
            rcus=rcuDict[dumptype]
        elif 'default' in list(rcuDict.keys()):
            rcus=rcuDict['default']
        else:
            rcus=list(range(0,96))

        # Check if trigger is within 5 seconds of the stop time
        if time1-d2[2] <4.2: #and time1 < stoptime:
            if 'L' in obsid:
                # Get the station list
                if dumptype in list(stationlist.keys()):
                    allowedlist=stationlist[dumptype]
                else:
                    allowedlist=stationlist['default']
                stations=get_stations(currentobs(),allowedlist=allowedlist)
                if p['Observation.Campaign.name'] in ['LC6_002']: # Solar observation
                    stations=get_stations('L545033',allowedlist=allowedlist)
                if dumptype in ['Effelsberg','EffelsbergTest']:
                    stations=allowedlist                
                for st in brokenstations:
                    if st in stations:
                        stations.remove(st)
#                if p['triggersource']=='validation':
#                    print "overwriting choice to all for validation observation
#                    stations=get_stations(currentobs(),allowedlist='all')
                print("new stations =",stations)


            shutil.copyfile(fDumping,fCurrentlyDumping)  # Sign to datawriter that we're dumping at the moment.
            dumptime=d2[2]+1e-9*d2[3]
            time3=time.time()
            if time3<dumptime:
                print("Stop time is in the future. Sleeping for ",dumptime-time3," sec")
                time.sleep(dumptime-time3)
            stopall(stations) # Freeze TBB memory
            if time3<dumptime:
                print("Stop time wass in the future. Slept for ",dumptime-time3," sec")

            time2=time.time()

            # Check if user input is required to verify the trigger
            if(p['triggersource'] in list(waitforresponse.keys())):
                waitresponse=waitforresponse[p['triggersource']]
            else:
                waitresponse=waitforresponse['default']

            abortDump=False


            if waitresponse:
                answer='wait'
                print("Waiting for input for",waittime,"seconds")
                try:
                    signal.alarm(waittime)
                    if p['triggersource']=='FRATS' or p['triggersource']=='manual':
                        print("ssh",str(address[0])," /home/veen/FRATS/checkpulse.sh",d2[2],d2[3],str(time2))
                        #proc=subprocess.Popen(["ssh",str(address[0])," /home/veen/FRATS/checkpulse.sh",str(d2[2]),str(d2[3]),str(time2)])
                        try:
                            proc5=subprocess.Popen(["python","/home/veen/FRATS/mailImage.py","--image=/home/veen/FRATS/look.gif","-a s.terveen@astro.ru.nl","-a e.enriquez@astro.ru.nl","-a a.bilous@astro.ru.nl","--subject=ssh "+str(address[0])+" /home/veen/FRATS/checkpulse.sh "+str(d2[2])+" "+str(d2[3])+" "+str(time2)])
                        except:
                            print("Sending alert e-mail failed")
                            pass

                        #proc.wait()
                    (tstart,tend)=findtimerange3(stations[0],48,1048000)
                    answer=input('continue dump? Requested: '+str(d2[2])+' '+ str(d2[3])+'. Time in buffer '+str(tstart)+' till '+str(tend)+' [yes/y/no/n/wait]')
                    while answer=='wait':
                        # Reset timeout time
                        signal.alarm(waittime)
                        answer=input('continue dump now? [yes/y/no/n/wait]')
                    signal.alarm(0)
                except ValueError:
                    # If timed out ....
                    signal.alarm(0)
                    abortDump=True
                    f2.writelines([str(d2[2])+" "+str(d2[3])+" allowed "+dumptype+" " + obsid + "durations check:"+str(time3-time1)+" total "+str(time2-time3)+" waiting too long for manual response. Continueing..\n"])
                    f2.flush()

                # Dump or not?
                if answer=='yes' or answer=='y':
                    print("no longer dumping from end, please uncomment lines below if you want that instead")
                    #dumpsec=int(tend)
                    #dumpsam=int((tend-dumpsec-0.2)*200000000)
                    #print "Updating dump time, new time:",dumpsec,dumpsam
                    abortDump=False
                else:
                    abortDump=True

            if abortDump: # Abort dump, restart recording of stations
                recordall(stations)
                shutil.copyfile(fNotDumping,fCurrentlyDumping)
                f2.writelines([str(d2[2])+" "+str(d2[3])+" allowed "+dumptype+" " + obsid + " durations check:"+str(time3-time1)+" total "+str(time2-time3)+" manual response refused the dump. Continueing..\n"])
                f2.flush()
                time.sleep(7)
                continue


            # Set number of pages (of 5 us) to dump as specified in allowconditions.py
            if(p['triggersource'] in list(pagesDict.keys())):
                pages=pagesDict[p['triggersource']]
            else:
                pages=pagesDict['default']
            print("Number of pages set to ",pages)
            prepages=pages
            postpages=pages
            # Set dump method as specified in allowconditions.py
            if(p['triggersource'] in list(dumpmethod.keys())):
                method=dumpmethod[p['triggersource']]
            else:
                method=dumpmethod['default']
            print("Dumptype set to ",dumpmethod)

            time.sleep(3)

            # check if stations are stopped, before requesting them to dump their data
            notstopped=[]
            for st in stations:
                if not checkstopped(st):
                    notstopped.append(st)
            print("These stations did not stop:",notstopped)
            # update station list to exclude non-stopped stations
            stations=[st for st in stations if st not in notstopped]
            f2.writelines([str(d2[2])+" "+str(d2[3])+" allowed "+dumptype+" " + obsid + " durations check:"+str(time3-time1)+" total "+str(time2-time3)+" nr stations "+str(stations)+"\n"])
            f2.flush()
            if method=='readtime' and len(stations)>0:
                time4=time.time()
                #(sec,nsec)=getStopTime(stations)
                (tstart,tend)=findtimerange3(stations[0],48,1048000)
                sec=int(tend)
                nsec=int((tend%1)*1e9)
                f3.writelines([str(d2[2])+" "+str(d2[3])+" allowed "+dumptype+" " + obsid + " durations check:"+str(time3-time1)+" total "+str(time2-time3)+" nr stations "+str(stations)+"\n"])
                #f3.writelines("Printing : Readtime; Trigger time: "+str(d2[2])+" "+str(d2[3])+" ; Received: "+str(time1)+" ; TBBs stopped at: "+str(sec)+" "+str(nsec)+" ; Delay time: "+str(sec-d2[2]+1e-9*(nsec-d2[3]))+"\n")
                f3.writelines("Printing first station: Readtime; Trigger time: "+str(d2[2])+" "+str(d2[3])+" ; Received: "+str(time1)+" ; TBBs stopped at: "+str(sec)+" "+str(nsec)+" ; Delay time: "+str(sec-d2[2]+1e-9*(nsec-d2[3]))+" ; Buffer length: "+str(tend-tstart)+"\n")
                f3.flush()
                (tstart,tend)=findtimerange3(stations[-1],48,1048000)
                sec=int(tend)
                nsec=int((tend%1)*1e9)
                f3.writelines("Printing last station: Readtime ; Trigger time: "+str(d2[2])+" "+str(d2[3])+" ; Received: "+str(time1)+" ; TBBs stopped at: "+str(sec)+" "+str(nsec)+" ; Delay time: "+str(sec-d2[2]+1e-9*(nsec-d2[3]))+" ; Buffer length: "+str(tend-tstart)+"\n")
                f3.flush()
                recordall(stations)
                continue

            # reroute some stations to a different storage node, as specified in allowconditions.py
            storagefile=open('setstorage.log','w')
            procstrg=subprocess.Popen(['./setstorage.sh'],stdout=storagefile,stderr=storagefile)
            procstrg.wait()
            storagefile.flush()
            storagefile.close()
            storagefiler=open('setstorage.log','r')
            ttext=storagefiler.read()
            print("storage nodes set to",sorted(set([c for c in ttext.split() if 'cpu' in c])))
            #for st,node in reroute.iteritems():
            #    setstorage(st,node)
            print("Duration from receiving UDP package to stopping: ",time2-time1, " s")
            stoptimes.append(time2-time1)
            print("Events recorded",len(stoptimes))
            #(sec,nsec)=getStopTime(stations)
            (sec,nsec)=("empty","empty")
            packets.append(data)
            print("( " ,address[0], " " , address[1] , " ) said at ",sec,nsec,data)
            rectime.append([d2[2],d2[3],sec,nsec,stoptimes[-1],time2])
            #f.writelines([str(d2[2])+" "+str(d2[3])+" "+str(sec)+" "+str(nsec)+" "+str(stoptimes[-1])+" "+str(time2-1303890000)+"\n"])
            # Write LORA triggers to file
            if dumptype=="LORA":
                f.writelines([str(d2[2])+" "+str(d2[3])+" "+"0.0"+" "+"0.0"+" "+"0.0"+" "+"0.0"+"\n"])
            f.flush()
            print(rectime[-1])

            # PART THAT READS OUT THE DATA
            if True: #abs(sec-d2[2])<3:
                # get dump second and dumpsample
                #fulldumpsec=sec
                #fulldumpsam=nsec/5-1000*1024
                fulldumppages=250000
                # Make sure dumpsample is positive
                if dumpsam<0:
                    dumpsam+=2e9
                    dumpsec-=1

                # Keep track which rcus are dumped for which station
                st_rcunr=[0 for st in stations]
                # These methods use a similar read-out command
                if method in ['read','readbefore','readafter']:
                    # List with dumpprocesses for each station
                    proclist=[]
                    # List filled when finished dumping
                    finished=[False for st in stations]
                    # Log idle time
                    idling=[0 for st in stations]
                    # For eqch station
                    for num,station in enumerate(stations):
                        # New method, now implemented everywhere, using time in seconds
                        # instead of sample numbers when requesting data
                        newreadmethod=True
                        rcu=rcus[st_rcunr[num]]
                        if method == 'read':
                            prepages=pages
                            postpages=pages
                        elif method == 'readbefore':
                            prepages=pages
                            postpages=0
                        elif method == 'readafter':
                            prepages=0
                            postpages=pages
                        # read command for this station
                        proclist.append(read(station,rcu,dumpsec,dumpsam,prepages,postpages,newreadmethod))
                    while(False in finished): # continued until all stations are finished
                        for num,proc in enumerate(proclist):
                            if proc.poll()==0: # if reading for this station is finished, read out the next rcu
                                idling[num]=0
                                st_rcunr[num]+=1
                                if st_rcunr[num]<len(rcus): # check if more rcus need to be dumped
                                    rcu=rcus[st_rcunr[num]]
                                    station=stations[num]
                                    newreadmethod=True
                                    proclist[num]=read(station,rcu,dumpsec,dumpsam,prepages,postpages,newreadmethod)
                                else:
                                    finished[num]=True # this station has finished
                            else:
                                idling[num]+=1 # increase idling time
                                if(idling[num]>maxidletime): # time-out to prevent hanging of station dumps
                                    finished[num]=True
                        if(max(idling)<10):
                            print("idling for",idling)
                        time.sleep(1)
                if method in ['readparallel','readparallelbefore','readparallelafter']:
                    start0=0
                    if min(rcus)<16:
                        start1=sum([r<16 for r in rcus])
                        
                    
                    st_rcunr=[0 for st in stations]+[sum([r<16 for r in rcus]) for st in stations]+[sum([r<32 for r in rcus]) for st in stations]+[sum([r<48 for r in rcus]) for st in stations]+[sum([r<64 for r in rcus]) for st in stations]+[sum([r<80 for r in rcus]) for st in stations]
                    max_rcunr=[16 for st in stations]+[32 for st in stations]+[48 for st in stations]+[64 for st in stations]+[80 for st in stations]+[96 for st in stations]
                    stations=stations+stations+stations+stations+stations+stations
                    # List with dumpprocesses for each station
                    proclist=[]
                    # List filled when finished dumping
                    finished=[False for st in stations]
                    # Log idle time
                    idling=[0 for st in stations]
                    # For eqch station
                    for num,station in enumerate(stations):
                        # New method, now implemented everywhere, using time in seconds
                        # instead of sample numbers when requesting data
                        newreadmethod=True
                        rcu=rcus[st_rcunr[num]]
                        if method == 'read':
                            prepages=pages
                            postpages=pages
                        elif method == 'readbefore':
                            prepages=pages
                            postpages=0
                        elif method == 'readafter':
                            prepages=0
                            postpages=pages
                        # read command for this station
                        proclist.append(read(station,rcu,dumpsec,dumpsam,prepages,postpages,newreadmethod))
                    while(False in finished): # continued until all stations are finished
                        for num,proc in enumerate(proclist):
                            if proc.poll()==0: # if reading for this station is finished, read out the next rcu
                                idling[num]=0
                                st_rcunr[num]+=1
                                if st_rcunr[num]<max_rcunr[num]: # check if more rcus need to be dumped
                                    rcu=rcus[st_rcunr[num]]
                                    station=stations[num]
                                    newreadmethod=True
                                    print(len(proclist),num,station,rcu)
                                    proclist[num]=read(station,rcu,dumpsec,dumpsam,prepages,postpages,newreadmethod)
                                else:
                                    finished[num]=True # this station has finished
                            else:
                                idling[num]+=1 # increase idling time
                                if(idling[num]>maxidletime): # time-out to prevent hanging of station dumps
                                    finished[num]=True
                        if(max(idling)<10):
                            print("idling for",idling)
                        time.sleep(1)

                if method == 'readall': # use the readall method, this reads the last N pages of data. Data will not start at the same time for all RCUs
                    proclist=[]
                    for station in stations:
                        proclist.append(readall(station,pages))
                    for proc in proclist:
                            proc.wait()


                time.sleep(2)
                if waitresponse:
                    answer=input('Restart recording? [yes/y/no/n/wait]')
                    if answer in ['yes','y']:
                        recordall(stations) # record stations
                    else:
                       print("Recording not started for",stations)
                else:
                     recordall(stations) # record stations
                shutil.copyfile(fNotDumping,fCurrentlyDumping) # no longer dumping
            else:
                print("time not in data, rerecording")
                recordall(stations)
                shutil.copyfile(fNotDumping,fCurrentlyDumping)

                time.sleep(7)

f2.writelines(["observation stopped at "+str(time.time())+" "+str(time.strftime("%D %T"))+"\n"])
f2.close()
f3.close()
quit()
