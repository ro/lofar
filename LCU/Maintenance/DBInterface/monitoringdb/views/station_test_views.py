from datetime import datetime

from django_filters import rest_framework as filters
from rest_framework.viewsets import ReadOnlyModelViewSet
from .common import *
from lofar.maintenance.mdb.tests_validators import is_station_test
from lofar.maintenance.monitoringdb.tasks.insert_raw_tests import insert_station_test
from ..models.component import Component
from ..models.component_error import ComponentError
from ..models.element import Element
from ..models.element_error import ElementError
from ..models.log import ActionLog
from ..models.station import Station
from ..models.station_test import StationTest
from ..serializers.component import ComponentSerializer
from ..serializers.component_error import ComponentErrorSerializer
from ..serializers.element import ElementSerializer
from ..serializers.element_error import ElementErrorSerializer
from ..serializers.station import StationSerializer
from ..serializers.station_tests import StationTestSerializer
import logging
from django.http import HttpRequest
from rest_framework.decorators import api_view

logger = logging.getLogger('views')


class StationTestFilterSet(filters.FilterSet):
    station_name = filters.CharFilter(field_name='station__name',
                                      lookup_expr='contains', help_text='name of the station')
    station_type = filters.CharFilter(field_name='station__type',
                                      lookup_expr='contains',
                                      help_text='selects the station type:' +
                                                'one of [Core[C], Remote[R], International[I]]')

    from_date = filters.DateFilter(field_name='start_datetime', lookup_expr='gt',
                                   help_text='select station tests from date time')
    to_date = filters.DateFilter(field_name='end_datetime', lookup_expr='lt',
                                 help_text='select station tests until date time')


class ComponentErrorFilterSet(filters.FilterSet):
    component_id = filters.NumberFilter(field_name='component', lookup_expr='component_id',
                                        help_text='select by component id')
    component_type = filters.CharFilter(field_name='component', lookup_expr='type',
                                        help_text='select by component type')

    station_name = filters.CharFilter(field_name='station_test__station',
                                      lookup_expr='name__contains',
                                      help_text='station name with name like')
    station_type = filters.CharFilter(field_name='station_test__station',
                                      lookup_expr='type__contains',
                                      help_text='station type like')

    from_date = filters.DateFilter(field_name='station_test', lookup_expr='start_datetime__gt',
                                   help_text='select component errors from date time')
    to_date = filters.DateFilter(field_name='station_test', lookup_expr='end_datetime__lt',
                                 help_text='select component errors until date time')
    type = filters.CharFilter(field_name='type',
                              lookup_expr='contains',
                              help_text='component error type')


class ElementErrorFilterSet(filters.FilterSet):
    component_id = filters.NumberFilter(field_name='component_error__component',
                                        lookup_expr='component_id',
                                        help_text='id of the parent component')
    element_id = filters.NumberFilter(field_name='element', lookup_expr='element_id',
                                      help_text='element id')
    station_name = filters.CharFilter(field_name='component_error__station_test__station',
                                      lookup_expr='name__contains',
                                      help_text='name of the station')
    station_type = filters.CharFilter(field_name='component_error__station_test__station',
                                      lookup_expr='type__contains',
                                      help_text='station type')

    from_date = filters.DateFilter(field_name='component_error__station_test',
                                   lookup_expr='start_datetime__gt',
                                   help_text='select element errors from date time')
    to_date = filters.DateFilter(field_name='component_error__station_test',
                                 lookup_expr='end_datetime__lt',
                                 help_text='select element errors until date time')

    type = filters.CharFilter(field_name='type',
                              lookup_expr='contains', help_text='element error type')


class StationViewSet(ReadOnlyModelViewSet):
    """
    retrieve:
    retrieve a specific station from the database

    list:
    list all the stations present in the database
    """
    serializer_class = StationSerializer
    queryset = Station.objects.all()
    filter_fields = '__all__'


class ComponentViewSet(ReadOnlyModelViewSet):
    serializer_class = ComponentSerializer
    queryset = Component.objects.all()
    filter_fields = '__all__'


class ElementViewSet(ReadOnlyModelViewSet):
    serializer_class = ElementSerializer
    queryset = Element.objects.all()
    filter_fields = '__all__'


class StationTestViewSet(ReadOnlyModelViewSet):
    serializer_class = StationTestSerializer
    queryset = StationTest.objects.all().order_by('-start_datetime', 'station__name')
    filter_fields = '__all__'
    filterset_class = StationTestFilterSet


class ComponentErrorViewSet(ReadOnlyModelViewSet):
    serializer_class = ComponentErrorSerializer
    queryset = ComponentError.objects.all().order_by('-station_test__start_datetime',
                                                     'type',
                                                     'component__component_id',
                                                     'component__station__name')
    filter_fields = '__all__'
    filterset_class = ComponentErrorFilterSet


class ElementErrorViewSet(ReadOnlyModelViewSet):
    serializer_class = ElementErrorSerializer
    queryset = ElementError.objects.all().order_by('-component_error__station_test__start_datetime',
                                                   'component_error__station_test__station__name',
                                                   'component_error__component__component_id',
                                                   'element__element_id')
    filter_fields = '__all__'
    filterset_class = ElementErrorFilterSet


def handle_stationtest_insert(content, remote_addr):
    action_log = ActionLog(who=remote_addr,
                           what='INSERT_REQUESTED',
                           when=datetime.now(),
                           model_name='STATIONTEST',
                           entry_id=-1)

    action_log.save()

    insert_station_test.delay(action_log.pk, content)

    return action_log.pk


@api_view(['POST'])
def insert_raw_station_test(request: HttpRequest):
    """
    This function is meant to parse a request of the form
    {
    "content": "[STATION TEST RAW TEXT]"
    }
    :param request: HTTP request
    :return:
    """
    if request.method == 'POST':
        remote_addr = request.META['REMOTE_ADDR']
        logs_pk = []
        if 'content' in request.data:
            content = request.data
            logs_pk += [handle_stationtest_insert(content, remote_addr)]
        elif request.FILES:
            files = request.FILES
            for file in files.values():
                content_data = file.read().decode('UTF-8')
                if not is_station_test(content_data):
                    return Response(exception=True,
                                    data="the attached file is not a station test %s" % file.name,
                                    status=status.HTTP_400_BAD_REQUEST)

                logs_pk += [handle_stationtest_insert(content_data, remote_addr)]
        else:
            return Response(exception=True,
                            data="the post message is not correct." +
                                 " It has to be of the form \{'content':[RAWSTRING]\}",
                            status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK,
                        data='station test insert acknowledged.'
                             ' Check log for id %s' % logs_pk)
