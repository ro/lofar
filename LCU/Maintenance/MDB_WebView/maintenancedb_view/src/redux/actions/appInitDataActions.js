import axios from 'axios';
import { stringSort } from '../../utils/utils.js';

export const FETCH_ERRORTYPES_BEGIN   = 'FETCH_ERRORTYPES_BEGIN';
export const FETCH_ERRORTYPES_SUCCESS = 'FETCH_ERRORTYPES_SUCCESS';
export const FETCH_ERRORTYPES_FAILURE = 'FETCH_ERRORTYPES_FAILURE';

export const FETCH_STATIONS_BEGIN = 'FETCH_STATIONS_BEGIN';
export const FETCH_STATIONS_SUCCESS = 'FETCH_STATIONS_SUCCESS';
export const FETCH_STATIONS_FAILURE = 'FETCH_STATIONS_FAILURE';


/* ERROR TYPES */
const errorTypesURL = '/api/view/ctrl_list_component_error_types';

export const fetchErrorTypesBegin = () => ({
  type: FETCH_ERRORTYPES_BEGIN
});

export const fetchErrorTypesSuccess = errorTypes => ({
  type: FETCH_ERRORTYPES_SUCCESS,
  payload: { errorTypes }
});

export const fetchErrorTypesFailure = error => ({
  type: FETCH_ERRORTYPES_FAILURE,
  payload: { error }
});


export function fetchErrorTypes() {
  return dispatch => {
    // Not used: dispatch(fetchErrorTypesBegin());

    return axios.get(errorTypesURL)
        .then(res => {
            dispatch(fetchErrorTypesSuccess(res.data));
        })
        .catch(error => {
            console.log("Error fetching error types: "+error);
            dispatch(fetchErrorTypesFailure(error));
            // Try again in 30s
            setTimeout(() => dispatch(fetchErrorTypes()), 10000);
        });
  };
}



/* STATIONS */
const stationsURL = '/api/stationtests/station/?page_size=200';

export const fetchStationsBegin = () => ({
  type: FETCH_STATIONS_BEGIN
});

export const fetchStationsSuccess = stations => ({
  type: FETCH_STATIONS_SUCCESS,
  payload: { stations }
});

export const fetchStationsFailure = error => ({
  type: FETCH_STATIONS_FAILURE,
  payload: { error }
});

export function fetchStations() {
  return dispatch => {
    // Not used: dispatch(fetchstationsBegin());
    return axios.get(stationsURL)
        .then(res => {
            let stations = res.data.results ? res.data.results : [];
            stations.sort((a,b) => stringSort(a.name, b.name) );
            dispatch(fetchStationsSuccess(stations));
        })
        .catch(error => {
            console.log("Error fetching error types: "+error);
            dispatch(fetchStationsFailure(error));
            // Try again in 30s
            setTimeout(() => dispatch(fetchStations()), 10000);
        });
  };
}
