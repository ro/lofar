"""
In this file all the model regarding the real time station monitor output are collected
"""
from django.contrib.postgres.fields import ArrayField
from django.db import models

from .component import Component
from .test import GenericTest

MODE_TO_COMPONENT = {
    1: 'LBL',
    2: 'LBL',
    3: 'LBH',
    4: 'LBH',
    5: 'HBA',
    6: 'HBA',
    7: 'HBA'
}

MODE_TO_FREQ_RANGE = {1: (10, 90),
                      2: (30, 90),
                      3: (10, 90),
                      4: (30, 90),
                      5: (110, 190),
                      6: (170, 230),
                      7: (210, 250)}


def antenna_id_polarization_from_rcu_type(rcu, type):
    """
    Compute the antenna id for a given rcu, type and polarization
    :param rcu: id of the rcu
    :param type: type of the antenna
    :return: the antenna id and polarization
    :rtype: (int, str)
    """
    polarization_index = rcu % 2

    if type in ['LBH', 'HBA']:
        antenna_id = rcu - polarization_index
        antenna_id /= 2.
        polarization = 'Y' if polarization_index > 0 else 'X'
    elif type == 'LBL':
        antenna_id = (rcu - polarization_index) / 2. + 48
        polarization = 'X' if polarization_index > 0 else 'Y'
    else:
        antenna_id = -1
        polarization = ''
    return antenna_id, polarization


class RTSMObservation(GenericTest):
    sas_id = models.PositiveIntegerField(default=0)
    samples = models.PositiveIntegerField(default=0)


class RTSMError(models.Model):
    observation = models.ForeignKey(RTSMObservation, related_name='errors', on_delete=models.CASCADE)
    component = models.ForeignKey(Component, on_delete=models.SET_NULL, null=True)
    count = models.PositiveIntegerField(default=0, null=True)
    rcu = models.SmallIntegerField(default=None, null=True)
    mode = models.SmallIntegerField(default=None, null=True)
    percentage = models.FloatField(default=None, null=True)
    polarization = models.CharField(max_length=1)
    error_type = models.CharField(max_length=50)

    @staticmethod
    def get_or_create_error_from_rtsm_result(observation: RTSMObservation,
                                      rcu,
                                      mode,
                                      percentage,
                                      error_type,
                                      count):
        station = observation.station
        component_type = MODE_TO_COMPONENT[mode]
        component_id, polarization = antenna_id_polarization_from_rcu_type(rcu, component_type)
        component_entity, _ = Component.objects.get_or_create(station=station,
                                        type=component_type,
                                        component_id=component_id)
        rtsm_error, created = RTSMError.objects.get_or_create(observation=observation,
                                        component=component_entity,
                                        rcu=rcu,
                                        mode=mode,
                                        polarization=polarization,
                                        error_type=error_type,
                                        percentage=percentage,
                                        count=count
                                        )

        return rtsm_error, created


class RTSMErrorSample(models.Model):
    time = models.DateTimeField()
    error = models.ForeignKey(RTSMError, related_name='samples', on_delete=models.CASCADE)

    start_frequency = models.FloatField(default=None, null=True)
    stop_frequency = models.FloatField(default=None, null=True)

    bad_spectrum = ArrayField(models.FloatField())
    average_spectrum = ArrayField(models.FloatField())


class RTSMSummaryPlot(models.Model):
    error_summary = models.ForeignKey(RTSMError,
                                      related_name='summary_plot',
                                      on_delete=models.SET_NULL,
                                      null=True)

    uri = models.CharField(max_length=10000, default=None, null=True)

