# - Download and install locally the library using the npm package tool
#  npm_install(project.json PUBLIC public_dir SOURCE source_dir DESTINATION install_dir)
# execute npm build in and install the files in the install_dir
# public_dir is the directory where the static files are stored
# source_dir is the directory where the javascript/css files are stored

##
## NPM/REACT Quickstart tutorial
## The tool create-react-app can be used to create a barebone react html/js project.
## This tool can be installed systemwide with npm executing the command '#npm install -g create-react-app'.
## Then, the barebone project setup can be created in the [project-directory] executing the command
## '$create-react-app [project-directory]'
## The script will create the [project-directory], if it doesnt exist, containing the public and src folder and default package.json file,
## it will download the basic dependencies and will start a git repository in the [project-directory].
## However, the file/directories required are only the public and the src directory and the package.json file.
## PLEASE MAKE SURE TO COPY ONLY THOSE FILES/DIRECTORIES IN THE LOFAR SRC TREE!
## Finally, the cmake file for the project has to contains these two lines to make the LOFAR cmake setup build the project
## 'include(NPMInstall)'
## 'npm_install([package.json file] PUBLIC [public directory] SOURCE [source directory] DESTINATION [install destination folder])'

# Copyright (C) 2008-2009
# ASTRON (Netherlands Foundation for Research in Astronomy)
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands, softwaresupport@astron.nl
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# $Id: PythonInstall.cmake 32905 2015-11-17 15:31:54Z schaap $

find_package(NPM)

function(extract_relative_path OUTPUT_FILE_LIST BASE_PATH ARGC)
    set(OUTPUT "")
    foreach(FILE ${ARGC})
        file(RELATIVE_PATH RELPATH ${BASE_PATH} "${FILE}")
        list(APPEND OUTPUT "${RELPATH}")
    endforeach(FILE)
    set(${OUTPUT_FILE_LIST} ${OUTPUT} PARENT_SCOPE)
endfunction(extract_relative_path)

function(append_basepath_to_file OUTPUT_FILE_LIST BASE_PATH ARGC)
    set(OUTPUT "")
    foreach(FILE ${ARGC})
        set(ITEM "${BASE_PATH}/${FILE}")
        list(APPEND OUTPUT "${ITEM}")
    endforeach(FILE)
    set(${OUTPUT_FILE_LIST} ${OUTPUT} PARENT_SCOPE)
endfunction(append_basepath_to_file)

#
# function npm_install
#
function(npm_install NPM_PACKAGE_SPECIFICATION)
    # Precondition check.
    if(NOT NPM_EXECUTABLE)
        message(FATAL_ERROR "npm_install: cannot find npm")
    endif(NOT NPM_EXECUTABLE)

    # Parsing arguments
    set(one_value_arguments PUBLIC SOURCE DESTINATION)
    ## syntax is cmake_parse_arguments(prefix options one_value_arguments multi_value_arguments arguments_to_be_parsed)
    cmake_parse_arguments(NPM_INSTALL "" "${one_value_arguments}" "" ${ARGN} )


    # Checks if all the required arguments have been specified
    if(NOT NPM_INSTALL_PUBLIC)
        message(FATAL_ERROR "specify public directory.\n ex npm_install(project.json PUBLIC public_dir SOURCE source_dir DESTINATION install_dir)")
    endif(NOT NPM_INSTALL_PUBLIC)

    if(NOT NPM_INSTALL_SOURCE)
        message(FATAL_ERROR "specify source directory.\n ex npm_install(project.json PUBLIC public_dir SOURCE source_dir DESTINATION install_dir)")
    endif(NOT NPM_INSTALL_SOURCE)


    
    string(REPLACE ".json" "-lock.json" JSON_PACKAGE_LOCK_SPECIFICATION "${JSON_PACKAGE_SPECIFICATION}")
    # Compute the full path to the source, and the public directory, and the json spec
    get_filename_component(WEBSITE_PUBLIC_DIR "${NPM_INSTALL_PUBLIC}" REALPATH)
    get_filename_component(WEBSITE_SOURCE_DIR "${NPM_INSTALL_SOURCE}" REALPATH)
    get_filename_component(JSON_PACKAGE_SPECIFICATION "${NPM_PACKAGE_SPECIFICATION}" REALPATH)
    get_filename_component(NPM_BINARY_DIR "${NPM_BINARY_DIR}" REALPATH)
    # Checks if the directories public and source are actually present in the disk
    if(EXISTS WEBSITE_PUBLIC_DIR)
        message(FATAL_ERROR "public directory \"${NPM_INSTALL_PUBLIC}\" cannot be found.")
    endif(EXISTS WEBSITE_PUBLIC_DIR)

    if(EXISTS WEBSITE_SOURCE_DIR)
        message(FATAL_ERROR "source directory \"${NPM_INSTALL_SOURCE}\" cannot be found.")
    endif(EXISTS WEBSITE_SOURCE_DIR)

    get_filename_component(NPM_BINARY_DIR "${CMAKE_CURRENT_BINARY_DIR}" REALPATH)

    file(GLOB_RECURSE SOURCE_FILES_PATH ${WEBSITE_SOURCE_DIR} "${WEBSITE_SOURCE_DIR}/*")
    file(GLOB_RECURSE PUBLIC_FILES_PATH ${WEBSITE_PUBLIC_DIR} "${WEBSITE_PUBLIC_DIR}/*")

    extract_relative_path(SOURCE_FILES ${WEBSITE_SOURCE_DIR} "${SOURCE_FILES_PATH}")
    extract_relative_path(PUBLIC_FILES ${WEBSITE_PUBLIC_DIR} "${PUBLIC_FILES_PATH}")

    append_basepath_to_file(INSTALLED_SOURCE_FILES "${NPM_BINARY_DIR}/src" "${SOURCE_FILES}")
    append_basepath_to_file(INSTALLED_PUBLIC_FILES "${NPM_BINARY_DIR}/public" "${PUBLIC_FILES}")

    add_custom_command(
    OUTPUT "${NPM_BINARY_DIR}/package.json"
    OUTPUT "${NPM_BINARY_DIR}/package-lock.json"
    COMMAND ${CMAKE_COMMAND} -E copy_if_different "${JSON_PACKAGE_SPECIFICATION}" "${NPM_BINARY_DIR}/package.json"
    DEPENDS 
            ${JSON_PACKAGE_LOCK_SPECIFICATION}
    COMMENT "Copying ${JSON_PACKAGE_SPECIFICATION} to ${NPM_BINARY_DIR}/package.json for ${PACKAGE_NAME}")

   
    add_custom_command(
    OUTPUT "${NPM_BINARY_DIR}/package-lock.json"
    COMMAND ${CMAKE_COMMAND} -E copy_if_different "${JSON_PACKAGE_LOCK_SPECIFICATION}" "${NPM_BINARY_DIR}/package-lock.json"
    DEPENDS 
            ${JSON_PACKAGE_LOCK_SPECIFICATION}
    COMMENT "Copying ${JSON_PACKAGE_SPECIFICATION} to ${NPM_BINARY_DIR}/package.json for ${PACKAGE_NAME}")

    foreach(file ${SOURCE_FILES})
        add_custom_command(OUTPUT "${NPM_BINARY_DIR}/src/${file}"
                    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${WEBSITE_SOURCE_DIR}/${file} ${NPM_BINARY_DIR}/src/${file}
                    DEPENDS "${WEBSITE_SOURCE_DIR}/${file}"
                    COMMENT "Copying file from ${WEBSITE_SOURCE_DIR}/${file} to ${NPM_BINARY_DIR}/src/${file}")
    endforeach(file)

    add_custom_command(OUTPUT  "${NPM_BINARY_DIR}/.env"
                    COMMAND ${CMAKE_COMMAND} -E copy_if_different "${CMAKE_CURRENT_SOURCE_DIR}/.env" "${NPM_BINARY_DIR}/.env"
                    DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/.env"
                    COMMENT "Copying enviroment file")

    foreach(file ${PUBLIC_FILES})
        add_custom_command(OUTPUT "${NPM_BINARY_DIR}/public/${file}"
            COMMAND ${CMAKE_COMMAND} -E copy_if_different "${WEBSITE_PUBLIC_DIR}/${file}" "${NPM_BINARY_DIR}/public/${file}"
            DEPENDS "${WEBSITE_PUBLIC_DIR}/${file}"
            COMMENT "Copying file from ${WEBSITE_PUBLIC_DIR}/${file} to ${NPM_BINARY_DIR}/src/${file}")
    endforeach(file)

    add_custom_target(copy_package_json_${PACKAGE_NAME} SOURCES "${JSON_PACKAGE_SPECIFICATION}")
    add_custom_target(download_npm_dependencies_${PACKAGE_NAME} SOURCES "${NPM_BINARY_DIR}/package.json")
    add_custom_target(packing_javascript_files_${PACKAGE_NAME} ALL
    SOURCES  ${INSTALLED_SOURCE_FILES} ${INSTALLED_PUBLIC_FILES} "${NPM_BINARY_DIR}/node_modules" "${NPM_BINARY_DIR}/package-lock.json" ${NPM_BINARY_DIR}/.env)

    add_custom_target(start_development_server_${PACKAGE_NAME}
        SOURCES "${NPM_BINARY_DIR}/node_modules" "${NPM_BINARY_DIR}/package-lock.json" "${NPM_BINARY_DIR}/package.json" ${NPM_BINARY_DIR}/.env
                                                               COMMENT "Start start_development_server for ${PACKAGE_NAME}")


    add_custom_command(
    OUTPUT "${NPM_BINARY_DIR}/src"
    COMMAND ${CMAKE_COMMAND} -E make_directory ${NPM_BINARY_DIR}/src
    COMMENT "Creating javascript src directory in ${NPM_BINARY_DIR}/src for ${PACKAGE_NAME}")

    add_custom_command(
    OUTPUT "${NPM_BINARY_DIR}/public"
    COMMAND ${CMAKE_COMMAND} -E make_directory ${NPM_BINARY_DIR}/public
    COMMENT "Creating public directory ${NPM_BINARY_DIR}/public for ${PACKAGE_NAME}")

    add_custom_command(
    OUTPUT "${NPM_BINARY_DIR}/.env"
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_CURRENT_SOURCE_DIR}/.env ${NPM_BINARY_DIR}/.env
    COMMENT "Copying enviroment file")


    add_custom_command(
    OUTPUT "${NPM_BINARY_DIR}/node_modules" 
    COMMAND NODE_OPTIONS="--max-old-space-size=8192" npm ci 
    DEPENDS "${NPM_BINARY_DIR}/package.json" "${NPM_BINARY_DIR}/package-lock.json"
    WORKING_DIRECTORY "${NPM_BINARY_DIR}"
    COMMENT "Downloading npm dependencies for ${NPM_BINARY_DIR}/package.json with the help of the lock file")

    add_custom_command(
    TARGET start_development_server_${PACKAGE_NAME}
    COMMAND npm start
    DEPENDS ${INSTALLED_SOURCE_FILES} ${INSTALLED_PUBLIC_FILES}
    WORKING_DIRECTORY "${NPM_BINARY_DIR}"
    COMMENT "Starting development server for ${PACKAGE_NAME}")


    add_custom_command(
    TARGET packing_javascript_files_${PACKAGE_NAME}
    COMMAND CI=false npm run build
    DEPENDS "${INSTALLED_SOURCE_FILES}" "${INSTALLED_PUBLIC_FILES}"
    WORKING_DIRECTORY "${NPM_BINARY_DIR}"
    COMMENT "Packing javascript files for ${PACKAGE_NAME} into ${NPM_BINARY_DIR}/build for deployment")

    install(DIRECTORY ${NPM_BINARY_DIR}/build/ DESTINATION ${NPM_INSTALL_DESTINATION})

endfunction(npm_install)
