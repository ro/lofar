import React from 'react';
import {Popover, PopoverHeader, PopoverBody} from 'reactstrap';


/**
 * PopoverWithTitle:
 * Function component that renders a Popover with a title
 *
 * @props:
 * target: [string] id of the HTML element that triggers the popover
 * isOpen: [bool] should the Popover be displayed (true) or hidden (false)
 * togglePopover: [function] function that is called to toggle the popover
 * title: [string] title of the Popover
 * children: [JSX fragment] the body of the Popover
 */
const PopoverWithTitle = ({target, isOpen, togglePopover, title, children}) => {

    if (!isOpen) {
        return null;
    }

    return (
        <Popover placement="auto" isOpen={isOpen} target={target} toggle={togglePopover} key={title}>
            <PopoverHeader>
                {title}
            </PopoverHeader>
            <PopoverBody>
                { children() }
            </PopoverBody>
        </Popover>
    );
}

export default PopoverWithTitle;
