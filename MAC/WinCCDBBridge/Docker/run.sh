#!/bin/bash
set -e

exec /opt/lofar/bin/winccbridge "$@" ${WINCCPROJ}
