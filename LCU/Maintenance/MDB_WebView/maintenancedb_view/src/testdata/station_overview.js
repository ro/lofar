const station_overview_data = [
    {
        "station_name": "CS001C",
        "station_tests": [
            {
                "total_component_errors": 0,
                "start_datetime": "2017-06-07T09:54:00Z",
                "end_datetime": "2017-06-07T10:01:17Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 TV TBC TM",
                "component_error_summary": {}
            }, {
                "total_component_errors": 22,
                "start_datetime": "2017-01-03T14:40:56Z",
                "end_datetime": "2017-01-03T14:49:29Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "TBB": {
                        "MEMORY": 18
                    },
                    "HBA": {
                        "MODEM": 1
                    },
                    "LBH": {
                        "HIGH_NOISE": 1,
                        "SPURIOUS": 1
                    },
                    "LBL": {
                        "SPURIOUS": 1
                    }
                }
            }, {
                "total_component_errors": 1,
                "start_datetime": "2016-11-07T13:27:41Z",
                "end_datetime": "2016-11-07T13:39:21Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "LBH": {
                        "DOWN": 1
                    }
                }
            }
        ],
        "rtsm": [
            {
                "observation_id": 672048,
                "start_datetime": "2018-10-14T19:55:00Z",
                "end_datetime": "2018-10-14T20:15:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }, {
                "observation_id": 671992,
                "start_datetime": "2018-10-14T15:25:00Z",
                "end_datetime": "2018-10-14T15:55:00Z",
                "mode": [5],
                "total_component_errors": 2,
                "error_summary": {
                    "SUMMATOR_NOISE": 1,
                    "LOW_NOISE": 1
                }
            }, {
                "observation_id": 671980,
                "start_datetime": "2018-10-14T13:44:00Z",
                "end_datetime": "2018-10-14T14:04:00Z",
                "mode": [5],
                "total_component_errors": 1,
                "error_summary": {
                    "SUMMATOR_NOISE": 1
                }
            }
        ]
    }, {
        "station_name": "CS002C",
        "station_tests": [
            {
                "total_component_errors": 15,
                "start_datetime": "2014-05-12T12:11:30Z",
                "end_datetime": "2014-05-12T12:29:38Z",
                "checks": "O1 SP1 NS1=180 O3 SP3 NS3=180 M O5 SN SP5 NS5=300",
                "component_error_summary": {
                    "LBH": {
                        "HIGH_NOISE": 5,
                        "RF_FAIL": 5
                    },
                    "HBA": {
                        "HIGH_NOISE": 3,
                        "JITTER": 2
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS004C",
        "station_tests": [
            {
                "total_component_errors": 11,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:24Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 4,
                        "MODEM": 2,
                        "OSCILLATION": 1,
                        "C_SUMMATOR": 1
                    },
                    "LBL": {
                        "LOW_NOISE": 1,
                        "RF_FAIL": 1
                    },
                    "TBB": {
                        "TEMPERATURE": 1
                    }
                }
            }, {
                "total_component_errors": 7,
                "start_datetime": "2018-09-11T10:30:00Z",
                "end_datetime": "2018-09-11T10:57:35Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 2,
                        "JITTER": 1
                    },
                    "LBH": {
                        "JITTER": 1
                    },
                    "LBL": {
                        "LOW_NOISE": 1,
                        "RF_FAIL": 1
                    },
                    "TBB": {
                        "TEMPERATURE": 1
                    }
                }
            }, {
                "total_component_errors": 9,
                "start_datetime": "2018-09-10T20:00:00Z",
                "end_datetime": "2018-09-10T22:54:06Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 2,
                        "MODEM": 2,
                        "RF_FAIL": 1,
                        "JITTER": 1
                    },
                    "LBL": {
                        "HIGH_NOISE": 1,
                        "RF_FAIL": 1
                    },
                    "TBB": {
                        "TEMPERATURE": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS005C",
        "station_tests": [
            {
                "total_component_errors": 3,
                "start_datetime": "2016-11-07T13:27:41Z",
                "end_datetime": "2016-11-07T13:39:46Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "MODEM": 2
                    },
                    "LBL": {
                        "OSCILLATION": 1
                    }
                }
            }, {
                "total_component_errors": 5,
                "start_datetime": "2014-05-12T12:11:30Z",
                "end_datetime": "2014-05-12T12:29:43Z",
                "checks": "O1 SP1 NS1=180 O3 SP3 NS3=180 M O5 SN SP5 NS5=300",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 4
                    },
                    "LBH": {
                        "RF_FAIL": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS006C",
        "station_tests": [
            {
                "total_component_errors": 15,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:46:26Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "JITTER": 4,
                        "MODEM": 3,
                        "RF_FAIL": 2,
                        "HIGH_NOISE": 2
                    },
                    "LBH": {
                        "RF_FAIL": 2,
                        "HIGH_NOISE": 1
                    },
                    "TBB": {
                        "TEMPERATURE": 1
                    }
                }
            }, {
                "total_component_errors": 8,
                "start_datetime": "2018-09-11T10:30:00Z",
                "end_datetime": "2018-09-11T10:57:37Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "MODEM": 3,
                        "HIGH_NOISE": 2
                    },
                    "LBH": {
                        "RF_FAIL": 2
                    },
                    "TBB": {
                        "TEMPERATURE": 1
                    }
                }
            }, {
                "total_component_errors": 16,
                "start_datetime": "2018-09-10T20:00:00Z",
                "end_datetime": "2018-09-10T22:57:35Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "RF_FAIL": 4,
                        "MODEM": 4,
                        "HIGH_NOISE": 3,
                        "JITTER": 1
                    },
                    "LBH": {
                        "RF_FAIL": 2,
                        "OSCILLATION": 1
                    },
                    "TBB": {
                        "TEMPERATURE": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS007C",
        "station_tests": [
            {
                "total_component_errors": 5,
                "start_datetime": "2015-09-03T08:20:00Z",
                "end_datetime": "2015-09-03T09:28:52Z",
                "checks": "RV SPU RBV SH1 F1 D1 O1 SP1 NS1=120 S1 SH3 F3 D3 O3 SP3 NS3=120 S3 M5 O5 SN5 SP5 NS5=120 S7 E7 TV TM",
                "component_error_summary": {
                    "SPU": {
                        "VOLTAGE": 3
                    },
                    "HBA": {
                        "HIGH_NOISE": 1
                    },
                    "LBL": {
                        "RF_FAIL": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS011C",
        "station_tests": [
            {
                "total_component_errors": 8,
                "start_datetime": "2017-01-03T14:40:56Z",
                "end_datetime": "2017-01-03T14:49:59Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "TBB": {
                        "MEMORY": 6,
                        "VERSION": 1,
                        "VOLTAGE": 1
                    }
                }
            }, {
                "total_component_errors": 5,
                "start_datetime": "2015-09-03T08:20:00Z",
                "end_datetime": "2015-09-03T09:37:26Z",
                "checks": "RV SPU RBV SH1 F1 D1 O1 SP1 NS1=120 S1 SH3 F3 D3 O3 SP3 NS3=120 S3 M5 O5 SN5 SP5 NS5=120 S7 E7 TV TM",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 3,
                        "SUMMATOR_NOISE": 1
                    },
                    "RSP": {
                        "VOLTAGE": 1
                    }
                }
            }, {
                "total_component_errors": 2,
                "start_datetime": "2014-05-12T12:11:30Z",
                "end_datetime": "2014-05-12T12:29:34Z",
                "checks": "O1 SP1 NS1=180 O3 SP3 NS3=180 M O5 SN SP5 NS5=300",
                "component_error_summary": {
                    "LBH": {
                        "HIGH_NOISE": 1,
                        "RF_FAIL": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS013C",
        "station_tests": [
            {
                "total_component_errors": 3,
                "start_datetime": "2016-11-07T13:27:41Z",
                "end_datetime": "2016-11-07T13:37:43Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "SUMMATOR_NOISE": 1
                    },
                    "LBH": {
                        "DOWN": 1,
                        "SPURIOUS": 1
                    }
                }
            }, {
                "total_component_errors": 6,
                "start_datetime": "2015-08-20T19:53:48Z",
                "end_datetime": "2015-08-20T20:05:12Z",
                "checks": "RV SH1 F1 D1 O1 SP1 NS1=120 S1 SH3 F3 D3 O3 SP3 NS3=120 S3 M5 O5 SN5 SP5 NS5=120 S5",
                "component_error_summary": {
                    "LBH": {
                        "OSCILLATION": 3,
                        "HIGH_NOISE": 1,
                        "SPURIOUS": 1
                    },
                    "LBL": {
                        "DOWN": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS017C",
        "station_tests": [
            {
                "total_component_errors": 6,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:32Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "RF_FAIL": 3,
                        "HIGH_NOISE": 1,
                        "SPURIOUS": 1
                    },
                    "LBH": {
                        "LOW_NOISE": 1
                    }
                }
            }, {
                "total_component_errors": 2,
                "start_datetime": "2018-09-11T10:30:00Z",
                "end_datetime": "2018-09-11T10:57:21Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "LBH": {
                        "JITTER": 1,
                        "LOW_NOISE": 1
                    }
                }
            }, {
                "total_component_errors": 5,
                "start_datetime": "2018-09-10T20:00:00Z",
                "end_datetime": "2018-09-10T22:57:33Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "RF_FAIL": 3
                    },
                    "LBH": {
                        "HIGH_NOISE": 1,
                        "LOW_NOISE": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS024C",
        "station_tests": [
            {
                "total_component_errors": 5,
                "start_datetime": "2015-09-03T08:20:00Z",
                "end_datetime": "2015-09-03T09:28:53Z",
                "checks": "RV SPU RBV SH1 F1 D1 O1 SP1 NS1=120 S1 SH3 F3 D3 O3 SP3 NS3=120 S3 M5 O5 SN5 SP5 NS5=120 S7 E7 TV TM",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 2,
                        "RF_FAIL": 2
                    },
                    "LBH": {
                        "OSCILLATION": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS028C",
        "station_tests": [
            {
                "total_component_errors": 9,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:21Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "RF_FAIL": 3,
                        "HIGH_NOISE": 1,
                        "OSCILLATION": 1
                    },
                    "LBH": {
                        "DOWN": 2
                    },
                    "LBL": {
                        "DOWN": 2
                    }
                }
            }, {
                "total_component_errors": 11,
                "start_datetime": "2018-09-11T10:30:00Z",
                "end_datetime": "2018-09-11T10:57:28Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "LBH": {
                        "RF_FAIL": 3,
                        "DOWN": 2,
                        "LOW_NOISE": 1
                    },
                    "LBL": {
                        "DOWN": 2,
                        "RF_FAIL": 1
                    },
                    "HBA": {
                        "OSCILLATION": 1,
                        "C_SUMMATOR": 1
                    }
                }
            }, {
                "total_component_errors": 9,
                "start_datetime": "2018-09-10T20:00:00Z",
                "end_datetime": "2018-09-10T22:50:54Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "RF_FAIL": 3,
                        "OSCILLATION": 1,
                        "SPURIOUS": 1
                    },
                    "LBL": {
                        "DOWN": 2
                    },
                    "LBH": {
                        "DOWN": 1,
                        "LOW_NOISE": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS032C",
        "station_tests": [
            {
                "total_component_errors": 5,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:46:02Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "MODEM": 2,
                        "JITTER": 1,
                        "RF_FAIL": 1
                    },
                    "LBH": {
                        "JITTER": 1
                    }
                }
            }, {
                "total_component_errors": 2,
                "start_datetime": "2018-09-11T10:30:00Z",
                "end_datetime": "2018-09-11T10:57:29Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 1,
                        "MODEM": 1
                    }
                }
            }, {
                "total_component_errors": 9,
                "start_datetime": "2018-08-30T03:46:00Z",
                "end_datetime": "2018-08-30T06:42:28Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 3,
                        "JITTER": 2,
                        "MODEM": 2,
                        "RF_FAIL": 1
                    },
                    "LBH": {
                        "DOWN": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS103C",
        "station_tests": [
            {
                "total_component_errors": 17,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:23Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "LBH": {
                        "DOWN": 3,
                        "RF_FAIL": 2,
                        "FLAT": 1
                    },
                    "HBA": {
                        "RF_FAIL": 3,
                        "MODEM": 1,
                        "C_SUMMATOR": 1
                    },
                    "LBL": {
                        "DOWN": 2,
                        "FLAT": 2,
                        "RF_FAIL": 1,
                        "LOW_NOISE": 1
                    }
                }
            }, {
                "total_component_errors": 4,
                "start_datetime": "2018-09-11T10:30:00Z",
                "end_datetime": "2018-09-11T10:57:31Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "SUMMATOR_NOISE": 1
                    },
                    "LBH": {
                        "FLAT": 1
                    },
                    "LBL": {
                        "FLAT": 1,
                        "LOW_NOISE": 1
                    }
                }
            }, {
                "total_component_errors": 8,
                "start_datetime": "2018-09-10T20:00:00Z",
                "end_datetime": "2018-09-10T22:57:23Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "RF_FAIL": 3,
                        "HIGH_NOISE": 2
                    },
                    "LBH": {
                        "FLAT": 1
                    },
                    "LBL": {
                        "FLAT": 1,
                        "LOW_NOISE": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS201C",
        "station_tests": [
            {
                "total_component_errors": 14,
                "start_datetime": "2018-09-10T20:00:00Z",
                "end_datetime": "2018-09-10T22:57:38Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "LBH": {
                        "RF_FAIL": 2,
                        "SPURIOUS": 2,
                        "OSCILLATION": 2,
                        "DOWN": 1,
                        "JITTER": 1
                    },
                    "HBA": {
                        "RF_FAIL": 2,
                        "C_SUMMATOR": 1,
                        "MODEM": 1
                    },
                    "LBL": {
                        "OSCILLATION": 1,
                        "RF_FAIL": 1
                    }
                }
            }, {
                "total_component_errors": 9,
                "start_datetime": "2018-08-30T03:46:00Z",
                "end_datetime": "2018-08-30T06:42:17Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "LBL": {
                        "DOWN": 4
                    },
                    "HBA": {
                        "RF_FAIL": 2,
                        "C_SUMMATOR": 1
                    },
                    "LBH": {
                        "RF_FAIL": 2
                    }
                }
            }, {
                "total_component_errors": 7,
                "start_datetime": "2018-08-14T00:15:00Z",
                "end_datetime": "2018-08-14T03:42:28Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "LBH": {
                        "RF_FAIL": 2
                    },
                    "LBL": {
                        "DOWN": 2,
                        "RF_FAIL": 1
                    },
                    "HBA": {
                        "C_SUMMATOR": 1,
                        "RF_FAIL": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS301C",
        "station_tests": [
            {
                "total_component_errors": 33,
                "start_datetime": "2017-01-03T14:40:56Z",
                "end_datetime": "2017-01-03T14:49:32Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "TBB": {
                        "MEMORY": 18
                    },
                    "HBA": {
                        "MODEM": 7,
                        "C_SUMMATOR": 2,
                        "JITTER": 1,
                        "SPURIOUS": 1,
                        "HIGH_NOISE": 1
                    },
                    "LBH": {
                        "JITTER": 1
                    },
                    "LBL": {
                        "HIGH_NOISE": 1,
                        "SPURIOUS": 1
                    }
                }
            }, {
                "total_component_errors": 3,
                "start_datetime": "2016-11-07T13:27:41Z",
                "end_datetime": "2016-11-07T13:39:22Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 1,
                        "MODEM": 1
                    },
                    "TBB": {
                        "TEMPERATURE": 1
                    }
                }
            }, {
                "total_component_errors": 19,
                "start_datetime": "2015-09-03T08:20:00Z",
                "end_datetime": "2015-09-03T09:37:46Z",
                "checks": "RV SPU RBV SH1 F1 D1 O1 SP1 NS1=120 S1 SH3 F3 D3 O3 SP3 NS3=120 S3 M5 O5 SN5 SP5 NS5=120 S7 E7 TV TM",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 14,
                        "RF_FAIL": 2,
                        "C_SUMMATOR": 1,
                        "JITTER": 1
                    },
                    "LBH": {
                        "FLAT": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS302C",
        "station_tests": [
            {
                "total_component_errors": 26,
                "start_datetime": "2018-10-10T20:45:00Z",
                "end_datetime": "2018-10-11T00:52:17Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "RF_FAIL": 9,
                        "MODEM": 7,
                        "HIGH_NOISE": 3,
                        "SUMMATOR_NOISE": 3,
                        "JITTER": 2,
                        "SPURIOUS": 1
                    },
                    "LBL": {
                        "FLAT": 1
                    }
                }
            }, {
                "total_component_errors": 17,
                "start_datetime": "2018-09-11T10:30:00Z",
                "end_datetime": "2018-09-11T10:57:26Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "SUMMATOR_NOISE": 6,
                        "HIGH_NOISE": 3,
                        "MODEM": 3,
                        "C_SUMMATOR": 1
                    },
                    "LBH": {
                        "DOWN": 2
                    },
                    "LBL": {
                        "DOWN": 1,
                        "FLAT": 1
                    }
                }
            }, {
                "total_component_errors": 30,
                "start_datetime": "2018-09-10T20:00:00Z",
                "end_datetime": "2018-09-10T22:50:23Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=60 S1 SH3 F3 D3 O3 SP3 NS3=60 S3 M5 O5 SN5 SP5 NS5=60 E7 TV TBC TM",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 9,
                        "RF_FAIL": 8,
                        "MODEM": 4,
                        "SUMMATOR_NOISE": 3,
                        "JITTER": 2,
                        "C_SUMMATOR": 1
                    },
                    "LBH": {
                        "DOWN": 2
                    },
                    "LBL": {
                        "FLAT": 1
                    }
                }
            }
        ],
        "rtsm": []
    }, {
        "station_name": "CS401C",
        "station_tests": [
            {
                "total_component_errors": 19,
                "start_datetime": "2015-09-03T08:20:00Z",
                "end_datetime": "2015-09-03T09:37:40Z",
                "checks": "RV SPU RBC SH1 F1 D1 O1 SP1 NS1=120 S1 SH3 F3 D3 O3 SP3 NS3=120 S3 M5 O5 SN5 SP5 NS5=120 S7 E7 TV TM",
                "component_error_summary": {
                    "HBA": {
                        "HIGH_NOISE": 8,
                        "SUMMATOR_NOISE": 2,
                        "C_SUMMATOR": 1,
                        "MODEM": 1,
                        "RF_FAIL": 1
                    },
                    "SPU": {
                        "VOLTAGE": 3,
                        "TEMPERATURE": 2
                    },
                    "LBH": {
                        "FLAT": 1
                    }
                }
            }
        ],
        "rtsm": []
    }
];


export default station_overview_data;
