#ifndef PROGRAM_H
#define PROGRAM_H

#include <QObject>
#include <QSqlDatabase>

#include <WinCCWrapper.h>
#include <memory>

#include <map>
#include <QString>

#include "DatapointRule.hpp"

class QSettings;

class Program : public QObject
{
    Q_OBJECT

public:
    Program(QObject * parent, const QString& configurationFile, const QString & projectName);
    void loadDatapointsConfiguration(QString configurationFile);
    virtual ~Program();
    void run();

public slots:
    void consolePrint(std::map<std::string, std::string> datapoints); //!< Prints a log message for each changed datapoint name
    void messageDispatch(std::map<std::string, std::string> datapoints); //!< Send the datapoint change value into the database
    void checkpointRequested(DatapointRule *); //! < React when a rule triggers a checkpoint request

signals:
    void datapointChanged(std::map<std::string, std::string> datapoints); //!< This event is triggered when a registered datapoint value changes
    void startRulesCheckpointTriggers();

private:
    std::shared_ptr<LOFAR::WINCCWRAPPER::WinCCWrapper> wrapper; //!< Pointer to an instance of the WinCCWrapper

    void readConfigurationFile(QString fileName); //!< Read the configuration file
    void readDBConfigurationSection(QSettings &); //!< Read the database section of the configuration file

    void startWorkers(int num);
    void connectToDatapoints(); //! Ask WinCC to get an update every time one of the specified datapoint changes
    std::map<std::string, DatapointRule *> uriToRule;
    std::vector<std::shared_ptr<DatapointRule>> dataPointRules;
    std::shared_ptr<QSqlDatabase> database;

};

#endif // PROGRAM_H
