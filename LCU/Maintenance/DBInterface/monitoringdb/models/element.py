from django.db import models
from .component import Component


class Element(models.Model):
    component = models.ForeignKey(Component, on_delete=models.DO_NOTHING, related_name='elements')
    element_id = models.PositiveIntegerField()

    class Meta:
        unique_together = ('component', 'element_id')
