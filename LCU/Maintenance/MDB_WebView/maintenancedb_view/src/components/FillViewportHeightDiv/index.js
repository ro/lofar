import React from "react";

/**
 * FillViewportHeightDiv: Creates a div that fills the remaining height of the viewport.
 * Window resize is monitored to scale the div accordingly.
 *
 * This component uses the 'render prop' pattern where the 'children' prop is used
 * as the render function. The height of the div (in an object) is passed to
 * the function for use by subcomponents. E.g.:
 *
 * @prop {string} className - Class applied to the container div
 * @prop {integer} gutterBottom - Bottom margin for the container div
 * @prop {integer} minHeight - Mininum height of container div
 * @prop {jsx} children - Content of container div
 *
 * @example
 * <FillViewportHeightDiv>
 *     { (props) => <MyComponent hgt={props.height} /> }
 * </FillViewportHeightDiv>
 */
class FillViewportHeightDiv extends React.Component {

    static defaultProps = {
        className: 'fill-height-container',  // classname for the container div
        gutterBottom: 10,  // px, leave some space at the bottom
        minHeight: 200     // px, minimum height
    };

    state = {
        height: 500        // px, height of the container div
    };

    node = null;           // internal ref to the DOM node (div)
    mounted = false;       // bool
    resizeTimeout = null;  // throttling of render during resize


    componentDidMount() {
        this.mounted = true;

        window.addEventListener("resize", () => {
            window.clearTimeout(this.resizeTimeout);
            this.resizeTimeout = window.setTimeout(this.onWindowResize, 200);
        });

        // force a render now that it has been mounted
        // (the offset is known at this point)
        this.onWindowResize();
    }

    componentWillUnmount() {
        this.mounted = false;
        window.removeEventListener("resize", this.onWindowResize);
    }

    onWindowResize = () => {
        if (!this.mounted || !this.node) {
            return;
        }

        let offset = this.node.getBoundingClientRect().top;
        let height = window.innerHeight - offset - this.props.gutterBottom;
        if (height < this.props.minHeight) {
            height = this.props.minHeight;
        }

        this.setState({
            height
        });
    };

    getNodeRef = (node) => {
        this.node = node;
    }

    render() {
        // Create a simple div for measuring the viewport offset when it mounts,
        // then re-render.
        if (!this.mounted) {
            return (
                <div ref={this.getNodeRef} />
            );
        }

        // Render prop pattern; pass height to child render function
        return (
            <div ref={this.getNodeRef} className={this.props.className} style={{height: this.state.height+'px'}}>
                { this.props.children({height: this.state.height}) }
            </div>
        );
    }
};

export default FillViewportHeightDiv;
