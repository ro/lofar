#!/usr/bin/env python3

import time
import json

from lofar.mac.PipelineControl import *
from lofar.messaging import ServiceMessageHandler, TemporaryQueue, RPCService, EventMessage,\
    TemporaryExchange, BusListenerJanitor

import subprocess
import unittest
from unittest.mock import patch, MagicMock, call
import datetime
from lofar.common.test_utils import integration_test, unit_test

import logging
logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)


def setUpModule():
    pass


def tearDownModule():
    pass


@integration_test
class TestRunCommand(unittest.TestCase):
    def test_basic(self):
        """ Test whether we can run a trivial command. """
        runCommand("true")

    def test_invalid_command(self):
        """ Test whether an invalid command produces an error. """
        with self.assertRaises(subprocess.CalledProcessError):
            runCommand("invalid-command")

    def test_shell(self):
        """ Test whether the command is parsed by a shell. """
        runCommand("true --version")

    def test_output(self):
        """ Test whether we catch the command output correctly. """
        output = runCommand("echo yes")
        self.assertEqual(output, "yes")

    def test_input(self):
        """ Test whether we can provide input. """
        output = runCommand("cat -", "yes")
        self.assertEqual(output, "yes")


@unit_test
class TestPipelineControlTMSSClassMethods(unittest.TestCase):
    def test_shouldHandle(self):
        """ Test whether we filter the right OTDB trees. """

        logger.warning('TEST_SHOULDHANDLE')

        trials = [{"type": "Observation", "cluster": "CEP2", "shouldHandle": False},
                  {"type": "Observation", "cluster": "CEP4", "shouldHandle": False},
                  {"type": "Observation", "cluster": "foo", "shouldHandle": False},
                  {"type": "Observation", "cluster": "", "shouldHandle": False},
                  {"type": "Pipeline", "cluster": "CEP2", "shouldHandle": False},
                  {"type": "Pipeline", "cluster": "CEP4", "shouldHandle": True},
                  {"type": "Pipeline", "cluster": "foo", "shouldHandle": True},
                  {"type": "Pipeline", "cluster": "", "shouldHandle": False},
                  ]

        for t in trials:
            parset = {"ObsSW.Observation.processType": t["type"],
                      "ObsSW.Observation.Cluster.ProcessingCluster.clusterName": t["cluster"]}
            self.assertEqual(PipelineControlTMSSHandler._shouldHandle(Parset(parset)), t["shouldHandle"])

        logger.warning('END TEST_SHOULDHANDLE')


@integration_test
class TestPipelineControlTMSS(unittest.TestCase):
    # TODO: write similar test as t_qaservice, which integrates tmss_test_env and (in this case) pipelinecontrol
    pass

if __name__ == "__main__":
    unittest.main()
