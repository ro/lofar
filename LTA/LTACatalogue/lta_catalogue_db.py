#!/usr/bin/env python3

# Copyright (C) 2012-2015  ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

# $Id$

'''
Module with nice postgres helper methods and classes.
'''

import logging
from datetime import datetime, timedelta
from lofar.common.dbcredentials import DBCredentials
from lofar.common.oracle import OracleDatabaseConnection, FETCH_NONE, FETCH_ONE, FETCH_ALL
import cx_Oracle

logger = logging.getLogger(__name__)


class LTACatalogueDatabaseConnection(OracleDatabaseConnection):
    '''The LTACatalogueDatabaseConnection is a simple API to query a very limited subset of the full API provided by astrowise.
    It is intended to be used by some lofar services, and not as a replacement for astrowise.
    It is also highly discouraged to use this connection object to tinker with the oracle database yourself, unless you really know what you're doing.'''

    DEFAULT_RELEASE_DATE = datetime(2050,1,1)

    def get_projects(self):
        return self.executeQuery("SELECT * FROM awoper.aweprojects", fetch=FETCH_ALL)

    def get_project_release_date(self, project_name:str) -> datetime:
        return self.executeQuery("SELECT RELEASEDATE FROM awoper.aweprojects where NAME=%s", qargs=(project_name,), fetch=FETCH_ONE)['RELEASEDATE']

    def get_resources(self) -> []:
        return self.executeQuery("SELECT * FROM AWOPER.AWERESOURCES_VIEW", fetch=FETCH_ALL)

    def set_project_release_date(self, project_name:str, release_date: datetime):
        # we update the release date as if we were the IDM system
        # thus, we insert new release date into the lofaridm.project table where it will be picked up by crontabbed the AWOPER.SYNCFROMIDM procedure
        # only projects with a default release date of 2050,1,1 are updated in this SYNCFROMIDM procedure, so set it to default first.
        # after the new release date has been sync'ed and updated to awoper.aweprojects in AWOPER.SYNCFROMIDM, the new release date will be
        # applied to all its observations/pipelines in another 'crontab'/scheduled job 'AWOPER.SYNC_RELEASE_DATE_CHANGES',
        # so mind you... Changes in the LTA web UI do not take immediate effect
        self.executeQuery("UPDATE awoper.aweprojects SET RELEASEDATE=%s WHERE NAME=%s", qargs=(self.DEFAULT_RELEASE_DATE, project_name), fetch=FETCH_NONE)
        self.executeQuery("UPDATE lofaridm.project SET RELEASEDATE=%s WHERE NAME=%s", qargs=(release_date, project_name), fetch=FETCH_NONE)
        self.commit()

    def create_project(self, project_name:str, description: str, release_date: datetime=None):
        # create a new project in the LTA as if we were the IDM system
        # thus, we insert new project into the lofaridm.project table where it will be picked up by crontabbed the AWOPER.SYNCFROMIDM procedure
        # so mind you... Changes in the LTA web UI do not take immediate effect
        # TODO:  at this moment we do not set a PI and/or COI user. Add these when TMSS has/uses a user autorization system.
        # raises if the project already exists.
        if release_date is None:
            release_date = self.DEFAULT_RELEASE_DATE
        self.executeQuery("INSERT INTO lofaridm.project (name, description, releasedate) VALUES (%s, %s, %s)", qargs=(project_name, description, release_date), fetch=FETCH_NONE)
        self.commit()

    def add_project_storage_resource(self, project_name:str, nr_of_bytes: int, uri: str, remove_existing_resources: bool=False):
        # add a new primary storage resource in bytes to the given project.  a new project in the LTA as if we were the IDM system
        # The URI is usually given as srm://<LTA_SITE_HOST:PORT>/lofar/ops/projects/<PROJECT_NAME_IN_LOWERCASE>/
        # if remove_existing_resources==True then all existing resources for this given project are remove (and replaced by this new resource)
        # thus, we insert new project into the lofaridm.project table where it will be picked up by crontabbed the AWOPER.SYNCFROMIDM procedure
        # so mind you... Changes in the LTA web UI do not take immediate effect
        # raises if the project wit <project_name> does not exist.
        project_id = self.executeQuery("SELECT id FROM lofaridm.project WHERE NAME=%s", qargs=(project_name,), fetch=FETCH_ONE)['ID']

        if remove_existing_resources:
            self.executeQuery("DELETE FROM lofaridm.resource$ WHERE id in (SELECT idr FROM lofaridm.resource_project WHERE idp=%s)", qargs=(project_id,), fetch=FETCH_NONE)

        resource_name = "lta_storage_for_%s_%s" % (project_name, datetime.utcnow().isoformat())
        self.executeQuery("INSERT INTO lofaridm.resource$ (NAME, TYPE, UNIT, CATEGORY, ALLOCATION, URI) VALUES (%s, %s, %s, %s, %s, %s)",
                          qargs=(resource_name, "LTA_STORAGE", "B", "Primary", nr_of_bytes, uri), fetch=FETCH_NONE)
        new_resource_id = self.executeQuery("SELECT id FROM lofaridm.resource$ WHERE NAME=%s", qargs=(resource_name,), fetch=FETCH_ONE)['ID']

        self.executeQuery("INSERT INTO lofaridm.resource_project (IDR, IDP) VALUES (%s, %s)", qargs=(new_resource_id, project_id), fetch=FETCH_NONE)

        self.commit()


if __name__ == '__main__':
    logging.basicConfig(format = '%(asctime)s %(levelname)s %(message)s', level = logging.INFO)

    dbcreds = DBCredentials().get('LTA')
    print(dbcreds.stringWithHiddenPassword())

    with LTACatalogueDatabaseConnection(dbcreds=dbcreds) as db:
        from pprint import pprint

        # pprint(db.create_project("Commissioning_TMSS", "A commissioning project for the TMSS project"))
        db.add_project_storage_resource("Commissioning_TMSS", 1, "my_uri", remove_existing_resources=True)

        pprint(db.get_projects())
        pprint(db.get_resources())

