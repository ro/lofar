import React, { Component } from 'react';
import * as moment from 'moment';
import { date_format, time_format } from 'utils/constants'


/**
 * StationTestRow
 * Class to render the row for a station test.
 */
class StationTestRow extends Component {

    renderStartDate() {
        return this.props.date ? moment.utc(this.props.date).format(date_format) : "";
    }

    renderStartTime() {
        return this.props.data.start_datetime ? moment.utc(this.props.data.start_datetime).format(time_format) : "";
    }

    render() {
        let props = this.props,
            component = props.component,
            errors = props.data.component_error_summary[component] || {},
            cols = [];

        props.errorTypes.forEach((type) => {
            cols.push(<td key={type}>{ errors[type] }</td>);
        });

        return (
            <tr>
                <th scope="row" className="nowrap">{ this.renderStartDate() }</th>
                <th>{ props.time }</th>
                <th>{ props.station }</th>
                <td>{ component }</td>
                { cols }
            </tr>
        );
    }
}


export default StationTestRow;
