import React, {Component} from 'react';
import {
    Nav,
    NavItem,
    Input
} from 'reactstrap';
import {connect} from 'react-redux';
import {setStationStatisticsTestType, setStationStatisticsAveragingWindow} from 'redux/actions/landingPageActions'

import styles from './styles.module.scss'


/**
 * Toolbar
 * Toolbar for the station statistics component providing graphing options. 
 */
class ToolbarC extends Component {

    state = {
        isNavbarCollapsed: true
    }

    setAveragingWindow = (e) => {
        this.props.setStationStatisticsAveragingWindow(e.target.value);
    }

    setTestType = (e) => {
        this.props.setStationStatisticsTestType(e.target.value);
    }

    toggle = () => {
        this.setState({
            isNavbarCollapsed: !this.state.isNavbarCollapsed
        });
    }

    render() {
        return (
            <Nav className={styles.toolbar + " ml-auto"}>
                <NavItem>
                    <Input  type="select"
                            className="form-control custom-select custom-select-sm"
                            id="selected-group"
                            value={this.props.test_type}
                            onChange={this.setTestType}>
                        <option value="B">Both test types</option>
                        <option value="R">RTSM only</option>
                        <option value="S">StationTest only</option>
                    </Input>
                </NavItem>
                <NavItem>
                    <Input type="select"
                           className="form-control custom-select custom-select-sm"
                           onChange={this.setAveragingWindow}
                           value={this.props.averaging_window}>
                        <option value={1}>day</option>
                        <option value={7}>week</option>
                        <option value={30}>month</option>
                    </Input>
                </NavItem>
                <NavItem>
                    <Input type="select"
                           className="form-control custom-select custom-select-sm"
                           onChange={this.props.switchHistogramEvent}
                           value={this.props.histogramType}>
                        <option value="per_error_type">per error type</option>
                        <option value="per_station">per station</option>
                    </Input>
                </NavItem>
            </Nav>);
    }
}

const mapStateToPropsToolBar = state => {
    return {
        ...state.landing_page.station_statistics
    };
};

const mapDispatchToPropsToolBar = {
    setStationStatisticsAveragingWindow,
    setStationStatisticsTestType
};

const Toolbar = connect(mapStateToPropsToolBar, mapDispatchToPropsToolBar)(ToolbarC);

export default Toolbar;
