import React, {Component} from 'react';
import {connect} from "react-redux";
import {unpinChildPanelData} from 'redux/actions/stationOverviewPageActions'
import {Badge, Button, Table} from 'reactstrap';
import {IoMdCloseCircleOutline as CloseIcon} from 'react-icons/io';
import EnlargeableImage from 'components/EnlargeableImage'
import {componentStatusOK, LOFARTESTS} from 'utils/LOFARDefinitions';
import {datetime_format} from 'utils/constants'
import {formatDateUTC} from 'utils/utils'

import moment from 'moment';


// CSS
import styles from './styles.module.scss';


/**
 * UnpinButton: simple component to optionally display an unpin button
 */
function UnpinButton({doDisplay, onClick}) {
    return ( !doDisplay ? null :
        <Button title="Click to unpin the error details"
                color="secondary"
                size="xs"
                className={styles.unpin_button}
                onClick={onClick}>
            <CloseIcon/>
            &nbsp;unpin
        </Button>
    );
}


/**
 * ChildViewContainer: main structure for child view panel
 */
function ChildViewContainer({title, isPinned, onUnpin, children}) {
    return <div className={styles.child_view_container}>
                <div className={styles.header}>
                    {title}
                    <UnpinButton doDisplay={isPinned} onClick={onUnpin}/>
                </div>
                {children}
           </div>;
}


function StatusRow({data}) {
    let color = componentStatusOK(data.status) ? 'success' : 'danger';

    return (
        <tr>
            <th scope="row">
                Component status
                 <Table borderless className={styles.details_table}>
                    <tbody>
                        <tr><td>Status:</td><td><Badge color={color}>{data.status}</Badge></td></tr>
                        <tr><td>Code:</td><td>{data.status_code}</td></tr>
                        <tr><td>Modified:</td><td>{moment.utc(data.last_modified).format(datetime_format)}</td></tr>
                        <tr><td>Reason:</td><td>{data.reason}</td></tr>
                    </tbody>
                </Table>
            </th>
            <td>

            </td>
        </tr>
    );
}


/**
 * ErrorDetailRow: render a row with the eror details and optional image.
 * TODO: document the data structure, rendering depends on it..
 */
function ErrorDetailRow({data, rowkey}) {
    let pol = data['polarization'];
    let rcuId = data['rcu_id'];
    let err_items = [],
        ignore = {
            start_frequency: 1,
            stop_frequency: 1,
            polarization: 1,
            rcu_id: 1,
            antenna_id: 1,
            url: 1
        },
        img = null;

    // first process frequency range
    if (data.details.hasOwnProperty("start_frequency")) {
        err_items.push(<li key="freq">frequency-range: {data.details.start_frequency}-{data.details.stop_frequency} MHz</li>);
    }

    // ...then process remaining items that are not in 'ignore'
    Object.keys(data.details).forEach((parameter, key) =>{
        if (! ignore.hasOwnProperty(parameter)) {
            const parameter_value = data.details[parameter];
            let rendered_parameter;
            if(parameter === 'element_errors'){
                let rendered_elements = parameter_value.map((element_error_data) => <li key={element_error_data.element.element_id}> {element_error_data.element.element_id}: {element_error_data.type}</li>)
                rendered_parameter = <ul>{rendered_elements}</ul>
            }else{
                rendered_parameter = parameter === 'percentage'? parameter_value.toFixed(2) + '%' : parameter_value;
            }
            err_items.push(<li key={parameter}>{parameter}: {rendered_parameter}</li>);
        }});

    // No error details? Must be element error see tiles page.
    if (err_items.length === 0) {
        err_items.push(<li key="default"><em>See element error.</em></li>);
    }

    if (data.details.url) {
        img = <EnlargeableImage url={data.details.url} />
    }

    return (
        <tr>
            {rcuId !== undefined &&
                <th scope="row">
                    RCU {rcuId} ({pol}){img}
                </th>
            }
            <td>
                <ul className={styles.details_list}>
                    <li><Badge className='error-type-badge' color="danger">{data.error_type}</Badge></li>
                    {err_items}
                </ul>
            </td>
        </tr>
    );
}

/**
 * StationTestChildView: controller for child panel
 */
class StationTestChildViewC extends Component {

    unpinPanel = () => {
        this.props.unpinChildPanelData();
    };

    composeTitle(data) {
        let test_type = LOFARTESTS[data.test_type];
        let dateStr
        if (data.start_date !== null) {
            dateStr = test_type + " " + formatDateUTC(data.start_date)
        } else {
            dateStr = 'Current status'
        }

        return `${dateStr}, ${data.component_type}, Antenna ${data.component_id}`
    }


    render() {
        let rows = [],
            data = this.props.data;

        // Return default message when no data
        if (data === null) {
            return (
                <ChildViewContainer title="Error details" isPinned={this.props.isPinned} onUnpin={this.unpinPanel}>
                    <i>Hover the mouse over an error to view the details. Right-click on the error to pin it on this panel.</i>
                </ChildViewContainer>
            )
        }

        if (data.status && !componentStatusOK(data.status.status)) {
            rows.push(<StatusRow data={data.status} key="comp_status" />);
        }

        // data.errors is either array or object for error data (HIGH_NOISE, MODEM, ...)
        if (data.errors) {
            for (const idx of Object.keys(data.errors)){
                let edata = data.errors[idx];
                const hasX = edata.hasOwnProperty('X');
                const hasY = edata.hasOwnProperty('Y');
                if (hasX) {
                    rows.push(<ErrorDetailRow data={edata['X']} key={edata.error_type+'X'} />)
                }
                if (hasY) {
                    rows.push(<ErrorDetailRow data={edata['Y']} key={edata.error_type+'Y'} />)
                }
                if (! hasX && ! hasY) {
                    rows.push(<ErrorDetailRow data={edata} key={edata.error_type} />)
                }
            }
        }

        return (
            <ChildViewContainer title={this.composeTitle(data)}
                                isPinned={this.props.isPinned}
                                onUnpin={this.unpinPanel}>
                <Table size='sm'>
                    <tbody>{rows}</tbody>
                </Table>
            </ChildViewContainer>
        );
    }
}

/* Add some magic; use the AutoLoadWrapper to create a HOC that handles the
   auto-loading of the data for StationOverviewC.
 */
const StationTestChildView = connect(state => {
    return {
        ...state.station_page.child_panel,
    };
}, {
    unpinChildPanelData
})(StationTestChildViewC);


export default StationTestChildView;
