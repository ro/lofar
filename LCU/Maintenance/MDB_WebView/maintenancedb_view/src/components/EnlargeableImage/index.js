import React, {
    Component
} from 'react';
import 'react-treeview/react-treeview.css';
import {
    Modal,
    ModalHeader,
    ModalBody
} from 'reactstrap';


/**
 * EnlargeableImage:
 * Creates a clickable <img> tag. When the image is clicked, it is opened in a modal
 * and typically enlarged.
 *
 * @prop {string} url - the URL of the image
 * @prop {string} className - classname applied to the Modal
 */
class EnlargeableImage extends Component {

    state = {
        modal: false,
        modalUrl: ''
    };

    timeout = null
    load_cnt = 1

    toggleModal = (e) => {
        this.setState({
            modal: !this.state.modal,
            modalUrl: e.currentTarget.src
        });
    };

    onImgError = (e) => {
        const img = e.currentTarget;
        img.alt = `Reloading in 2s [${this.load_cnt++}]`;
        // let the window figure out if the timeout id is still valid
        if(this.timeout){
            clearTimeout(this.timeout);
        }
        // eslint-disable-next-line no-self-assign
        this.timeout = setTimeout(() => { img.src = img.src; }, 2000 );
    };

    clearTimeout() {
        clearTimeout(this.timeout);
        this.timeout = null;
    }

    render(){
        this.clearTimeout();
        this.load_cnt = 1;

        return (
            <React.Fragment>
                <img    src={this.props.url}
                        onClick={this.toggleModal}
                        onError={this.onImgError}
                        title="Click to enlarge"
                        alt="Not present"/>
                <Modal isOpen={this.state.modal} fade={false} size="lg" toggle={this.toggle} className={this.props.className}>
                  <ModalHeader toggle={this.toggleModal}></ModalHeader>
                  <ModalBody>
                    <img width="100%" src={this.state.modalUrl} alt="Not present" />
                  </ModalBody>
                </Modal>
            </React.Fragment>
        );
    }
}


export default EnlargeableImage;
