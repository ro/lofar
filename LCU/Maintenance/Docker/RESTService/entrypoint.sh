#!/usr/bin/env bash

source /opt/lofar/lofarinit.sh
gunicorn -w $N_WORKERS -k gevent lofar.maintenance.django_postgresql.wsgi:application -b 0.0.0.0:8000 $@