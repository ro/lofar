//# MSLofarSpectralWindow.h: LOFAR MS SPECTRAL_WINDOW subtable
//# Copyright (C) 2021
//# ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$
//#
//# @author Jan David Mol

#ifndef MSLOFAR_MSLOFARSPECTRALWINDOW_H
#define MSLOFAR_MSLOFARSPECTRALWINDOW_H

#include <casacore/ms/MeasurementSets/MSSpectralWindow.h>
#include <MSLofar/MSLofarTable.h>

namespace LOFAR {
  class MSLofar;

  class MSLofarSpectralWindow: public casacore::MSSpectralWindow
  {
  public:

    // Create from an existing table.
    MSLofarSpectralWindow (const casacore::String& tableName,
                    casacore::Table::TableOption option);

    // Create a new table.
    MSLofarSpectralWindow (casacore::SetupNewTable& newTab, casacore::uInt nrrow,
                    casacore::Bool initialize);

    // Create from an existing Table object.
    MSLofarSpectralWindow (const casacore::Table& table);

    // Copy constructor (reference semnatics).
    MSLofarSpectralWindow (const MSLofarSpectralWindow& that);

    // Assignment (reference semantics).
    MSLofarSpectralWindow& operator= (const MSLofarSpectralWindow& that);
  
    // Create the table description containing the required columns. 
    static casacore::TableDesc requiredTableDesc();

  protected:
    // Default constructor creates an unusable object.
    // This constructor is used in MSLofar to create a placeholder until
    // references to the actual tables are initialised.
    MSLofarSpectralWindow();

    friend class MSLofar;
  };

} //# end namespace

#endif
