from .settings import *
from .utils import test_database_configuration

DATABASES['default'] = test_database_configuration()