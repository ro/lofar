# toposort.py: Topological Sort, to sort nodes of DAGs
#
# copyright (c) 2021
# astron (netherlands institute for radio astronomy)
# p.o.box 2, 7990 aa dwingeloo, the netherlands
#
# this file is part of the lofar software suite.
# the lofar software suite is free software: you can redistribute it
# and/or modify it under the terms of the gnu general public license as
# published by the free software foundation, either version 3 of the
# license, or (at your option) any later version.
#
# the lofar software suite is distributed in the hope that it will be
# useful, but without any warranty; without even the implied warranty of
# merchantability or fitness for a particular purpose.  see the
# gnu general public license for more details.
#
# you should have received a copy of the gnu general public license along
# with the lofar software suite. if not, see <http://www.gnu.org/licenses/>.

class Graph:
    """ Graph definition to use in the topological sort. """

    def __init__(self, vertices: list = []):
        """ Initialise the graph with a list of vertices. """

        self.vertices = vertices
        self.edges = {v: [] for v in vertices}

    def add_vertex(self, v: object):
        """ Add another vertex. """

        self.vertices.append(v)
        self.edges[v] = []

    def add_edge(self, u: object, v: object):
        """ Add an edge u -> v. """

        self.edges[u].append(v)

    def add_successors(self, u: object, vlist: list):
        """ Add edges u -> v for each v in vlist. """

        self.edges[u].extend(vlist)

    def add_predecessors(self, ulist: list, v: object):
        """ Add edges u -> v for each u in ulist. """

        for u in ulist:
            self.edges[u].append(v)

    def predecessor_key(self, predecessor_func):
        """ Generate the edges based on a predecessor function of signature:
                predecessor_func = lambda v: predecessors[v]. """

        for v in self.vertices:
            # Add only relations between nodes within this graph.
            self.add_predecessors([p for p in predecessor_func(v) if p in self.vertices], v)

    def successor_key(self, successor_func):
        """ Generate the edges based on a successor function of signature:
                successor_func = lambda v: successors[v]. """

        for v in self.vertices:
            # Add only relations between nodes within this graph.
            self.add_successors(v, [s for s in successor_func(v) if s in self.vertices])

def toposorted(graph: Graph) -> list:
    """ Return the list of vertices, where, for each pair of vertices (a,b),
        a appears earlier in the list than b if there is a path a -> b.
        
        Regular sorting routines (like list.sort) fundamentally cannot do this,
        as they demand a total ordering, that is, the comparison function
        must yield the correct result for every pair of elements (a,b).
        
        NOTE: Since a graph provides only a partial ordering, the order of
        the output list can depend on the order of the vertices and edges in
        the graph. For many graphs multiple solutions are valid, after all. """

    # whether we've processed each vertex
    visited = {v: False for v in graph.vertices}

    # vertices ordered so far, from back to front
    stack = []

    # helper function to walk the graph recursively from a certain vertex
    def _sort(graph, current_vertex, visited, stack):
        visited[current_vertex] = True

        # visit everything after this vertex, recursively
        for successor in graph.edges[current_vertex]:
            if not visited[successor]:
                _sort(graph, successor, visited, stack)

        # all vertices after this one are on the stack, now add us as well
        stack.append(current_vertex)

    # process all vertices. we at least need to trigger the
    # source vertices (those with no incoming edges), but
    # the algorithm is forgiving so we just do all of them.
    for v in graph.vertices:
        if not visited[v]:
            _sort(graph, v, visited, stack)

    # return the list in front-to-back order
    return stack[::-1]
