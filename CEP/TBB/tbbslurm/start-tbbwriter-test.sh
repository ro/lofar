#!/bin/bash
DATE=$(date +%Y%m%dT%H%M%S)
echo logfile=/data/log/tbbwriter-$DATE.log
exec sbatch --partition=cpu -w cpu01 --cpus-per-task=1 \
      --dependency=singelton \
      --job-name="run-tbbwriter" \
      --output=/data/log/tbbwriter/tbbwriter-$DATE.log \
      hostname
#      ./start-single-tbbwriter.sh
