export const SET_COMPONENT_SIZES = "SET_COMPONENT_SIZES";
export const SET_AVERAGING_WINDOW = "SET_AVERAGING_WINDOW";
export const SET_STATISTICS_TEST_TYPE = "SET_STATISTICS_TEST_TYPE";

export const setNewLayout = function(newLayout) {
  var payload = {};
  for(const i in newLayout){
    const item = newLayout[i];
    payload[item['i']] = item;
  }
  return {
          type: SET_COMPONENT_SIZES,
          payload: payload
         }
};

export const setStationStatisticsAveragingWindow = averagingWindow => ({
  type: SET_AVERAGING_WINDOW,
  payload: { averagingWindow }
});

export const setStationStatisticsTestType = test_type => ({
  type: SET_STATISTICS_TEST_TYPE,
  payload: { test_type }
});
