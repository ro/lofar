import logging
from typing import List

from ..component import HBA_ELEMENT_ID_RANGE, Component, \
    antenna_type_antenna_id_to_component_type_component_id
from ..element import Element
from ..test import GenericTest
from ..wincc import ElementStatus, \
    ComponentStatus, \
    StationStatus, WinCCAntennaStatus, STATUS_CODE_TO_STATUS, latest_status_per_station_raw

logger = logging.getLogger(__name__)


def insert_hba_element_status(component_status: ComponentStatus,
                              hba_wincc_status: WinCCAntennaStatus):
    element_statuses = []
    for element_id in HBA_ELEMENT_ID_RANGE:
        element_status_field = 'element_{:02d}'.format(element_id)
        element_status_code = hba_wincc_status.serializable_value(element_status_field)
        element_status_description = STATUS_CODE_TO_STATUS[element_status_code]
        element_status_message = hba_wincc_status.serializable_value(element_status_field +
                                                                     '_reason')
        element_status_modified_at = hba_wincc_status.serializable_value(element_status_field +
                                                                         '_modified_at')

        element, created = Element.objects.get_or_create(element_id=element_id,
                                                         component=component_status.component)
        element_status = ElementStatus(
            component_status=component_status,
            status_code=element_status_code,
            status_description=element_status_description,
            status_message=element_status_message,
            modified_at=element_status_modified_at,
            element=element)
        element_statuses.append(element_status)

    return ElementStatus.objects.bulk_create(element_statuses)


def insert_component_status(station_status: StationStatus, component_status: WinCCAntennaStatus):
    station = station_status.station
    component_type, component_id = antenna_type_antenna_id_to_component_type_component_id(
        station.name,
        component_status.antenna_type,
        component_status.antenna_id)

    component, created = Component.objects.get_or_create(station=station,
                                                         type=component_type,
                                                         component_id=component_id)
    component_status_entity = ComponentStatus(
        component=component,
        status_code=component_status.status_code,
        status_description=STATUS_CODE_TO_STATUS[
            component_status.status_code],
        status_message=component_status.status_code_reason,
        modified_at=component_status.status_code_modified_at,
        station_status=station_status)
    component_status_entity.save()

    if component_status.antenna_type == 'HBA':
        insert_hba_element_status(component_status_entity, component_status)
    return component_status


def insert_station_status(test_instance: GenericTest, statuses: List[WinCCAntennaStatus]):
    station_status_instance = StationStatus(station=test_instance.station)
    station_status_instance.save()

    test_instance.station_status = station_status_instance
    test_instance.save()
    for status in statuses:
        insert_component_status(station_status_instance, status)


def insert_station_status_per_test(test: GenericTest):
    if test.station_status is None:
        queryset = WinCCAntennaStatus.objects.all().order_by()
        query_results = list(latest_status_per_station_raw(queryset=queryset,
                                                       station=test.station.name,
                                                       to_date=test.start_datetime))

        insert_station_status(test, query_results)


def on_inserting_test(sender, instance: GenericTest, **kwargs):
    logger.info('received post_insert signal from %s for instance %s', sender, instance)
    insert_station_status_per_test(instance)
