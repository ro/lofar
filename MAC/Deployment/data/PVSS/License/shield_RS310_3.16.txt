[license]
code        = "RS310C 10499420922"
version     = 31600002
sn          = "471_3031_Astron_Station_5_2/34"
date        = 2020.04.10;10:39:00,000 
comment     = "Centos7 RS310"
expire      = 0000.00.00;00:00:00,000 
redundancy  = 0
ui          = 1
para        = 1
dde         = 5
event       = 1
ios         = 4000
ssi         = 0
api         = 80
excelreport = 5
http        = 0
infoserver  = 1000
comcenter   = 5
maintenance = 1
scheduler   = 1
s7          = 1
recipe      = 1
distributed = 255
ultralight  = 1
mobile_app  = 1
s7plus      = 8
uifix       = 0
parafix     = 0
pararemote  = 0
ctrlext     = 1
update      = 0

