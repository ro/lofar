from rest_framework.serializers import ModelSerializer
from ..models.component import Component

class ComponentSerializer(ModelSerializer):
    class Meta:
        model = Component
        fields = '__all__'
        depth = 1