find_program(SPHINX_APIDOC_EXECUTABLE
             NAMES sphinx-apidoc sphinx-apidoc-3 sphinx-apidoc-3.6
             DOC "Path to sphinx-apidoc executable")

find_program(SPHINX_BUILD_EXECUTABLE
             NAMES sphinx-build sphinx-build-3 sphinx-build-3.6
             DOC "Path to sphinx-build executable")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Sphinx
                                  "Failed to locate sphinx-apidoc executable"
                                  SPHINX_APIDOC_EXECUTABLE)

find_package_handle_standard_args(Sphinx
                                  "Failed to locate sphinx-build executable"
                                  SPHINX_BUILD_EXECUTABLE)
