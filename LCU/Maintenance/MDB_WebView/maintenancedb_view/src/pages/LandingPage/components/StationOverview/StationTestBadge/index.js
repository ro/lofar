import React, {Component} from 'react';
import {withRouter} from "react-router";
import {unique_id} from 'utils/utils.js'
import * as moment from 'moment';
import {Table} from 'reactstrap';
import { datetime_format } from 'utils/constants'
import classNames from "classnames";

import PopoverWithTitle from 'components/PopoverWithTitle'
import Badge from '../Badge'

// CSS
import styles from './styles.module.scss'

/**
 * StationTestBadge; class to render one stationtest badge in the SORow.
 */
class StationTestBadgeC extends Component {

    id = unique_id();

    state = {
        popoverOpen: false
    };

    getClass() {
        let cnt = this.props.data.total_component_errors;

        let cls = classNames({
            [styles.stationtestbadge]: true,
            'hilite-serious': cnt > 10,
            'hilite-alarming': cnt > 5 && cnt <=10,
            'hilite-warning': cnt > 0 && cnt <= 5,
            'hilite-good': cnt === 0
        });

        return cls;
    }

    onClick = () => {
        let station = this.props.station;
        this.props.history.push(`/station_overview?station=${station}`);
    }

    togglePopover = () => {
        this.setState({
            popoverOpen: !this.state.popoverOpen
        });
    }

    popoverBody = () => {
        let data = this.props.data;
        let summary = data.component_error_summary;
        let components = Object.keys(summary).sort();

        return (
            <Table borderless size="sm">
                <tbody>
                    <tr>
                        <th>Start:</th><td>{moment.utc(data.start_datetime).format(datetime_format)}</td>
                    </tr>
                    <tr>
                        <th>End:</th><td>{moment.utc(data.end_datetime).format(datetime_format)}</td>
                    </tr>
                    <tr>
                        <th>Checks:</th><td>{data.checks}</td>
                    </tr>
                    {   components.map((comp, key) => {
                            let comp_sum = summary[comp];
                            let errors = Object.keys(comp_sum).sort();
                            return (
                                <tr key={key}>
                                    <th>{comp}</th>
                                    <td>
                                        {errors.map((e, id) =>
                                            <Badge key={id} count={comp_sum[e]} label={e}/>
                                        )}
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </Table>
        )
    }

    render() {
        return (
            <div>
                <div id={this.id} onClick={this.onClick} onMouseOver={this.togglePopover} onMouseOut={this.togglePopover} className={this.getClass()}>
                    {this.props.data.total_component_errors}
                </div>
                <PopoverWithTitle target={this.id} isOpen={this.state.popoverOpen} togglePopover={this.togglePopover} title={this.props.data.observation_id}>
                    {this.popoverBody}
                </PopoverWithTitle>
            </div>
        );
    }
}

const StationTestBadge = withRouter(StationTestBadgeC);

export default StationTestBadge;
