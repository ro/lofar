#!/bin/bash
DATE=$(date +%Y%m%dT%H%M%S)
echo logfile=/data/log/tbbwriter/tbbwriter-$DATE.log
exec sbatch --partition=cpu -N 24 --cpus-per-task=1 \
      --nice=0 \
      --job-name=run-tbbwriter \
      --output=/data/log/tbbwriter/tbbwriter-$DATE.log \
      --dependency=singleton --mail-user=veen@astron.nl \
      --mail-type=ALL \
      ./start-single-tbbwriter-noqpid.sh

