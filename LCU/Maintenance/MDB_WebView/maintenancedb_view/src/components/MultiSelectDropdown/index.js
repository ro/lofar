import React, {Component} from 'react';
import { IoMdCheckmark as IsSelectIcon } from 'react-icons/io';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'

// CSS
import styles from './styles.module.scss'


/**
 * SelectableOption: renders the boilerplate JSX for selectable items in the DropdownMenu.
 *
 * @prop {bool} isSelected - if the item is selected
 * @prop {function} onSelectedItem - callback for click event. Gets the option value as argument.
 * @prop {jsx} children - label content
 */
function SelectableOption(props) {
    const selectMark = props.isSelected ? <IsSelectIcon /> : null;

    return (
        <DropdownItem onClick={() => {props.onSelectedItem(props.value)}} >
            <table>
                <tbody>
                    <tr className={styles.selectable_option}>
                        <td className={styles.marker}>
                            {selectMark}
                        </td>
                        <td>
                            {props.children}
                        </td>
                    </tr>
                </tbody>
            </table>
        </DropdownItem>
    );
}

/**
 * MultiSelectDropdown:
 * Creates a multi-select box. The selectable items are presented in a dropdown.
 *
 * @props:
 * options: list of objects {value, label}
 */
export class MultiSelectDropdown extends Component{

    state = {
        isOpen: false,
        selectedItems: {}
    }

    // Toggle the dropdown state
    toggle = () => {
        if (!this.state.isOpen) {
            this.setState({
                isOpen: true
            });
        }
        else if (!this.state.mouseOverMenu) {
            this.closeMenu()
        }

    }

    closeMenu(){
        this.setState({
            isOpen:false
        })
        this.props.onSelectionChange(this.getSelectedItemsList())
    }

    itemSelected = (e) => {
        if (e === 'all'){
            this.setState({
                selectedItems: {}
            })
            this.props.onSelectionChange([])
            this.setState({
                isOpen:false
            })
        }else{
            const newSelectedItems = this.state.selectedItems
            newSelectedItems[e] = !newSelectedItems[e]
            this.setState({
                selectedItems: newSelectedItems
            })
        }
    }

    getSelectedItemsList (selectedItems) {
        if (selectedItems === undefined) {
            selectedItems = this.state.selectedItems;
        }
        return Object.keys(selectedItems).filter(item => this.state.selectedItems[item]);


    }

    renderLabel(){
        const selectedItemsList = this.getSelectedItemsList()
        if (selectedItemsList.length === 0 ) {
            return this.props.placeHolder;
        }
        else if (selectedItemsList.length <= 4) {
            return selectedItemsList.join(', ')
        } else {
            const firstFour = selectedItemsList.slice(0, 4)
            return firstFour.join(', ') + ', ...'
        }
    }

    isItemSelected = (e) => {
        if (this.state.selectedItems.hasOwnProperty(e)) {
            return this.state.selectedItems[e];
        }
        return false
    }

    componentDidMount() {
        const selectedItems = this.state.selectedItems
        let update = false
        if (this.props.selectedItems === undefined) {
            return
        }
        for (let item of this.props.selectedItems) {
            if (!selectedItems.hasOwnProperty(item) || !selectedItems[item]) {
                selectedItems[item] = true;
                update = true
            }
        }

        if (update) {
            this.setState({
                selectedItems: selectedItems
            })
        }
    }

    mouseOverMenu = () => {
        this.setState({mouseOverMenu:true})
    }

    mouseExitsMenu = () => {
        this.setState({mouseOverMenu:false})
    }

    render() {
        let allOptions = [{value:'all', label:'<ALL>'}].concat(this.props.options)
        let options = allOptions.map((item, key) =>
            <SelectableOption key={key} value={item.value} isSelected={this.isItemSelected(item.value)} onSelectedItem={this.itemSelected}>
                {item.label}
            </SelectableOption>
        )

        const jsx = (
            <Dropdown isOpen={this.state.isOpen} toggle={this.toggle} className={this.props.className}>
                <DropdownToggle caret>
                    {this.renderLabel()}
                </DropdownToggle>
                <DropdownMenu className={styles.dropdown}
                              onMouseOver={this.mouseOverMenu}
                              onMouseOut={this.mouseExitsMenu}>
                    {options}
                </DropdownMenu>
            </Dropdown>
        )
        return jsx;
    }
}

export default MultiSelectDropdown
