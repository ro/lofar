#!/usr/bin/env python3

# Copyright (C) 2012-2015  ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

# $Id$

'''
Module with nice postgres helper methods and classes.
'''

import logging
from threading import Thread, Lock
from queue import Queue, Empty
from datetime import  datetime, timedelta
import collections
import time
from typing import Union
import re
import select
import psycopg2
import psycopg2.extras
import psycopg2.extensions
from lofar.common.util import single_line_with_single_spaces
from lofar.common.datetimeutils import totalSeconds
from lofar.common.dbcredentials import DBCredentials
from lofar.common.database import AbstractDatabaseConnection, DatabaseError, DatabaseConnectionError, DatabaseExecutionError, FETCH_NONE, FETCH_ONE, FETCH_ALL

logger = logging.getLogger(__name__)

def truncate_notification_channel_name(notification_channel_name: str) -> str:
    # see: https://www.postgresql.org/docs/current/sql-syntax-lexical.html#SQL-SYNTAX-IDENTIFIERS
    POSTGRES_MAX_NOTIFICATION_LENGTH = 63
    truncated_notification = notification_channel_name[:POSTGRES_MAX_NOTIFICATION_LENGTH]
    return truncated_notification


def makePostgresNotificationQueries(schema, table, action, columns: list=None, quoted_columns: list=None, id_column_name: str='id', quote_id_value: bool=False):
    action = action.upper()
    if action not in ('INSERT', 'UPDATE', 'DELETE'):
        raise ValueError('''trigger_type '%s' not in ('INSERT', 'UPDATE', 'DELETE')''' % action)

    change_name = '''{table}_{action}'''.format(table=table, action=action)
    if columns is not None:
        change_name += '_' + '_'.join([column_name for column_name in columns])
    if quoted_columns is not None:
        change_name += '_' + '_'.join([column_name for column_name in quoted_columns])
    function_name = '''NOTIFY_{change_name}'''.format(change_name=change_name)

    def select_column_subquery(column_name: str, new_or_old: str, quote: bool) -> str:
        return '''"{column_name}": {quote_or_not}' || CASE WHEN {new_or_old}.{column_name} IS NULL THEN 'NULL' ELSE CAST({new_or_old}.{column_name}  AS text) END || '{quote_or_not}'''.format(column_name=column_name, new_or_old=new_or_old, quote_or_not='"' if quote else '')

    def select_columns_subquery(column_names: list, new_or_old: str, quote: bool) -> str:
        return ', '.join(select_column_subquery(column_name, new_or_old, quote) for column_name in column_names or [])

    select_payload = '''SELECT '{ ''' + select_columns_subquery([id_column_name], new_or_old='OLD' if action=='DELETE' else 'NEW', quote=quote_id_value)
    select_payload += ''', "new": { '''
    if action in ('INSERT', 'UPDATE'):
        select_payload += select_columns_subquery(columns, new_or_old='NEW', quote=False)
        if columns and quoted_columns:
            select_payload += ', '
        select_payload += select_columns_subquery(quoted_columns, new_or_old='NEW', quote=True)
    select_payload += ''' }, "old": { '''
    if action in ('DELETE', 'UPDATE'):
        select_payload += select_columns_subquery(columns, new_or_old='OLD', quote=False)
        if columns and quoted_columns:
            select_payload += ', '
        select_payload += select_columns_subquery(quoted_columns, new_or_old='OLD', quote=True)
    select_payload += ''' } }' INTO payload; '''

    if action == 'UPDATE':
        begin_update_check = 'IF '
        if columns or quoted_columns:
            begin_update_check += ' OR '.join('ROW(NEW.{what}) IS DISTINCT FROM ROW(OLD.{what})'.format(what=column_name) for column_name in ((columns or []) + (quoted_columns or [])))
        else:
            begin_update_check += ' ROW(NEW.*) IS DISTINCT FROM ROW(OLD.*)'
        begin_update_check += ' THEN'
        end_update_check = 'END IF;'
    else:
        begin_update_check = ''
        end_update_check = ''

    function_sql = '''
    CREATE OR REPLACE FUNCTION {schema}{function_name}()
    RETURNS TRIGGER AS $$
    DECLARE payload text;
    BEGIN
    {begin_update_check}
    {select_payload}
    PERFORM pg_notify(CAST('{change_name}' AS text), payload);
    {end_update_check}
    RETURN {value};
    END;
    $$ LANGUAGE plpgsql;
    '''.format(schema=schema+'.' if schema else '',
                function_name=function_name,
                table=table,
                action=action,
                value='OLD' if action == 'DELETE' else 'NEW',
                change_name=truncate_notification_channel_name(change_name).lower(),
                begin_update_check=begin_update_check,
                select_payload=select_payload,
                end_update_check=end_update_check)

    trigger_name = 'T_%s' % function_name

    trigger_sql = '''
    CREATE TRIGGER {trigger_name}
    AFTER {action} ON {schema}{table}
    FOR EACH ROW
    EXECUTE PROCEDURE {schema}{function_name}();
    '''.format(trigger_name=trigger_name,
                function_name=function_name,
                schema=schema+'.' if schema else '',
                table=table,
                action=action)

    drop_sql = '''
    DROP TRIGGER IF EXISTS {trigger_name} ON {schema}{table} CASCADE;
    DROP FUNCTION IF EXISTS {schema}{function_name}();
    '''.format(trigger_name=trigger_name,
               function_name=function_name,
               schema=schema+'.' if schema else '',
               table=table)

    sql = drop_sql + '\n' + function_sql + '\n' + trigger_sql
    sql_lines = '\n'.join([s.strip() for s in sql.split('\n')]) + '\n'
    return sql_lines

class PostgresDBError(DatabaseError):
    pass

class PostgresDBConnectionError(PostgresDBError, DatabaseConnectionError):
    pass

class PostgresDBQueryExecutionError(PostgresDBError, DatabaseExecutionError):
    pass

class PostgresDatabaseConnection(AbstractDatabaseConnection):
    '''A DatabaseConnection to a postgres database using the common API from lofar AbstractDatabaseConnection'''
    def _do_create_connection_and_cursor(self):
        connection = psycopg2.connect(host=self._dbcreds.host,
                                            user=self._dbcreds.user,
                                            password=self._dbcreds.password,
                                            database=self._dbcreds.database,
                                            port=self._dbcreds.port,
                                            connect_timeout=5)

        cursor = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        return connection, cursor

    @property
    def is_connected(self) -> bool:
        return super().is_connected and self._connection.closed==0

    def _is_recoverable_connection_error(self, error: psycopg2.DatabaseError) -> bool:
        '''test if psycopg2.DatabaseError is a recoverable connection error'''
        if isinstance(error, psycopg2.OperationalError) and re.search('connection', str(error), re.IGNORECASE):
            return True

        try:
            if error.pgcode is not None:
                # see https://www.postgresql.org/docs/current/errcodes-appendix.html#ERRCODES-TABLE
                if error.pgcode.startswith('08') or error.pgcode.startswith('57P') or error.pgcode.startswith('53'):
                    return True
        except:
            return False

        return False

    def _do_execute_query(self, query, qargs=None, fetch=FETCH_NONE):
        '''execute the query and reconnect upon OperationalError'''
        query_log_line = self._queryAsSingleLine(query, qargs)

        try:
            self.connect_if_needed()

            # log
            logger.debug('executing query: %s', query_log_line)

            # execute (and time it)
            start = datetime.utcnow()
            self._cursor.execute(query, qargs)
            elapsed = datetime.utcnow() - start
            elapsed_ms = 1000.0 * totalSeconds(elapsed)

            # log execution result
            logger.info('executed query in %.1fms%s yielding %s rows: %s', elapsed_ms,
                                                                           ' (SLOW!)' if elapsed_ms > 250 else '', # for easy log grep'ing
                                                                           self._cursor.rowcount,
                                                                           query_log_line)

            # log any notifications from within the database itself
            self._log_database_notifications()

            self._commit_selects_if_needed(query)

            # fetch and return results
            if fetch == FETCH_ONE:
                row = self._cursor.fetchone()
                return dict(row) if row is not None else None
            if fetch == FETCH_ALL:
                return [dict(row) for row in self._cursor.fetchall() if row is not None]
            return []

        except psycopg2.OperationalError as oe:
            if self._is_recoverable_connection_error(oe):
                raise PostgresDBConnectionError("Could not execute query due to connection errors. '%s' error=%s" %
                                                (query_log_line,
                                                 single_line_with_single_spaces(oe)))
            else:
                self._log_error_rollback_and_raise(oe, query_log_line)

        except Exception as e:
            self._log_error_rollback_and_raise(e, query_log_line)

    def _log_error_rollback_and_raise(self, e: Exception, query_log_line: str):
        self._log_database_notifications()
        error_string = single_line_with_single_spaces(e)
        logger.error("Rolling back query=\'%s\' due to error: \'%s\'" % (query_log_line, error_string))
        self.rollback()
        if isinstance(e, PostgresDBError):
            # just re-raise our PostgresDBError
            raise
        else:
            # wrap original error in PostgresDBQueryExecutionError
            raise PostgresDBQueryExecutionError("Could not execute query '%s' error=%s" % (query_log_line, error_string))

    def _log_database_notifications(self):
        try:
            if self._connection.notices:
                for notice in self._connection.notices:
                    logger.debug('database log message %s', notice.strip())
                if isinstance(self._connection.notices, collections.deque):
                    self._connection.notices.clear()
                else:
                    del self._connection.notices[:]
        except Exception as e:
            logger.error(str(e))


class PostgresListener(PostgresDatabaseConnection):
    ''' This class lets you listen to postgres notifications
    It execute callbacks when a notification occurs.
    Make your own subclass with your callbacks and subscribe them to the appropriate channel.
    Example:

    class MyListener(PostgresListener):
        def __init__(self, host, database, username, password):
            super(MyListener, self).__init__(host=host, database=database, username=username, password=password)
            self.subscribe('foo', self.foo)
            self.subscribe('bar', self.bar)

        def foo(self, payload = None):
            print "Foo called with payload: ", payload

        def bar(self, payload = None):
            print "Bar called with payload: ", payload

    with MyListener(...args...) as listener:
        #either listen like below in a loop doing stuff...
        while True:
            #do stuff or wait,
            #the listener calls the callbacks meanwhile in another thread

        #... or listen like below blocking
        #while the listener calls the callbacks meanwhile in this thread
        listener.waitWhileListening()
    '''
    def __init__(self, dbcreds: DBCredentials):
        '''Create a new PostgresListener'''
        super(PostgresListener, self).__init__(dbcreds=dbcreds,
                                               auto_commit_selects=True)
        self.__listening = False
        self.__lock = Lock()
        self.__callbacks = {}
        self.__waiting = False
        self.__queue = Queue()

    def connect(self):
        super(PostgresListener, self).connect()
        self._connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

    def subscribe(self, notification, callback):
        '''Subscribe to a certain postgres notification.
        Call callback method in case such a notification is received.'''
        logger.debug("Subscribing %sto %s" % ('and listening ' if self.isListening() else '', notification))
        with self.__lock:
            truncated_notification = truncate_notification_channel_name(notification)
            self.executeQuery("LISTEN %s;", (psycopg2.extensions.AsIs(truncated_notification),))
            self.__callbacks[truncated_notification] = callback
        logger.info("Subscribed %sto %s" % ('and listening ' if self.isListening() else '', notification))

    def unsubscribe(self, notification):
        '''Unubscribe from a certain postgres notification.'''
        logger.debug("Unsubscribing from %s" % notification)
        with self.__lock:
            self.executeQuery("UNLISTEN %s;", (psycopg2.extensions.AsIs(notification),))
            if notification in self.__callbacks:
                del self.__callbacks[notification]
        logger.info("Unsubscribed from %s" % notification)

    def isListening(self):
        '''Are we listening? Has the listener been started?'''
        with self.__lock:
            return self.__listening

    def start(self):
        '''Start listening. Does nothing if already listening.
        When using the listener in a context start() and stop()
        are called upon __enter__ and __exit__

        This method return immediately.
        Listening and calling callbacks takes place on another thread.
        If you want to block processing and call the callbacks on the main thread,
        then call waitWhileListening() after start.
        '''
        if self.isListening():
            return

        self.connect()

        logger.info("Started listening to %s on database %s", ', '.join([str(x) for x in list(self.__callbacks.keys())]), self.dbcreds.stringWithHiddenPassword())

        def eventLoop():
            while self.isListening():
                if select.select([self._connection],[],[],2) != ([],[],[]):
                    self._connection.poll()
                    while self._connection.notifies:
                        try:
                            notification = self._connection.notifies.pop(0)
                            logger.debug("Received notification on channel %s payload %s" % (notification.channel, notification.payload))

                            if self.isWaiting():
                                # put notification on Queue
                                # let waiting thread handle the callback
                                self.__queue.put((notification.channel, notification.payload))
                            elif self.isListening():
                                # call callback on this listener thread
                                self._callCallback(notification.channel, notification.payload)
                        except Exception as e:
                            logger.error(str(e))

        self.__thread = Thread(target=eventLoop)
        self.__thread.daemon = True
        self.__listening = True
        self.__thread.start()

    def stop(self):
        '''Stop listening. (Can be restarted)'''
        with self.__lock:
            if not self.__listening:
                return
            self.__listening = False

        self.__thread.join()
        self.__thread = None

        logger.info("Stopped listening for notifications on database %s", self.dbcreds.stringWithHiddenPassword())
        self.stopWaiting()
        self.disconnect()

    def __enter__(self):
        '''starts the listener upon 'with' context enter'''
        try:
            self.start()
        except Exception as e:
            logger.exception(str(e))
            self.stop()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        '''stops the listener upon 'with' context enter'''
        self.stop()

    def _callCallback(self, channel, payload = None):
        '''call the appropiate callback based on channel'''
        try:
            callback = None
            with self.__lock:
                if channel in self.__callbacks:
                    callback = self.__callbacks[channel]

            if callback:
                if payload:
                    callback(payload)
                else:
                    callback()
        except Exception as e:
            logger.error(str(e))

    def isWaiting(self):
        '''Are we waiting in the waitWhileListening() method?'''
        with self.__lock:
            return self.__waiting

    def stopWaiting(self):
        '''break from the blocking waitWhileListening() method'''
        with self.__lock:
            if self.__waiting:
                self.__waiting = False
                logger.info("Continuing from blocking waitWhileListening")

    def waitWhileListening(self):
        '''
        block calling thread until interrupted or
        until stopWaiting is called from another thread
        meanwhile, handle the callbacks on this thread
        '''
        logger.info("Waiting while listening to %s" % ', '.join([str(x) for x in list(self.__callbacks.keys())]))

        with self.__lock:
            self.__waiting = True

        try:
            while self.isListening() and self.isWaiting():
                try:
                    notification = self.__queue.get(True, 1)
                    channel = notification[0]
                    payload = notification[1]

                    self._callCallback(channel, payload)
                except KeyboardInterrupt:
                    # break
                    break
                except Empty:
                    pass
        finally:
            self.stopWaiting()

