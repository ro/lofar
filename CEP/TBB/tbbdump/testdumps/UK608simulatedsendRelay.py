#! /usr/bin/env python
# UDP server example
import socket
import struct
import time

port1=31662 # receiving port
port2=31660 # sending port

triggersource=dict()
triggersource['\x68']='UK608'
triggersource['\x69']='FR606'
triggersource['\x67']='manual'
triggersource['\x66']='LORA'

sendadress="10.135.252.101" #lhn001
#sendadress="localhost"
fmt='>cciiffc'
fmtsend='>cciic'

#senddata=str(sec)+"_"+str(nsec) 
client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind(("",port1))

print("listening at port ",port1)
while True:
    recvdata, address = server_socket.recvfrom(256)
    (magic1,magic2,sec,nsec,DM,referenceFrequency,sourceID)=struct.unpack(fmt,recvdata)
    if sourceID in list(triggersource.keys()):
        print("received data ",sec,nsec,DM,referenceFrequency,triggersource[sourceID])
    else:
        print("received data ",sec,nsec,DM,referenceFrequency,sourceID)
    #('\x99','\xA0',sec,nsec,DM,referenceFrequency,sourceID)
    print("sending time only to ",sendadress,port2)
    senddata=struct.pack(fmtsend,magic1,magic2,sec,nsec,sourceID)
    client_socket.sendto(senddata, (sendadress,port2))
    


