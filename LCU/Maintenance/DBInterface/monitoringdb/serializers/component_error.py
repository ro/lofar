import logging

from rest_framework.serializers import PrimaryKeyRelatedField, CharField, IntegerField

from .utils import NotNullModelSerializer
from ..models.component import Component
from ..models.component_error import ComponentError
from .log import ActionLogSerializer

logger = logging.getLogger(__name__)


class ComponentErrorSerializer(NotNullModelSerializer):
    failing_elements = PrimaryKeyRelatedField(many=True, read_only=True)
    component_id = IntegerField(source='component.component_id')
    component_type = CharField(source='component.type')

    class Meta:
        model = ComponentError
        fields = '__all__'
        depth = 3

    def __init__(self, *args, **kwargs):
        self.Meta.depth = kwargs.pop('depth', 2)
        super(ComponentErrorSerializer, self).__init__(*args, **kwargs)

    @staticmethod
    def insert_component_error(station_test_entry,
                               station_entry,
                               component_error):

        component = component_error.pop('component')

        component_entry = Component.objects.filter(station=station_entry,
                                                   **component).first()
        if component_entry is None:
            logger.debug('Component entry is not present, inserting ...')
            component_entry = Component(station=station_entry,
                                        **component)
            logger.debug(component_entry, component_error)


            component_entry.save()
            # Log the action
            ActionLogSerializer.log_model_insert(component_entry)

        component_error_entry = ComponentError.objects.filter(component=component_entry,
                                                              station_test=station_test_entry,
                                                              **component_error).first()
        if component_error_entry is None:
            logger.debug('Component error entry is not present, inserting ...')

            component_error_entry = ComponentError(component=component_entry,
                                                   station_test=station_test_entry,
                                                   **component_error)
            component_error_entry.save()
            # Log the action
            ActionLogSerializer.log_model_insert(component_error_entry)

        return component_error_entry

    @staticmethod
    def insert_component_errors(station_test_entry,
                                station_entry,
                                component_errors):
        results = []
        for component_error in component_errors:
            try:
                results.append(ComponentErrorSerializer.insert_component_error(
                    station_test_entry, station_entry, component_error))
            except Exception as e:
                logger.exception(
                    'Cannot insert component error %s'
                    ' for station_test_entry %s and station_entry %s: %s',
                    component_error, station_test_entry, station_entry, e)
                raise e
        return results
