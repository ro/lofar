import React, {
    Component
} from 'react';
import 'react-treeview/react-treeview.css';
import {
    Badge,
    Card,
    CardSubtitle,
    CardBody,
    Container,
    Row,
    Col
} from 'reactstrap';
import EnlargeableImage from 'components/EnlargeableImage'

// CSS
import styles from './styles.module.scss'


/**
 * RTSMDetails
 */
class RTSMDetails extends Component {

    renderRTSMPolarizationDetails(polarization, pol_data, error_type){

        if (! pol_data) {
            return;
        }

        return (
            <Card className={styles.rcucard}>
                <CardBody>
                    <CardSubtitle className={styles.header}>
                        RCU {pol_data.rcu} - {polarization}
                    </CardSubtitle>
                    <Container>
                        <Row noGutters={true}>
                            <Col md="4">
                                <Row>
                                    <Badge color="danger">{error_type}</Badge>
                                </Row>
                                <Row>
                                    <div>percentage: {pol_data.percentage.toFixed(2)} %</div>
                                </Row>
                                <Row>
                                    <div>mode: {pol_data.mode}</div>
                                </Row>
                            </Col>
                            <Col md="8">
                                <EnlargeableImage url={pol_data.url} />
                            </Col>
                        </Row>
                    </Container>
                </CardBody>
            </Card>
        );
    }

    render() {
        let error_type = this.props.data.itemValue;
        let error_data = this.props.data.content.component_errors[error_type];

        if (! error_data) {
            return null;
        }

        return (
            <React.Fragment>
                {this.renderRTSMPolarizationDetails('X', error_data['X'], error_type)}
                {this.renderRTSMPolarizationDetails('Y', error_data['Y'], error_type)}
            </React.Fragment>
        )

    }
}


export default RTSMDetails;
