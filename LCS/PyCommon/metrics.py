# Copyright (C) 2024  ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

from functools import wraps
import time

from prometheus_client import start_http_server, Gauge, Histogram
from prometheus_client.utils import INF

# Buckets that are more useful for tracking longer durations
LONG_DURATION_BUCKETS = (0.1, 0.2, 0.5, 1.0, 10.0, 30.0, 60.0, 120.0, 300.0, 600.0, 1800.0, 3600.0, INF)

def _add_default_metrics():
    """Create generic metrics that we expose by default."""
  
    uptime = Gauge("uptime", "Uptime of the service")
    start_time = time.time()
    uptime.set_function(lambda: time.time() - start_time)

def start_metrics_server(port: int = 8000):
    """Start a Prometheus endpoint to scrape."""

    try:
        # older versions of prometheus_client do not have this method disable_created_metrics, and that's ok
        from prometheus_client import disable_created_metrics
        disable_created_metrics()
    except ImportError:
        pass

    start_http_server(port)

    _add_default_metrics()

def metric_track_duration(prefix: str = "", doc: str | None = None, buckets = Histogram.DEFAULT_BUCKETS):
    """Decorator for functions, to expose their call durations as a
    Histogram in Prometheus."""

    def inner(func):
        func_name = func.__qualname__.replace(".", "_")
        metric = Histogram(f"{prefix}{func_name.lower()}_duration", doc or f"Duration of {func_name}", buckets=buckets)

        @wraps(func)
        def metric_wrapper(*args, **kwargs):
            with metric.time():
                return func(*args, **kwargs)

        return metric_wrapper

    return inner
