import React, { Component } from 'react';
import NavigationBar from 'components/NavigationBar'
import moment from 'moment'
import {connect} from "react-redux";
import { Toolbar, StationAutoComplete, DateRangeSelector, TestTypeSelector, AntennaTypeSelector, AntennaIdSelector } from 'components/Toolbar'
import {Alert, Container, Col, Row} from "reactstrap";
import { composeQueryString } from 'utils/utils.js';
import AntennaErrorDetails from './components/AntennaErrorDetails'
import AntennaView from './components/AntennaView';


// Component to display an alert
function StationAlert(props) {
    if (props.show) {
        return <Alert style={{margin: '10px'}} color="warning">Please select a station</Alert>;
    }
    return null;
}


class TilesPageC extends Component {

  isParameterMissing(){
        const stationName = this.props.selectedStation
        return stationName === undefined ||
               stationName === ""
  }

  getTilesDataURL () {
      const url = '/api/view/ctrl_station_component_element_errors?format=json';
      const parameters = {};
      // ---- Mandatory parameters
      parameters.station_name = this.props.selectedStation;
      // ---- Optional parameters
      parameters.from_date = moment(this.props.startDate).format('YYYY-MM-DD');
      parameters.to_date = moment(this.props.endDate).format('YYYY-MM-DD');
      parameters.component_type = this.props.antennaType;
      parameters.antenna_id = this.props.antennaId;
      parameters.test_type = this.props.testType === 'B' ? 'A' : this.props.testType;
      const parametersString = composeQueryString(parameters);

      return `${url}&${parametersString}`;
  }

  render() {
    let parmMissing = this.isParameterMissing(),
        url = this.getTilesDataURL();

    return (
      <div>
        <NavigationBar active_page={this.props.location} />
        <Toolbar>
            <StationAutoComplete />
            <AntennaTypeSelector />
            <AntennaIdSelector />
            <TestTypeSelector />
            <DateRangeSelector />
        </Toolbar>
        <StationAlert show={parmMissing} />
        { parmMissing ? null :
            <Container fluid={true}>
                <Row>
                    <Col md="8" className="col-padding">
                        <AntennaView url={url}/>
                    </Col>
                    <Col md="4" className="col-padding">
                        <AntennaErrorDetails/>
                    </Col>
                </Row>
            </Container>
        }
      </div>
    );
  }
}

const TilesPage = connect(state => {
    return {
        ...state.mainFilters
    };
})(TilesPageC);

export default TilesPage;
