import moment from "moment";
import {datetime_format} from './constants'


let lastId = 0;

function unique_id(prefix='id_') {
    lastId++;
    return `${prefix}${lastId}`;
}

function capitalize(s) {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase()
}

function composeQueryString(params) {
   const parametersString = Object.keys(params).map((key) => {
       return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
   }).join('&');
   return `${parametersString}`;
}

// Function to pass to Array.sort for sorting strings. Can be used
// to sort objects that have a string key. E.g. my_array is
// an array of objects that have a name key:
//   my_array.sort((a,b) => stringSort(a.name, b.name) );
function stringSort(a, b, caseInsensitive = true) {
    let A = caseInsensitive ? a.toUpperCase() : a;
    let B = caseInsensitive ? b.toUpperCase() : b;
    if (A < B) {
      return -1;
    }
    if (A > B) {
      return 1;
    }
    return 0;
}

function formatDateUTC(date) {
    return moment.utc(date).format(datetime_format);
}

function renderDateRange(data){
    const lastTest = data[0];
    const firstTest = data[data.length - 1];
    const duration = moment.duration(moment(lastTest.end_date)-moment(firstTest.start_date));
    const years = duration.years();
    const days = duration.days();
    const months = duration.months();
    const hours = duration.hours();
    let string = `${data.length} obs `;
    if (years > 0) {string += `${years}Y`;}
    if (months > 0) {string += `${months}M`;}
    if (days > 0) {string += `${days}d`;}
    if (hours > 0) {string += `${hours}h`;}
    return string;
}

export { unique_id, capitalize, composeQueryString, stringSort, renderDateRange, formatDateUTC };
