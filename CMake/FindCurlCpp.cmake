# - Try to find lib curlpp, a c++ library for http calls
# Variables used by this module:
#  CURLCPP_ROOT_DIR     - CurlCpp root directory
# Variables defined by this module:
#  CURLCPP_FOUND        - system has CurlCpp
#  CURLCPP_INCLUDE_DIR  - the CurlCpp include directory (cached)
#  CURLCPP_INCLUDE_DIRS - the CurlCpp include directories
#                          (identical to CURLCPP_INCLUDE_DIR)
#  CURLCPP_LIBRARY      - the CurlCpp library (cached)
#  CURLCPP_LIBRARIES    - the CurlCpp library

# Copyright (C) 2009
# ASTRON (Netherlands Institute for Radio Astronomy)
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
#
# $Id$

if(NOT CURLCPP_FOUND)

  find_path(CURLCPP_INCLUDE_DIR curlpp/cURLpp.hpp
    HINTS ${CURLCPP_ROOT_DIR} PATH_SUFFIXES include)
  find_path(CURL_INCLUDE_DIR curl/curl.h
    HINTS ${CURL_ROOT_DIR} PATH_SUFFIXES include)  # curlpp depends on curl headers

  find_library(CURLCPP_LIBRARY curlpp
    HINTS ${CURLCPP_ROOT_DIR} PATH_SUFFIXES lib)
  find_library(CURL_LIBRARY curl)   # curlpp depends on libcurl
  mark_as_advanced(CURLCPP_INCLUDE_DIR CURLCPP_LIBRARY CURL_LIBRARY)

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(curlcpp DEFAULT_MSG
    CURLCPP_LIBRARY CURLCPP_INCLUDE_DIR)

  set(CURLCPP_INCLUDE_DIRS ${CURLCPP_INCLUDE_DIR} ${CURL_INCLUDE_DIR})
  set(CURLCPP_LIBRARIES ${CURLCPP_LIBRARY} ${CURL_LIBRARY})

endif(NOT CURLCPP_FOUND)
