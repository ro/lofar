
from invoke import task
import fabric
from fabric.connection import Connection
import time

import os

BUILD_DIR = os.getcwd()
LOCAL_DIR = os.path.join(BUILD_DIR, 'installed')
REMOTE_DIR = '/data/mancini/deploy/'
REMOTE_INSTALL_DIR = 'installed'
PATH_SETTINGS_FILE = 'lib64/python3.6/site-packages/lofar/maintenance/django_postgresql/settings.py'
BASE_SETTINGS_FILE_PATH = 'settings.py'
MANAGE_SCRIPT = 'maintenance_d.sh'


LOG_FILE = 'django_postgres.log'

users = {
    'test': 'mancini',
    'production': 'mancini'
}

servers = {
    'test': 'lofarmonitortest',
    'production': 'lofarmonitortest',
}


def rsync_compressed(c, from_path='address', to_path='address'):
    to_address = '{}@{}:{}'.format(c.user, c.host, to_path)
    command = 'rsync -avz {} {}'.format(from_path, to_address)
    c.local(command)


def configure_remote_server(c, type):
    # Copying django settings file
    from_path = os.path.join(REMOTE_DIR, type, BASE_SETTINGS_FILE_PATH)
    to_path = os.path.join(
        REMOTE_DIR, type, REMOTE_INSTALL_DIR, PATH_SETTINGS_FILE)
    print('copying django settings file {} to {}'.format(from_path, to_path))
    cp_cmd = 'cp {} {}'.format(from_path, to_path)
    c.run(cp_cmd)
    print('file copied')


def copy_to_remote_server(c, type):
    deploy_dir = os.path.join(REMOTE_DIR, type)
    print('deploying on host {} directory {}'.format(c.host, deploy_dir))
    rsync_compressed(c, LOCAL_DIR, deploy_dir)


def _connection(type):
    host = servers[type]
    user = users[type]
    return Connection(host, user=user)


@task
def run_server(c, type):
    deploy_dir = os.path.join(REMOTE_DIR, type)
    start_script = os.path.join(deploy_dir, MANAGE_SCRIPT)

    with _connection(type) as c:
        log_path = os.path.join(deploy_dir, LOG_FILE)

        with c.cd(deploy_dir):
            cmd = 'sh {} start'.format(start_script)
            print('executing command', cmd)
            print(c.run(cmd, pty=False))

            time.sleep(5)
            print('log can be found at', log_path)
            c.run('tail {}'.format(log_path))


@task
def tear_down_server(c, type):
    deploy_dir = os.path.join(REMOTE_DIR, type)
    stop_script = os.path.join(deploy_dir, MANAGE_SCRIPT)
    with _connection(type) as c:
        log_path = os.path.join(deploy_dir, LOG_FILE)
        with c.cd(deploy_dir):
            print(c.run('sh {} stop'.format(stop_script)))
            print(c.run('tail {}'.format(log_path)))


@task
def update(c):
    # make
    with c.cd(BUILD_DIR):
        c.run('pwd')
        c.run('make')
        c.run('make install')
    print('ALL Updated')


@task(pre=[update])
def deploy_to_server(c, type):
    if isinstance(c, fabric.Connection):
        print('To be implemented')
        raise NotImplementedError
    else:
        host = servers[type]
        user = users[type]
        with Connection(host, user=user) as c:
            copy_to_remote_server(c, type)
            configure_remote_server(c, type)


@task(pre=[update])
def restart(c, type):
    deploy_to_server(c, type)
    tear_down_server(c, type)
    run_server(c, type)
