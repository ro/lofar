#!/usr/bin/python
import time
import sys
sec=float(sys.argv[1])
timestr=time.strftime("D%Y%m%dT%H%M%S",time.gmtime(sec))
print(timestr, end=' ')
for s in sys.argv[2:]:
    print(s, end=' ')
