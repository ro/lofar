import { combineReducers } from "redux";
import appInitData from "./appInitDataReducers";
import mainFilters from "./mainFilters";
import landingPageReducers from "./landingPageReducers";
import stationOverviewPageReducers from "./stationOverviewPageReducers";
import antennaOverviePageReducers from "./antennaOverviewPageReducers";

export default combineReducers({
    appInitData,
    mainFilters,
    landing_page: landingPageReducers,
    station_page: stationOverviewPageReducers,
    antenna_page: antennaOverviePageReducers
});
