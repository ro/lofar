import React, {
    Component
} from 'react';
import {Badge} from 'reactstrap';
import TreeviewFromObject from 'components/TreeviewFromObject'

// CSS
//import styles from './styles.module.scss'


class StationTestErrorType extends Component {

    getErrorData() {
        if (this.props.data.content.component_errors) {
            return this.props.data.content.component_errors[this.props.data.itemValue]
        }
        return {}
    }

    render() {
        let data = this.props.data;
        let errorData = this.getErrorData()
        let element_errors = errorData.element_errors

        return (
            <div>
                <Badge color="danger">{data.itemValue}</Badge>
                <TreeviewFromObject label={"details"} objdata={errorData.details} />
                {Object.keys(element_errors).map( (element_id) =>
                    <TreeviewFromObject key={element_id} label={"Element "+element_id} objdata={element_errors[element_id]} />
                )}
            </div>
        );
    }
}


export default StationTestErrorType;
