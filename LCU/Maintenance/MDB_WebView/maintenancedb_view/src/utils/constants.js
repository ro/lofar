
// List of known component error types and their abbreviations
export const componentErrorTypes = {
    "BOARD":        "BD",
    "C_SUMMATOR":   "CS",
    "CHECKSRV":     "CK",
    "DOWN":         "DW",
    "FLAT":         "FL",
    "HIGH_NOISE":   "HN",
    "JITTER":       "JI",
    "LOW_NOISE":    "LN",
    "MEMORY":       "MY",
    "MODEM":        "MO",
    "NOSIGNAL":     "NS",
    "OSCILLATION":  "OS",
    "P_SUMMATOR":   "PS",
    "RF_FAIL":      "RF",
    "SHORT":        "SH",
    "SPURIOUS":     "SP",
    "SUMMATOR_NOISE":"SM",
    "TEMPERATURE":  "TE",
    "VERSION":      "VS",
    "VOLTAGE":      "VO"
};

export const date_format = 'YYYY-MM-DD';
export const time_format = 'HH:mm';
export const datetime_format = 'YYYY-MM-DD HH:mm';
