#!/usr/bin/env python3
# $Id$

'''
'''
import logging
import os.path
import socket
import time
import subprocess
from datetime import datetime
from optparse import OptionParser
from lofar.messaging import RPCService, ServiceMessageHandler
from lofar.messaging import EventMessage, ToBus, DEFAULT_BROKER, DEFAULT_BUSNAME
from lofar.messaging.rpc import RPCTimeoutException
from lofar.common.util import waitForInterrupt, humanreadablesize
from lofar.common.subprocess_utils import communicate_returning_strings

from lofar.sas.datamanagement.common.config import CEP4_DATA_MOUNTPOINT
from lofar.sas.datamanagement.common.path import PathResolver, wrap_command_in_cep4_head_node_ssh_call_if_needed
from lofar.sas.datamanagement.cleanup.config import DEFAULT_CLEANUP_SERVICENAME
from lofar.sas.datamanagement.common.config import DEFAULT_DM_NOTIFICATION_PREFIX

from lofar.sas.datamanagement.storagequery.rpc import StorageQueryRPC
from lofar.sas.datamanagement.cleanup.rpc import CleanupRPC

from lofar_tmss_client.tmss_http_rest_client import TMSSsession
from lofar_tmss_client.tmss_bus_listener import *


logger = logging.getLogger(__name__)

#TODO: this local pinfile is a temporary solution to store the pins in until it can be specified and stored for each task in mom/radb
pinfile = os.path.join(os.environ.get('LOFARROOT', '.'), 'var', 'run', 'auto_cleanup_pinned_tasks.py')

#TODO: this local method is a temporary solution to store the pins in until it can be specified and stored for each task in mom/radb
def _setOTDBTaskDataPinned(otdb_id, pinned=True):
    try:
        pins = {}

        if os.path.exists(pinfile):
            with open(pinfile) as f:
                pins = eval(f.read())

        pins[otdb_id] = pinned

        if not os.path.exists(os.path.dirname(pinfile)):
            os.makedirs(os.path.dirname(pinfile))

        with open(pinfile, 'w') as f:
            f.write(str(pins))
            return True
    except Exception as e:
        logger.error(str(e))
    return False

#TODO: this local method was a temporary solution to store the pins for otdb tasks. The method can be removed once we use TMSS only.
def _isOTDBTaskDataPinned(otdb_id):
    try:
        if os.path.exists(pinfile):
            with open(pinfile) as f:
                pins = eval(f.read())
                return pins.get(otdb_id)
    except Exception as e:
        logger.error(str(e))

    return False

#TODO: this local method was a temporary solution to store the pins for otdb tasks. The method can be removed once we use TMSS only.
def _getOTDBPinnedStatuses():
    try:
        if os.path.exists(pinfile):
            with open(pinfile) as f:
                pins = eval(f.read())
                return pins
    except Exception as e:
        logger.error(str(e))
        raise
    return {}


class CleanupHandler(ServiceMessageHandler):
    def __init__(self, mountpoint=CEP4_DATA_MOUNTPOINT, tmss_dbcreds_id: str=None):
        super().__init__()
        self.mountpoint = mountpoint
        self.path_resolver = None
        self._sqrpc = None
        self._tmss_client = TMSSsession.create_from_dbcreds_for_ldap(tmss_dbcreds_id)

    def init_service_handler(self, service_name: str):
        super().init_service_handler(service_name)

        self.register_service_method('GetPathForTMSSId', self.path_resolver.getPathForTMSSId)
        self.register_service_method('RemovePath', self._removePath)
        self.register_service_method('RemoveTaskData', self._removeTaskData)
        self.register_service_method('SetTaskDataPinned', self._setTaskDataPinned)
        self.register_service_method('IsTaskDataPinned', self._isTaskDataPinned)
        self.register_service_method('GetPinnedStatuses', self._getPinnedStatuses)

    def init_tobus(self, exchange: str = DEFAULT_BUSNAME, broker: str = DEFAULT_BROKER):
        super().init_tobus(exchange, broker)

        self.path_resolver = PathResolver(mountpoint=self.mountpoint, exchange=exchange, broker=broker)
        self._sqrpc = StorageQueryRPC.create(exchange=exchange, broker=broker)

    def start_handling(self):
        self._tmss_client.open()
        self.path_resolver.open()
        self._sqrpc.open()
        super().start_handling()
        logger.info("%s started with projects_path=%s", self, self.path_resolver.projects_path)

    def stop_handling(self):
        super().stop_handling()
        self.path_resolver.close()
        self._sqrpc.close()
        self._tmss_client.close()

    def _setTaskDataPinned(self, otdb_id:int=None, tmss_id:int=None, pinned: bool=True):
        logger.info('setTaskDataPinned(otdb_id=%s, tmss_id=%s, pinned=%s)', otdb_id, tmss_id, pinned)
        if otdb_id is not None:
            _setOTDBTaskDataPinned(otdb_id, pinned)
        elif tmss_id is not None:
            subtask = self._tmss_client.get_subtask(tmss_id)
            for task_blueprint_url in subtask['task_blueprints']:
                self._tmss_client.session.patch(task_blueprint_url, json={'output_pinned': pinned})

        self._sendNotification(subject='TaskDataPinned', content={ 'otdb_id':otdb_id, 'tmss_id':tmss_id, 'pinned': self._isTaskDataPinned(otdb_id, tmss_id) })

    def _isTaskDataPinned(self, otdb_id:int, tmss_id:int):
        # TODO: otdb handling can be removed once we use TMSS only.
        if otdb_id is not None:
            return _isOTDBTaskDataPinned(otdb_id)

        subtask = self._tmss_client.get_subtask(tmss_id)
        task_blueprint = self._tmss_client.get_url_as_json_object(subtask['task_blueprint'])
        return task_blueprint['output_pinned']

    def _getPinnedStatuses(self):
        # TODO: otdb handling can be removed once we use TMSS only.
        # This method is currently only used in the web-scheduler for otdb/mom tasks. No need to TMSS-ify it.
        return _getOTDBPinnedStatuses()

    def _are_dataproducts_accessible(self, tmss_id: int) -> bool:
        subtask = self._tmss_client.get_subtask(tmss_id)

        # we can only access CEP4 at the moment
        return subtask.cluster_name == "CEP4"

    def _has_unfinished_non_cleanup_successors(self, otdb_id: int, tmss_id: int) -> bool:
        # TODO: otdb handling can be removed once we use TMSS only.
        if otdb_id is not None:
            radbrpc = self.path_resolver.radbrpc
            task = radbrpc.getTask(otdb_id=otdb_id)
            if task:
                suc_tasks = radbrpc.getTasks(task_ids=task['successor_ids'])
                unfinished_suc_tasks = [t for t in suc_tasks if not (t['status'] == 'finished' or t['status'] == 'obsolete')]
                return len(unfinished_suc_tasks)>0

        successors = self._tmss_client.get_subtask_successors(tmss_id)
        unfinished_successors = [x for x in successors
                                 if x['state_value'] not in ('finished', 'cancelled')
                                 and x['subtask_type'] != 'cleanup' and x['obsolete_since'] is None]
        return len(unfinished_successors) > 0

    def _has_uningested_output_dataproducts(self, otdb_id: int, tmss_id: int) -> bool:
        # TODO: otdb/mom handling can be removed once we use TMSS only.
        if otdb_id is not None:
            radbrpc = self.path_resolver.radbrpc
            task = radbrpc.getTask(otdb_id=otdb_id)
            if task:
                momrpc = self.path_resolver.momrpc
                dataproducts = momrpc.getDataProducts(task['mom_id']).get(task['mom_id'])
                ingestable_dataproducts = [dp for dp in dataproducts if dp['status'] not in [None, 'has_data', 'no_data', 'populated'] ]
                ingested_dataproducts = [dp for dp in ingestable_dataproducts if dp['status'] == 'ingested']

                if len(ingestable_dataproducts) > 0 and len(ingested_dataproducts) < len(ingestable_dataproducts):
                    uningested_dataproducts = [dp for dp in ingestable_dataproducts if dp['status'] != 'ingested']
                    return len(uningested_dataproducts) > 0
            return False

        subtask = self._tmss_client.get_subtask(tmss_id)
        subtasks = self._tmss_client.get_subtasks_in_same_scheduling_unit(subtask)
        return any([s for s in subtasks if s['subtask_type'] == 'ingest' and s['state_value'] != 'finished' and s['obsolete_since'] is None])

    def _sendNotification(self, subject, content):
        try:
            msg = EventMessage(subject="%s.%s" % (DEFAULT_DM_NOTIFICATION_PREFIX, subject), content=content)
            logger.info('Sending notification with subject %s to %s: %s', msg.subject, self.exchange, msg.content)
            self.send(msg)
        except Exception as e:
            logger.error(str(e))

    def _removeTaskData(self, otdb_id, tmss_id, delete_is=True, delete_cs=True, delete_uv=True, delete_im=True, delete_img=True, delete_pulp=True, delete_scratch=True, force=False):
        logger.info("Remove task data for otdb_id=%s, tmss_id=%s force=%s" % (otdb_id, tmss_id, force))

        if otdb_id is not None and not isinstance(otdb_id, int):
            message = "Provided otdb_id is not an int"
            logger.error(message)
            return {'deleted': False, 'message': message}

        if tmss_id is not None and not isinstance(tmss_id, int):
            message = "Provided tmss_id is not an int"
            logger.error(message)
            return {'deleted': False, 'message': message}

        self._sendNotification(subject='TaskDeleting', content={ 'otdb_id': otdb_id, 'tmss_id': tmss_id })

        if self._isTaskDataPinned(otdb_id, tmss_id):
            message = "Task otdb_id=%s tmss_id=%s is pinned. Not deleting data." % (otdb_id, tmss_id)
            logger.error(message)
            self._sendNotification(subject='TaskDeleted', content={'deleted': False,
                                                                   'otdb_id': otdb_id,
                                                                   'tmss_id': tmss_id,
                                                                   'message': message})
            return {'deleted': False, 'message': message}

        if self._has_unfinished_non_cleanup_successors(otdb_id, tmss_id):
            message = "Task otdb_id=%s tmss_id=%s has unfinished successor tasks. Not deleting data." % (otdb_id, tmss_id)
            logger.error(message)
            self._sendNotification(subject='TaskDeleted', content={'deleted': False,
                                                                   'otdb_id': otdb_id,
                                                                   'tmss_id': tmss_id,
                                                                   'message': message})
            return {'deleted': False, 'message': message}

        if not force and self._has_uningested_output_dataproducts(otdb_id, tmss_id):
            message = "Task otdb_id=%s tmss_id=%s has un-ingested dataproducts. Not deleting data." % (otdb_id, tmss_id)
            logger.error(message)
            self._sendNotification(subject='TaskDeleted', content={'deleted': False,
                                                                   'otdb_id': otdb_id,
                                                                   'tmss_id': tmss_id,
                                                                   'message': message})
            return {'deleted': False, 'message': message}


        if not self._are_dataproducts_accessible(tmss_id):
            message = "Task otdb_id=%s tmss_id=%s cannot be accessed by this service. Not deleting data." % (otdb_id, tmss_id)
            logger.error(message)
            self._sendNotification(subject='TaskDeleted', content={'deleted': False,
                                                                   'otdb_id': otdb_id,
                                                                   'tmss_id': tmss_id,
                                                                   'message': message})
            return {'deleted': False, 'message': message}


        path_result = self.path_resolver.getPathForOTDBId(otdb_id) if otdb_id is not None else self.path_resolver.getPathForTMSSId(tmss_id)
        if path_result['found']:
            rm_results = []
            if delete_is and delete_cs and delete_uv and  delete_im and delete_img and delete_pulp:
                rm_results.append(self._removePath(path_result['path']))
            else:
                if delete_is and self.path_resolver.pathExists(os.path.join(path_result['path'], 'is')):
                    rm_results.append(self._removePath(os.path.join(path_result['path'], 'is')))
                if delete_cs and self.path_resolver.pathExists(os.path.join(path_result['path'], 'cs')):
                    rm_results.append(self._removePath(os.path.join(path_result['path'], 'cs')))
                if delete_uv and self.path_resolver.pathExists(os.path.join(path_result['path'], 'uv')):
                    rm_results.append(self._removePath(os.path.join(path_result['path'], 'uv')))
                if delete_im and self.path_resolver.pathExists(os.path.join(path_result['path'], 'im')):
                    rm_results.append(self._removePath(os.path.join(path_result['path'], 'im')))
                if delete_img and self.path_resolver.pathExists(os.path.join(path_result['path'], 'img')):
                    rm_results.append(self._removePath(os.path.join(path_result['path'], 'img')))
                if delete_pulp and self.path_resolver.pathExists(os.path.join(path_result['path'], 'pulp')):
                    rm_results.append(self._removePath(os.path.join(path_result['path'], 'pulp')))

            if delete_scratch and 'scratch_paths' in path_result:
                for scratch_path in path_result['scratch_paths']:
                    rm_results.append(self._removePath(scratch_path))

            rm_result = {'deleted': all(x['deleted'] for x in rm_results),
                         'paths': [x.get('path') for x in rm_results],
                         'message': '',
                         'size': sum([x.get('size', 0) or 0 for x in rm_results])}

            combined_message = '\n'.join(x.get('message','') for x in rm_results)

            if rm_result['deleted'] and not 'does not exist' in combined_message:
                task_type = path_result.get('task',{}).get('type', 'task') if otdb_id else self._tmss_client.get_subtask(tmss_id).get('subtask_type', 'task')
                rm_result['message'] = 'Deleted %s of data from disk for %s with otdb_id=%s tmss_id=%s\n' % (humanreadablesize(rm_result['size']), task_type, otdb_id, tmss_id)

            rm_result['message'] += combined_message

            self._sendNotification(subject='TaskDeleted', content={'deleted':rm_result['deleted'],
                                                                   'otdb_id':otdb_id,
                                                                   'tmss_id':tmss_id,
                                                                   'paths': rm_result['paths'],
                                                                   'message': rm_result['message'],
                                                                   'size': rm_result['size'],
                                                                   'size_readable': humanreadablesize(rm_result['size'])})

            if rm_result['deleted']:
                if otdb_id is not None:
                    self._endStorageResourceClaim(otdb_id=otdb_id)

                if tmss_id is not None:
                    # annotate the dataproducts in tmss that they are deleted
                    self._tmss_client.mark_output_dataproducts_as_deleted(tmss_id)

            return rm_result

        return {'deleted': False, 'message': path_result['message']}

    def _endStorageResourceClaim(self, otdb_id=None):
        try:
            #check if all data has actually been removed,
            #and adjust end time of claim on storage
            path_result = self.path_resolver.getPathForTask(otdb_id=otdb_id)
            if path_result['found']:
                path = path_result['path']

                if not self.path_resolver.pathExists(path):
                    # data was actually deleted
                    #update resource claim
                    radbrpc = self.path_resolver.radbrpc
                    storage_resources = radbrpc.getResources(resource_types='storage')
                    cep4_storage_resource = next(x for x in storage_resources if 'CEP4' in x['name'])
                    task = radbrpc.getTask(otdb_id=otdb_id)
                    if task:
                        claims = radbrpc.getResourceClaims(task_ids=task['id'], resource_type='storage')
                        cep4_storage_claim_ids = [c['id'] for c in claims if c['resource_id'] == cep4_storage_resource['id']]
                        for claim_id in cep4_storage_claim_ids:
                            logger.info("setting endtime for claim %s on resource %s %s to task's endtime '%s' (resulting in a deleted claim)", claim_id, cep4_storage_resource['id'], cep4_storage_resource['name'], task['endtime'])
                            radbrpc.updateResourceClaim(claim_id, endtime=task['endtime'])
        except Exception as e:
            logger.error(str(e))

    def _removePath(self, path, do_recurse=False):
        logger.info("Remove path: %s" % (path,))

        # do various sanity checking to prevent accidental deletes
        if not isinstance(path, str):
            message = "Provided path is not a string"
            logger.error(message)
            return {'deleted': False, 'message': message, 'path': path}

        if not path:
            message = "Empty path provided"
            logger.error(message)
            return {'deleted': False, 'message': message, 'path': path}

        if '*' in path or '?' in path:
            message = "Invalid path '%s': No wildcards allowed" % (path,)
            logger.error(message)
            return {'deleted': False, 'message': message, 'path': path}

        # remove any trailing slashes
        if len(path) > 1:
            path = path.rstrip('/')

        required_base_paths = [self.path_resolver.projects_path, self.path_resolver.scratch_path, self.path_resolver.share_path]

        if not any(path.startswith(base_path) for base_path in required_base_paths):
            message = "Invalid path '%s': Path does not start with any of the base paths: '%s'" % (path, ' '.join(required_base_paths))
            logger.error(message)
            return {'deleted': False, 'message': message, 'path': path}

        for base_path in required_base_paths:
            if path.startswith(base_path) and path[len(base_path):].count('/') == 0:
                message = "Invalid path '%s': Path should be a subdir of '%s'" % (path, base_path)
                logger.error(message)
                return {'deleted': False, 'message': message, 'path': path}

        if not self.path_resolver.pathExists(path):
            message = "Nothing to delete, path '%s' does not exist." % (path)
            logger.warn(message)
            return {'deleted': True, 'message': message, 'path': path}

        try:
            du_result = self._sqrpc.getDiskUsageForPath(path) if do_recurse else {}
        except RPCTimeoutException:
            du_result = {}

        if du_result.get('found'):
            logger.info("Attempting to delete %s in %s", du_result.get('disk_usage_readable', '?B'), path)
        else:
            logger.info("Attempting to delete %s", path)

        if do_recurse:
            # LustreFS on CEP4 like many small deletes better than one large tree delete
            # so, recurse into the sub_directories,
            # and take a small sleep in between so other processes (like observation datawriters) can access LustreFS
            # (we've seen observation data loss when deleting large trees)
            subdirs_result = self.path_resolver.getSubDirectories(path)
            if subdirs_result.get('found') and subdirs_result.get('sub_directories'):
                sub_directories = subdirs_result['sub_directories']

                for subdir in sub_directories:
                    subdir_path = os.path.join(path, subdir)
                    self._removePath(subdir_path, do_recurse=False) #recurse only one level deep
                    time.sleep(0.01)
        else:
            self._sendNotification(subject='PathDeleting', content={'path': path, 'size': du_result.get('disk_usage', 0) })


        cmd = ['rm', '-rf', path]
        cmd = wrap_command_in_cep4_head_node_ssh_call_if_needed(cmd)
        logger.info(' '.join(cmd))
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = communicate_returning_strings(proc)

        if proc.returncode == 0:
            message = "Deleted %s in '%s'" % (du_result.get('disk_usage_readable', '?B'), path)
            logger.info(message)

            if do_recurse:
                #only send notification if not recursing
                self._sendNotification(subject='PathDeleted', content={'deleted': True, 'path': path, 'message':message, 'size': du_result.get('disk_usage', 0)})

            return {'deleted': True, 'message': message, 'path': path, 'size': du_result.get('disk_usage', 0)}

        if do_recurse:
            #only send notification if not recursing
            self._sendNotification(subject='PathDeleted', content={'deleted': False, 'path': path, 'message':'Failed to delete (part of) %s' % path})

        logger.error(err)

        return {'deleted': False,
                'message': 'Failed to delete (part of) %s' % path,
                'path': path }

def create_rpc_service(exchange=DEFAULT_BUSNAME, broker=DEFAULT_BROKER,
                  mountpoint=CEP4_DATA_MOUNTPOINT,
                  tmss_dbcreds_id: str=None):
    return RPCService(DEFAULT_CLEANUP_SERVICENAME,
                   handler_type=CleanupHandler,
                   handler_kwargs={'mountpoint': mountpoint,
                                   'tmss_dbcreds_id': tmss_dbcreds_id},
                   exchange=exchange,
                   broker=broker,
                   num_threads=4)


class TMSSEventMessageHandlerForCleanup(TMSSEventMessageHandler):
    def __init__(self, tmss_dbcreds_id: str="TMSSClient", exchange: str=DEFAULT_BUSNAME, broker: str=DEFAULT_BROKER):
        super().__init__(log_event_messages=False)
        self._tmss_client = TMSSsession.create_from_dbcreds_for_ldap(tmss_dbcreds_id)
        self._cleanup_rpc = CleanupRPC.create(exchange=exchange, broker=broker)

    def start_handling(self):
        self._cleanup_rpc.open()
        self._tmss_client.open()
        super().start_handling()

        # handle cleanup tasks in 'queued'/'scheduled/started' state upon startup (for which we may have missed events)
        for state in ('queued', 'scheduled', 'started'):
            cleanup_subtasks = self._tmss_client.get_subtasks(subtask_type='cleanup', state=state)
            for cleanup_subtask in cleanup_subtasks:
                try:
                    if cleanup_subtask['state_value']=='queued':
                        self.run_cleanup_subtask_if_prerequisites_met(cleanup_subtask)
                    elif cleanup_subtask['state_value']=='scheduled':
                        self.queue_cleanup_subtask_if_prerequisites_met(cleanup_subtask)
                    elif cleanup_subtask['state_value']=='started':
                        self.run_cleanup_subtask_if_prerequisites_met(cleanup_subtask)
                except Exception as e:
                    logger.error(e)

    def stop_handling(self):
        super().start_handling()
        self._tmss_client.close()
        self._cleanup_rpc.close()

    def onSubTaskStatusChanged(self, id: int, status: str):
        logger.info("onSubTaskStatusChanged: id=%s status=%s", id, status)

        if status in ('scheduled', 'queued', 'started', 'finished'):
            subtask = self._tmss_client.get_subtask(id)

            if subtask['subtask_type'] == 'cleanup':
                if status == 'scheduled':
                    # a scheduled cleanup subtask should "just be startable",
                    # but we also need to check if the dataproducts are ingested.
                    # So, we change the state to queued,
                    # as a result this method onSubTaskStatusChanged will be called again for the queued status,
                    # and we can check the prerequisites before starting it
                    self.queue_cleanup_subtask_if_prerequisites_met(subtask)
                elif status == 'queued':
                    self.run_cleanup_subtask_if_prerequisites_met(subtask)

            elif subtask['subtask_type'] == 'ingest':
                if status == 'finished':
                    # when an ingest subtask finishes, then it is safe for the related cleanup subtask(s) to be started
                    subtasks = self._tmss_client.get_subtasks_in_same_scheduling_unit(subtask)
                    cleanup_subtasks = [s for s in subtasks if s['subtask_type'] == 'cleanup' and s['state_value']=='scheduled']
                    for cleanup_subtask in cleanup_subtasks:
                        self.queue_cleanup_subtask_if_prerequisites_met(cleanup_subtask)

    def queue_cleanup_subtask_if_prerequisites_met(self, subtask: dict):
        logger.debug("queue_cleanup_subtask_if_prerequisites_met: subtask id=%s type=%s status=%s", subtask['id'], subtask['subtask_type'], subtask['state_value'])

        # check prerequisites
        if subtask['subtask_type'] != 'cleanup':
            # skip non-cleanup subtasks
            return

        if subtask['state_value'] != 'scheduled':
            # skip cleanup subtasks which are not scheduled
            return

        # when an ingest subtask finishes, then it is safe for the related cleanup subtask(s) to be started
        subtasks = self._tmss_client.get_subtasks_in_same_scheduling_unit(subtask)
        ingest_subtasks = [s for s in subtasks if s['subtask_type'] == 'ingest']
        unfinished_ingest_subtasks = [s for s in ingest_subtasks if s['state_value'] != 'finished' and s['obsolete_since'] is None]

        if len(unfinished_ingest_subtasks) > 0:
            logger.info("cleanup subtask id=%s is scheduled, but waiting for ingest id=%s to finish before queueing the cleanup subtask...",
                        subtask['id'], [s['id'] for s in unfinished_ingest_subtasks])
            return

        logger.info("cleanup subtask id=%s is scheduled, and all ingest subtasks id=%s are finished. queueing the cleanup subtask...",
                    subtask['id'], [s['id'] for s in ingest_subtasks])

        self._tmss_client.set_subtask_status(subtask['id'], 'queueing', retry_count=5)
        self._tmss_client.set_subtask_status(subtask['id'], 'queued', retry_count=5)
        # as a result of setting the queued state, run_cleanup_subtask_if_prerequisites_met is called in onSubTaskStatusChanged

    def run_cleanup_subtask_if_prerequisites_met(self, subtask: dict):
        logger.debug("run_cleanup_subtask_if_prerequisites_met: subtask id=%s type=%s status=%s", subtask['id'], subtask['subtask_type'], subtask['state_value'])

        # check prerequisites
        if subtask['subtask_type'] != 'cleanup':
            # skip non-cleanup subtasks
            return

        if subtask['state_value'] not in ('queued', 'started'):
            # skip cleanup subtasks which are not queued or dangling in started-state
            return

        # prerequisites are met. Proceed.

        if subtask['state_value'] == 'queued':
            logger.info("starting cleanup subtask id=%s...", subtask['id'])
            self._tmss_client.set_subtask_status(subtask['id'], 'starting', retry_count=5)
            self._tmss_client.set_subtask_status(subtask['id'], 'started', retry_count=5)

        logger.info("running cleanup subtask id=%s...", subtask['id'])

        predecessors = self._tmss_client.get_subtask_predecessors(subtask['id'])
        results = []

        for predecessor in predecessors:
            logger.info("cleanup subtask id=%s removing output data for subtask id=%s ...", subtask['id'], predecessor['id'])
            result = self._cleanup_rpc.removeTaskData(tmss_id=predecessor['id'])
            results.append(result)
            logger.info("cleanup subtask id=%s: %s", subtask['id'], result.get('message', ""))

        if any([not r['deleted'] for r in results]):
            self._tmss_client.set_subtask_status(subtask['id'], 'error', 'Error during cleanup: not all predecessor task data could be deleted.', retry_count=5)
        else:
            self._tmss_client.set_subtask_status(subtask['id'], 'finishing', retry_count=5)
            self._tmss_client.set_subtask_status(subtask['id'], 'finished', retry_count=5)


def create_tmss_buslistener(exchange: str=DEFAULT_BUSNAME, broker: str=DEFAULT_BROKER, tmss_dbcreds_id: str="TMSSClient"):
    return TMSSBusListener(handler_type=TMSSEventMessageHandlerForCleanup,
                           handler_kwargs={'tmss_dbcreds_id': tmss_dbcreds_id,
                                           'exchange': exchange,
                                           'broker': broker},
                           exchange=exchange, broker=broker)


def main():
    # make sure we run in UTC timezone
    import os
    os.environ['TZ'] = 'UTC'

    # Check the invocation arguments
    parser = OptionParser("%prog [options]",
                          description='runs the cleanup service')
    parser.add_option('-b', '--broker', dest='broker', type='string', default=DEFAULT_BROKER,
                      help='Address of the messaging broker, default: %default')
    parser.add_option("-e", "--exchange", dest="exchange", type="string", default=DEFAULT_BUSNAME,
                      help="Name of the bus exchange on the broker, [default: %default]")
    parser.add_option("--mountpoint", dest="mountpoint", type="string", default=CEP4_DATA_MOUNTPOINT, help="path of local cep4 mount point, default: %default")
    parser.add_option('-R', '--rest_credentials', dest='rest_credentials', type='string', default='TMSSClient', help='TMSS django REST API credentials name, default: %default')
    parser.add_option('-V', '--verbose', dest='verbose', action='store_true', help='verbose logging')
    (options, args) = parser.parse_args()

    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=logging.DEBUG if options.verbose else logging.INFO)

    with create_rpc_service(exchange=options.exchange, broker=options.broker, tmss_dbcreds_id=options.rest_credentials, mountpoint=options.mountpoint):
        with create_tmss_buslistener(exchange=options.exchange, broker=options.broker, tmss_dbcreds_id=options.rest_credentials):
            waitForInterrupt()

if __name__ == '__main__':
    main()
