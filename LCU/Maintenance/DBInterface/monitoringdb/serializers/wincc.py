from collections import OrderedDict

from .utils import NotNullModelSerializer
from ..models.wincc import WinCCAntennaStatus, StationStatus, ComponentStatus, ElementStatus
from ..serializers.component import ComponentSerializer


class WinCCAntennaStatusSerializer(NotNullModelSerializer):
    class Meta:
        model = WinCCAntennaStatus
        fields = '__all__'


class ElementStatusSerializer(NotNullModelSerializer):
    class Meta:
        model = ElementStatus
        fields = '__all__'


class ComponentStatusSerializer(NotNullModelSerializer):
    component = ComponentSerializer()

    class Meta:
        model = ComponentStatus
        fields = '__all__'


class SummaryComponentStatusSerializer(NotNullModelSerializer):
    component = ComponentSerializer()

    class Meta:
        model = ComponentStatus
        fields = ['status_code', 'status_description', 'status_message', 'modified_at']


class StationStatusSerializer(NotNullModelSerializer):
    component_status = ComponentStatusSerializer()

    class Meta:
        model = StationStatus
        fields = '__all__'


from typing import List


def serialize_station_status(component_statuses: List[ComponentStatus], type: str):
    station_status = OrderedDict()
    for component_status in component_statuses:
        component_id = component_status.component.component_id
        component_type = component_status.component.type
        if component_type != type:
            continue
        station_status[str(component_id)] = dict(
            status_code=component_status.status_code,
            status=component_status.status_description,
            reason=component_status.status_message,
            last_modified=component_status.modified_at
        )

    return station_status
