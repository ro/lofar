from rest_framework.serializers import ModelSerializer
from ..models.log import ActionLog
import logging

logger = logging.getLogger(__name__)


class ActionLogSerializer(ModelSerializer):
    class Meta:
        model = ActionLog
        fields = '__all__'
        depth = 1

    @staticmethod
    def append_log(primary_key, model_name, who, what):
        action_log = ActionLog(entry_id=primary_key,
                  model_name=model_name,
                  who=who,
                  what=what)
        action_log.save()

        return action_log

    @staticmethod
    def log_model_insert(element_entry, who='system'):
        try:
            ActionLogSerializer.append_log(element_entry.pk,
                                           model_name=element_entry.__class__.__name__,
                                           who=who,
                                           what='Inserted')
        except Exception as e:
            logger.exception('Cannot log entry %s', element_entry)
            raise e