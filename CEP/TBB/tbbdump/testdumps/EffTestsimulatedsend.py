#! /usr/bin/env python
# UDP server example
import socket
import struct
import time
import sys

port1=31660
port2=31660

client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

#sendadress="10.135.255.6" #locus081
#sendadress="10.135.252.101" #lhn001
#sendadress="10.149.96.2" #kis001
sendadress="localhost"
#sendadress="195.169.3.106" #cosmic
packets=[]
rectime=[]
fmt='>cciic'
f=open('/opt/lofar/tbb/logs/Effsimulated','a')

def getStopTime(rcu=0):
    t=time.time()
    sec=int(t)
    nsec=int(1e9*(t-sec))
    return (sec,nsec)


(sec,nsec)=getStopTime()
if len(sys.argv)>=2:
    sec+=int(sys.argv[1])
    print("sending future timestamp+",sys.argv[1])
senddata=struct.pack(fmt,'\x99','\xA0',sec,nsec,'\x51') #\x66 for real LORA events
#senddata=str(sec)+"_"+str(nsec)
client_socket.sendto(senddata, (sendadress,port1))
print("sending trigger",sec,nsec)
f.writelines([str(sec)+" "+str(nsec)+"\n"])


while False:
    recvdata, address = server_socket.recvfrom(256)

    #print "( " ,address[0], " " , address[1] , " ) said at ",sec,nsec,data
    if senddata==recvdata:
        (recsec,recnsec)=getStopTime()
        timedif=-1*(sec+1e-9*nsec-recsec-1e-9*recnsec)
        print(sec,nsec,recsec,recnsec,sec+1e-9*nsec-recsec-1e-9*recnsec)
        f.writelines([str(timedif)+"\n"])

    time.sleep(3)

    (sec,nsec)=getStopTime()
    senddata=str(sec)+"_"+str(nsec)
    client_socket.sendto(senddata, (sendadress,port1))
