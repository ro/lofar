import time
import subprocess
import os
import signal
import math

from TBBsetup import *

def startup(stationname=station):
    free(stationname)
    time.sleep(2)
    alloc(stationname)
    time.sleep(2)
    settransient(stationname)
    time.sleep(2)
    record(stationname)

def startupall(stationlist):
    for station in stationlist:
        free(station)
    time.sleep(2)
    for station in stationlist:
        alloc(station)
    time.sleep(2)
    for station in stationlist:
        settransient(station)
    time.sleep(2)
    for station in stationlist:
        record(station)

def startRSP(station,rcumode=2):
    if rcumode not in [1,2,3,4]:
        print("rcu mode ",rcumode,"not supported")
        return False
    import subprocess
    proc=subprocess.Popen(['ssh',station,'/opt/lofar/bin/rspctl','--rcumode='+str(rcumode)])
    proc.wait()
    from time import sleep
    sleep(3)
    proc=subprocess.Popen(['ssh',station,'/opt/lofar/bin/rspctl','--rcuenable'])
    proc.wait()


def alloc(stationname=station):
    proc=subprocess.Popen(['ssh',stationname,'/opt/lofar/bin/tbbctl','--alloc'])
    proc.wait()

def free(stationname=station):
    proc=subprocess.Popen(['ssh',stationname,'/opt/lofar/bin/tbbctl','--free'])
    proc.wait()


def rcui(stationname=station):
    proc=subprocess.Popen(['ssh',stationname,'/opt/lofar/bin/tbbctl','--rcui'],stdout=subprocess.PIPE)
    for line in proc.stdout.readlines():
        print(line, end=' ')
    return proc

def swlevel(stationname=station):
    proc=subprocess.Popen(['ssh',stationname,'/opt/lofar/bin/swlevel'],stdout=subprocess.PIPE)
    for line in proc.stdout.readlines():
        print(line, end=' ')
    return proc

def rcuishort(stationname=station):
    proc=subprocess.Popen(['ssh',stationname,'/opt/lofar/bin/tbbctl','--rcui'],stdout=subprocess.PIPE)
    for line in proc.stdout.readlines():
        if "262144" in line:
            if "0x00080000" in line:
                print(line, end=' ')
        elif "104857" in line:
            if "0x00200000" in line:
                print(line, end=' ')
    return proc

def checkstopped(stationname=station,completestop=False):
    proc=subprocess.Popen(['ssh','-oBatchMode=yes','-oConnectTimeout=2',stationname,'/opt/lofar/bin/tbbctl','--rcui'],stdout=subprocess.PIPE)
    proc.wait()
    if proc.poll()==0:
        counter=0
        for line in proc.stdout.readlines():
            if "0x" in line:
                if "S" in line:
                    counter+=1
        if "CS" in station or "RS" in station:
            if counter==96:
                return True
            elif counter==192:
                return True
            elif counter>0 and counter%16==0 and not completestop:
                return True
            elif counter==12: #lightningi
                return True
    return False


def getclock(stationname=station):
    proc=subprocess.Popen(['ssh',stationname,'/opt/lofar/bin/rspctl','--clock'],stdout=subprocess.PIPE)
    for line in proc.stdout.readlines():
        if "200" in line:
            return 200
        if "160" in line:
            return 160


def getrcus(stationname=station):
    proc=subprocess.Popen(['ssh',stationname,'/opt/lofar/bin/tbbctl','--rcui'],stdout=subprocess.PIPE)
    rculist=[]
    for line in proc.stdout.readlines():
        if '-' not in line and "=" not in line and "NOT" not in line and "subscri" not in line and len(line)>5:
            rculist.append(int(line.split()[0]))
    return rculist

def settransient(stationname=station):
    proc=subprocess.Popen(['ssh',stationname,'/opt/lofar/bin/rspctl','--tbbmode="transient"'])
    proc.wait()
    time.sleep(2)
    proc=subprocess.Popen(['ssh',stationname,'/opt/lofar/bin/tbbctl','--mode=transient'])
    proc.wait()

def stop(stationname=station):
    proc=subprocess.Popen(['timeout','10','ssh','-o BatchMode=yes','-o ConnectTimeout=2',stationname,'/opt/lofar/bin/tbbctl','--stop'])
    return proc

def record(stationname=station):
    proc=subprocess.Popen(['ssh','-o BatchMode=yes','-o ConnectTimeout=2',stationname,'/opt/lofar/bin/tbbctl','--record'])
    return proc

def readall(stationname=station,pages=1024):
    proc=subprocess.Popen(['ssh','-o BatchMode=yes','-o ConnectTimeout=2',stationname,'/opt/lofar/bin/tbbctl','--readall='+str(pages)])
    return proc

def read(stationname,rcunr,secondstime,sampletime,prepages,postpages,newreadmethod=False):
    if newreadmethod:
        timestamp="%i.%09.0i" %(secondstime,sampletime*5)
        prepages="%0.9f" %(prepages*1024*5e-9)
        postpages="%0.9f" %(postpages*1024*5e-9)
        proc=subprocess.Popen(['ssh','-o BatchMode=yes','-o ConnectTimeout=2',stationname,'/opt/lofar/bin/tbbctl','--read='+str(rcunr)+','+timestamp+','+prepages+','+postpages])
    else:
        proc=subprocess.Popen(['ssh','-o BatchMode=yes','-o ConnectTimeout=2',stationname,'/opt/lofar/bin/tbbctl','--read='+str(rcunr)+','+str(secondstime)+','+str(sampletime)+','+str(prepages)+','+str(postpages)])
    return proc



def setstorage(stationname,lsenode):
    proc=subprocess.Popen(['ssh',stationname,'/opt/lofar/bin/tbbctl','--storage='+lsenode])


def startstations(singlestationmode=False):
    return startup(station)

def findtimerange(rcunr,startsample=0,maxsample=1048000,step=10000,station=station):
    while sampletime(rcunr,startsample,station)<sampletime(rcunr,startsample+step,station) and startsample+step < maxsample:
        startsample+=step
    proc=subprocess.Popen(['ssh',station,'rm','*.nfo'])
    proc.wait()
    proc=subprocess.Popen(['ssh',station,'rm','*.dat'])
    proc.wait()
    return sampletime(rcunr,startsample+step,station),sampletime(rcunr,startsample,station)


def findtimerange2(rcunr,startsample=0,maxsample=1048000,step=10000,station=station):
    while sampletime(rcunr,startsample,station)<sampletime(rcunr,startsample+step,station) and startsample+step < maxsample:
        startsample+=step
    proc=subprocess.Popen(['ssh',station,'rm','*.nfo'])
    proc.wait()
    proc=subprocess.Popen(['ssh',station,'rm','*.dat'])
    proc.wait()
    return rcunr,startsample,startsample+step

def findtimerange3(station, rcu, maxpage):
    s0 = 0
    s1 = step = maxpage-1
    t0 = sampletime2(station, rcu, s0)
    t1 = sampletime2(station, rcu, s1)
    if (t1 > t0):
        return (t0,t1+5.12E-6)
    else:
        while not (step==1 and t0<t1 and s1<s0):
            step = (step+1)//2
            sign = int(math.copysign(1,t1-t0))
            s0 = s1
            s1 = s1 + sign*step
            t0 = t1
            t1 = sampletime2(station,rcu,s1)
        print(s0, s1)
        return (t0,t1+5.12E-6)

def sampletime(rcunr,page,station=station):
    proc=subprocess.Popen(['ssh',station,'/opt/lofar/bin/tbbctl','--readpage='+str(rcunr)+','+str(page)+',1'],stdout=subprocess.PIPE)
    l=proc.stdout.readlines()
    if len(l)>8:
        if 'Time of' in l[8]:
            second=int(l[8].split('(')[1].split(')')[0])
            sample=int(l[9].split(': ')[1].strip('\n'))
            return(second,sample)
        else:
            print("Format of readpage has changed, update sampletime function in TBBcontroller")
        return(0,0)
    return(0,0)

def sampletime2(station, rcu, page):
    (second,sample) = sampletime(rcu,page,station)
    return second+sample*5.E-9

def stopstations():
    return stop(station)

def dumpstations(pages):
    t0=time.time()
    proc=stop(station)
    proc.wait()
    print("stop duration",station,time.time()-t0)
    t1=time.time()
    print("time before, after stopping stations:",t0,t1)
    time.sleep(2)
    t0=int(t0)
    proc={}
    for rcu in rcus:
        proc=read(station,rcu,t0,1000,0,pages)
        proc.wait()


def recordstations():
    record(station)
