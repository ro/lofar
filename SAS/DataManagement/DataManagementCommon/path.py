#!/usr/bin/env python3

import os
import os.path
import logging
import socket
import subprocess

from lofar.common import isProductionEnvironment
from lofar.common.subprocess_utils import communicate_returning_strings
from lofar.common.cep4_utils import wrap_command_in_cep4_head_node_ssh_call

from lofar.messaging import DEFAULT_BROKER, DEFAULT_BUSNAME

from lofar.sas.datamanagement.common.config import CEP4_DATA_MOUNTPOINT

from lofar_tmss_client.tmss_http_rest_client import TMSSsession

logger = logging.getLogger(__name__)

class PathResolver:
    def __init__(self,
                 mountpoint=CEP4_DATA_MOUNTPOINT,
                 exchange=DEFAULT_BUSNAME,
                 broker=DEFAULT_BROKER,
                 tmss_dbcreds_id: str=None):

        self.mountpoint = mountpoint
        self.projects_path = os.path.join(self.mountpoint, 'projects' if isProductionEnvironment() else 'test-projects')
        self.scratch_path = os.path.join(self.mountpoint, 'scratch', 'pipeline')
        self.share_path = os.path.join(self.mountpoint, 'share', 'pipeline')

        self._tmss_client = TMSSsession.create_from_dbcreds_for_ldap(tmss_dbcreds_id)

    def open(self):
        self._tmss_client.open()

    def close(self):
        self._tmss_client.close()

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def getPathForTMSSId(self, tmss_id):
        logger.debug("Get path for tmss_id %s" % (tmss_id,))
        return self.getPathForTask(tmss_id=tmss_id)

    def getPathForTask(self, tmss_id=None, include_scratch_paths=True):
        logger.info("getPathForTask(tmss_id=%s)", tmss_id)
        '''get the path for a task for either the given tmss_id'''
        result = self._getProjectPathAndDetails(tmss_id=tmss_id)
        if result['found']:
            project_path = result['path']

            if tmss_id is not None:
                task_data_path = os.path.join(project_path, 'L%s' % tmss_id)
            else:
                task_data_path = None

            path_result = {'found': task_data_path is not None, 'message': '', 'path': task_data_path, 'tmss_id': tmss_id}

            logger.info("constructed path '%s' for tmss_id=%s" % (task_data_path, tmss_id))

            if include_scratch_paths:
                path_result['scratch_paths'] = []

                if tmss_id is not None:
                    subtask = self._tmss_client.get_subtask(tmss_id)
                    if subtask['subtask_type'].lower() == 'pipeline':
                        path_result['scratch_paths'].append(os.path.join(self.scratch_path, 'Observation%s' % tmss_id))
                        path_result['scratch_paths'].append(os.path.join(self.share_path, 'Observation%s' % tmss_id))

                logger.info("Checking scratch paths %s for tmss_id=%s" % (path_result['scratch_paths'], tmss_id))
                path_result['scratch_paths'] = [path for path in path_result['scratch_paths'] if self.pathExists(path)]

            logger.info("result for getPathForTask(tmss_id=%s): %s", tmss_id, path_result)
            return path_result

        result = {'found': False, 'message': result.get('message', ''), 'path': '', 'tmss_id': tmss_id}
        logger.warning("result for getPathForTask(tmss_id=%s): %s", tmss_id, result)
        return result

    def _getProjectPathAndDetails(self, tmss_id=None):
        '''get the project path and details of a task for the given tmss_id'''
        if tmss_id is not None:
            output_dataproducts = self._tmss_client.get_subtask_output_dataproducts(tmss_id)
            directories = set([dp['directory'] for dp in output_dataproducts])
            subtask_dir_name = 'L%s' % (tmss_id,)
            # extract the unique project path (there should only be one)
            project_paths = list(set([dir[:dir.find(subtask_dir_name)] for dir in directories]))

            if len(project_paths) != 1:
                message = "Could not determine project path for tmss_id=%s" % (tmss_id,)
                logger.error(message)
                return {'found': False, 'message': message, 'path': None}

            project_path = project_paths[0]
            return {'found': True, 'path': project_path}

        raise NotImplementedError("MoM/RADB/OTDB obsolete")



    def getProjectPath(self, tmss_id=None):
        result = self._getProjectPathAndDetails(tmss_id=tmss_id)

        if result['found']:
            if 'task' in result:
                del result['task']

        return result

    def getProjectDirAndSubDirectories(self, tmss_id=None, project_name=None):
        '''get the project directory and its subdirectories of either the project_name, or the task's project for the given tmss_id'''
        if project_name:
            project_path = os.path.join(self.projects_path, "_".join(project_name.split()))
            return self.getSubDirectories(project_path)

        result = self.getProjectPath(tmss_id=tmss_id)
        if result['found']:
            return self.getSubDirectories(result['path'])
        return result

    def getSubDirectoriesForTMSSId(self, tmss_id):
        return self.getSubDirectoriesForTask(tmss_id=tmss_id)

    def getSubDirectoriesForTask(self, tmss_id=None):
        result = self.getPathForTask(tmss_id=tmss_id)
        if result['found']:
            return self.getSubDirectories(result['path'])
        return result

    def getSubDirectories(self, path):
        logger.debug('getSubDirectories(%s)', path)
        # get the subdirectories of the given path
        cmd = ['find', path.rstrip('/'), '-maxdepth', '1', '-type', 'd']
        cmd = wrap_command_in_cep4_head_node_ssh_call_if_needed(cmd)
        logger.debug(' '.join(cmd))
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = communicate_returning_strings(proc)

        if proc.returncode != 0:
            # lfs puts it's error message in stdout
            logger.error(out + err)
            return {'found': False, 'path': path, 'message': out + err}

        # parse out, clean lines and skip first line which is path itself.
        lines = [l.strip() for l in out.split('\n')][1:]
        subdir_names = [l.split('/')[-1].strip().strip('/') for l in lines if l]

        result = {'found': True, 'path': path, 'sub_directories': subdir_names}
        logger.debug('getSubDirectories(%s) result: %s', path, result)
        return result

    def pathExists(self, path):
        cmd = ['ls', path]
        cmd = wrap_command_in_cep4_head_node_ssh_call_if_needed(cmd)
        logger.debug(' '.join(cmd))
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = communicate_returning_strings(proc)

        if proc.returncode != 0 and 'No such file or directory' in err:
            return False

        return True

def wrap_command_in_cep4_head_node_ssh_call_if_needed(cmd: []):
    """wrap the Popen cmd in a cep4 ssh call if not running on cep4 or lexar"""
    hostname = socket.gethostname()
    if not ('head' in hostname or 'lexar' in hostname):
        return wrap_command_in_cep4_head_node_ssh_call(cmd)
    return cmd

def main():
    import sys
    from optparse import OptionParser

    # Check the invocation arguments
    parser = OptionParser('%prog [options]',
                          description='get path for tmss_id')
    parser.add_option('-p', '--path', dest='path', action='store_true', help='get the path for the given tmss_id')
    parser.add_option('-P', '--project', dest='project', action='store_true', help='get the project path and all its sub directories for the given tmss_id')
    parser.add_option('-s', '--subdirs', dest='subdirs', action='store_true', help='get the sub directories of the path for the given tmss_id')
    parser.add_option('-t', '--tmss_id', dest='tmss_id', type='int', default=None, help='tmss_id of the TMSS subtask to get the path for')
    parser.add_option('-q', '--broker', dest='broker', type='string', default=DEFAULT_BROKER, help='Address of the broker, default: localhost')
    parser.add_option("--mountpoint", dest="mountpoint", type="string", default=CEP4_DATA_MOUNTPOINT, help="path of local cep4 mount point, default: %default")
    parser.add_option("--exchange", dest="exchange", type="string", default=DEFAULT_BUSNAME, help="Name of the exchange on which the services listen, default: %default")
    parser.add_option('-R', '--rest_credentials', dest='rest_credentials', type='string', default='TMSSClient', help='TMSS django REST API credentials name, default: %default')
    parser.add_option('-V', '--verbose', dest='verbose', action='store_true', help='verbose logging')
    (options, args) = parser.parse_args()

    if not options.tmss_id:
        parser.print_help()
        exit(1)

    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=logging.INFO if options.verbose else logging.WARN)

    with PathResolver(exchange=options.exchange, broker=options.broker) as path_resolver:

        if options.path:
            result = path_resolver.getPathForTask(tmss_id=options.tmss_id)
            if result['found']:
                print("path: %s" % (result['path']))
            else:
                print(result['message'])
                exit(1)

        if options.project:
            result = path_resolver.getProjectDirAndSubDirectories(tmss_id=options.tmss_id)
            if result['found']:
                print("projectpath: %s" % (result['path']))
                print("subdirectories: %s" % (' '.join(result['sub_directories'])))
            else:
                print(result['message'])
                exit(1)

        if options.subdirs:
            result = path_resolver.getSubDirectoriesForTask(tmss_id=options.tmss_id)
            if result['found']:
                print("path: %s" % (result['path']))
                print("subdirectories: %s" % (' '.join(result['sub_directories'])))
            else:
                print(result['message'])
                exit(1)


if __name__ == '__main__':
    main()
