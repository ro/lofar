import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, ButtonGroup} from 'reactstrap';
import {setPeriod} from "redux/actions/mainFiltersActions";

// CSS
import toolbarStyles from '../styles.module.scss'


/**
 * Class to display a secondary header for selecting data filters.
 * The state is managed by the LandingPage class.
 */
class PeriodSelectorC extends Component {

    onPeriodClick(i) {
        this.props.setPeriod(i);
    }

    render() {

        return (<div className={toolbarStyles.toolbar_ctrl}>
            <label>Period</label>
            <ButtonGroup size="sm">
                <Button color="info" onClick={() => this.onPeriodClick(7)} active={this.props.period === 7}>1 wk</Button>
                <Button color="info" onClick={() => this.onPeriodClick(14)} active={this.props.period === 14}>2 wk</Button>
                <Button color="info" onClick={() => this.onPeriodClick(21)} active={this.props.period === 21}>3 wk</Button>
                <Button color="info" onClick={() => this.onPeriodClick(28)} active={this.props.period === 28}>4 wk</Button>
            </ButtonGroup>
        </div>);
    }
}

const mapStatePeriodSelector = state => {
    return {
        period: state.mainFilters.period
    };
};

const mapDispatchPeriodSelector = {
    setPeriod
};

export const PeriodSelector = connect(mapStatePeriodSelector, mapDispatchPeriodSelector)(PeriodSelectorC);
