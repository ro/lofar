

class ItemAlreadyExists(Exception):
    def __init__(self, instance, message=''):
        self.instance_id = instance.id

        message = 'Item %d was already present. %s'.format(self.instance_id, message)
        super(ItemAlreadyExists, self).__init__(message)