import React, {
    Component
} from 'react';
import {connect} from "react-redux";
import {
    NavItem,
    NavLink,
    TabPane,
    TabContent,
    Nav
} from 'reactstrap';
import AutoLoadWrapper from 'components/AutoLoadWrapper'
import * as LOFARDefinitions from 'utils/LOFARDefinitions'
import FillViewportHeightDiv from 'components/FillViewportHeightDiv';
import ComponentType from './ComponentType';

// CSS
import styles from './styles.module.scss';


/*
 * Render a Tab item
 */
function Tab({label, onClick, isActive}) {
    const cls = isActive ? styles.clickable_tab_active : styles.clickable_tab_inactive;

    return (
        <NavItem className={styles.clickable_tab}>
            <NavLink className={cls} onClick={onClick}>
                {label}
            </NavLink>
        </NavItem>
    );
}

/**
 * StationTestView class.
 */
class StationTestViewC extends Component {

    state = {
        activeTab: undefined
    }

    // Set the activeTab to the first component if it wasn't set yet or when
    // the station changed and doesn't have the active component
    static getDerivedStateFromProps(props, state) {
        let componentTypes = Object.keys(props.data).sort(),
            currentComponent = state.activeTab;

        if (componentTypes.length === 0) {
            return null;
        }

        if (! currentComponent || componentTypes.findIndex((c) => currentComponent === c) === -1) {
            return {
                activeTab: componentTypes[0]
            };
        }
        return null;
    }

    // Do not (re)render when data is loading (performance improvement)
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.isLoading ? false : true;
    }

    toggleTab = (e) => {
        let tab = e.currentTarget.innerHTML;
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    render() {
        const stationType = LOFARDefinitions.stationTypeFromName(this.props.selectedStation);
        const componentTypes = Object.keys(this.props.data).sort();

        if (this.props.isLoading) {
            return null;
        }

        return (
            <div>
                <Nav tabs className="component-type-selector">
                {
                    componentTypes.map((componentType, key) =>
                        <Tab key={key} onClick={this.toggleTab} isActive={this.state.activeTab===componentType} label={componentType} />
                    )
                }
                </Nav>
                <FillViewportHeightDiv className="border-right">
                    { ({height}) => (
                        <TabContent  activeTab={this.state.activeTab}>
                        {
                            componentTypes.map((componentType, key) =>
                            <TabPane key={key} tabId={componentType}>
                                <ComponentType  key={componentType}
                                                active={componentType === this.state.activeTab}  // needed to force rerender for the ReactTableContainer
                                                station_type={stationType}
                                                type={componentType}
                                                station_name={this.props.selectedStation}
                                                data={this.props.data[componentType]}
                                                height={height}  />
                            </TabPane>
                        )}
                        </TabContent>
                    )}
                </FillViewportHeightDiv>
            </div>);
    }

}

/* Add some magic; use the AutoLoadWrapper to create a HOC that handles the
   auto-loading of the data for StationOverviewC.
 */
const StationTestViewController = connect(state => {
    return {
        selectedStation: state.mainFilters.selectedStation
    };
})(StationTestViewC);

const StationTestView = AutoLoadWrapper(StationTestViewController);

export default StationTestView;
