import unittest
from tempfile import mkdtemp, mkstemp
import time
from lofar.maintenance.utils.cli.station_tests_watchdog import *
from mock import patch

logger = logging.getLogger(__name__)


class TestStationTestsWatchdog(unittest.TestCase):
    def setUp(self):
        self.temp_file_list = []
        self.temp_dir_list = []

    @patch('lofar.maintenance.utils.cli.station_tests_watchdog.'
           'handle_event_file_created')
    def test_observer_loop(self, handle_event_file_created_mock):
        tmp_dir = mkdtemp()
        self.temp_dir_list.append(tmp_dir)

        logger.info('created temp dir %s', tmp_dir)
        observer = instantiate_file_creation_event_observer(tmp_dir)

        tmp_file = mkstemp(dir=tmp_dir)
        self.temp_file_list.append(tmp_file[1])

        with open(tmp_file[1], 'w') as f_out_stream:
            f_out_stream.write('test')
        logger.info('created temp file %s', tmp_file)
        main_wait_event_loop(observer, tmp_file[1].split('/')[-1], 'fake_address', 1, run_once=True)
        handle_event_file_created_mock.assert_called()

    def attemptDelete(self, path):
        """
        Attempts the to remove a file. If an exception is thrown prints out
        the exception message, and the stacktrace, and the path that it was supposed to remove
        :param path: the path to the file/directory to delete
        """
        try:
            if os.path.isfile(path):
                os.remove(path)
            else:
                os.rmdir(path)
        except Exception as e:
            logger.exception('Cannot remove temporary file/directory %s : %s', path, e)

    def tearDown(self):
        try:
            for file in self.temp_file_list:
                self.attemptDelete(file)
            for directory in self.temp_dir_list:
                self.attemptDelete(directory)
        except Exception as e:
            logger.exception('Cannot remove temp file/directory: %s', e)

            
if __name__ == '__main__':
    setup_logging_framework(logging.DEBUG)
    unittest.main()
