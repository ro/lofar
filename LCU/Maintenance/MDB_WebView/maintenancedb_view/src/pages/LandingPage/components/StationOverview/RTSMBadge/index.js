import React, {Component} from 'react';
import {unique_id} from 'utils/utils.js'
import * as moment from 'moment';
import { datetime_format } from 'utils/constants'
import {Table} from 'reactstrap';

import PopoverWithTitle from 'components/PopoverWithTitle'
import Badge from '../Badge'


/**
 * RTSMBadge; class to render one RTSM badge in the SORow.
 */
class RTSMBadge extends Component {

    id = unique_id();

    state = {
        popoverOpen: false
    };

    togglePopover = () => {
        this.setState({
            popoverOpen: !this.state.popoverOpen
        });
    }

    popoverBody = () => {
        let data = this.props.data;
        let errors = Object.keys(data.error_summary).sort();
        let badges = errors.map((e, i) => <Badge key={i} count={data.error_summary[e]} label={e}/>);

        return (
            <Table borderless size="sm">
                <tbody>
                    <tr>
                        <th>Start:</th><td>{moment.utc(data.start_datetime).format(datetime_format)}</td>
                    </tr>
                    <tr>
                        <th>End:</th><td>{moment.utc(data.end_datetime).format(datetime_format)}</td>
                    </tr>
                    <tr>
                        <th>Mode:</th><td>{data.mode}</td>
                    </tr>
                    <tr>
                        <th>Errors:</th><td>{badges}</td>
                    </tr>
                </tbody>
            </Table>
        )
    }

    render() {
        let data = this.props.data;

        return (<React.Fragment>
            <Badge myid={this.id} className={'obs-'+data.observation_id} togglePopOver={this.togglePopover} count={data.total_component_errors} label={data.observation_id}/>
            <PopoverWithTitle target={this.id} isOpen={this.state.popoverOpen} togglePopover={this.togglePopover} title={data.observation_id}>
                {this.popoverBody}
            </PopoverWithTitle>
        </React.Fragment>);
    }
}

export default RTSMBadge;
