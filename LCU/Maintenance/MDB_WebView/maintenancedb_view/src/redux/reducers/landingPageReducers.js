import {
    SET_AVERAGING_WINDOW,
    SET_STATISTICS_TEST_TYPE,
    SET_COMPONENT_SIZES
} from '../actions/landingPageActions.js'

const initialState = {
    station_statistics: {
        averaging_window: 1,
        test_type: 'B'
    },
    layout: {
        breakpoints: {
            md: 996,
            xxs: 0
        },
        cols: {
            md: 12,
            xxs: 1
        },
        panels: {
            md: [{
                    i: 'ul',
                    x: 0,
                    y: 0,
                    w: 8,
                    h: 3
                },
                {
                    i: 'ur',
                    x: 9,
                    y: 0,
                    w: 4,
                    h: 3
                },
                {
                    i: 'bl',
                    x: 0,
                    y: 0,
                    w: 8,
                    h: 2
                },
                {
                    i: 'br',
                    x: 9,
                    y: 0,
                    w: 4,
                    h: 2
                }
            ],
            xxs: [{
                    i: 'ul',
                    x: 0,
                    y: 0,
                    w: 1,
                    h: 3
                },
                {
                    i: 'ur',
                    x: 0,
                    y: 0,
                    w: 1,
                    h: 3
                },
                {
                    i: 'bl',
                    x: 0,
                    y: 0,
                    w: 1,
                    h: 2
                },
                {
                    i: 'br',
                    x: 0,
                    y: 0,
                    w: 1,
                    h: 2
                }
            ]
        },
        current_layout: {}
    }
};


export default function(state = initialState, action) {
    switch (action.type) {
        case SET_AVERAGING_WINDOW:
            {
                return {
                    ...state,
                    station_statistics: { ...state.station_statistics,
                        averaging_window: action.payload.averagingWindow
                    }
                };
            }
        case SET_STATISTICS_TEST_TYPE:
            {
                return {
                    ...state,
                    station_statistics: { ...state.station_statistics,
                        test_type: action.payload.test_type
                    }
                };
            }
        case SET_COMPONENT_SIZES:
            {
                return {
                    ...state,
                    layout: { ...state.layout,
                        current_layout: action.payload
                    }
                };
            }
        default:
            return state;
    }
}
